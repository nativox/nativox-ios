/* @flow */

let userSession = null

class UserSession {
  constructor() {
    if (!userSession) {
      userSession = this
      userSession.user = {
        name: '',
        email: '',
        token: '',
        idUser: '',
        language: '',
        idSession: '',
      }
    }

    return userSession
  }

  setUser(user) {
    userSession.user = user
  }

  getUser() {
    return userSession.user
  }

  resetUser() {
    userSession.user = {
      name: '',
      email: '',
      token: '',
      idUser: '',
      language: '',
      idSession: '',
    }
  }
}

export default new UserSession()
