/* @flow */

import React, {Component, PropTypes} from 'react'
import {View, TextInput, NativeModules, StyleSheet, Alert} from 'react-native'
import {strings} from '../../stringsApp'
import Screen from '../../common/Screen'
import Button from '../../common/Button'
import {post} from '../../common/apiRequest'
import Device from 'react-native-device'

const {UserFeedbackView, ProgressDialogModule} = NativeModules
const styles = StyleSheet.create({
  messageContainer: {
    flexGrow: 1,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 5
  },
  button: {
    height: 35,
  },
  feedbackTextInput: {
    width: 320,
    height: 100,
    borderColor: 'gray',
    backgroundColor: 'rgb(146, 147, 148)',
    borderWidth: 1,
    padding: 10,
    fontSize: 16
  },
})

export default class UserFeedback extends Component {

  constructor(){
    super()

    this.state = {
      feedback: '',
    }
    this.onSendButtonPressed = this.onSendButtonPressed.bind(this)
  }

  onSendButtonPressed() {
    const params = {
      mensaje : `
      ${strings.userFeedbackPlaceHolder}
      ${this.state.feedback}<br>
<br>
      Informacion del Dispositivo<br>
      ---------------------------<br>
<br>
      Modelo: ${Device.model}<br>
      Nombre: ${Device.deviceName}<br>
      Sistema Operativo: ${Device.systemName}<br>
      Version del Sistema Operativo: ${Device.systemVersion}<br>
<br>
      ---------------------------<br>
      `,
      correo: this.props.userEmail,
      id_usuario : this.props.idUser,
    }

    ProgressDialogModule.showMessage(strings.progressSendingFeedback)
    post('contacto', params).then(() => {
      ProgressDialogModule.dismissMessage()
      Alert.alert('Nativox', strings.successfulSendFeedback, [{text: 'OK', onPress: () => UserFeedbackView.sentUserFeedback()}])
    }).catch(() => {
      ProgressDialogModule.dismissMessage()
      Alert.alert('Nativox', strings.issueSendFeedback, [{text: 'OK', onPress: () => UserFeedbackView.sentUserFeedback()}])
    })
  }

  renderFeedbackTextInput() {
    return (
        <TextInput
            multiline
            onChangeText={(feedback) => this.setState({feedback})}
            placeholder={strings.userFeedbackPlaceHolder}
            placeholderTextColor={'rgb(112, 112, 112)'}
            style={styles.feedbackTextInput}
            value={this.state.text}/>
    )
  }

  render() {
    return (
      <Screen>
        <View style={styles.messageContainer}>
          {this.renderFeedbackTextInput()}
        </View>
        <Button
            disabled={!this.state.feedback}
            onPress={this.onSendButtonPressed}
            style={styles.button}
            title={strings.sendButtonTitle}
            width={120}/>
      </Screen>
    )
  }
}

UserFeedback.propTypes = {
  idUser: PropTypes.string,
  userEmail: PropTypes.string,
}
