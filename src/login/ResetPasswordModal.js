/* @flow */

import React, {PropTypes} from 'react'
import {View, Text, StyleSheet} from 'react-native'
import TextBox from '../common/TextBox'
import Button from '../common/Button'
import {EXPLAINATION_LABEL_SIZE} from '../common/NativoxTheme'
import emailValidator from 'email-validator'
import {strings} from '../stringsApp'

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
  },
  explaination: {
    color: 'rgb(255, 255, 255)',
    fontSize: EXPLAINATION_LABEL_SIZE,
    textAlign: 'center',
    marginTop: 10,
    marginBottom: 20,
  },
  textbox: {
    marginBottom: 15,
  }
})

const shouldResetButton = (email) => email && emailValidator.validate(email)
const ResetPasswordModal = ({
  email,
  onChangeEmail,
  onResetPassword,
}) => (
  <View style={styles.container}>
    <Text style={styles.explaination}>
      {strings.labelResetPassword}
    </Text>
    <TextBox
        onChangeText={onChangeEmail}
        placeholder={'Email'}
        style={styles.textbox}
        value={email}/>
    <Button
        disabled={!shouldResetButton(email)}
        onPress={onResetPassword}
        title={strings.resetButtonTitle}/>
  </View>
)

export default ResetPasswordModal

ResetPasswordModal.propTypes = {
  email: PropTypes.string,
  onChangeEmail: PropTypes.func,
  onResetPassword: PropTypes.func,
}
