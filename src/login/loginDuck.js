/* @flow */

import {fromJS} from 'immutable'
import {createAction, handleActions} from 'redux-actions'
import {NativeModules, Alert} from 'react-native'
import {post} from '../common/apiRequest'
import userSession from '../user/userSession'
import {strings} from '../stringsApp'
import {Answers} from 'react-native-fabric'
import categories from '../categories'

const {onLoadCategories} = categories.eventHandlers
const ProgressDialogModule = NativeModules.ProgressDialogModule
const {postNotification} = NativeModules.ReactNativeView

export const SET_EMAIL = 'SET_EMAIL'
export const SET_PASSWORD = 'SET_PASSWORD'
export const SET_RESET_EMAIL = 'SET_RESET_EMAIL'
export const SHOW_RESET_PASSWORD = 'SHOW_RESET_PASSWORD'
export const HIDE_RESET_PASSWORD = 'HIDE_RESET_PASSWORD'

const setEmail = createAction(SET_EMAIL)
const setPassword = createAction(SET_PASSWORD)
const setResetEmail = createAction(SET_RESET_EMAIL)
const showResetPassword = createAction(SHOW_RESET_PASSWORD)
const hideResetPassword = createAction(HIDE_RESET_PASSWORD)

export const changeEmail = (email) => {
  return (dispatch) => {
    dispatch(setEmail(email))
  }
}

export const changePassword = (password) => {
  return (dispatch) => {
    dispatch(setPassword(password))
  }
}

export const changeResetEmail = (email) => {
  return (dispatch) => {
    dispatch(setResetEmail(email))
  }
}

export const makeVisibleResetPassword = () => {
  return (dispatch) => {
    dispatch(showResetPassword())
  }
}

export const makeInvisibleResetPassword = () => {
  return (dispatch) => {
    dispatch(hideResetPassword())
  }
}

export const resetPassword = () => {
  return (dispatch, getState) => {
    const loginState = getState().login
    const params = {
      correo : loginState.get('resetEmail'),
    }

    ProgressDialogModule.showMessage(strings.progressResetting)
    post('correo_recuperar_contrasenya', params).then((response) => {
      ProgressDialogModule.dismissMessage()
      if (response.status === 'true') {
        dispatch(changeResetEmail(''))
        Answers.logCustom('Reset Password', {email: params.correo})
        setTimeout(() => Alert.alert('Nativox', response.result, [{text: 'OK', onPress: () => dispatch(makeInvisibleResetPassword())}]), 500) //eslint-disable-line no-undef
      } else {
        setTimeout(() => Alert.alert('Nativox', response.result), 500) //eslint-disable-line no-undef
      }
    }).catch(() => {
      ProgressDialogModule.dismissMessage()
      setTimeout(() => Alert.alert('Nativox', strings.issueResetPassword), 500) //eslint-disable-line no-undef
    })
  }
}

export const login = () => {
  return (dispatch, getState) => {
    const loginState = getState().login
    const params = {
      correo : loginState.get('email'),
      password : loginState.get('password'),
    }

    ProgressDialogModule.showMessage(strings.progressLogin)
    post('login', params).then((response) => {
      ProgressDialogModule.dismissMessage()

      if (response.status === 'true') {
        dispatch(changeEmail(''))
        dispatch(changePassword(''))
        dispatch(onLoadCategories())

        Answers.logLogin('Email', true)

        userSession.setUser({
          email: loginState.get('email'),
          name: response.result.nombre,
          token: response.result.token,
          idUser: response.result.id_usuario,
        })
        postNotification('OnLoginComplete', {
          ...response.result,
          email: loginState.get('email'),
          name: response.result.nombre,
        })
      } else {
        Answers.logLogin('Email', false)
        setTimeout(() => Alert.alert('Nativox', response.result), 500)
      }
    }).catch((error) => {
      console.log(error) //eslint-disable-line
      ProgressDialogModule.dismissMessage()
      Answers.logLogin('Email', false)
      setTimeout(() => Alert.alert('Nativox', strings.issueLogin), 500)
    })
  }
}

export const loginFB = (facebookParams, isSignUp) => {
  return (dispatch) => {
    const params = {
      correo : facebookParams.email,
      nombre_usuario : facebookParams.userName,
      id_facebook : facebookParams.idFacebook,
      nombre : facebookParams.name,
      access_token : facebookParams.token,
    }

    ProgressDialogModule.showMessage(strings.progressLogin)
    post('login_fb', params).then((response) => {
      ProgressDialogModule.dismissMessage()

      if (response.status === 'true') {
        if (isSignUp) {
          Answers.logSignUp('Email', true)
        } else {
          Answers.logLogin('Facebook', true)
        }

        userSession.setUser({
          email: facebookParams.email,
          name: response.result.nombre,
          token: response.result.token,
          idUser: response.result.id_usuario,
        })
        postNotification('OnLoginComplete', {
          ...response.result,
          email: facebookParams.email,
          name: response.result.nombre,
        })

        dispatch(onLoadCategories())
      } else {
        Answers.logLogin('Facebook', false)
        setTimeout(() => Alert.alert('Nativox', response.result), 500)
      }
    }).catch((error) => {
      console.log('error: ', error) //eslint-disable-line no-console
      ProgressDialogModule.dismissMessage()
      Answers.logLogin('Facebook', false)
      setTimeout(() => Alert.alert('Nativox', strings.issueLogin), 500)
    })
  }
}

const defaultState = fromJS({
  email: '',
  password: '',
  showResetPassword: false,
  resetEmail: '',
})

export default handleActions({
  [SET_EMAIL]: (state, action) => state.set('email', action.payload),
  [SET_PASSWORD]: (state, action) => state.set('password', action.payload),
  [SHOW_RESET_PASSWORD]: (state) => state.set('showResetPassword', true),
  [HIDE_RESET_PASSWORD]: (state) => state.set('showResetPassword', false),
  [SET_RESET_EMAIL]: (state, action) => state.set('resetEmail', action.payload),
}, defaultState)
