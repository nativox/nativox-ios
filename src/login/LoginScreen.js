/* @flow */

import React, {PropTypes} from 'react'
import {
  Text,
  StyleSheet,
  View,
  NativeModules
} from 'react-native'
import Screen from '../common/Screen'
import Device from 'react-native-device'
import {connect} from 'react-redux'
import TextBox from '../common/TextBox'
import Button from '../common/Button'
import ImageButton from '../common/ImageButton'
import {isSmallDevice} from '../common/DeviceUtility'
import {
  changeEmail,
  changePassword,
  makeVisibleResetPassword,
  makeInvisibleResetPassword,
  changeResetEmail,
  resetPassword,
  login,
  loginFB,
} from './loginDuck'
import emailValidator from 'email-validator'
import ModalScreen from '../common/ModalScreen'
import ResetPasswordModal from './ResetPasswordModal'
import connectFB from './connectFB'
import {strings} from '../stringsApp'

const {postNotification} = NativeModules.ReactNativeView
const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    justifyContent: 'center',
  },
  loginContainer: {
    width: Device.isIpad() ? 270 : 170,
    backgroundColor: 'rgb(162, 162, 161)',
    borderRadius: 6,
    paddingTop: Device.isIpad() ? 25 : 15,
    paddingLeft:  Device.isIpad() ? 20 : 13,
    paddingRight: Device.isIpad() ? 20 : 13,
    alignSelf: 'center',
  },
  loginTitle: {
    color: 'rgb(162, 162, 161)',
    fontSize: Device.isIpad() ? 41 : 21,
    marginBottom: 20,
    fontFamily: 'Helvetica Neue',
    fontWeight: '300',
    textAlign: 'center',
  },
  loginButton: {
    height: Device.isIpad() ? 45 : 30,
    marginBottom: 15,
    marginTop: 15,
  },
  forgotButtonTitle: {
    fontSize: Device.isIpad() ? 25 : 14,
  },
  signupButtonTitle: {
    fontSize: Device.isIpad()  ? 25 : 16,
    fontWeight: 'bold',
    color: 'rgb(255,255,255)'
  },
  loginTitleButton: {
    fontSize: Device.isIpad() ? 22 : 16,
  },
  noAccountLabel: {
    fontSize: Device.isIpad() ? 25 : 17,
    color: 'rgb(255, 255, 255)',
  },
  facebookButtonContainer: {
    paddingTop: Device.isIpad() ? 80 : isSmallDevice() ? 60 : 80
  },
  signupButtonContainer: {
    flexDirection: 'row',
    marginTop: 40,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
  }
})

const shouldEnableLoginButton = (email, password) => email && emailValidator.validate(email) && password

class LoginScreen extends React.PureComponent {

  constructor() {
    super()

    this.connectFB = this.connectFB.bind(this)
  }

  connectFB() {
    connectFB(this.props.loginFB)
  }

  render() {
    return (
        <Screen>
          <ModalScreen
              height={Device.isIpad() ? 250 : 200}
              isVisible={this.props.showResetPassword}
              onCloseButtonPressed={this.props.makeInvisibleResetPassword}
              onTouchBackground={this.props.makeInvisibleResetPassword}
              width={Device.isIpad() ? 300 : 250}>
              <ResetPasswordModal
                  email={this.props.resetEmail}
                  onChangeEmail={this.props.changeResetEmail}
                  onResetPassword={this.props.resetPassword}/>
          </ModalScreen>
          <View style={styles.container}>
            <Text style={styles.loginTitle}>
                {strings.loginTitle}
            </Text>
            <View style={styles.loginContainer}>
                <TextBox
                    onChangeText={this.props.changeEmail}
                    placeholder='Email'
                    value={this.props.email}/>
                <TextBox
                    isPassword
                    onChangeText={this.props.changePassword}
                    placeholder={strings.placeholderPassword}
                    value={this.props.password}/>
                <Button
                    disabled={!shouldEnableLoginButton(this.props.email, this.props.password)}
                    onPress={this.props.login}
                    style={styles.loginButton}
                    title={'Log in'}
                    titleStyle={styles.loginTitleButton}/>
            </View>

            <Button
                invisible
                onPress={this.props.makeVisibleResetPassword}
                title={strings.labelForgotPassword}
                titleStyle={styles.forgotButtonTitle}/>
            <View style={styles.facebookButtonContainer}>
              <ImageButton
                  height={Device.isIpad() ? 44 : 25}
                  image='fb_button'
                  onPress={this.connectFB}
                  width={Device.isIpad() ? 220 : 150}/>
            </View>
            <View style={styles.signupButtonContainer}>
                <Text style={styles.noAccountLabel}>
                  {strings.labelRegister}
                </Text>
                <Button
                    invisible
                    onPress={() => postNotification('PressedSignUpButton', null)}
                    style={{marginLeft: 20}}
                    title={strings.signUpButtonTitle}
                    titleStyle={styles.signupButtonTitle}/>
            </View>
          </View>
        </Screen>
    )
  }
}

LoginScreen.propTypes = {
  changeEmail: PropTypes.func,
  changePassword: PropTypes.func,
  changeResetEmail: PropTypes.func,
  email: PropTypes.string,
  login: PropTypes.func,
  loginFB: PropTypes.func,
  makeInvisibleResetPassword: PropTypes.func,
  makeVisibleResetPassword: PropTypes.func,
  password: PropTypes.string,
  resetEmail: PropTypes.string,
  resetPassword: PropTypes.func,
  showResetPassword: PropTypes.bool,
}

export default connect(state => ({
  email: state.login.get('email'),
  resetEmail: state.login.get('resetEmail'),
  password: state.login.get('password'),
  showResetPassword: state.login.get('showResetPassword'),
}),
  (dispatch) => ({
    changeEmail: (text) => dispatch(changeEmail(text)),
    changePassword: (text) => dispatch(changePassword(text)),
    changeResetEmail: (text) => dispatch(changeResetEmail(text)),
    makeVisibleResetPassword: () => dispatch(makeVisibleResetPassword()),
    makeInvisibleResetPassword: () => dispatch(makeInvisibleResetPassword()),
    resetPassword: () => dispatch(resetPassword()),
    login: () => dispatch(login()),
    loginFB: (params) => dispatch(loginFB(params, false)),
  })
)(LoginScreen)
