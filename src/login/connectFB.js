/* @flow */

import {
  GraphRequest,
  GraphRequestManager,
  LoginManager,
  AccessToken,
} from 'react-native-fbsdk'
import {Alert} from 'react-native'

const getFacebookUserInfo = (token, onLoggedInFB) => {
  new GraphRequestManager().addRequest(new GraphRequest(
    '/me?fields=id,name,email',
    null,
    (error, result) => {
      if (error) {
        setTimeout(() => Alert.alert('Nativox', error.message), 500)

        return
      }

      onLoggedInFB({
        email: result.email,
        userName: result.name,
        idFacebook: result.id,
        name: result.name,
        token,
      })
    }
  )).start()
}

export default async (onLoggedInFB) => {
  try {
    const data = await AccessToken.getCurrentAccessToken()

    if (data && data.accessToken) {
      getFacebookUserInfo(data.accessToken, onLoggedInFB)

      return
    }

    const result = await LoginManager.logInWithReadPermissions(['public_profile', 'email'])

    if (!result.isCancelled) {
      const newData = await AccessToken.getCurrentAccessToken()

      getFacebookUserInfo(newData.accessToken, onLoggedInFB)
    }
  } catch (error) {
    console.log('error: ', error.message) // eslint-disable-line
    setTimeout(() => Alert.alert('Nativox', 'There has been an error trying to login with Facebook, try it again later'), 500); //eslint-disable-line
  }
}
