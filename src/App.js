/* @flow */

import React, {Component, PropTypes} from 'react'
import userSession from './user/userSession'
import {combineReducers, applyMiddleware, createStore} from 'redux'
import {Provider} from 'react-redux'
import thunk from 'redux-thunk'
import account from './account'
import contact from './contact/contact'
import register from './register/registerDuck'
import login from './login/loginDuck'
import categories from './categories'

import UpdateAccountScreen from './account/UpdateAccountScreen'
import ContactScreen from './contact/ContactScreen'
import RegisterScreen from './register/RegisterScreen'
import LoginScreen from './login/LoginScreen'
import InitialTutorialScreen from './tutorial/InitialTutorialScreen'
import PresentationTutorialScreen from './tutorial/PresentationTutorialScreen'
import CategoriesScreen from './categories/CategoriesScreen'
import {sendPendingPurchases} from './categories/PurchasesRepository'

const {onLoadCategories} = categories.eventHandlers
const createStoreWithMiddleware = applyMiddleware(thunk)(createStore)
const store = createStoreWithMiddleware(combineReducers({
  account: account.reducer,
  contact,
  register,
  login,
  categories: categories.reducer,
}))
const screenMap = {
  UpdateAccountScreen: (props) => <UpdateAccountScreen {...props}/>, //eslint-disable-line
  ContactScreen: (props) => <ContactScreen {...props}/>, //eslint-disable-line
  RegisterScreen: (props) => <RegisterScreen {...props}/>, //eslint-disable-line
  LoginScreen: (props) => <LoginScreen {...props}/>, //eslint-disable-line
  InitialTutorialScreen: (props) => <InitialTutorialScreen {...props}/>, //eslint-disable-line
  PresentationTutorialScreen: (props) => <PresentationTutorialScreen {...props}/>, //eslint-disable-line
  CategoriesScreen: (props) => <CategoriesScreen {...props}/>, //eslint-disable-line
}

export default class App extends Component {

  constructor() {
    super()

    store.dispatch(onLoadCategories())
    sendPendingPurchases(store.dispatch)
  }

  componentWillMount() {
    userSession.setUser({
      email: this.props.userEmail,
      name: this.props.userName,
      token: this.props.token,
      idUser: this.props.idUser,
      language: this.props.language,
      idSession: this.props.idSession,
    })
  }

  render() {
    return (
      <Provider store={store}>
        {screenMap[this.props.screenName](this.props)}
      </Provider>
    )
  }
}

App.propTypes = {
  idSession: PropTypes.string,
  idUser: PropTypes.string,
  language: PropTypes.string,
  screenName: PropTypes.string,
  token: PropTypes.string,
  userEmail: PropTypes.string,
  userName: PropTypes.string,
}
