/* @flow */

import React from 'react'
import {
  View,
  Text,
  StyleSheet,
  Image,
  NativeModules,
} from 'react-native'
import Screen from '../common/Screen'
import Carousel from 'react-native-carousel'
import {PRIMARY_COLOR} from '../common/NativoxTheme.js'
import Button from '../common/Button'
import {strings} from '../stringsApp'
import Device from 'react-native-device'

const {postNotification} = NativeModules.ReactNativeView
const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  containerPage: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent',
  },
  image: {
    resizeMode: 'contain',
  },
  descriptionContainer: {
    flex: 2,
  },
  description: {
    color: 'rgb(255, 255, 255)',
    fontSize: Device.isIpad() ? 28 : 16,
    textAlign: 'center',
    fontFamily: 'HelveticaNeue-Light',
  },
  startButton: {
    marginTop: 10,
    marginBottom: 10,
  }
})

const pages = [
  {
    image: 'welcome_first_tutorial',
    description: strings.text_first_page_welcome_tutorial,
  },
  {
    image: 'entonation_first_tutorial',
    description: strings.text_second_page_welcome_tutorial,
  },
  {
    image: 'beats_first_tutorial',
    description: strings.text_third_page_welcome_tutorial,
  },
  {
    image: 'melody_first_tutorial',
    description: strings.text_forth_page_welcome_tutorial,
  },
  {
    image: 'visualize_first_tutorial',
    description: strings.text_fifth_page_welcome_tutorial,
  }
]

class InitialTutorialScreen extends React.PureComponent {

  constructor() {
    super()

    this.onRenderContainerLayout = this.onRenderContainerLayout.bind(this)
    this.onPressNextPage = this.onPressNextPage.bind(this)

    this.state = {
      width: null,
      height: null,
    }
  }

  onRenderContainerLayout(event) {
    const layout = event.nativeEvent.layout

    this.setState({
      width: layout.width,
      height: layout.height,
      activePage: 0,
    })
  }

  onPressNextPage() {
    if(this.state.activePage === 4) {
      postNotification('PressedStartButton', null)
    } else {
      this.refs.carousel.indicatorPressed(this.state.activePage + 1)
    }
  }

  render() {
    return (
        <Screen>
          <View
              onLayout={this.onRenderContainerLayout}
              style={styles.container}>
            <Carousel
                animate={false}
                indicatorAtBottom
                indicatorColor={PRIMARY_COLOR}
                indicatorOffset={10}
                indicatorSize={20}
                onPageChange={(activePage) => this.setState({activePage})}
                ref='carousel'
                width={this.state.width}>
                {
                  pages.map((page, index) => (
                    <View
                        key={index}
                        style={[styles.containerPage, {width: this.state.width}]}
                    >
                      <Image source={{uri: page.image, isStatic: true}}
                          style={[styles.image, {width: this.state.width, height: this.state.height * 0.5}]} />
                      <View style={styles.descriptionContainer}>
                          <Text style={styles.description}>
                            {page.description}
                          </Text>
                      </View>
                    </View>
                  ))
                }
            </Carousel>
            <Button onPress={this.onPressNextPage}
                style={styles.startButton}
                title={this.state.activePage === 4 ? 'Start' : 'Next'}
                width={Device.isIpad() ? 200 : 120}/>
          </View>
        </Screen>
    )
  }
}

export default InitialTutorialScreen
