/* @flow */

import React from 'react'
import {
  ActivityIndicator,
  StyleSheet,
  NativeModules,
  Dimensions,
  View,
} from 'react-native'
import Screen from '../common/Screen'
import WebViewBridge from 'react-native-webview-bridge'

const {width, height} = Dimensions.get('window')
const {postNotification} = NativeModules.ReactNativeView

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgba(255, 255, 255, 0)'
  },
})

const injectScript = `
$('.go_to_tutorial').click(function(){
		WebViewBridge.send('goToTutorial');

		return false;
});

$('.go_to_courses').click(function(){
  WebViewBridge.send('goToCourses');

  return false;
});

$('.go_to_cart').click(function(){
  WebViewBridge.send('goToCheckout');

  goto('cart');return false;
});
`

const ActivityIndicatorView = () => (
  <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
    <ActivityIndicator
        animating
        size='large'
        style={{height: 80}}
    />
  </View>
)

class PresentationTutorialScreen extends React.PureComponent {

  constructor() {
    super()

    this.onBridgeMessage = this.onBridgeMessage.bind(this)
  }

  onBridgeMessage(message) {
    switch (message) {
    case 'goToTutorial':
      postNotification('GoToTutorial', null)
      break
    case 'goToCourses':
      postNotification('GoToCourses', null)
      break
    case 'goToCheckout':
      postNotification('GoToCheckout', null)
      break
    }
  }

  render() {
    return (
        <Screen
            fullscreen
          >
          <WebViewBridge
              injectedJavaScript={injectScript}
              onBridgeMessage={this.onBridgeMessage}
              renderLoading={() => <ActivityIndicatorView />}
              source={{uri: `http://nativox.com/ini/app_sales?os=ios&width=${width}&height=${height}`}}
              startInLoadingState
              style={styles.container}
          />
        </Screen>
    )
  }
}

export default PresentationTutorialScreen
