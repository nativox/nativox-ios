/* @flow */

import React, {PropTypes} from 'react'
import {View, StyleSheet, TextInput} from 'react-native'
import Screen from '../common/Screen'
import Device from 'react-native-device'
import Button from '../common/Button'
import ImageButton from '../common/ImageButton'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import {
  changeContactEmail,
  changeContactMessage,
  sendContactMessage,
} from './contact'
import userSession from '../user/userSession'
import {isSmallDevice} from '../common/DeviceUtility'
import openUrl from '../common/openUrl'
import {strings} from '../stringsApp'

const socialImageSize = isSmallDevice() ? 40 : Device.isIpad() ? 70 : 55
const styles = StyleSheet.create({
  email: {
    height: 45,
    backgroundColor: 'rgb(162, 162, 161)',
    padding: 5,
    fontSize: Device.isIpad() ? 24 : 16,
    color: 'white',
    textAlign: 'center',
    fontWeight: 'bold',
    fontFamily: 'HelveticaNeue',
    marginBottom: 10,
  },
  message: {
    flex: 4,
    backgroundColor: 'rgb(162, 162, 161)',
    fontSize: Device.isIpad() ? 24 : 16,
    padding: 5,
    color: 'white',
    marginBottom: 15,
    fontFamily: 'HelveticaNeue',
  },
  sendButton: {
    marginBottom: 15,
    height: isSmallDevice() ? 35 :  Device.isIpad() ? 70 : 40
  },
  saveButtonTitle: {
    fontSize: Device.isIpad() ? 40 : 19
  },
  socialButtonsContainer: {
    flexDirection:'row',
    alignItems: 'center',
    justifyContent: 'center',
    flexGrow: 1,
    marginBottom: 15,
  },
  socialButton: {
    margin: Device.isIpad()  ? 30 : 15
  }
})

class ContactScreen extends React.Component {

  constructor(props) {
    super(props)

    props.changeContactEmail(userSession.getUser().email)
    this.onTwitterIconPressed = this.onTwitterIconPressed.bind(this)
    this.onFacebookIconPressed = this.onFacebookIconPressed.bind(this)
  }

  onFacebookIconPressed() {
    openUrl('fb://profile/1637969713101267', 'https://www.facebook.com/nativox.english')
  }

  onTwitterIconPressed() {
    openUrl('twitter://user?screen_name=nativox_app', 'https://twitter.com/nativox_app')
  }

  render() {
    return (
        <Screen isTabScreen
            showLeftNavBar
            title={strings.contactTitleScreen}>
          <TextInput onChangeText={this.props.changeContactEmail}
              placeholder={strings.emailPlaceHolder}
              style={styles.email}
              value={this.props.contactEmail}/>
          <TextInput blurOnSubmit
              multiline
              onChangeText={this.props.changeContactMessage}
              placeholder={strings.messagePlaceHolder}
              style={styles.message}
              value={this.props.contactMessage}/>

          <Button onPress={this.props.sendContactMessage}
              style={styles.sendButton}
              title={strings.sendButtonTitle}
              titleStyle={styles.saveButtonTitle}/>
          <View style={styles.socialButtonsContainer}>
            <ImageButton height={socialImageSize}
                image='facebook_share_button'
                onPress={this.onFacebookIconPressed}
                size={socialImageSize}
                style={styles.socialButton}
                width={socialImageSize}/>
            <ImageButton height={socialImageSize}
                image='twitter_share_button'
                onPress={this.onTwitterIconPressed}
                style={styles.socialButton}
                width={socialImageSize}/>
          </View>
        </Screen>
    )
  }
}

ContactScreen.propTypes = {
  actions: PropTypes.any,
  changeContactEmail: PropTypes.func,
  changeContactMessage: PropTypes.func,
  contactEmail: PropTypes.string,
  contactMessage: PropTypes.string,
  sendContactMessage: PropTypes.string,
}

export default connect(state => ({
  contactEmail: state.contact.get('contactEmail'),
  contactMessage: state.contact.get('contactMessage'),
}),
  (dispatch) => (
    bindActionCreators({
      changeContactEmail,
      changeContactMessage,
      sendContactMessage,
    }, dispatch)
  )
)(ContactScreen)
