/* @flow */

import immutable from 'immutable'
import {
  createAction,
  handleActions,
} from 'redux-actions'
import {NativeModules, Alert} from 'react-native'
import {post} from '../common/apiRequest'
import userSession from '../user/userSession'
import {strings} from '../stringsApp'

const {disableBackButton,enableBackButton} = NativeModules.ReactNativeView
const ProgressDialogModule = NativeModules.ProgressDialogModule

export const SET_CONTACT_MESSAGE = 'SET_CONTACT_MESSAGE'
export const SET_CONTACT_EMAIL = 'SET_CONTACT_EMAIL'
export const START_SENDING_CONTACT_MESSAGE = 'START_SENDING_CONTACT_MESSAGE'
export const FINISH_SENDING_CONTACT_MESSAGE = 'FINISH_SENDING_CONTACT_MESSAGE'

const setContactMessage = createAction(SET_CONTACT_MESSAGE)
const setContactEmail = createAction(SET_CONTACT_EMAIL)
const startSendingContactMessage= createAction(START_SENDING_CONTACT_MESSAGE)
const finishSendingContactMessage= createAction(FINISH_SENDING_CONTACT_MESSAGE)

export const changeContactMessage = (message) => {
  return (dispatch) => {
    dispatch(setContactMessage(message))
  }
}

export const changeContactEmail = (email) => {
  return (dispatch) => {
    dispatch(setContactEmail(email))
  }
}

export const sendContactMessage = () => {
  return (dispatch, getState) => {
    const email = getState().contact.get('contactEmail')
    const message = getState().contact.get('contactMessage')

    if (!message || !message.length) {
      Alert.alert('Nativox', 'There is no message')
      return
    }

    const params = {
      mensaje : message,
      correo : email,
      id_usuario : userSession.getUser().idUser,
    }

    disableBackButton()
    dispatch(startSendingContactMessage())
    ProgressDialogModule.showMessage(strings.sendingProgressDialogTitle)
    post('contacto', params).then((response) => {
      ProgressDialogModule.dismissMessage()
      enableBackButton()
      dispatch(finishSendingContactMessage())
      if (response.status === 'true') {
        dispatch(setContactMessage(''))
      }
      Alert.alert('Nativox', 'Message sent successfully.')
    }).catch(() => {
      ProgressDialogModule.dismissMessage()
      enableBackButton()
      dispatch(finishSendingContactMessage())
      Alert.alert('Nativox', 'There has been some issue sending your message.')
    })
  }
}

export default handleActions({
  [SET_CONTACT_MESSAGE]: (state, action) => state.set('contactMessage', action.payload),
  [SET_CONTACT_EMAIL]: (state, action) => state.set('contactEmail', action.payload),
  [START_SENDING_CONTACT_MESSAGE]: (state) => state.set('isSendingContactMessage', true),
  [FINISH_SENDING_CONTACT_MESSAGE]: (state) => state.set('isSendingContactMessage', false)
}, immutable.fromJS({
  contactEmail: '',
  contactMessage: '',
  isSendingContactMessage: false,
}))
