/* @flow */

import eventHandlers from './eventHandlers'
import actionCreators from './actionCreators'
import reducer from './reducer'

export default {
  eventHandlers,
  actionCreators,
  reducer,
}
