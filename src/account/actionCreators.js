/* @flow */

import {createAction} from 'redux-actions'

export const setUserName = createAction('SET_USER_NAME')
export const setEmail= createAction('SET_EMAIL')
export const setPassword= createAction('SET_PASSWORD')
export const setConfirmationPassword= createAction('SET_CONFIRMATION_PASSWORD')
export const startSavingUserDetails= createAction('START_SAVING_USER_DETAILS')
export const finishSavingUserDetails= createAction('FINISH_SAVING_USER_DETAILS')
