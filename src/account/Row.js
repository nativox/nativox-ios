/* @flow */

import React, {PropTypes} from 'react'
import {Text, View, TextInput, StyleSheet} from 'react-native'
import {isSmallDevice} from '../common/DeviceUtility.js'
import Device from 'react-native-device'
import {FONT_SIZE_TEXT_INPUT, HEIGHT_TEXT_INPUT} from '../common/NativoxTheme'

const styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: isSmallDevice() ? 6 : Device.isIpad() ? 18 : 12,
    marginBottom: isSmallDevice() ? 6 : Device.isIpad() ? 18 : 12,
  },
  label: {
    flex: 2,
    color: 'rgb(255, 255, 255)',
    fontSize: Device.isIpad() ? 26 : isSmallDevice() ? 12 : 16
  },
  textInput: {
    height: HEIGHT_TEXT_INPUT,
    flexGrow: 4,
    backgroundColor: 'rgb(255, 255, 255)',
    borderRadius: 7,
    padding: Device.isIpad() ? 10 : 5,
    fontSize: FONT_SIZE_TEXT_INPUT
  }
})

export default class Row extends React.PureComponent {
  render() {
    return (
      <View style={styles.row}>
         <Text style={styles.label}>{this.props.label}</Text>
         <TextInput editable={!this.props.disable}
             onChangeText={this.props.onChangeText}
             onFocus={this.props.onRowFocus}
             secureTextEntry={this.props.password}
             style={styles.textInput}
             value={this.props.text}/>
      </View>
    )
  }
}

Row.propTypes = {
  disable: PropTypes.bool,
  label: PropTypes.string,
  onChangeText: PropTypes.func,
  onRowFocus: PropTypes.func,
  password: PropTypes.bool,
  text: PropTypes.string,
}
