/* @flow */

import categories from '../categories'
import {post} from '../common/apiRequest'
import {strings} from '../stringsApp'
import userSession from '../user/userSession'
import {NativeModules, Alert} from 'react-native'
import {
  setConfirmationPassword,
  setEmail,
  setUserName,
  setPassword,
  startSavingUserDetails,
  finishSavingUserDetails,
} from './actionCreators'

const {onClearCategories, onLoadCategories} = categories.eventHandlers
const ProgressDialogModule = NativeModules.ProgressDialogModule
const {
  disableBackButton,
  enableBackButton,
  saveUserSession,
  popScreen,
  postNotification,
} = NativeModules.ReactNativeView
const {InAppUtils} = NativeModules

export const changeUserName = (userName: string) => (dispatch: func) => dispatch(setUserName(userName))
export const changeEmail = (email: string) => (dispatch: func) => dispatch(setEmail(email))
export const changePassword = (password: string) => (dispatch: func) => dispatch(setPassword(password))

export const changeConfirmationPassword = (confirmationPassword: string) => (
  (dispatch) => dispatch(setConfirmationPassword(confirmationPassword: string))
)

export const onSaveUserDetails = () => {
  return (dispatch, getState) => {
    const password = getState().account.get('password')
    const confirmationPassword = getState().account.get('confirmationPassword')

    if (password.length > 0 && password.length < 6) {
      Alert.alert('Nativox', strings.errorNewPasswordUpdateAccount)
      return
    }

    if (password !== confirmationPassword) {
      Alert.alert('Nativox', strings.errorNewOldPasswordUpdateAccount)
      return
    }

    const params = {
      nuevo_correo: getState().account.get('email'),
      nuevo_nombre: getState().account.get('name'),
      suscripcion: '0'
    }

    if (password.length > 0) {
      params['nuevo_password'] = password
    }

    disableBackButton()
    dispatch(startSavingUserDetails())
    ProgressDialogModule.showMessage(strings.savingProgressDialogTitle)
    post('usuario_editar', params).then((response) => {
      ProgressDialogModule.dismissMessage()
      dispatch(finishSavingUserDetails())
      enableBackButton()
      if (response.status === 'true') {
        saveUserSession({
          name: getState().account.get('name'),
          email: getState().account.get('email'),
        })
        userSession.getUser().name = getState().account.get('name')
        dispatch(setPassword(''))
        dispatch(setConfirmationPassword(''))
      }
      Alert.alert('Nativox', response.result, [{text: 'OK', onPress: () => popScreen()}])
    }).catch(() => {
      ProgressDialogModule.dismissMessage()
      enableBackButton()
      dispatch(finishSavingUserDetails())
    })
  }
}

export const onRestoreCourses = () => async (dispatch) => {
  ProgressDialogModule.showMessage('Restoring courses')
  InAppUtils.restorePurchases((error, response)=> {
    ProgressDialogModule.dismissMessage()

    if (error) {
      Alert.alert('itunes Error', 'Could not connect to itunes store.')

      return
    }

    if (response.length == 0) {
      Alert.alert('No Purchases', "We didn't find any purchases to restore.")
      return
    }

    Alert.alert('Restore Successful', 'Successfully restores all your purchases.')
    dispatch(onLoadCategories())
  })
}

export const onLogout = () => async (dispatch) => {
  ProgressDialogModule.showMessage('Logging out')

  try {
    await post('logout')

    ProgressDialogModule.dismissMessage()
  } catch (error) {
    console.log(error) //eslint-disable-line
    ProgressDialogModule.dismissMessage()
  }

  dispatch(onClearCategories())
  userSession.resetUser()
  postNotification('LogoutButtonPressed', null)
}
