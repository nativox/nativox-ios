/* @flow */

import React, {PropTypes} from 'react'
import {
  Text,
  View,
  StyleSheet,
} from 'react-native'
import Button from '../common/Button'
import {
  changeUserName,
  changeEmail,
  changePassword,
  changeConfirmationPassword,
  onSaveUserDetails,
  onLogout,
  onRestoreCourses,
} from './eventHandlers'
import {connect} from 'react-redux'
import {isSmallDevice} from '../common/DeviceUtility.js'
import Screen from '../common/Screen'
import userSession from '../user/userSession'
import Device from 'react-native-device'
import Row from './Row'
import {strings} from '../stringsApp'
import Orientation from 'react-native-orientation'

const styles = StyleSheet.create({
  explain_update_data: {
    color: 'rgb(255, 255, 255)',
    fontSize: Device.isIpad() ? 27 : isSmallDevice() ? 13 : 16,
    fontFamily: 'HelveticaNeue',
    textAlign: 'justify',
    marginTop: Device.isIpad() ? 25 : 5,
    marginBottom: Device.isIpad() ? 25 : 5
  },
  container: {
    flexGrow: 1,
    justifyContent: 'center',
  },
  saveButton: {
    height: isSmallDevice() ? 27 :  Device.isIpad() ? 70 : 40,
    marginBottom: isSmallDevice() ? 11 : 15,
  },
  saveButtonTitle: {
    fontSize: Device.isIpad() ? 40 : isSmallDevice() ? 17 : 19,
  },
  logoutButtonTitle: {
    fontSize: Device.isIpad() ? 40 : isSmallDevice() ? 17 : 19,
  },
  restoreButtonTitle: {
    fontSize: Device.isIpad() ? 40 : isSmallDevice() ? 17 : 19,
  },
  logoutButton: {
    height: isSmallDevice() ? 27 :  Device.isIpad() ? 70 : 40,
    marginBottom: isSmallDevice() ? 11 : 15,
    backgroundColor: 'rgb(140, 1, 4)',
  },
  restoreButton: {
    height: isSmallDevice() ? 27 :  Device.isIpad() ? 70 : 40,
    marginBottom: isSmallDevice() ? 11 : 15,
  }
})

class UpdateAccountScreen extends React.Component {

  constructor(props) {
    super(props)

    props.changeEmail(userSession.getUser().email)
    props.changeUserName(userSession.getUser().name)

    this.state = {
      focusedField: null,
      accountState: props.state,
      orientation: Orientation.getInitialOrientation(),
    }

    this.onRowFocus = this.onRowFocus.bind(this)
    Orientation.addOrientationListener(this.orientationDidChange.bind(this))
  }

  orientationDidChange(orientation) {
    this.setState({orientation})
  }

  onRowFocus(ref: any){
    this.setState({focusedField: ref})
  }

  render() {
    const {state} = this.props

    return (
        <Screen focusedField={this.refs[this.state.focusedField]}
            isTabScreen
            loadingTitle={strings.loadingProgressDialogTitle}
            showLeftNavBar
            title='Account'>
              <Text style={styles.explain_update_data}>{strings.userDataExplanaitionLabel}</Text>
              <View style={styles.container}>
                <Row disable={true}
                    label='Email'
                    text={state.get('email')}
                />
                <Row label={strings.userNameLabel}
                    onChangeText={this.props.changeUserName}
                    onRowFocus={() => this.onRowFocus('name')}
                    ref='name'
                    text={state.get('name')}/>
                <Row label={strings.userPasswordLabel}
                    onChangeText={this.props.changePassword}
                    onRowFocus={() => this.onRowFocus('password')}
                    password
                    ref='password'
                    text={state.get('password')}/>
                <Row label={strings.userPasswordConfirmLabel}
                    onChangeText={this.props.changeConfirmationPassword}
                    onRowFocus={() => this.onRowFocus('confirmationPassword')}
                    password
                    ref='confirmationPassword'
                    text={state.get('confirmationPassword')}/>
               </View>
               <Button
                   onPress={this.props.onSaveUserDetails}
                   style={styles.saveButton}
                   title={strings.saveButtonTitle}
                   titleStyle={styles.saveButtonTitle}/>
               {
                 this.state.orientation === 'LANDSCAPE' && Device.isIpad() ? null :
                 <View>
                    <Button
                        onPress={this.props.onRestoreCourses}
                        style={styles.restoreButton}
                        title={strings.restoreButtonTitle}
                        titleStyle={styles.restoreButtonTitle}/>
                    <Button
                        onPress={this.props.onLogout}
                        style={styles.logoutButton}
                        title={strings.logoutButtonTitle}
                        titleStyle={styles.logoutButtonTitle}/>
                  </View>
               }
        </Screen>
    )
  }
}

UpdateAccountScreen.propTypes = {
  changeConfirmationPassword: PropTypes.func,
  changeEmail: PropTypes.func,
  changePassword: PropTypes.func,
  changeUserName: PropTypes.func,
  onLogout: PropTypes.func,
  onRestoreCourses: PropTypes.func,
  onSaveUserDetails: PropTypes.func,
  state: PropTypes.any
}

export default connect(state => ({
  state: state.account
}),
  (dispatch) => ({
    changeUserName: (text: string) => dispatch(changeUserName(text)),
    changeEmail: (text: string) => dispatch(changeEmail(text)),
    changePassword: (text: string) => dispatch(changePassword(text)),
    changeConfirmationPassword: (text: string) => dispatch(changeConfirmationPassword(text)),
    onSaveUserDetails: () => dispatch(onSaveUserDetails()),
    onLogout: () => dispatch(onLogout()),
    onRestoreCourses: () => dispatch(onRestoreCourses())
  })
)(UpdateAccountScreen)
