/* @flow */

import React, {PropTypes} from 'react'
import {Picker, StyleSheet, View} from 'react-native'
import languages from './languages'
import Button from '../common/Button'
import {strings} from '../stringsApp'

const styles = StyleSheet.create({
  itemStyle: {
    color: 'rgb(255, 255, 255)',
    fontSize: 23,
    fontWeight: 'bold',
  },
  setButtonTitle: {
    fontSize: 20,
  },
})

export default class LanguageSelector extends React.PureComponent {

  constructor() {
    super()

    this.state = {
      selectedLanguage: languages.get(0).get('key'),
    }

    this.changeLanguage = this.changeLanguage.bind(this)
    this.onLanguageSelected = this.onLanguageSelected.bind(this)
  }

  changeLanguage(value) {
    this.setState({selectedLanguage: value})
  }

  onLanguageSelected() {
    this.props.onLanguageSelected(this.state.selectedLanguage)
  }

  render() {
    return (
      <View>
        <Picker itemStyle={styles.itemStyle}
            onValueChange={this.changeLanguage}
            selectedValue={this.state.selectedLanguage}>
          {
            languages.map((language) =>
              <Picker.Item key={language.hashCode()}
                  label={language.get('description')}
                  value={language.get('key')}/>)
          }
        </Picker>
        <Button
            invisible
            onPress={this.onLanguageSelected}
            title={strings.setButtonTitle}
            titleStyle={styles.setButtonTitle}/>
      </View>
    )
  }
}

LanguageSelector.propTypes= {
  onLanguageSelected: PropTypes.func,
}
