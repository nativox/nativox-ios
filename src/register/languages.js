/* @flow */

import {fromJS} from 'immutable'

const languages = fromJS([
  {
    key: 'zh-CN',
    description: 'Chinese (Simplified)',
  },
  {
    key: 'nl-NL',
    description: 'Dutch',
  },
  {
    key: 'fr-FR',
    description: 'French',
  },
  {
    key: 'de-DE',
    description: 'German',
  },
  {
    key: 'el-GR',
    description: 'Greek',
  },
  {
    key: 'hi-IN',
    description: 'Hindi',
  },
  {
    key: 'it-IT',
    description: 'Italian',
  },
  {
    key: 'ja-JP',
    description: 'Japanese',
  },
  {
    key: 'ko-KR',
    description: 'Korean (South Korea)',
  },
  {
    key: 'pl-PL',
    description: 'Polish',
  },
  {
    key: 'ru-RU',
    description: 'Russian',
  },
  {
    key: 'es-ES',
    description: 'Spanish',
  },
  {
    key: 'sv-SE',
    description: 'Swedish',
  },
  {
    key: 'tr-TR',
    description: 'Turkish',
  },
  {
    key: 'uk',
    description: 'Ukrainian',
  },
  {
    key: 'af',
    description: 'Afrikaans',
  },
  {
    key: 'am',
    description: 'Amharic',
  },
  {
    key: 'ar',
    description: 'Arabic',
  },
  {
    key: 'hy-AM',
    description: 'Armenian',
  },
  {
    key: 'az-AZ',
    description: 'Azerbaijani',
  },
  {
    key: 'eu-ES',
    description: 'Basque',
  },
  {
    key: 'be',
    description: 'Belarusian',
  },
  {
    key: 'bn-BD',
    description: 'Bengali',
  },
  {
    key: 'bg',
    description: 'Bulgarian',
  },
  {
    key: 'my-MM',
    description: 'Burmese',
  },
  {
    key: 'ca',
    description: 'Catalan',
  },
  {
    key: 'zh-TW',
    description: 'Chinese (Traditional)',
  },
  {
    key: 'hr',
    description: 'Croatian',
  },
  {
    key: 'cs-CZ',
    description: 'Czech',
  },
  {
    key: 'da-DK',
    description: 'Danish',
  },
  {
    key: 'en-US',
    description: 'English',
  },
  {
    key: 'et',
    description: 'Estonian',
  },
  {
    key: 'fil',
    description: 'Filipino',
  },
  {
    key: 'fi-FI',
    description: 'Finnish',
  },
  {
    key: 'gl-ES',
    description: 'Galician',
  },
  {
    key: 'ka-GE',
    description: 'Georgian',
  },
  {
    key: 'iw-IL',
    description: 'Hebrew',
  },
  {
    key: 'hu-HU',
    description: 'Hungarian',
  },
  {
    key: 'is-IS',
    description: 'Icelandic',
  },
  {
    key: 'id',
    description: 'Indonesian',
  },
  {
    key: 'kn-IN',
    description: 'Kannada',
  },
  {
    key: 'km-KH',
    description: 'Khmer',
  },
  {
    key: 'ky-KG',
    description: 'Kyrgyz',
  },
  {
    key: 'lo-LA',
    description: 'Lao',
  },
  {
    key: 'lv',
    description: 'Latvian',
  },
  {
    key: 'lt',
    description: 'Lithuanian',
  },
  {
    key: 'mk-MK',
    description: 'Macedonian',
  },
  {
    key: 'ms',
    description: 'Malay',
  },
  {
    key: 'ml-IN',
    description: 'Malayalam',
  },
  {
    key: 'mr-IN',
    description: 'Marathi',
  },
  {
    key: 'mn-MN',
    description: 'Mongolian',
  },
  {
    key: 'ne-NP',
    description: 'Nepali',
  },
  {
    key: 'no-NO',
    description: 'Norwegian',
  },
  {
    key: 'fa',
    description: 'Persian',
  },
  {
    key: 'pt-PT',
    description: 'Portuguese',
  },
  {
    key: 'ro',
    description: 'Romanian',
  },
  {
    key: 'rm',
    description: 'Romansh',
  },
  {
    key: 'sr',
    description: 'Serbian',
  },
  {
    key: 'si-LK',
    description: 'Sinhala',
  },
  {
    key: 'sk',
    description: 'Slovak',
  },
  {
    key: 'sl',
    description: 'Slovenian',
  },
  {
    key: 'sw',
    description: 'Swahili',
  },
  {
    key: 'ta-IN',
    description: 'Tamil',
  },
  {
    key: 'te-IN',
    description: 'Telugu',
  },
  {
    key: 'th',
    description: 'Thai',
  },
  {
    key: 'vi',
    description: 'Vietnamese',
  },
  {
    key: 'zu',
    description: 'Zulu',
  },
])

export default languages
