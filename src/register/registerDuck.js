/* @flow */

import immutable from 'immutable'
import {createAction, handleActions} from 'redux-actions'
import {NativeModules, Alert} from 'react-native'
import userSession from '../user/userSession'
import {post} from '../common/apiRequest'
import {Answers} from 'react-native-fabric'
import {strings} from '../stringsApp'

const ProgressDialogModule = NativeModules.ProgressDialogModule
const {popScreen, savePreferredLanguage} = NativeModules.ReactNativeView

export const SET_REGISTER_NAME = 'SET_REGISTER_NAME'
export const SET_REGISTER_EMAIL = 'SET_REGISTER_EMAIL'
export const SET_REGISTER_PASSWORD = 'SET_REGISTER_PASSWORD'
export const SET_REGISTER_CONFIRM_PASSWORD = 'SET_REGISTER_CONFIRM_PASSWORD'
export const CHANGE_TC_ACCEPTED = 'CHANGE_TC_ACCEPTED'
export const SHOW_LANGUAGE_SELECTOR = 'SHOW_LANGUAGE_SELECTOR'
export const HIDE_LANGUAGE_SELECTOR = 'HIDE_LANGUAGE_SELECTOR'
export const SELECT_LANGUAGE = 'SELECT_LANGUAGE'
export const SHOW_CONFIRM_REGISTRATION = 'SHOW_CONFIRM_REGISTRATION'
export const HIDE_CONFIRM_REGISTRATION = 'HIDE_CONFIRM_REGISTRATION'
export const CLEAR_REGISTRATION_DATA = 'CLEAR_REGISTRATION_DATA'

const setRegisterName = createAction(SET_REGISTER_NAME)
const setRegisterEmail = createAction(SET_REGISTER_EMAIL)
const setRegisterPassword = createAction(SET_REGISTER_PASSWORD)
const setRegisterConfirmPassword = createAction(SET_REGISTER_CONFIRM_PASSWORD)
const toogleTermsAndConditionsAccepted = createAction(CHANGE_TC_ACCEPTED)
const showLanguageSelector = createAction(SHOW_LANGUAGE_SELECTOR)
const hideLanguageSelector = createAction(HIDE_LANGUAGE_SELECTOR)
const selectLanguage = createAction(SELECT_LANGUAGE)
const showConfirmRegistration = createAction(SHOW_CONFIRM_REGISTRATION)
const hideConfirmRegistration = createAction(HIDE_CONFIRM_REGISTRATION)
const clearRegistrationData = createAction(CLEAR_REGISTRATION_DATA)

export const changeRegisterName = (name) => {
  return (dispatch) => {
    dispatch(setRegisterName(name))
  }
}

export const changeRegisterEmail = (email) => {
  return (dispatch) => {
    dispatch(setRegisterEmail(email))
  }
}

export const changeRegisterPassword = (password) => {
  return (dispatch) => {
    dispatch(setRegisterPassword(password))
  }
}

export const changeRegisterConfirmPassword = (confirmPassword) => {
  return (dispatch) => {
    dispatch(setRegisterConfirmPassword(confirmPassword))
  }
}

export const changeTermsAndConditionsAccepted = () => {
  return (dispatch) => {
    dispatch(toogleTermsAndConditionsAccepted())
  }
}

export const openLanguageSelector = () => {
  return (dispatch) => {
    dispatch(showLanguageSelector())
  }
}

export const closeLanguageSelector = () => {
  return (dispatch) => {
    dispatch(hideLanguageSelector())
  }
}

export const changeSelectedLanguage = (language) => {
  return (dispatch) => {
    dispatch(selectLanguage(language))
  }
}

export const openConfirmRegistration = () => {
  return (dispatch) => {
    dispatch(showConfirmRegistration())
  }
}

export const closeConfirmRegistration = () => {
  return (dispatch) => {
    dispatch(hideConfirmRegistration())
  }
}

export const registerUser = () => {
  return (dispatch, getState) => {
    dispatch(closeConfirmRegistration())
    const registerState = getState().register
    const params = {
      nombre : registerState.get('registerName'),
      correo : registerState.get('registerEmail'),
      password : registerState.get('registerPassword'),
      lang : registerState.get('selectedLanguage'),
    }

    ProgressDialogModule.showMessage('Registering')
    post('register', params).then((response) => {
      ProgressDialogModule.dismissMessage()
      if (response.status === 'true') {
        Answers.logSignUp('Email', true)
        savePreferredLanguage(registerState.get('selectedLanguage'), registerState.get('registerEmail'))
        userSession.getUser().language = registerState.get('selectedLanguage')
        dispatch(clearRegistrationData())
        setTimeout(() => Alert.alert('Nativox', response.result.mensaje, [{text: 'OK', onPress: () => popScreen()}]), 500) //eslint-disable-line no-undef
      } else {
        setTimeout(() => Alert.alert('Nativox', response.result), 500) //eslint-disable-line no-undef
      }
    }).catch(() => {
      ProgressDialogModule.dismissMessage()
      setTimeout(() => Alert.alert('Nativox', strings.error_registration_service_failure), 500) //eslint-disable-line no-undef
    })
  }
}

const defaultState = immutable.fromJS({
  registerName: '',
  registerEmail: '',
  registerPassword: '',
  registerConfirmPassword: '',
  termsAndConditionsAccepted: false,
  showLanguageSelector: false,
  showConfirmRegistration: false,
  selectedLanguage: null,
})

export default handleActions({
  [SET_REGISTER_NAME]: (state, action) => state.set('registerName', action.payload),
  [SET_REGISTER_EMAIL]: (state, action) => state.set('registerEmail', action.payload),
  [SET_REGISTER_PASSWORD]: (state, action) => state.set('registerPassword', action.payload),
  [SET_REGISTER_CONFIRM_PASSWORD]: (state, action) => state.set('registerConfirmPassword', action.payload),
  [CHANGE_TC_ACCEPTED]: (state) => state.set('termsAndConditionsAccepted', !state.get('termsAndConditionsAccepted')),
  [SHOW_LANGUAGE_SELECTOR]: (state) => state.set('showLanguageSelector', true),
  [HIDE_LANGUAGE_SELECTOR]: (state) => state.set('showLanguageSelector', false),
  [SELECT_LANGUAGE]: (state, action) => state.set('selectedLanguage', action.payload),
  [SHOW_CONFIRM_REGISTRATION]: (state) => state.set('showConfirmRegistration', true),
  [HIDE_CONFIRM_REGISTRATION]: (state) => state.set('showConfirmRegistration', false),
  [CLEAR_REGISTRATION_DATA]: () => defaultState,
}, defaultState)
