/* @flow */

import React, {PropTypes} from 'react'
import {StyleSheet, Text, View, TouchableOpacity, Alert} from 'react-native'
import Screen from '../common/Screen'
import ModalScreen from '../common/ModalScreen'
import Button from '../common/Button'
import Checkbox from '../common/Checkbox'
import {
  changeRegisterName,
  changeRegisterEmail,
  changeRegisterPassword,
  changeRegisterConfirmPassword,
  changeTermsAndConditionsAccepted,
  openLanguageSelector,
  closeLanguageSelector,
  changeSelectedLanguage,
  openConfirmRegistration,
  closeConfirmRegistration,
  registerUser,
} from './registerDuck'
import {loginFB} from '../login/loginDuck'
import {connect} from 'react-redux'
import LanguageSelector from './LanguageSelector'
import ConfirmRegistration from './ConfirmRegistration'
import languages from './languages'
import emailValidator from 'email-validator'
import openUrl from '../common/openUrl'
import Device from 'react-native-device'
import {strings} from '../stringsApp'
import ImageButton from '../common/ImageButton'
import TextBox from '../common/TextBox'
import connectFB from '../login/connectFB'
import {isSmallDevice} from '../common/DeviceUtility'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: -20,
  },
  registerContainer: {
    width: Device.isIpad() ? 300 : 200,
    backgroundColor: 'rgb(162, 162, 161)',
    borderRadius: 6,
    paddingTop: Device.isIpad() ? 25 : 15,
    paddingLeft:  Device.isIpad() ? 20 : 13,
    paddingRight: Device.isIpad() ? 20 : 13,
  },
  registerTitle: {
    color: 'rgb(162, 162, 161)',
    fontSize: Device.isIpad() ? 41 : 21,
    marginBottom: 20,
    fontFamily: 'Helvetica Neue',
    fontWeight: '300',
  },
  selectLanguageButton: {
    paddingTop: 7,
    paddingBottom: 10,
    textAlign: 'center',
    color: 'rgb(51, 51, 51)',
    fontSize: Device.isIpad() ? 22 : 16,
  },
  checkBoxLabel: {
    fontSize: Device.isIpad() ? 19 : 12,
    color: 'rgb(255, 255, 255)',
    margin: 8,
    flex: 5,
    fontFamily: 'Helvetica Neue',
    fontWeight: '300',
  },
  checkboxContainer: {
    marginTop: 12,
    width: Device.isIpad() ? 270 : 180,
    height: Device.isIpad() ? 65 : 45,
    flexDirection: 'row',
    alignItems: 'center',
  },
  registerButton: {
    height: Device.isIpad() ? 45 : 30,
    marginBottom: 15,
  },
  registerTitleButton: {
    fontSize: Device.isIpad() ? 22 : 16,
  },
  languageButton: {
    backgroundColor: 'transparent',
    marginTop: 10,
    marginBottom: 10,
  },
  facebookButtonContainer: {
    paddingTop: Device.isIpad() ? 80 : isSmallDevice() ? 60 : 80
  },
})

const getDescriptionLanguageSelected = (languageKey) => {
  const languageSelected = languages.find((language) => language.get('key') === languageKey)

  return languageSelected ? languageSelected.get('description') : strings.selectLanguage
}

const openTermsAndConditionsUrl = () => openUrl('http://nativox.com/index.php/ini/terms')

class RegisterScreen extends React.PureComponent {

  constructor() {
    super()

    this.state = {
      focusedField: null,
    }

    this.onRowFocus = this.onRowFocus.bind(this)
    this.onPressRegistrationButton = this.onPressRegistrationButton.bind(this)
  }

  onRowFocus(ref){
    this.setState({focusedField: ref})
  }

  onPressRegistrationButton() {
    if (!this.props.registerName) {
      Alert.alert('Nativox', strings.error_registration_name) // eslint-disable-line no-undef
      return
    }

    if (!this.props.registerEmail) {
      Alert.alert('Nativox', strings.error_registration_email) // eslint-disable-line no-undef
      return
    }

    if (!emailValidator.validate(this.props.registerEmail)) {
      Alert.alert('Nativox', strings.error_registration_email_no_valid) // eslint-disable-line no-undef
      return
    }

    if (!this.props.registerPassword) {
      Alert.alert('Nativox', strings.error_registration_password) // eslint-disable-line no-undef
      return
    }

    if (!this.props.registerConfirmPassword) {
      Alert.alert('Nativox', strings.error_registration_confirmation_password) // eslint-disable-line no-undef
      return
    }

    if (this.props.registerPassword.length < 6) {
      Alert.alert('Nativox', strings.error_registration_password_length) // eslint-disable-line no-undef
      return
    }

    if (this.props.registerPassword !== this.props.registerConfirmPassword) {
      Alert.alert('Nativox', strings.error_registration_password_different) // eslint-disable-line no-undef
      return
    }

    if (!this.props.selectedLanguage) {
      Alert.alert('Nativox', strings.error_registration_no_language) // eslint-disable-line no-undef
      return
    }

    if (!this.props.termsAndConditionsAccepted) {
      Alert.alert('Nativox', strings.error_registration_no_tcs) // eslint-disable-line no-undef
      return
    }

    this.props.openConfirmRegistration()
  }

  render() {
    return (
        <Screen
            focusedField={this.refs[this.state.focusedField]}
            showLeftNavBar>
          <ModalScreen height={320}
              isVisible={this.props.showLanguageSelector}
              onCloseButtonPressed={this.props.closeLanguageSelector}
              onTouchBackground={this.props.closeLanguageSelector}
              width={250}>
                <LanguageSelector onLanguageSelected={this.props.changeSelectedLanguage}/>
          </ModalScreen>
          <ModalScreen height={320}
              isVisible={this.props.showConfirmRegistration}
              onCloseButtonPressed={this.props.closeConfirmRegistration}
              onTouchBackground={this.props.closeConfirmRegistration}
              width={Device.isIpad()  ? 290 : 250}>
                <ConfirmRegistration email={this.props.registerEmail}
                    onChangeDataPressed={this.props.closeConfirmRegistration}
                    onYesButtonPressed={this.props.registerUser}
                    password={this.props.registerPassword}/>
          </ModalScreen>
          <View style={styles.container}>
            <Text style={styles.registerTitle}>
                {strings.registerTitle}
            </Text>
            <View style={styles.registerContainer}>
                <TextBox
                    onChangeText={this.props.changeRegisterName}
                    placeholder={strings.placeholderName}
                    value={this.props.registerName}/>
                <TextBox
                    onChangeText={this.props.changeRegisterEmail}
                    placeholder={strings.placeholderEmail}
                    value={this.props.registerEmail}/>
                <TextBox
                    isPassword
                    onChangeText={this.props.changeRegisterPassword}
                    placeholder={strings.placeholderPassword}
                    value={this.props.registerPassword}/>
                <TextBox
                    isPassword
                    onChangeText={this.props.changeRegisterConfirmPassword}
                    onRowFocus={() => this.onRowFocus('confirmPassword')}
                    placeholder={strings.placeholderConfirmPassword}
                    ref='confirmPassword'
                    value={this.props.registerConfirmPassword}/>
                <Button
                    onPress={this.props.openLanguageSelector}
                    style={styles.languageButton}
                    title={getDescriptionLanguageSelected(this.props.selectedLanguage)}
                    titleStyle={styles.selectLanguageButton}/>
                <Button
                    onPress={this.onPressRegistrationButton}
                    style={styles.registerButton}
                    title={strings.registerButtonTitle}
                    titleStyle={styles.registerTitleButton}/>
            </View>
            <View style={styles.checkboxContainer}>
                <Checkbox checked={this.props.termsAndConditionsAccepted}
                    onChange={this.props.changeTermsAndConditionsAccepted}
                    size={Device.isIpad() ? 32 : 24}/>
                <TouchableOpacity onPress={openTermsAndConditionsUrl}>
                  <Text style={styles.checkBoxLabel}>
                    {strings.termsAndConditionsDescription}
                  </Text>
                </TouchableOpacity>
            </View>
            {
              !this.props.hasPresentedRegister ?
              <View style={styles.facebookButtonContainer}>
                <ImageButton
                    height={Device.isIpad() ? 44 : 25}
                    image='fb_button'
                    onPress={() => connectFB(this.props.loginFB)}
                    width={Device.isIpad() ? 220 : 150}/>
              </View> : null
            }

          </View>
        </Screen>
    )
  }
}

RegisterScreen.propTypes = {
  changeRegisterConfirmPassword: PropTypes.func,
  changeRegisterEmail: PropTypes.func,
  changeRegisterName: PropTypes.func,
  changeRegisterPassword: PropTypes.func,
  changeSelectedLanguage: PropTypes.func,
  changeTermsAndConditionsAccepted: PropTypes.func,
  closeConfirmRegistration: PropTypes.func,
  closeLanguageSelector: PropTypes.func,
  hasPresentedRegister: PropTypes.bool,
  loginFB: PropTypes.func,
  openConfirmRegistration: PropTypes.func,
  openLanguageSelector: PropTypes.func,
  registerConfirmPassword: PropTypes.string,
  registerEmail: PropTypes.string,
  registerName: PropTypes.string,
  registerPassword: PropTypes.string,
  registerUser: PropTypes.func,
  selectedLanguage: PropTypes.string,
  showConfirmRegistration: PropTypes.bool,
  showLanguageSelector: PropTypes.bool,
  termsAndConditionsAccepted: PropTypes.bool,
}

export default connect(state => ({
  registerName: state.register.get('registerName'),
  registerEmail: state.register.get('registerEmail'),
  registerPassword: state.register.get('registerPassword'),
  registerConfirmPassword: state.register.get('registerConfirmPassword'),
  showLanguageSelector: state.register.get('showLanguageSelector'),
  showConfirmRegistration: state.register.get('showConfirmRegistration'),
  termsAndConditionsAccepted: state.register.get('termsAndConditionsAccepted'),
  selectedLanguage: state.register.get('selectedLanguage'),
}),
  (dispatch) => ({
    changeRegisterName: (text) => dispatch(changeRegisterName(text)),
    changeRegisterEmail: (text) => dispatch(changeRegisterEmail(text)),
    changeRegisterPassword: (text) => dispatch(changeRegisterPassword(text)),
    changeRegisterConfirmPassword: (text) => dispatch(changeRegisterConfirmPassword(text)),
    changeTermsAndConditionsAccepted: () => dispatch(changeTermsAndConditionsAccepted()),
    openLanguageSelector: () => dispatch(openLanguageSelector()),
    closeLanguageSelector: () => dispatch(closeLanguageSelector()),
    openConfirmRegistration: () => dispatch(openConfirmRegistration()),
    closeConfirmRegistration: () => dispatch(closeConfirmRegistration()),
    changeSelectedLanguage: (language) => {
      dispatch(changeSelectedLanguage(language))
      dispatch(closeLanguageSelector())
    },
    registerUser: () => dispatch(registerUser()),
    loginFB: (params) => dispatch(loginFB(params, true)),
  })
)(RegisterScreen)
