/* @flow */

import React, {PropTypes} from 'react'
import {StyleSheet, View, Text, TextInput} from 'react-native'
import {HEADING_TITLE} from '../common/NativoxTheme.js'
import Button from '../common/Button'
import {strings} from '../stringsApp'
import Device from 'react-native-device'

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
  },
  title: {
    color: HEADING_TITLE,
    fontWeight: 'bold',
    fontSize: Device.isIpad() ? 18 : 16,
    marginBottom: 20,
    flexGrow: 1,
    textAlign: 'center',
  },
  label: {
    color: 'rgb(255, 255, 255)',
    fontWeight: 'bold',
    fontSize: Device.isIpad() ? 18 : 15,
    marginBottom: 10,
    flexGrow: 1,
    textAlign: 'center',
  },
  value: {
    color: 'rgb(255, 255, 255)',
    fontSize: Device.isIpad() ? 18 : 15,
    marginBottom: 15,
    flexGrow: 1,
    textAlign: 'center',
  },
  textInput: {
    height: 35,
    fontSize:  Device.isIpad() ? 18 : 16,
    color: 'rgb(255, 255, 255)',
    flexGrow: 1,
    textAlign: 'center'
  },
  passwordRow: {
    flexDirection: 'row',
  },
  showButton: {
    backgroundColor: 'transparent',
  },
  showButtonTitle: {
    fontSize: Device.isIpad() ? 18 : 15,
    color: HEADING_TITLE,
  },
  changeButton: {
    backgroundColor: 'transparent',
    marginTop: 5,
  },
  changeButtonTitle: {
    fontSize: Device.isIpad() ? 18 : 16,
    color: HEADING_TITLE,
  },
  yesButton: {
    height: 25,
    marginTop: 20,
  },
  yesButtonTitle: {
    fontSize: Device.isIpad() ? 18 : 16,
  }
})

export default class ConfirmRegistration extends React.PureComponent {

  constructor() {
    super()
    this.onPressShowPasswordButton = this.onPressShowPasswordButton.bind(this)

    this.state = {
      showPassword: false,
    }
  }

  onPressShowPasswordButton() {
    this.setState({showPassword: !this.state.showPassword})
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.title}>{strings.confirmRegistrationTitle}</Text>
        <Text style={styles.label}>{strings.placeholderName}</Text>
        <Text style={styles.value}>{this.props.email}</Text>
        <Text style={styles.label}>{strings.placeholderPassword}</Text>
        <View style={styles.passwordRow}>
          <View style={{flex: 1}}/>
          <View style={{flex: 4}}>
            <TextInput
                editable={false}
                secureTextEntry={!this.state.showPassword}
                style={styles.textInput}
                value={this.props.password}
              />
          </View>
          <View style={{flex: 1}}>
            <Button
                invisible
                onPress={this.onPressShowPasswordButton}
                style={styles.showButton}
                title={strings.showPasswordTitle}
                titleStyle={styles.showButtonTitle}
              />
          </View>
        </View>
        <Button onPress={this.props.onYesButtonPressed}
            style={styles.yesButton}
            title={strings.yesButtonTitle}
            titleStyle={styles.yesButtonTitle}
            width={100}/>
        <Button
            invisible
            onPress={this.props.onChangeDataPressed}
            style={styles.changeButton}
            title={strings.changeButtonTitle}
            titleStyle={styles.changeButtonTitle}
            width={100}/>
      </View>
    )
  }
}

ConfirmRegistration.propTypes = {
  email: PropTypes.string,
  onChangeDataPressed: PropTypes.func,
  onYesButtonPressed: PropTypes.func,
  password: PropTypes.string,
}
