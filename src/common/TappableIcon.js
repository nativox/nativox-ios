/* @flow */

import React, {PropTypes} from 'react'
import {TouchableOpacity, View} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'

const TappableIcon = ({
  name,
  onPress,
  size,
  style,
}) => (
  <TouchableOpacity
      onPress={onPress}
  >
    <Icon color = 'rgb(255, 255, 255)'
        name={name}
        size={size}
        style={style}
      />
  </TouchableOpacity>
)

export default TappableIcon

TappableIcon.propTypes = {
  name: PropTypes.string,
  onPress: PropTypes.func,
  size: PropTypes.number,
  style: View.propTypes.style,
}
