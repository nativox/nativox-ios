/* @flow */

import Device from 'react-native-device'
import {isSmallDevice} from '../common/DeviceUtility.js'

export const PRIMARY_COLOR = 'rgb(62, 185, 230)'
export const BUTTON_BACKGROUND_DISABLE_COLOR = 'rgba(62, 185, 230, 0.3)'
export const BUTTON_TITLE_COLOR = 'rgb(255, 255, 255)'
export const HEADING_TITLE = 'rgb(43, 152, 215)'
export const FONT_SIZE_TEXT_INPUT = Device.isIpad() ? 24 : isSmallDevice() ? 12 : 16
export const HEIGHT_TEXT_INPUT = isSmallDevice() ? 26 : Device.isIpad() ? 45 : 35
export const EXPLAINATION_LABEL_SIZE = Device.isIpad() ? 19 : 15
