/* @flow */

import React, {PropTypes} from 'react'
import {Image, StyleSheet, View, Modal, TouchableWithoutFeedback} from 'react-native'
import TappableIcon from '../common/TappableIcon'

const styles = StyleSheet.create({
  backgroundContainer: {
    resizeMode: 'stretch',
    flexDirection: 'column',
    flexGrow: 1,
    backgroundColor: 'rgba(255,255,255,0.35)'
  },
  container: {
    flexGrow: 1,
    backgroundColor: 'rgba(0, 0, 0, 0.6)',
    alignItems: 'center',
    justifyContent: 'center',
  },
  closeButton: {
    alignSelf: 'flex-end',
  },
  childrenContainer: {
    padding: 10,
  }
})

const ModalScreen = ({
  children,
  height,
  isVisible,
  onCloseButtonPressed,
  onTouchBackground,
  width,
}) => (
  <Modal
      animationType={'slide'}
      transparent={true}
      visible={isVisible}>
    <TouchableWithoutFeedback
        onPress={onTouchBackground}>
      <View style={styles.container}>
        <TouchableWithoutFeedback>
          <View style={[{height, width}]}>
            <Image source={{uri: 'background_app', isStatic: true}}
                style={[styles.backgroundContainer, {height, width}]}>
                  <View style={styles.childrenContainer}>
                    <View style={{alignItems: 'flex-end'}}>
                      <TappableIcon
                          name='times'
                          onPress={onCloseButtonPressed}
                          size={22}
                          style={styles.closeButton}/>
                    </View>
                      {children}
                  </View>
            </Image>
          </View>
        </TouchableWithoutFeedback>
      </View>
    </TouchableWithoutFeedback>
  </Modal>
)

export default ModalScreen

ModalScreen.propTypes = {
  children: PropTypes.any,
  height: PropTypes.number.isRequired,
  isVisible: PropTypes.bool,
  onCloseButtonPressed: PropTypes.func,
  onTouchBackground: PropTypes.func,
  width: PropTypes.number.isRequired,
}
