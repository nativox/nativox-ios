/* @flow */

import React, {PropTypes} from 'react'
import {Image, StyleSheet, ScrollView, NativeModules, View, Text, findNodeHandle} from 'react-native'
import KeyboardEvents from 'react-native-keyboardevents'
import TappableIcon from './TappableIcon'
const {popScreen} = NativeModules.ReactNativeView
import Device from 'react-native-device'
import {isSmallDevice} from './DeviceUtility.js'

const KeyboardEventEmitter = KeyboardEvents.Emitter
const UIManager = NativeModules.UIManager
const styles = StyleSheet.create({
  backgroundContainer: {
    resizeMode: 'stretch',
    flexDirection: 'column',
    width: null,
    height: null,
    flexGrow: 1,
  },
  topLevelContainer: {
    flexGrow: 1
  },
  container: { //eslint-disable-line
    flexGrow: 1,
    paddingTop: isSmallDevice() ? 3 : 15,
    paddingLeft: 15,
    paddingRight: 15,
    paddingBottom: isSmallDevice() ? 3 : 15,
  },
  scrollContainer: {
  },
  scrollContentContainer: {
    flexGrow: 1,
  },
  title: {
    color: 'white',
    textAlign: 'center',
    fontSize: Device.isIpad() ? 90 : 44,
    fontFamily: 'HelveticaNeue-Thin',
    marginTop: 5,
    marginRight: 30,
    marginBottom: 10,
    flexGrow: 1,
  },
})

const NavigationBar = ({
  showLeftNavBar,
  title,
}) => (
  <View style={{flexDirection: 'row', paddingTop: 17}}>
    {showLeftNavBar ?
      <View style={{alignSelf: 'flex-start'}}>
        <TappableIcon name='angle-left'
            onPress={() => popScreen()}
            size={40}
            style={{padding: 7}}/>
      </View> : null
    }
     {
       title ?
       <Text style={styles.title}>{title}</Text> : null
     }
  </View>
)

NavigationBar.propTypes = {
  showLeftNavBar: PropTypes.bool,
  title: PropTypes.string,
}

export default class Screen extends React.PureComponent {

  constructor(){
    super()

    this.state = {
      keyboardSpace: 0,
    }

    this.updateKeyboardSpace = this.updateKeyboardSpace.bind(this)
    this.resetKeyboardSpace = this.resetKeyboardSpace.bind(this)
  }

  resetKeyboardSpace() {
    if (this.props.focusedField) {
      this.setState({keyboardHeight: 0})
      this.refs.scrollScreenView.scrollTo({x: 0, y: 0, animated: true})
    }
  }

  componentDidMount() {
    KeyboardEventEmitter.on(KeyboardEvents.KeyboardDidShowEvent, this.updateKeyboardSpace)
    KeyboardEventEmitter.on(KeyboardEvents.KeyboardWillHideEvent, this.resetKeyboardSpace)
  }

  componentWillUnmount() {
    KeyboardEventEmitter.off(KeyboardEvents.KeyboardDidShowEvent, this.updateKeyboardSpace)
    KeyboardEventEmitter.off(KeyboardEvents.KeyboardWillHideEvent, this.resetKeyboardSpace)
  }

  updateKeyboardSpace(frames) {
    if (!this.props.focusedField) {
      return
    }

    if (frames.end) {
      this.setState({keyboardHeight: frames.end.height})

      const containerNode = findNodeHandle(this.refs.container)
      const fieldViewNode = findNodeHandle(this.props.focusedField)

      UIManager.measureLayout(fieldViewNode, containerNode, () => {
      }, (fieldX, fieldY) => {
        this.refs.container.measure((x, y, width, height) => {
          if ((height - frames.end.height - 40) < fieldY){
            const moveY = fieldY - (height - frames.end.height) + 60

            this.refs.scrollScreenView.scrollTo({x: 0, y: moveY, animated: true})
          }
        })
      })
    }
  }

  getContainerStyle() {
    if (this.props.fullscreen) {
      return {flex: 1, paddingBottom: 70}
    }

    return this.props.isTabScreen ? [styles.container, {paddingBottom: 70}] : styles.container
  }

  render() {
    return (
      <View style={styles.topLevelContainer}>
        <Image ref='container'
            source={{uri: 'background_app', isStatic: true}}
            style={styles.backgroundContainer}>
             <NavigationBar showLeftNavBar={this.props.showLeftNavBar}
                 title={this.props.title}/>
             <View style={this.getContainerStyle()}>
               <ScrollView contentContainerStyle={styles.scrollContentContainer}
                   contentInset={{bottom: this.state.keyboardSpace, }}
                   keyboardDismissMode='interactive'
                   ref='scrollScreenView'
                   scrollEnabled={false}
                   showsVerticalScrollIndicator
                   style={styles.scrollContainer}>
                    {this.props.children}
               </ScrollView>
             </View>
        </Image>
      </View>
    )
  }
}

Screen.propTypes = {
  children: PropTypes.any,
  focusedField: PropTypes.any,
  fullscreen: PropTypes.bool,
  isTabScreen: PropTypes.bool,
  showLeftNavBar: PropTypes.bool,
  title: PropTypes.string,
}
