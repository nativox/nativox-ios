/* @flow */

import {Dimensions} from 'react-native'

const IPHONE4S_WIDTH = 320
const {width} = Dimensions.get('window')

export const isSmallDevice = () => width <= IPHONE4S_WIDTH
