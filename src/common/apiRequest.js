/* @flow */

import superagent from 'superagent'
import userSession from '../user/userSession'
import Device from 'react-native-device'
import queryString from 'query-string'
import DeviceInfo from 'react-native-device-info'
import {Platform} from 'react-native'

const HOST_NAME = 'https://nativox.com/apis/2.1/'

const handleResponse = (resolve: any, reject: any) => (error, res) => {
  if (error) {
    reject(error)
  }

  if (__DEV__ && res) {
    console.log('Response: ', res.body) // eslint-disable-line
  }

  if(res) {
    resolve(res.body)
  } else {
    reject('Error in Nativox try again.')
  }
}

const getQuery = (parameters: object) => {
  const queryParameters = {
    id_usuario: userSession.getUser().idUser,
    token: userSession.getUser().token,
    lang: userSession.getUser().language,
    os: Platform.OS,
    device: DeviceInfo.getDeviceName(),
    height: Device.height,
    width: Device.width,
    version: DeviceInfo.getVersion(),
    id_session: userSession.getUser().idSession,
    id_device: DeviceInfo.getUniqueID(),
    ...parameters,
  }
  console.log('queryParameters: ', queryParameters) // eslint-disable-line

  return queryParameters
}


export const post = (endPoint, parameters) => {
  const postParameters = {
    id_usuario: userSession.getUser().idUser,
    token: userSession.getUser().token,
    ...parameters,
  }

  return new Promise((resolve, reject) => {
    superagent
    .post(`${HOST_NAME}${endPoint}`)
    .query(getQuery(parameters))
    .send(queryString.stringify(postParameters))
    .set('Content-Type', 'application/x-www-form-urlencoded')
    .set('Accept', 'application/x-www-form-urlencoded')
    .end(handleResponse(resolve, reject))
  })
}

export const get = (endPoint, parameters) => {
  return new Promise((resolve, reject) => {
    superagent
    .get(`${HOST_NAME}${endPoint}`)
    .query(getQuery(parameters))
    .end(handleResponse(resolve, reject))
  })
}
