/* @flow */

import React, {PropTypes} from 'react'
import {Text, View, TouchableOpacity, StyleSheet} from 'react-native'
import {
  PRIMARY_COLOR,
  BUTTON_BACKGROUND_DISABLE_COLOR,
  BUTTON_TITLE_COLOR,
} from './NativoxTheme.js'
import Device from 'react-native-device'

const styles = StyleSheet.create({
  button: {
    justifyContent: 'center',
    alignItems: 'center',
    height: Device.isIpad() ? 60 : 35,
    borderRadius: 5,
  },
})

export default class Button extends React.PureComponent {

  getBackgroundStyle() {
    if (this.props.invisible) {
      return
    }

    return {
      backgroundColor: this.props.disabled ? BUTTON_BACKGROUND_DISABLE_COLOR : PRIMARY_COLOR,
    }
  }

  getStyleTitle() {
    return {
      color: this.props.invisible ? PRIMARY_COLOR : BUTTON_TITLE_COLOR,
      fontSize: Device.isIpad() ? 23 : 19,
    }
  }

  getButtonStyle() {
    return [styles.button, this.getBackgroundStyle(), this.props.style, {width: this.props.width}]
  }

  getTouchableStyle() {
    if (!this.props.width) {
      return
    }
    return {width: this.props.width + 3, alignSelf: 'center'}
  }

  render() {
    return (
      <TouchableOpacity disabled={this.props.disabled}
          onPress={this.props.onPress}
          style={this.getTouchableStyle()}>
        <View style={this.getButtonStyle()}>
            <Text style={[this.getStyleTitle(), this.props.titleStyle]}>{this.props.title}</Text>
        </View>
      </TouchableOpacity>
    )
  }
}

Button.propTypes = {
  disabled: PropTypes.bool,
  invisible: PropTypes.bool,
  onPress: PropTypes.func,
  style: PropTypes.any,
  title: PropTypes.string,
  titleStyle: PropTypes.any,
  width: PropTypes.number,
}
