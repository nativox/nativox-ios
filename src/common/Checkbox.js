/* @flow */

import React, {PropTypes} from 'react'
import {TouchableWithoutFeedback, View, StyleSheet} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'

const styles = StyleSheet.create({
  container: {
    flexGrow: 1
  },
})

const Checkbox = ({
  checked,
  onChange,
  size,
}) => (
  <View style={styles.container}>
    <TouchableWithoutFeedback onPress={onChange}>
      <Icon color="white"
          name={checked ? 'check-square-o' : 'square-o'}
          size={size}/>
    </TouchableWithoutFeedback>
  </View>
)

export default Checkbox

Checkbox.propTypes = {
  checked: PropTypes.bool,
  onChange: PropTypes.func,
  size: PropTypes.number,
}
