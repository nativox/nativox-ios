/* @flow */

import React, {PropTypes} from 'react'
import {StyleSheet, TextInput} from 'react-native'
import {FONT_SIZE_TEXT_INPUT, HEIGHT_TEXT_INPUT} from './NativoxTheme'
import Device from 'react-native-device'

const styles = StyleSheet.create({
  textInput: {
    height: HEIGHT_TEXT_INPUT,
    borderColor: 'rgb(109, 109, 109)',
    borderWidth: 1,
    backgroundColor: 'white',
    marginBottom: Device.isIpad() ? 20 : 10,
    padding: 5,
    fontSize: FONT_SIZE_TEXT_INPUT,
  },
})

class TextBox extends React.PureComponent {
  render() {
    return (
      <TextInput
          autoCapitalize='none'
          autoCorrect={false}
          onChangeText={this.props.onChangeText}
          onFocus={this.props.onRowFocus}
          placeholder={this.props.placeholder}
          secureTextEntry={this.props.isPassword}
          style={[styles.textInput, this.props.style]}
          textAlign={'left'}
          value={this.props.value}
        />
    )
  }
}

export default TextBox

TextBox.propTypes = {
  isPassword: PropTypes.bool,
  onChangeText: PropTypes.func,
  onRowFocus: PropTypes.func,
  placeholder: PropTypes.string,
  style: TextInput.propTypes.style,
  value: PropTypes.string,
}
