/* @flow */

import {Linking} from 'react-native'

export default (appUrl: string, webUrl: string) => {
  Linking.canOpenURL(appUrl).then(supported => {
    if (!supported) {
      return Linking.openURL(webUrl)
    }

    return Linking.openURL(appUrl)
  })
}
