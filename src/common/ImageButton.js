/* @flow */

import React, {PropTypes} from 'react'
import {View, Image, TouchableOpacity, StyleSheet} from 'react-native'

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    resizeMode: 'stretch',
  }
})

export default class ImageButton extends React.PureComponent {
  render() {
    return (
      <TouchableOpacity onPress={this.props.onPress}>
        <View style={[styles.container, this.props.style]}>
          <Image source={{uri: this.props.image, isStatic: true}}
              style={[styles.image, {width: this.props.width, height: this.props.height}]}/>
        </View>
      </TouchableOpacity>
    )
  }
}

ImageButton.propTypes = {
  height: PropTypes.number,
  image: PropTypes.string,
  onPress: PropTypes.func,
  style: View.propTypes.style,
  width: PropTypes.number,
}
