/* @flow */

import React, {PropTypes} from 'react'
import {Text, View, Image, NativeModules, StyleSheet} from 'react-native'
import Button from '../Button'
import Device from 'react-native-device'

const AlertDialogView = NativeModules.AlertDialogView

const styles = StyleSheet.create({
  background: {
    flexGrow: 1,
    resizeMode: 'stretch',
    width: null,
    height: null,
    flexDirection: 'column',
    borderRadius: 7
  },
  backgroundContainer: {
    flexGrow: 1,
    justifyContent: 'center',
    flexDirection: 'column',
    alignItems: 'center',
    borderRadius: 7,
    backgroundColor: 'rgba(255,255,255,0.10)'
  },
  messageContainer: {
    flexGrow: 1,
    justifyContent: 'center',
    alignItems: 'center',
    padding:5
  },
  message: {
    backgroundColor: 'transparent',
    color: 'white',
    fontSize: 17,
    textAlign: 'center',
  },
  button: {
    marginBottom: 15,
    height: Device.isIpad() ? 40 : 35
  },
})

export default class AlertDialog extends React.PureComponent {

  constructor() {
    super()
    this.onOkButtonPressed = this.onOkButtonPressed.bind(this)
  }

  onOkButtonPressed(){
    AlertDialogView.closeDialog()
  }

  render() {
    return (
      <Image source={{uri: 'background_app', isStatic: true}}
          style={styles.background}>
        <View style={styles.backgroundContainer}>
          <View style={styles.messageContainer}>
            <Text style={styles.message}>{this.props.message}</Text>
          </View>
          <Button onPress={this.onOkButtonPressed}
              style={styles.button}
              title='OK'
              width={100}/>
        </View>
      </Image>
    )
  }
}

AlertDialog.propTypes = {
  message: PropTypes.string.isRequired
}
