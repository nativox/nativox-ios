/* @flow */

import {onLoadCategories} from './eventHandlers'
import {Answers} from 'react-native-fabric'
import {AsyncStorage, Alert} from 'react-native'
import {post} from '../common/apiRequest'
import {strings} from '../stringsApp'

export const retrievePendingPurchases = async () => {
  try {
    const pendingPurchases = await AsyncStorage.getItem('pendingPurchases')

    if (pendingPurchases) {
      return JSON.parse(pendingPurchases)
    }

    return  []
  } catch (error) {
    return  []
  }
}

export const savePendingPurchase = async (checkoutParams: object) => {
  const pendingPurchases = await retrievePendingPurchases()

  AsyncStorage.setItem('pendingPurchases', JSON.stringify([...pendingPurchases, checkoutParams]))
}

export const sendPurchase = async (params: object, dispatch: func) => {
  try {
    const {status} = await post('pedido_completado', params)

    if (status === 'true') {
      Answers.logPurchase(params.total, params.moneda, true, params.nombre, 'Curso', params.in_app_purchase_id)
      dispatch(onLoadCategories())
      return true
    }

    return false
  } catch (error) {
    return false
  }
}

const sendPendingPurchase = async (pendingPurchases: Array<object>, purchase: object, dispatch: func) => {
  if (purchase) {
    const result = await sendPurchase(purchase, dispatch)

    if (!result) {
      Alert.alert('Nativox', strings.pendingPurchaseError)

      return
    }

    pendingPurchases.shift()
    AsyncStorage.setItem('pendingPurchases', JSON.stringify(pendingPurchases))

    if (pendingPurchases.length > 0) {
      const pendingPurchase = pendingPurchases[0]

      await sendPendingPurchase(pendingPurchases, pendingPurchase, dispatch)
    }
  }
}

export const sendPendingPurchases = async (dispatch: func) => {
  const pendingPurchases = await retrievePendingPurchases()

  if (pendingPurchases.length > 0) {
    const pendingPurchase = pendingPurchases[0]

    await sendPendingPurchase(pendingPurchases.splice(0, 1), pendingPurchase, dispatch)
  }
}
