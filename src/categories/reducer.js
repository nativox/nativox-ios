/* @flow */

import {handleActions} from 'redux-actions'
import immutable from 'immutable'
import {
  clearCategories,
  hydrateCategories,
  startHydratingCategories,
  finishHydratingCategories,
} from './actionCreators'

export default handleActions({
  [hydrateCategories]: (state, action) => state.set('categories', action.payload),
  [startHydratingCategories]: (state) => state.set('isHydrating', true),
  [finishHydratingCategories]: (state) => state.set('isHydrating', false),
  [clearCategories]: (state) => state.set('categories', immutable.List())
}, immutable.fromJS({
  categories: [],
  isHydrating: false,
}))
