/* @flow */

import Device from 'react-native-device'
import React, {PropTypes} from 'react'
import {
  StyleSheet,
  View,
  Text,
  Image,
  TouchableOpacity
} from 'react-native'

const styles = StyleSheet.create({
  categorieRow: {
    flex: 3,
    justifyContent: 'center',
    borderRadius: 5,
    margin: 3,
  },
  superPackRow: {
    flex: 2,
    flexDirection: 'row',
    justifyContent: 'center',
    borderRadius: 5,
    margin: 3,
    backgroundColor: 'rgb(56, 56, 56)',
  },
  categoriesListContainer: {
    flex: 1,
  },
  categorieImage: {
    flex: 1,
  },
  categorieLabelContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  categorieLabel: {
    fontSize: Device.isIpad() ? 22: 16,
    color: 'rgb(255, 255, 255)',
  },
})

const CategoryRow = ({
  category,
  onPressCategory,
}) => {
  return (<TouchableOpacity
      onPress={() => onPressCategory(category)}
      style={[styles.categorieRow, {backgroundColor: category.get('color_hex')}]}
  >
    <View style={{flex: 4, padding: 10}}>
      <Image
          resizeMode='contain'
          source={{uri: category.get('icono_url')}}
          style={styles.categorieImage}
      />
    </View>
    <View style={styles.categorieLabelContainer}>
      <Text style={styles.categorieLabel}>{category.get('categoria')}</Text>
    </View>
  </TouchableOpacity>)
}

CategoryRow.propTypes = {
  category: PropTypes.object,
  onPressCategory: PropTypes.func,
}

const SuperPackText = ({
  text,
}) => (
  <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
    <Text style={{color: 'rgb(62, 169, 222)', fontSize: 20, fontWeight: 'bold'}}>{text}</Text>
  </View>
)

SuperPackText.propTypes = {
  text: PropTypes.string,
}

const SuperPackRow = ({
  category,
  onPressCategory,
}) => (
  <TouchableOpacity
      onPress={() => onPressCategory(category)}
      style={styles.superPackRow}
  >
    <SuperPackText text='Super'/>
    <Image
        resizeMode='contain'
        source={{uri: 'icon_superpack', isStatic: true}}
        style={styles.categorieImage}
    />
    <SuperPackText text='Pack'/>
  </TouchableOpacity>
)

SuperPackRow.propTypes = {
  category: PropTypes.object,
  onPressCategory: PropTypes.func,
}

const CategoriesList = ({
  categories,
  onPressCategory,
}) => (
  <View style={styles.categoriesListContainer}>
    {
      categories.map((category: object, index: number) => {
        if (category.get('id_in_app_ios') === 'id_super_pack') {
          return (
            <SuperPackRow
                category={category}
                key={index}
                onPressCategory={onPressCategory}/>
          )
        }

        return (
          <CategoryRow
              category={category}
              key={index}
              onPressCategory={onPressCategory}
          />
        )
      })
    }
  </View>
)

CategoriesList.propTypes = {
  categories: PropTypes.object,
  hasBoughtSuperPack: PropTypes.bool,
  onPressCategory: PropTypes.func,
}

export default CategoriesList
