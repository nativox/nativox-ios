/* @flow */

import React, {PropTypes} from 'react'
import {
  ActivityIndicator,
  Linking,
  Modal,
  NativeModules,
  PanResponder,
  StyleSheet,
  View,
  TouchableOpacity,
} from 'react-native'
import Screen from '../common/Screen'
import {connect} from 'react-redux'
import CategoriesList from './CategoriesList'
import WebViewBridge from 'react-native-webview-bridge'
import {onCategoryPurchased, onCategoriesScreenAppear} from './eventHandlers'

const {postNotification} = NativeModules.ReactNativeView
const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingBottom: 5,
  },
})

const ActivityIndicatorView = () => (
  <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
    <ActivityIndicator
        animating
        size='large'
        style={{height: 80}}
    />
  </View>
)

const injectScriptPack = `
$('.go_to_comprar').click(function(){
	WebViewBridge.send('goToBuy:'+id_categoria);

	return false;
});

$('.go_to_cerrar').click(function(){
	WebViewBridge.send('goToClose:'+id_categoria);

	return false;
});

$('.go_to_more_info').click(function(){
	WebViewBridge.send('goToMoreInfo:'+'http://nativox.com/ini/app_info_pack/' + id_categoria);

	return false;
})
`
const injectScriptSuperPack = `
$('.go_to_comprar').click(function(){
	WebViewBridge.send('goToBuy:'+'superpack');
	return false;
});

$('.go_to_cerrar').click(function(){
	WebViewBridge.send('goToClose:'+'superpack');
	return false;
});

$('.go_to_more_info').click(function(){
	WebViewBridge.send('goToMoreInfo:'+'http://nativox.com/ini/app_info_superpack/');

	return false;
})
`

class CategoriesScreen extends React.PureComponent {

  constructor() {
    super()

    this.onPressCategory = this.onPressCategory.bind(this)
    this.onClosePurchasePopup = this.onClosePurchasePopup.bind(this)
    this.onPressBuyCategory = this.onPressBuyCategory.bind(this)
    this.onBridgeMessage = this.onBridgeMessage.bind(this)

    this.state = {
      categoriePopupVisible: false,
      categoryUrl : null,
      injectScript: null,
    }
  }

  componentWillMount() {
    this._panResponder = PanResponder.create({
      onStartShouldSetPanResponder: () => true,
      onStartShouldSetPanResponderCapture: () => true,
      onMoveShouldSetPanResponder: () => true,
      onMoveShouldSetPanResponderCapture: () => true
    })
  }

  componentDidMount() {
    this.props.onCategoriesScreenAppear()
  }

  onPressCategory(category) {
    if (!category.get('comprado')) {
      let injectScript = null

      if (category.get('id_in_app_ios') === 'id_super_pack') {
        injectScript = injectScriptSuperPack
      } else {
        injectScript = injectScriptPack
      }

      this.setState({
        categoriePopupVisible: true,
        categoryUrl : category.get('categoryUrl'),
        injectScript,
      })
    } else {
      postNotification('OnCategorySelected', category.toJS())
    }
  }

  onClosePurchasePopup() {
    this.setState({
      categoriePopupVisible: false,
      categoryUrl: null,
    })
  }

  onPressBuyCategory(categoryId: string) {
    const category = this.props.categories.find((category) => category.get('id_categoria') === categoryId)

    this.setState({
      categoriePopupVisible: false,
      categoryUrl: null,
    })
    this.props.onCategoryPurchased(category)
  }

  onBridgeMessage(message: string) {
    const datas = message.split(':')
    const option = datas[0]

    switch (option) {
    case 'goToBuy':
      this.onPressBuyCategory(message.split(`${option}:`)[1])
      break
    case 'goToClose':
      this.onClosePurchasePopup()
      break
    case 'goToMoreInfo':
      Linking.openURL(message.split(`${option}:`)[1])
      break
    }
  }

  render() {
    return (
        <Screen
            isTabScreen
            title={'Courses'}
          >
            <Modal
                animationType={'slide'}
                style={{flex: 1}}
                transparent
                visible={this.state.categoriePopupVisible}
            >
              <TouchableOpacity
                  activeOpacity={1}
                  onPress={this.onClosePurchasePopup}
                  style={{flex: 1, padding: 40, backgroundColor: 'rgba(0, 0, 0, 0.8)'}}
              >
                <WebViewBridge
                    {...this._panResponder.panHandlers}
                    injectedJavaScript={this.state.injectScript}
                    onBridgeMessage={this.onBridgeMessage}
                    renderLoading={() => <ActivityIndicatorView />}
                    source={{uri: this.state.categoryUrl}}
                    startInLoadingState
                    style={{flex: 1, backgroundColor: 'transparent'}}
                />
              </TouchableOpacity>
            </Modal>
            <View style={styles.container}>
              {
                this.props.isHydrating ?
                  <ActivityIndicatorView /> :
                  <CategoriesList
                      categories={this.props.categories}
                      onPressCategory={this.onPressCategory}
                  />
              }
            </View>
        </Screen>
    )
  }
}

CategoriesScreen.propTypes = {
  categories: PropTypes.object,
  isHydrating: PropTypes.bool,
  onCategoriesScreenAppear: PropTypes.func,
  onCategoryPurchased: PropTypes.func,
}

export default connect(state => ({
  isHydrating: state.categories.get('isHydrating'),
  categories: state.categories.get('categories'),
}), (dispatch) => ({
  onCategoryPurchased: (category) => dispatch(onCategoryPurchased(category)),
  onCategoriesScreenAppear: () => dispatch(onCategoriesScreenAppear())
}))(CategoriesScreen)
