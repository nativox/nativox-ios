/* @flow */

import {getCategories, removeCategories} from './CategoriesRepository'
import {
  clearCategories,
  hydrateCategories,
  startHydratingCategories,
  finishHydratingCategories,
} from './actionCreators'
import {NativeModules, Alert} from 'react-native'
import immutable from 'immutable'
import DeviceInfo from 'react-native-device-info'
import userSession from '../user/userSession'
import {savePendingPurchase, sendPurchase} from './PurchasesRepository'
import {get} from '../common/apiRequest'
import {strings} from '../stringsApp'

const ProgressDialogModule = NativeModules.ProgressDialogModule
const {InAppUtils} = NativeModules
const {postNotification} = NativeModules.ReactNativeView

export const onLoadCategories = () => async (dispatch) => {
  dispatch(startHydratingCategories())

  let categories = await getCategories()
  const boughtCategories = categories.filter((categorie) => categorie.get('comprado'))

  if (boughtCategories.size !== categories.size) {
    categories = categories.push(immutable.Map({
      id_in_app_ios: 'id_super_pack',
      id_categoria: 'superpack',
    }))
  }

  const productIds = categories
  .map((category) => category.get('id_in_app_ios'))
  .filter((id) => id).toJS()

  InAppUtils.loadProducts(productIds, (error, products) => {
    if (error) {
      dispatch(hydrateCategories(categories))
      dispatch(finishHydratingCategories())

      return
    }

    const newCategories = categories.map((category) => {
      const indexProduct = productIds.indexOf(category.get('id_in_app_ios'))

      if (indexProduct === -1) {
        return category
      }

      const product = products[indexProduct]

      return category.withMutations((newCategory) => {
        if (category.get('id_in_app_ios') === 'id_super_pack') {
          newCategory.set('categoria', 'Superpack')
          newCategory.set('categoryUrl', `http://nativox.com/ini/app_popup_superpack?os=ios&moneda=${product.currencyCode}&precio=${product.price}`)
        } else {
          newCategory.set('categoryUrl', `http://nativox.com/ini/app_popup_pack/${category.get('id_categoria')}?os=ios&moneda=${product.currencyCode}&precio=${product.price}`)
        }

        newCategory.set('total', product.price)
        newCategory.set('currency', product.currencyCode)
      })
    })

    postNotification('OnCategoriesLoaded', {categories: newCategories.toJS()})
    dispatch(hydrateCategories(newCategories))
    dispatch(finishHydratingCategories())
  })
}

export const onClearCategories = () => async (dispatch) => {
  await removeCategories()
  dispatch(clearCategories())
}

export const onCategoryPurchased = (category) => async (dispatch) => {
  const productId = category.get('id_in_app_ios')

  ProgressDialogModule.showMessage('Purchasing')
  InAppUtils.purchaseProduct(productId, async (error, response) => {
    if (error) {
      ProgressDialogModule.dismissMessage()
      Alert.alert('Error', error.message)

      return
    }

    if (response && response.productIdentifier) {
      const cursos = category.get('cursos').map((curso) => curso.get('id_curso')).toJS()
      const params = {
        total: category.get('total'),
        lang: userSession.getUser().language,
        moneda: category.get('currency'),
        transactionId: response.transactionIdentifier,
        receipt: response.transactionReceipt,
        in_app_purchase_id: category.get('id_in_app_ios'),
        bundle_id: DeviceInfo.getBundleId(),
        cursos,
        sandbox: 'false',
        nombre: category.get('categoria')
      }
      const result =  await sendPurchase(params, dispatch)

      ProgressDialogModule.dismissMessage()
      if (!result) {
        Alert.alert('Nativox', strings.purchaseError)
        savePendingPurchase(params)
      }
    } else {
      ProgressDialogModule.dismissMessage()
      Alert.alert('Nativox', strings.purchaseError)
    }
  })
}

export const onCategoriesScreenAppear = () => async (dispatch) => {
  const {status} = await get('test_token', {})

  if (status !== 'true') {
    dispatch(onClearCategories())
    userSession.resetUser()
    postNotification('OnSessionExpired', {})
  }
}
