/* @flow */


import {createAction} from 'redux-actions'

const HYDRATE_CATEGORIES = 'HYDRATE_CATEGORIES'
const CLEAR_CATEGORIES = 'CLEAR_CATEGORIES'
const START_HYDRATING_CATEGORIES = 'START_HYDRATING_CATEGORIES'
const FINISH_HYDRATING_CATEGORIES = 'FINISH_HYDRATING_CATEGORIES'

export const hydrateCategories = createAction(HYDRATE_CATEGORIES)
export const startHydratingCategories = createAction(START_HYDRATING_CATEGORIES)
export const finishHydratingCategories = createAction(FINISH_HYDRATING_CATEGORIES)
export const clearCategories = createAction(CLEAR_CATEGORIES)
