/* @flow */

import {get} from '../common/apiRequest'
import immutable from 'immutable'
import transit from 'transit-immutable-js'
import {AsyncStorage} from 'react-native'

export const removeCategories = () => AsyncStorage.removeItem('categories')

export const getOfflineCategories = async () => {
  const categoriesString = await AsyncStorage.getItem('categories')
  if (!categoriesString) {
    return immutable.List()
  }

  return transit.fromJSON(categoriesString)
}

export const getCategories = async () => {
  try {
    const offlineCategories = await getOfflineCategories()
    const {status, result} = await get('categorias', {incluir_cursos: 1})
    const categories = immutable.fromJS(result)

    if (status !== 'true') {
      return immutable.List()
    }

    if (immutable.is(offlineCategories, categories)) {
      return offlineCategories
    }

    await AsyncStorage.setItem('categories', transit.toJSON(categories))

    return categories
  } catch (error) {
    console.log('error: ', error) // eslint-disable-line
    return immutable.List()
  }
}
