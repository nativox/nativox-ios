/* @flow */

import App from './src/App'
import React from 'react-native'
import AlertDialog from './src/common/dialogs/AlertDialog'
import UserFeedback from './src/lesson/userFeedback/UserFeedback'

React.AppRegistry.registerComponent('AlertDialog', () => AlertDialog)
React.AppRegistry.registerComponent('UserFeedback', () => UserFeedback)
React.AppRegistry.registerComponent('NativoxApp', () => App)
