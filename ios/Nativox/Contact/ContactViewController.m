#import "ContactViewController.h"
#import "AlertDialogView.h"
#import "ReactNativeView.h"

@interface ContactViewController ()

@property (strong, nonatomic) IBOutlet ReactNativeView *reactNativeView;

@end

@implementation ContactViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
    [self.reactNativeView initializeReactView:@"ContactScreen"
                               viewController:self
                                   andOptions:nil];
}

@end
