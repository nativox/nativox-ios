//
// Created by redhair84 on 20/05/15.
// Copyright (c) 2015 Francisco Javier Morant. All rights reserved.
//

#import "DeviceUtils.h"


@interface DeviceUtils ()

@property UIDevice *device;

@end

@implementation DeviceUtils {

}

- (instancetype)initWithDevice:(UIDevice *)device {
    self = [super init];
    if (self) {
        _device = device;
    }

    return self;
}

+ (DeviceUtils *)instance {
    static DeviceUtils *_instance = nil;

    @synchronized (self) {
        if (_instance == nil) {
            _instance = [[self alloc] init];
            _instance.device = [UIDevice currentDevice];
        }
    }

    return _instance;
}

- (BOOL)isOnIpad {
    return self.device.userInterfaceIdiom == UIUserInterfaceIdiomPad;
}

@end