//
// Created by redhair84 on 10/04/2014.
// Copyright (c) 2014 Francisco Javier Morant. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Reachability.h"


@interface NetworkUtil : NSObject

- (NetworkStatus)getNetworkStatus;

- (BOOL)isReachable;

+ (NetworkUtil *)instance;


@end