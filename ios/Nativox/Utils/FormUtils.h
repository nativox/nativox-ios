//
//  FormUtils.h
//  Nativox
//
//  Created by redhair84 on 11/06/2014.
//  Copyright (c) 2014 Francisco Javier Morant. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FormUtils : NSObject

+(BOOL) isEmailValid:(NSString *)email;

@end
