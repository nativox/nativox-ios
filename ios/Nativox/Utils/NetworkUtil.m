//
// Created by redhair84 on 10/04/2014.
// Copyright (c) 2014 Francisco Javier Morant. All rights reserved.
//

#import "NetworkUtil.h"


@interface NetworkUtil ()

@property(strong) Reachability *reachability;

@end

@implementation NetworkUtil {

}

+ (NetworkUtil *)instance {
    static NetworkUtil *networkUtil = nil;

    @synchronized (self) {
        if (networkUtil == nil) {
            networkUtil = [[self alloc] init];
            networkUtil.reachability = [Reachability reachabilityForInternetConnection];
            [networkUtil.reachability startNotifier];
        }
    }

    return networkUtil;
}

- (BOOL)isReachable {
    return [self getNetworkStatus] == kReachableViaWiFi || [self getNetworkStatus] == kReachableViaWWAN;
}

- (NetworkStatus)getNetworkStatus {
    return [self.reachability currentReachabilityStatus];
}

@end