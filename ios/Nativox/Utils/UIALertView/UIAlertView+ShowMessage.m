//
// Created by redhair84 on 08/02/2014.
// Copyright (c) 2014 Francisco Javier Morant. All rights reserved.
//

#import "UIAlertView+ShowMessage.h"


@implementation UIAlertView (ShowMessage)

+ (void)showMessage:(NSString *)text withTitle:(NSString *)title withButtonOK:(NSString *)btnOK andDelegate:(id)delegate {
    UIAlertView *errorMessage = [[UIAlertView alloc] initWithTitle:title
                                                           message:text
                                                          delegate:delegate
                                                 cancelButtonTitle:btnOK
                                                 otherButtonTitles:nil];
    [errorMessage show];
}
@end