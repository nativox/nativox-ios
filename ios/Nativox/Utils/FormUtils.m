//
//  FormUtils.m
//  Nativox
//
//  Created by redhair84 on 11/06/2014.
//  Copyright (c) 2014 Francisco Javier Morant. All rights reserved.
//

#import "FormUtils.h"

@implementation FormUtils

+ (BOOL)isEmailValid:(NSString *)email {
    if ([email isEqualToString:@""]) {
        return NO;
    }
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

@end
