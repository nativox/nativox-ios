//
// Created by redhair84 on 20/05/15.
// Copyright (c) 2015 Francisco Javier Morant. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface DeviceUtils : NSObject
- (instancetype)initWithDevice:(UIDevice *)device;

+ (DeviceUtils *)instance;


- (BOOL)isOnIpad;

@end