//
// Created by redhair84 on 08/02/2014.
// Copyright (c) 2014 Francisco Javier Morant. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIViewController (TopMostController)

+ (UIViewController*) topMostController;

@end