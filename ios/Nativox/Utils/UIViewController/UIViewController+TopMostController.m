//
// Created by redhair84 on 08/02/2014.
// Copyright (c) 2014 Francisco Javier Morant. All rights reserved.
//

#import "UIViewController+TopMostController.h"


@implementation UIViewController (TopMostController)
+ (UIViewController *)topMostController {
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;

    while (topController.presentedViewController) {
        topController = topController.presentedViewController;
    }

    return topController;
}

@end