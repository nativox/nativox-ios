//
// Created by redhair84 on 14/10/2015.
// Copyright (c) 2015 Francisco Javier Morant. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIFont (DahDit)

- (CGFloat)ddLineHeight;
@end