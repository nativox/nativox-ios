//
// Created by redhair84 on 14/10/2015.
// Copyright (c) 2015 Francisco Javier Morant. All rights reserved.
//

#import "UIFont+DahDit.h"


@implementation UIFont (DahDit)

- (CGFloat)ddLineHeight
{
    if ( [self respondsToSelector:@selector(lineHeight)] )
        return self.lineHeight;
    return self.leading;
}

@end