//
// Created by redhair84 on 22/08/2014.
// Copyright (c) 2014 Francisco Javier Morant. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    NOT_PAID, PENDING, PAID
} HistoricalSaleStatusType;

@interface HistoricalSale : NSObject
@property(strong, nonatomic) NSString *idUser;
@property(strong, nonatomic) NSString *idOrder;
@property(strong, nonatomic) NSString *productDescription;
@property(strong, nonatomic) NSString *total;
@property(strong, nonatomic) NSString *method;
@property(strong, nonatomic) NSDate *date;
@property(strong, nonatomic) NSArray *courses;
@property(nonatomic) HistoricalSaleStatusType historicalSaleStatusType;

+ (NSArray *) fromDictionary:(NSDictionary *)dictionary;

@end