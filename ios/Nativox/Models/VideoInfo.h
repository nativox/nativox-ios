//
//  VideoInfo.h
//  Nativox
//
//  Created by redhair84 on 09/03/2014.
//  Copyright (c) 2014 Francisco Javier Morant. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface VideoInfo : NSManagedObject

@property (nonatomic, retain) NSString *pathAudio;
@property (nonatomic, retain) NSString *pathVideo;
@property (nonatomic, retain) NSString *id;
@property (nonatomic, retain) NSString * url;
@property (nonatomic, retain) NSNumber * acceso;

@end
