
#import "BooleanConverter.h"
#import "DCDynamicAttribute.h"


@implementation BooleanConverter

- (id)transformValue:(id)value
 forDynamicAttribute:(DCDynamicAttribute *)attribute
          dictionary:(NSDictionary *)dictionary
        parentObject:(id)parentObject {
    return @([dictionary[self.mapKeyPath] boolValue]);
}

- (id)serializeValue:(id)value forDynamicAttribute:(DCDynamicAttribute *)attribute {
    return nil;
}

- (BOOL)canTransformValueForClass:(Class)class {
    return YES;
}

@end