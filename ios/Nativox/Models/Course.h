#import <Foundation/Foundation.h>

@class CategoryCourse;
@class CourseStats;

@interface Course : NSObject

@property(nonatomic, strong) NSString *idCourse;
@property(nonatomic, strong) NSString *inAppId;
@property(nonatomic, strong) NSString *name;
@property(nonatomic, strong) NSString *descripcion;
@property(nonatomic) float precio;
@property(nonatomic) float precioCompra;
@property(nonatomic) BOOL purchased;
@property(nonatomic) NSString *demo;
@property(nonatomic) BOOL completado;
@property(nonatomic) BOOL activo;
@property(nonatomic) BOOL isGift;
@property(nonatomic) int numLogins;
@property(nonatomic) int numCompras;
@property(nonatomic, strong) NSArray *secciones;
@property(nonatomic, strong) NSDate *fechaComprado;
@property(nonatomic, strong) NSString *donde;
@property(nonatomic, strong) NSString *idCategory;
@property(nonatomic, strong) NSString *idPedido;
@property(nonatomic, strong) NSString *orden;
@property(nonatomic, strong) NSString *icono;
@property(nonatomic, strong) NSString *color;
@property(nonatomic, strong) CategoryCourse *categoria;
@property(nonatomic, strong) CourseStats *courseStats;

@end