//
// Created by redhair84 on 12/08/2014.
// Copyright (c) 2014 Francisco Javier Morant. All rights reserved.
//

#import "PackCourse.h"

@interface PackCourse ()

@property(nonatomic, strong) NSMutableArray *courses;

@end

@implementation PackCourse {

}

- (id)init {
    self = [super init];
    if (self) {
        _courses = [[NSMutableArray alloc] init];
    }

    return self;
}


- (NSArray *)coursesPack {
    return self.courses;
}

- (void)addCourses:(NSArray *)courses {
    [self.courses addObjectsFromArray:courses];
}


@end