//
// Created by redhair84 on 12/08/2014.
// Copyright (c) 2014 Francisco Javier Morant. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Course.h"


@interface PackCourse : Course

@property(nonatomic) BOOL isSuperPack;

- (NSArray *)coursesPack;

- (void)addCourses:(NSArray *)courses;

@end