@class DCParserConfiguration;
@class PackCourse;
@class Course;

@interface CategoryCourse : NSObject

@property(nonatomic, strong) NSString *idCategory;
@property(nonatomic, strong) NSString *name;
@property(nonatomic, strong) NSString *colorHex;
@property(nonatomic, strong) NSString *iconoUrl;
@property(nonatomic, strong) NSArray *cursos;
@property(nonatomic, strong) NSString *inAppId;
@property(nonatomic, strong) NSString *content;
@property(nonatomic) BOOL purchased;

+ (CategoryCourse *)categoryFromDictionary:(NSDictionary *)dictionary andConfiguration:(DCParserConfiguration *)configuration;

@end