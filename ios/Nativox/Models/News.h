//
// Created by redhair84 on 15/08/2014.
// Copyright (c) 2014 Francisco Javier Morant. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface News : NSObject

@property(nonatomic, strong) NSString *idNews;
@property(nonatomic, strong) NSString *title;
@property(nonatomic, strong) NSString *detailed;
@property(nonatomic, strong) NSString *summary;
@property(nonatomic, strong) NSString *image;
@property(nonatomic, strong) NSDate *date;

+ (instancetype)newsFromDictionary:(NSDictionary *)dictionary;

@end