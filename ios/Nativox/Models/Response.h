#import <Foundation/Foundation.h>

@interface Response : NSObject

@property(nonatomic, strong) id result;
@property(nonatomic, strong) NSString *status;


- (instancetype)initWithResult:(id)result status:(NSString *)status;

+ (instancetype)responseWithResult:(id)result status:(NSString *)status;


@end
