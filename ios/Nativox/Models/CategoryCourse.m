//
// Created by redhair84 on 16/01/2014.
// Copyright (c) 2014 Francisco Javier Morant. All rights reserved.
//

#import "CategoryCourse.h"
#import "Course.h"
#import "DCParserConfiguration.h"
#import "DCKeyValueObjectMapping.h"
#import "PackCourse.h"
#import "NativoxIAHelper.h"
#import "UserSession.h"

@implementation CategoryCourse


+ (CategoryCourse *)categoryFromDictionary:(NSDictionary *)dictionary
                          andConfiguration:(DCParserConfiguration *)configuration {
    DCKeyValueObjectMapping *parser = [DCKeyValueObjectMapping mapperForClass:[CategoryCourse class]
                                                             andConfiguration:configuration];
    CategoryCourse *category = [parser parseDictionary:dictionary];

    for (Course *course in category.cursos) {
        [self injectEntitiesInCourse:category course:course];
    }

    return category;
}

+ (void)injectEntitiesInCourse:(CategoryCourse *)category course:(Course *)course {
//    course.categoria = category;
//    for (Section *section in course.secciones) {
//        [self injectEntitiesInSubSeccion:course seccion:section];
//    }
}

+ (void)injectEntitiesInSubSeccion:(NSDictionary *)course seccion:(NSDictionary *)section {
//    for (Subsection *subsection in section.subsecciones) {
//        subsection.seccion = section;
//        [self injectEntitiesInVideo:course subseccion:subsection];
//    }
}

+ (void)injectEntitiesInVideo:(NSDictionary *)course subseccion:(NSDictionary *)subsection {
//    for (Video *video in subsection.videos) {
//        video.curso = course;
//        video.subseccion = subsection;
//    }
}

@end
