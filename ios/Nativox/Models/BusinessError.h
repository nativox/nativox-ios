//
// Created by redhair84 on 19/01/2014.
// Copyright (c) 2014 Francisco Javier Morant. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface BusinessError : NSObject
@property(nonatomic, strong) NSString *message;
@property(nonatomic, strong) NSError *error;

+ (instancetype)withError:(NSError *)error message:(NSString *)message;

@end