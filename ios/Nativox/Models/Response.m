
#import "Response.h"
@implementation Response {

        @private
        id _result;
        NSString *_status;
}


@synthesize result = _result;
@synthesize status = _status;

- (instancetype)initWithResult:(id)result status:(NSString *)status {
        self = [super init];
        if (self) {
                self.result = result;
                self.status = status;
        }

        return self;
}

+ (instancetype)responseWithResult:(id)result status:(NSString *)status {
        return [[self alloc] initWithResult:result status:status];
}

@end
