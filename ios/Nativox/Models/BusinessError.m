//
// Created by redhair84 on 19/01/2014.
// Copyright (c) 2014 Francisco Javier Morant. All rights reserved.
//

#import "BusinessError.h"


@implementation BusinessError {

@private
    NSError *_error;
    NSString *_message;
}
@synthesize error = _error;
@synthesize message = _message;

- (instancetype)initWithError:(NSError *)error message:(NSString *)message {
    self = [super init];
    if (self) {
        _error = error;
        _message = message;
    }

    return self;
}

+ (instancetype)withError:(NSError *)error message:(NSString *)message {
    return [[self alloc] initWithError:error message:message];
}

@end