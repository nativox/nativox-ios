//
// Created by redhair84 on 15/08/2014.
// Copyright (c) 2014 Francisco Javier Morant. All rights reserved.
//

#import "News.h"


@implementation News {

}
+ (instancetype)newsFromDictionary:(NSDictionary *)dictionary {
    News *news = [[News alloc] init];

    news.idNews = dictionary[@"id_noticia"];
    news.title = dictionary[@"titulo"];
    news.detailed = dictionary[@"texto"];
    news.summary = dictionary[@"resumen"];
    news.image = dictionary[@"imagen"];

    return news;
}

@end