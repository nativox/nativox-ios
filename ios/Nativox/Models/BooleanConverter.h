//
// Created by redhair84 on 22/11/14.
// Copyright (c) 2014 Francisco Javier Morant. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DCValueConverter.h"


@interface BooleanConverter : NSObject <DCValueConverter>
@property(nonatomic, weak) NSString *mapKeyPath;
@end