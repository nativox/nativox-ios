//
//  VideoInfo.m
//  Nativox
//
//  Created by redhair84 on 09/03/2014.
//  Copyright (c) 2014 Francisco Javier Morant. All rights reserved.
//

#import "VideoInfo.h"


@implementation VideoInfo

@dynamic pathAudio;
@dynamic pathVideo;
@dynamic id;
@dynamic url;
@dynamic acceso;

@end
