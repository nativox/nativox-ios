//
// Created by redhair84 on 05/02/2014.
// Copyright (c) 2014 Francisco Javier Morant. All rights reserved.
//

#import "User.h"


@implementation User


- (instancetype)initWithToken:(NSString *)token idUser:(NSString *)idUser name:(NSString *)name {
    self = [super init];
    if (self) {
        self.token = token;
        self.idUser = idUser;
        self.name = name;
    }

    return self;
}

+ (instancetype)sessionWithToken:(NSString *)token idUser:(NSString *)idUser name:(NSString *)name {
    return [[self alloc] initWithToken:token idUser:idUser name:name];
}

+ (User *)shareInstance {
    static User *_instance = nil;

    @synchronized (self) {
        if (_instance == nil) {
            _instance = [[self alloc] init];
        }
    }

    return _instance;
}


@end