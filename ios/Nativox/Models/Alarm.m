//
// Created by redhair84 on 04/07/15.
// Copyright (c) 2015 Francisco Javier Morant. All rights reserved.
//

#import "Alarm.h"


@implementation Alarm {

}
- (void)encodeWithCoder:(NSCoder *)coder {
    [coder encodeInteger:self.day forKey:@"day"];
    [coder encodeInteger:self.hour forKey:@"hour"];
    [coder encodeInteger:self.minute forKey:@"minute"];
    [coder encodeObject:self.notification forKey:@"notification"];
}

- (id)initWithCoder:(NSCoder *)coder {
    NSInteger day = [coder decodeIntegerForKey:@"day"];
    NSInteger hour = [coder decodeIntegerForKey:@"hour"];
    NSInteger minute = [coder decodeIntegerForKey:@"minute"];
    UILocalNotification *localNotification = [coder decodeObjectForKey:@"notification"];
    Alarm *alarm = [[Alarm alloc] init];

    alarm.day = day;
    alarm.hour = hour;
    alarm.minute = minute;
    alarm.notification = localNotification;

    return alarm;
}

@end