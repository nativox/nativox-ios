//
// Created by redhair84 on 04/07/15.
// Copyright (c) 2015 Francisco Javier Morant. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    MONDAY = 1,
    TUESDAY = 2,
    WEDNESDAY = 3,
    THURDSDAY = 4,
    FRIDAY = 5,
    SATURDAY = 6,
    SUNDAY = 7

} AlarmDayType;

@interface Alarm : NSObject <NSCoding>

@property(nonatomic) NSInteger day;
@property(nonatomic) NSInteger hour;
@property(nonatomic) NSInteger minute;
@property(nonatomic) UILocalNotification *notification;

@end