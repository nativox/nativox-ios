//
// Created by redhair84 on 05/02/2014.
// Copyright (c) 2014 Francisco Javier Morant. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface User : NSObject

@property(nonatomic, strong) NSString *token;
@property(nonatomic, strong) NSString *idUser;
@property(nonatomic, strong) NSString *name;
@property(nonatomic, strong) NSString *email;
@property(nonatomic, strong) NSString *password;

@property(nonatomic) int freeCoursesTime;

- (instancetype)initWithToken:(NSString *)token idUser:(NSString *)idUser name:(NSString *)name;

+ (instancetype)sessionWithToken:(NSString *)token idUser:(NSString *)idUser name:(NSString *)name;

+ (User *)shareInstance;


@end