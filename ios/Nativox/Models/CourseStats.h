//
// Created by redhair84 on 09/06/2015.
// Copyright (c) 2015 Francisco Javier Morant. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface CourseStats : NSObject

@property(nonatomic) float progress;
@property(nonatomic) float completed;
@property(nonatomic) float averageGrade;

@end