//
// Created by redhair84 on 22/08/2014.
// Copyright (c) 2014 Francisco Javier Morant. All rights reserved.
//

#import "HistoricalSale.h"
#import "Course.h"


@implementation HistoricalSale {

}
+ (NSArray *)fromDictionary:(NSDictionary *)dictionary {
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd hh:mm:ss"];

    NSMutableArray *historicalSales = [[NSMutableArray alloc] init];

    for (NSDictionary *course in dictionary[@"cursos"]) {
        HistoricalSale *historicalSale = [[HistoricalSale alloc] init];
        historicalSale.idUser = dictionary[@"id_usuario"];
        historicalSale.date = dictionary[@"fecha"] != [NSNull null] ? [dateFormat dateFromString:dictionary[@"fecha"]] : nil;
        historicalSale.total = dictionary[@"total"];

        if ([dictionary[@"pagado"] isEqualToString:@"1"]) {
            historicalSale.historicalSaleStatusType = PAID;
        } else {
            historicalSale.historicalSaleStatusType = NOT_PAID;
        }
        historicalSale.productDescription = course[@"curso"];
        [historicalSales addObject:historicalSale];
    }

    return [historicalSales copy];
}

@end