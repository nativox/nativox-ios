
#import <UIKit/UIKit.h>
#import "UserProvider.h"
#import "SalesProvider.h"

@class FBSession;
@class LoginViewController;
@class UserProvider;
@class DatabaseManager;
@class Reachability;
@class CategoryProvider;
@class DeviceUtils;
@class RCTBridge;
@class ApiAI;

static NSString *const kCoursesIdsPendingRegister = @"coursesIdsPending";

static NSString *const kTotalPricePendingToRegister = @"totalPricePendingToRegister";

static NSString *const kCurrencyPendingToRegister = @"currencyPendingToRegister";

static NSString *const kTransactionIdPendingToRegister = @"transactionIdPendingToRegister";

static NSString *const kInAppIdPendingToRegister = @"inAppIdPendingToRegister";

static NSString *const kUserId = @"userId";

static NSString *const kToken = @"token";

static NSString *const kStartupViewControllerIdentifier = @"startupViewController";

static NSString *const kInitialNavigationControllerIdentifier = @"initialNavigationController";

static NSString *const kShowSentenceAfterPlayedVideo = @"showSentenceAfterPlayedVideo";

@interface NativoxAppDelegate : UIResponder <UIApplicationDelegate, SalesProviderDelegate>

@property(strong, nonatomic) UIWindow *window;

@property(nonatomic, retain, readonly) NSManagedObjectModel *managedObjectModel;
@property(nonatomic, retain, readonly) NSManagedObjectContext *managedObjectContext;
@property(nonatomic, retain, readonly) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property(nonatomic, strong) DatabaseManager *databaseManager;
@property(nonatomic, strong) RCTBridge *bridge;
@property(nonatomic, strong) ApiAI *apiAI;

- (void)updatePendingSales;

@end
