#import "NativoxAppDelegate.h"
#import "UIAlertView+ShowMessage.h"
#import "UserSession.h"
#import "BusinessError.h"
#import "DatabaseManager.h"
#import "MRProgressOverlayView.h"
#import "StartupTutorialViewController.h"
#import "Reachability.h"
#import "UIColor+NativoxColors.h"
#import "NetworkUtil.h"
#import "ProvidersManager.h"
#import "iRate.h"
#import "MainNavigationViewController.h"
#import "FreeCoursesTimer.h"
#import "MetricsProvider.h"
#import "AlarmConstants.h"
#import "RCTBridge.h"
#import "AlertDialogView.h"
#import "GAI.h"
#import <Parse/Parse.h>
#import "TwitterKit/TwitterKit.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "Crashlytics.h"
#import "Fabric.h"
#import <FBSDKCoreKit/FBSDKApplicationDelegate.h>
#import <FBSDKCoreKit/FBSDKAppEvents.h>
#import "Orientation.h"


static NSString *const kIsShownFirstTutorial = @"isShownFirstTutorial";
static NSString *const kTrackingIdGoogleAnalytics = @"UA-51962702-4";
static NSString *const kDatabaseName = @"nativoxDB.sqlite";
static NSString *const kSchemeName = @"nativox";

static NSString *const applicationIdParse = @"VxIriE9SGEP0QY4QomGWU3gfUYXxsJroiyD88JGP";
static NSString *const clientKeyParse = @"ogcD71GHhgKB1DRduHBE1iHFbHse4tOHtUpmLfvm";

@interface NativoxAppDelegate ()
@property(nonatomic, strong) Reachability *reachability;
@end

@implementation NativoxAppDelegate

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;
@synthesize bridge;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {

    [self setupReactNative:launchOptions];
    [self setupParse:application withOptions:launchOptions];

#if !defined(DEBUG)
    [Fabric with:@[[Crashlytics class], [Twitter class]]];
#endif

    [self setupGoogleAnalytics];
    [self setupReachability];
    [self setupNativox];
    [self setupRootViewController:[self mainStoryBoard]];
    [self setupShowSentenceAfterPlayedVideo];

    [iRate sharedInstance].usesUntilPrompt = 5;
    [[FBSDKApplicationDelegate sharedInstance] application:application didFinishLaunchingWithOptions:launchOptions];

    return YES;
}

- (void)setupReactNative:(NSDictionary *)launchOptions {
    NSURL *url = nil;

#if defined(DEBUG)
    url = [NSURL URLWithString:@"http://localhost:8081/index.ios.bundle?platform=ios&dev=true"];
#else
    url = [[NSBundle mainBundle] URLForResource:@"main" withExtension:@"jsbundle"];
#endif
    bridge = [[RCTBridge alloc] initWithBundleURL:url
                                   moduleProvider:nil
                                    launchOptions:launchOptions];
}

- (void)setupShowSentenceAfterPlayedVideo {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    id showSentenceAfterPlayedVideo = [defaults objectForKey:kShowSentenceAfterPlayedVideo];

    if (!showSentenceAfterPlayedVideo) {
        [defaults setBool:YES forKey:kShowSentenceAfterPlayedVideo];
    }
}

- (void)setupReachability {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityDidChange:) name:kReachabilityChangedNotification object:nil];

    self.reachability = [Reachability reachabilityForInternetConnection];
    [self.reachability startNotifier];
}

- (void)setupGoogleAnalytics {
    [GAI sharedInstance].trackUncaughtExceptions = NO;

    [GAI sharedInstance].dispatchInterval = 20;
    [[GAI sharedInstance] trackerWithTrackingId:kTrackingIdGoogleAnalytics];
}

- (UIInterfaceOrientationMask)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window {
    return [Orientation getOrientation];
}

- (void)setupParse:(UIApplication *)application withOptions:(NSDictionary *)options {
    [Parse setApplicationId:applicationIdParse clientKey:clientKeyParse];

    if ([application respondsToSelector:@selector(registerUserNotificationSettings:)]) {
        UIUserNotificationSettings *notificationSettings = [UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert | UIUserNotificationTypeBadge | UIUserNotificationTypeSound categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:notificationSettings];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    } else {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
    }

    if (application.applicationState != UIApplicationStateBackground) {
        BOOL preBackgroundPush = ![application respondsToSelector:@selector(backgroundRefreshStatus)];
        BOOL oldPushHandlerOnly = ![self respondsToSelector:@selector(application:didReceiveRemoteNotification:fetchCompletionHandler:)];
        BOOL noPushPayload = !options[UIApplicationLaunchOptionsRemoteNotificationKey];
        if (preBackgroundPush || oldPushHandlerOnly || noPushPayload) {
            [PFAnalytics trackAppOpenedWithLaunchOptions:options];
        }
    }
}

- (void)setupRootViewController:(UIStoryboard *)mainStoryboard {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    BOOL shownFirstTutorial = [userDefaults boolForKey:kIsShownFirstTutorial];

    if (!shownFirstTutorial) {
        StartupTutorialViewController *startupTutorialViewController = [mainStoryboard instantiateViewControllerWithIdentifier:kStartupViewControllerIdentifier];
        self.window.rootViewController = startupTutorialViewController;
    } else if ([[UserSession instance] isUserLoggedIn]) {
        MainNavigationViewController *mainNavigationViewController = [mainStoryboard instantiateViewControllerWithIdentifier:kInitialNavigationControllerIdentifier];
        self.window.rootViewController = mainNavigationViewController;

        UIViewController *viewController = [mainNavigationViewController.storyboard instantiateViewControllerWithIdentifier:@"homeTabBarController"];
        [mainNavigationViewController pushViewController:viewController animated:NO];
    } else {
        MainNavigationViewController *mainNavigationViewController = [mainStoryboard instantiateViewControllerWithIdentifier:kInitialNavigationControllerIdentifier];
        self.window.rootViewController = mainNavigationViewController;
    }
}

- (UIStoryboard *)mainStoryBoard {
    UIStoryboard *mainStoryboard;

    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone) {
        mainStoryboard = [UIStoryboard storyboardWithName:@"Iphone5Storyboard" bundle:nil];
    } else {
        mainStoryboard = [UIStoryboard storyboardWithName:@"IpadStoryboard" bundle:nil];
    }
    return mainStoryboard;
}

- (void)setupNativox {
    [self setupAppearance];

    self.databaseManager = [DatabaseManager managerWithManager:self.managedObjectContext];
}

- (void)applicationWillResignActive:(UIApplication *)application {
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    [FBSDKAppEvents activateApp];
}

- (void)applicationWillTerminate:(UIApplication *)application {
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    [currentInstallation setDeviceTokenFromData:deviceToken];
    currentInstallation.channels = @[@"global"];
    [currentInstallation saveInBackground];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    [PFPush handlePush:userInfo];

    if (application.applicationState == UIApplicationStateInactive) {
        [PFAnalytics trackAppOpenedWithRemoteNotificationPayload:userInfo];
    }
}

- (void)   application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    if (application.applicationState == UIApplicationStateInactive) {
        [PFAnalytics trackAppOpenedWithRemoteNotificationPayload:userInfo];
    } else if (![self isSilentPushNotification:userInfo]) {
        [self showNotificationAlert:[self extractAlertMessageFromPushNotification:userInfo]];
    }

    if ([self isSilentPushNotification:userInfo]) {
        [self retrieveFreeCountDownTimeFromSilentNotification:YES andCompletionHandler:completionHandler];
    } else {
        completionHandler(UIBackgroundFetchResultNewData);
    }
    return;
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification {
    [[ProvidersManager instance].metricsProvider pingNotificationWithTitle:@"Nativox"
                                                               withMessage:notification.alertBody
                                                      withNotificationType:notification.alertAction
                                                                andHandler:^(BusinessError *error) {
                                                                    if (error) {
                                                                        DLog(@"%@", error.message);
                                                                    }
                                                                }
    ];
    UIApplicationState state = [application applicationState];
    if (state == UIApplicationStateActive) {
        [self showNotificationAlert:notification.alertBody];
    }

    application.applicationIconBadgeNumber = 0;
}

- (BOOL)isSilentPushNotification:(NSDictionary *)userInfo {
    return [userInfo[@"aps"][@"content-available"] intValue] == 1 && [userInfo[@"aps"][@"sound"] isEqualToString:@""];
}

- (void)retrieveFreeCountDownTimeFromSilentNotification:(BOOL)isSilentPushNotification
                                   andCompletionHandler:(void (^)(UIBackgroundFetchResult))handler {
    if ([UserSession instance].isUserLoggedIn && [UserSession instance].idUser && [UserSession instance].token) {
        [[ProvidersManager instance].userProvider retrieveUserInfo:[UserSession instance].idUser
                                                         withToken:[UserSession instance].token
                                                        andHandler:^(NSDictionary *data, BusinessError *error) {
                                                            [self handleFreeCountDownTime:data
                                                                                    error:error
                                                                   silentPushNotification:isSilentPushNotification
                                                                   andCompletitionHandler:handler];
                                                        }];
    }
}

- (void)handleFreeCountDownTime:(NSDictionary *)data error:(const BusinessError *)error
         silentPushNotification:(BOOL)isSilentPushNotification
         andCompletitionHandler:(void (^)(UIBackgroundFetchResult))handler {
    if (!error) {
        int countDownTime = [data[@"info_usuario"][@"tiempo_restante"] intValue];
        if ((countDownTime - [UserSession instance].freeCoursesTimer.countDownTime) > 360) {
            [UserSession instance].coursesLoaded = NO;
            [[UserSession instance] reloadUserSession:data];
            if (isSilentPushNotification) {
                [self presentFreeTimeAvailableNotification:NSLocalizedString(@"freeCourses_notification_message", @"")];
            }
        }

        if (handler) {
            handler(UIBackgroundFetchResultNewData);
        }
    } else if (handler) {
        handler(UIBackgroundFetchResultFailed);
    }
}

- (void)presentFreeTimeAvailableNotification:(NSString *)message {
    UILocalNotification *localNotification = [[UILocalNotification alloc] init];
    localNotification.alertAction = kAlarmTypeFreeCoursesReminder;
    localNotification.alertBody = message;
    localNotification.soundName = UILocalNotificationDefaultSoundName;
    localNotification.timeZone = [NSTimeZone defaultTimeZone];
    [[UIApplication sharedApplication] presentLocalNotificationNow:localNotification];
}

- (void)setupAppearance {
    [UINavigationBar appearance].backItem.title = @"";
    [UINavigationBar appearance].tintColor = [UIColor whiteColor];
    [[UINavigationBar appearance] setShadowImage:[[UIImage alloc] init]];
    [[UINavigationBar appearance] setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault];

    UIImage *tabBarBackground = [UIImage imageNamed:@"background_tabbar_home"];
    [[UITabBar appearance] setBackgroundImage:tabBarBackground];
    [[UITabBar appearance] setShadowImage:[[UIImage alloc] init]];
    [[UITabBar appearance] setTintColor:[UIColor nativoxBlueColor]];

    [[UIPageControl appearance] setCurrentPageIndicatorTintColor:[UIColor nativoxBlueColor]];
}

- (NSManagedObjectContext *)managedObjectContext {
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }

    return _managedObjectContext;
}

- (NSManagedObjectModel *)managedObjectModel {
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    _managedObjectModel = [NSManagedObjectModel mergedModelFromBundles:nil];

    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    NSURL *storeUrl = [NSURL fileURLWithPath:[[self applicationDocumentsDirectory]
            stringByAppendingPathComponent:kDatabaseName]];
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc]
            initWithManagedObjectModel:[self managedObjectModel]];
    NSDictionary *options = @{NSMigratePersistentStoresAutomaticallyOption : @YES, NSInferMappingModelAutomaticallyOption : @YES};
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType
                                                   configuration:nil URL:storeUrl options:options error:&error]) {
    }

    return _persistentStoreCoordinator;
}

- (NSString *)applicationDocumentsDirectory {
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    if ([[url scheme] isEqualToString:kSchemeName]) {
        return YES;
    } else {
        return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                                      openURL:url
                                                            sourceApplication:sourceApplication
                                                                   annotation:annotation
        ];
    }
}

- (void)reachabilityDidChange:(NSNotification *)notification {
    [self updatePendingSales];
}

- (void)updatePendingSales {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSArray *coursesIdsPendingToRegister = [userDefaults objectForKey:kCoursesIdsPendingRegister];
    float totalPricePendingToRegister = [userDefaults floatForKey:kTotalPricePendingToRegister];
    NSString *currencyUsed = [userDefaults objectForKey:kCurrencyPendingToRegister];
    NSString *transactionId = [userDefaults objectForKey:kTransactionIdPendingToRegister];
    NSString *inAppPurchaseId = [userDefaults objectForKey:kInAppIdPendingToRegister];
    NSString *idUser = [userDefaults objectForKey:kUserId];
    NSString *token = [UserSession instance].token ? [UserSession instance].token : [userDefaults objectForKey:kToken];

    if (coursesIdsPendingToRegister && coursesIdsPendingToRegister.count > 0 && idUser && [[NetworkUtil instance] isReachable]) {
        [ProvidersManager instance].salesProvider.delegate = self;
        [[ProvidersManager instance].salesProvider updatePendingOrder:idUser
                                                            withToken:token
                                                           withCursos:coursesIdsPendingToRegister
                                                            withTotal:totalPricePendingToRegister
                                                         withCurrency:currencyUsed
                                                     withTrasactionId:transactionId
                                                  withInAppPurchaseId:inAppPurchaseId];
    }
}

- (void)coursesCheckoutStarted {
    [MRProgressOverlayView showOverlayAddedTo:self.window
                                        title:NSLocalizedString(@"progress_dialog_updating_pending_purchases", @"")
                                         mode:MRProgressOverlayViewModeIndeterminate
                                     animated:YES];
}

- (void)coursesCheckoutFailed:(NSString *)message {
    [MRProgressOverlayView dismissAllOverlaysForView:self.window animated:YES];
}

- (void)coursesCheckoutCompleted:(BusinessError *)error andData:(NSDictionary *)data {
    [MRProgressOverlayView dismissOverlayForView:self.window animated:YES];

    if (!error) {
        [UIAlertView showMessage:NSLocalizedString(@"success_pending_courses_updated", @"")
                       withTitle:NSLocalizedString(@"app_name", @"")
                    withButtonOK:NSLocalizedString(@"ok_button", @"")
                     andDelegate:self];
    } else {
        [AlertDialogView showDialog:error.message];
    }
}

- (NSString *)extractAlertMessageFromPushNotification:(NSDictionary *)userInfo {
    id alert = [userInfo[@"aps"] objectForKey:@"alert"];

    if ([alert isKindOfClass:NSString.class]) {
        return alert;
    }
    else if ([alert isKindOfClass:NSDictionary.class]) {
        return [alert objectForKey:@"body"];
    }

    return @"";
}

- (void)showNotificationAlert:(NSString *)message {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"app_name", nil)
                                                    message:message
                                                   delegate:self cancelButtonTitle:NSLocalizedString(@"ok_button", nil)
                                          otherButtonTitles:nil];
    [alert show];

    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
}

@end
