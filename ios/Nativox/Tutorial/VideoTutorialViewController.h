
#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@class MPMoviePlayerController;
@protocol ChildTutorialViewControllerDelegate;

static NSString *const kUrlVideoTutorial = @"http://nativox.com/videos/tutorial/Nativox_640.mp4";
static NSString *const kUrlVideoTutorialSubtitled = @"http://nativox.com/videos/tutorial/Nativox_en_640.mp4";


@interface VideoTutorialViewController : BaseViewController

@property(nonatomic) NSUInteger index;
@property(strong, nonatomic) MPMoviePlayerController *moviePlayerController;
@end
