
#import "StartCoursesTutorialViewController.h"

@interface StartCoursesTutorialViewController ()
@property(strong, nonatomic) IBOutlet UIButton *startAppButton;
@end

@implementation StartCoursesTutorialViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.startAppButton.layer.cornerRadius = 5;
}

- (IBAction)onPressedStartAppBtn:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
    [self.parentTabBar setSelectedIndex:2];
}

@end
