
#import <UIKit/UIKit.h>
#import "ChildTutorialViewController.h"
#import "SWRevealViewController.h"

@protocol ChildTutorialViewControllerDelegate <NSObject>

- (void)nextPageButtonPressed;

@end

@interface TutorialViewController : BaseViewController <UIPageViewControllerDataSource, UIPageViewControllerDelegate,ChildTutorialViewControllerDelegate>

@property(nonatomic, strong) UIPageViewController *pageController;
@property(nonatomic, strong) UINavigationController *navigationController;

@property(nonatomic, strong) UITabBarController *parentTabBar;
@end
