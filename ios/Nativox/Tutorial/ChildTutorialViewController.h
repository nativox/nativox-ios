
#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@protocol ChildTutorialViewControllerDelegate;

@interface ChildTutorialViewController : BaseViewController

@property(retain) id delegate;
@property(assign, nonatomic) NSInteger index;
@property(strong, nonatomic) IBOutlet UIImageView *imageChildTutorial;
@property(strong, nonatomic) IBOutlet UITextView *textChildTutorial;

@end
