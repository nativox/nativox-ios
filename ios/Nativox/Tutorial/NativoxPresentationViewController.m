
#import "NativoxPresentationViewController.h"
#import "WebViewJavascriptBridge.h"
#import "TutorialViewController.h"
#import "ReactNativeView.h"


@interface NativoxPresentationViewController ()

@property(strong, nonatomic) IBOutlet UIWebView *presentationWebView;
@property WebViewJavascriptBridge *bridge;
@property(strong, nonatomic) IBOutlet ReactNativeView *reactNativeView;
@end

@implementation NativoxPresentationViewController

- (void)viewDidLoad {
    [super viewDidLoad];


    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(goToCourses)
                                                 name:@"GoToCourses"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(goToCheckout)
                                                 name:@"GoToCheckout"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(goToTutorial)
                                                 name:@"GoToTutorial"
                                               object:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.reactNativeView initializeReactView:@"PresentationTutorialScreen"
                               viewController:self
                                   andOptions:@{}];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"GoToCourses" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"GoToCheckout" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"GoToTutorial" object:nil];
}

- (void)goToCourses {
    [self.tabBarController setSelectedIndex:2];
}

- (void)goToTutorial {
    TutorialViewController *tutorialViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"tutorialScreen"];
    tutorialViewController.parentTabBar = self.tabBarController;
    [self.navigationController pushViewController:tutorialViewController animated:YES];
}

- (void)goToCheckout {
    [self.tabBarController setSelectedIndex:0];
}

@end
