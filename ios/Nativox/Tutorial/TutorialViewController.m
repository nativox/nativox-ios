
#import "TutorialViewController.h"
#import "VideoTutorialViewController.h"
#import "StartCoursesTutorialViewController.h"
#import "Answers.h"

@interface TutorialViewController ()
@property(strong, nonatomic) IBOutlet UIView *frameViewController;

@end


@implementation TutorialViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.pageController = [[UIPageViewController alloc]
            initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll
              navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal
                            options:nil];
    [self setupPageController];
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
    [[self.pageController view] setFrame:[[self frameViewController] bounds]];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController
      viewControllerBeforeViewController:(UIViewController *)viewController {
    NSUInteger index = (NSUInteger) [(ChildTutorialViewController *) viewController index];

    if (index == 0) {
        return nil;
    }

    index--;

    return [self viewControllerAtIndex:index];

}

- (void)setupPageController {
    self.pageController.dataSource = self;
    self.pageController.delegate = self;
    [[self.pageController view] setFrame:[[self frameViewController] bounds]];

    ChildTutorialViewController *initialViewController = (ChildTutorialViewController *) [self viewControllerAtIndex:0];

    NSArray *viewControllers = @[initialViewController];

    [self.pageController setViewControllers:viewControllers
                                  direction:UIPageViewControllerNavigationDirectionForward
                                   animated:YES
                                 completion:nil];

    [self addChildViewController:self.pageController];
    [self.frameViewController addSubview:[self.pageController view]];
    [self.pageController didMoveToParentViewController:self];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController
       viewControllerAfterViewController:(UIViewController *)viewController {

    NSUInteger index;

    if ([viewController isKindOfClass:[ChildTutorialViewController class]]) {
        index = (NSUInteger) [(ChildTutorialViewController *) viewController index];
    } else {
        index = (NSUInteger) [(VideoTutorialViewController *) viewController index];
    }


    if (index == 5) {
        return nil;
    }

    index++;

    return [self viewControllerAtIndex:index];
}

- (UIViewController *)viewControllerAtIndex:(NSUInteger)index {

    if (index < 4) {

        ChildTutorialViewController *childViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"childTutorial"];
        [childViewController view];
        childViewController.delegate = self;
        childViewController.index = index;

        switch (index) {
            case 0:
                childViewController.imageChildTutorial.image = [UIImage imageNamed:@"play_tutorial"];
                childViewController.textChildTutorial.text = NSLocalizedString(@"text_first_page_tutorial", @"");
                break;
            case 1:
                childViewController.imageChildTutorial.image = [UIImage imageNamed:@"record_tutorial"];
                childViewController.textChildTutorial.text = NSLocalizedString(@"text_second_page_tutorial", @"");
                break;
            case 2:
                childViewController.imageChildTutorial.image = [UIImage imageNamed:@"test_tutorial"];
                childViewController.textChildTutorial.text = NSLocalizedString(@"text_third_page_tutorial", @"");
                break;
            case 3:
                childViewController.imageChildTutorial.image = [UIImage imageNamed:@"translation_tutorial"];
                childViewController.textChildTutorial.text = NSLocalizedString(@"text_forth_page_tutorial", @"");
                break;
            default:
                break;
        }

        [childViewController viewDidLoad];

        return childViewController;
    } else if (index == 4) {
        VideoTutorialViewController *videoTutorialViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"videoTutorial"];
        videoTutorialViewController.index = index;
        [videoTutorialViewController viewDidLoad];
        return videoTutorialViewController;
    }
    else {
        StartCoursesTutorialViewController *startCoursesTutorialViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"startAppTutorial"];
        startCoursesTutorialViewController.index = index;
        startCoursesTutorialViewController.pageController = self.pageController;
        startCoursesTutorialViewController.parentTabBar = self.parentTabBar;

        [startCoursesTutorialViewController viewDidLoad];
        return startCoursesTutorialViewController;
    }
}

- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController {
    return 6;
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController {
    if (self.pageController.viewControllers.count > 0) {
        UIViewController *currentViewController = self.pageController.viewControllers[0];
        return (NSUInteger) ((ChildTutorialViewController *) currentViewController).index;
    } else {
        return 0;
    }
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (void)nextPageButtonPressed {
    UIViewController *currentViewController = self.pageController.viewControllers[0];
    NSUInteger index = (NSUInteger) ((ChildTutorialViewController *) currentViewController).index;

    ChildTutorialViewController *childTutorialViewController = (ChildTutorialViewController *) [self
            viewControllerAtIndex:index + 1];
    [self.pageController setViewControllers:@[childTutorialViewController]
                                  direction:UIPageViewControllerNavigationDirectionForward
                                   animated:YES
                                 completion:nil];
    [self addChildViewController:self.pageController];
    [self.frameViewController addSubview:[self.pageController view]];
    [self.pageController didMoveToParentViewController:self];
}


@end
