

#import "ChildTutorialViewController.h"
#import "TutorialViewController.h"

@interface ChildTutorialViewController ()
@property(strong, nonatomic) IBOutlet NSLayoutConstraint *topSpaceImageTutorialConstraint;
@property(strong, nonatomic) IBOutlet NSLayoutConstraint *topSpaceTextTutorialConstraint;
@end

@implementation ChildTutorialViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    if (isPreIphone5) {
        [self.textChildTutorial setFont:[UIFont fontWithName:@"Helvetica Neue Light" size:12.0f]];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self updateTutorialConstraints:self.interfaceOrientation];
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];

    [self updateTutorialConstraints:toInterfaceOrientation];
}

- (void)updateTutorialConstraints:(UIInterfaceOrientation)toInterfaceOrientation {
    if (toInterfaceOrientation == UIInterfaceOrientationPortrait) {
        self.topSpaceImageTutorialConstraint.constant = 0;
        self.topSpaceTextTutorialConstraint.constant = 0;
    } else {
        self.topSpaceImageTutorialConstraint.constant = -40;
        self.topSpaceTextTutorialConstraint.constant = -80;
    }
}

- (IBAction)nextPageButtonPressed:(id)sender {
    if (self.delegate) {
        [self.delegate nextPageButtonPressed];
    }
}

@end
