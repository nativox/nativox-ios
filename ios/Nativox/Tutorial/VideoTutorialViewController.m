

#import <MediaPlayer/MediaPlayer.h>
#import "VideoTutorialViewController.h"
#import "AudioHandler.h"

@interface VideoTutorialViewController ()


@property(strong, nonatomic) IBOutlet UIView *videoView;
@property(strong, nonatomic) IBOutlet UITextView *videoTutorialText;

@end

@implementation VideoTutorialViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    self.videoTutorialText.text = NSLocalizedString(@"video_tutorial_text", @"");
    [AudioHandler setupAVAudioSession];
}

- (void)didEnterFullScreen:(id)didEnterFullScreen {
    [self.moviePlayerController play];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];

    NSString *videoTutorialPath = [self isDeviceSetInSpanish] ? kUrlVideoTutorial : kUrlVideoTutorialSubtitled;
    self.moviePlayerController = [[MPMoviePlayerController alloc]
            initWithContentURL:[NSURL URLWithString:videoTutorialPath]];
    [self.moviePlayerController prepareToPlay];
    [self.moviePlayerController.view setFrame:self.videoView.bounds];
    [self.videoView addSubview:self.moviePlayerController.view];

    self.moviePlayerController.controlStyle = MPMovieControlStyleDefault;
    self.moviePlayerController.movieSourceType = MPMovieSourceTypeStreaming;
    self.moviePlayerController.shouldAutoplay = NO;
    [[NSNotificationCenter defaultCenter]
            addObserver:self
               selector:@selector(didEnterFullScreen:)
                   name:MPMoviePlayerDidEnterFullscreenNotification
                 object:nil];
}

- (BOOL)isDeviceSetInSpanish {
    NSString *language = [NSLocale preferredLanguages][0];
    language = [language componentsSeparatedByString:@"-"][0];
    return [language isEqualToString:@"es"];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [self.moviePlayerController stop];

    [[NSNotificationCenter defaultCenter]
            removeObserver:self name:MPMoviePlayerWillEnterFullscreenNotification object:nil];
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
    [self.moviePlayerController.view setFrame:self.videoView.bounds];

}


@end
