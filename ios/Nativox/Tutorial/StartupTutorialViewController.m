#import "StartupTutorialViewController.h"
#import "MainNavigationViewController.h"
#import "NativoxAppDelegate.h"
#import "ReactNativeView.h"

@interface StartupTutorialViewController ()
@property(strong, nonatomic) IBOutlet ReactNativeView *reactNativeView;

@end

@implementation StartupTutorialViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                         selector:@selector(onStartButtonPressed:)
                                             name:@"PressedStartButton"
                                           object:nil];
}

- (void)onStartButtonPressed:(id)object {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:YES forKey:@"isShownFirstTutorial"];
    [defaults synchronize];

    MainNavigationViewController *mainNavigationViewController = [self.storyboard instantiateViewControllerWithIdentifier:kInitialNavigationControllerIdentifier];
    [self presentViewController:mainNavigationViewController animated:NO completion:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.reactNativeView initializeReactView:@"InitialTutorialScreen"
                               viewController:self
                                   andOptions:@{}];
}

- (BOOL)shouldAutorotate {
    return UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad;

}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
