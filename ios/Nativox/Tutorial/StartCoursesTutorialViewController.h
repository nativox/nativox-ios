
#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@protocol ChildTutorialViewControllerDelegate;

@interface StartCoursesTutorialViewController : BaseViewController

@property(retain) id delegate;
@property(nonatomic) NSUInteger index;
@property(strong, nonatomic) UIPageViewController *pageController;

@property(nonatomic, strong) UITabBarController *parentTabBar;
@end
