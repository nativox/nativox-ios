#import "DashboardViewController.h"
#import "UserSession.h"
#import "AlertDialogView.h"
#import "NetworkUtil.h"

@interface DashboardViewController ()
@property(strong, nonatomic) IBOutlet UIButton *myLessonsBtn;
@property(strong, nonatomic) IBOutlet UIButton *settingBtn;
@property(strong, nonatomic) IBOutlet UIButton *myPurchasesBtn;
@property (strong, nonatomic) IBOutlet UIButton *contactBtn;

@end

@implementation DashboardViewController {
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.myLessonsBtn.layer.cornerRadius = 5;
    self.settingBtn.layer.cornerRadius = 5;
    self.myPurchasesBtn.layer.cornerRadius = 5;
    self.contactBtn.layer.cornerRadius = 5;

    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    BOOL hasShownTutorial = [userDefaults boolForKey:kHasShownTutorial];

    if (!hasShownTutorial) {
        [self.tabBarController setSelectedIndex:0];
        [userDefaults setBool:YES forKey:kHasShownTutorial];
        [userDefaults synchronize];
    }

    if (![[NetworkUtil instance] isReachable]) {
        [AlertDialogView showDialog:NSLocalizedString(@"internet_not_available", @"")];
        [self.tabBarController.navigationController popViewControllerAnimated:YES];
    } else if ([[UserSession instance] isUserLoggedIn]) {
        [UserSession instance].coursesLoaded = NO;
        [self retrieveCategories];
    }
}

@end
