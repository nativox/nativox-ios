
#import <UIKit/UIKit.h>
#import "BaseViewController.h"

static NSString *const kHasShownTutorial = @"hasShownTutorial";

@interface DashboardViewController : BaseViewController

@end
