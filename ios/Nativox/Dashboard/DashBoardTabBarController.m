#import "DashBoardTabBarController.h"

@interface DashBoardTabBarController ()

@end

@implementation DashBoardTabBarController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.navigationController.navigationBar.hidden = NO;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    UIViewController *tutorialViewController = [self childViewControllers][1];
    [tutorialViewController.view layoutSubviews];

    UIViewController *profileViewController = [self childViewControllers][2];
    [profileViewController.view layoutSubviews];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return interfaceOrientation == UIInterfaceOrientationMaskPortrait || interfaceOrientation == UIInterfaceOrientationMaskPortraitUpsideDown;
}

@end
