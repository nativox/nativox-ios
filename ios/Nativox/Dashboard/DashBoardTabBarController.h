#import <UIKit/UIKit.h>

@class BaseViewController;

@interface DashBoardTabBarController : UITabBarController <UITabBarDelegate>

@end
