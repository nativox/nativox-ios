#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "SalesProvider.h"
#import "CategoryCourse.h"
#import "RATreeView/RATreeView.h"
#import "SLExpandableTableView/SLExpandableTableView.h"

@class SKProduct;

@class Course;
@class SWRevealViewController;

static const int HEIGHT_SECTION_COURSE = 62;
static const int HEIGHT_SECTION_COURSE_IPHONE4 = 55;
static const int HEIGHT_ROW_COURSE = 224;
static const int HEIGHT_SECTION_COURSE_IPAD = 120;
static const int HEIGHT_ROW_COURSE_IPAD = 350;

@interface CourseViewController : BaseViewController <UIAlertViewDelegate, UITableViewDelegate,
        UITableViewDataSource, SLExpandableTableViewDatasource, SLExpandableTableViewDelegate, SalesProviderDelegate>

@property(strong, nonatomic) IBOutlet UIImageView *iconImage;
@property(nonatomic, strong) NSDictionary *selectedCategory;

@end
