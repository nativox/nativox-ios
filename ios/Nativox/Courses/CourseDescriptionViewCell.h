#import <UIKit/UIKit.h>
#import "SLExpandableTableView/SLExpandableTableView.h"

@class CourseViewController;
@class Course;

static NSString *const kCourseDescriptionCell = @"course_description_cell";

@interface CourseDescriptionViewCell : UITableViewCell <UIExpandingTableViewCell, UIWebViewDelegate>
@property(weak, nonatomic) IBOutlet UIWebView *courseDescription;
@property(weak, nonatomic) IBOutlet UIButton *playBtn;
@property(weak, nonatomic) IBOutlet UIButton *purchaseBtn;
@property(weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property(weak, nonatomic) UIViewController *controller;
@property(weak, nonatomic) NSDictionary *course;
@property(weak, nonatomic) IBOutlet NSLayoutConstraint *horizontalConstraintPurchaseButton;
@property(weak, nonatomic) IBOutlet NSLayoutConstraint *horizontalConstraintPlayButton;

+ (CourseDescriptionViewCell *)getCourseDescriptionViewCell:(UITableView *)tableView
                                             withController:(UIViewController *)controller
                                                  andCourse:(NSDictionary *)course;

@end
