//
//  CourseDescriptionViewCell.m
//  Nativox
//
//  Created by redhair84 on 11/10/2014.
//  Copyright (c) 2014 Francisco Javier Morant. All rights reserved.
//

#import "CourseDescriptionViewCell.h"
#import "CourseViewController.h"
#import "Course.h"
#import "SectionsHandler.h"
#import "SectionsViewController.h"
#import "SWRevealViewController.h"
#import "LessonViewController.h"

@interface CourseDescriptionViewCell ()
@property(nonatomic) CGFloat originalHorizontalConstraintPlayButtonConstant;
@end

@implementation CourseDescriptionViewCell

- (void)awakeFromNib {
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
        self.playBtn.layer.cornerRadius = 10;
        self.purchaseBtn.layer.cornerRadius = 10;
    } else {
        self.playBtn.layer.cornerRadius = 5;
        self.purchaseBtn.layer.cornerRadius = 5;
    }

    [self.purchaseBtn setTitle:NSLocalizedString(@"buy_course_button", @"") forState:UIControlStateNormal];
    self.originalHorizontalConstraintPlayButtonConstant = self.horizontalConstraintPlayButton.constant;
}

- (IBAction)onPlayButtonPressed:(id)sender {
    [self jumpToStartLesson:self.course];
}

- (BOOL)isLoading {
    return NO;
}

- (void)setLoading:(BOOL)loading {

}

- (UIExpansionStyle)expansionStyle {
    return UIExpansionStyleCollapsed;
}

- (void)setExpansionStyle:(UIExpansionStyle)style animated:(BOOL)animated {

}

- (void)jumpToStartLesson:(NSDictionary *)course {
    SectionsHandler *sectionsHelper = [SectionsHandler helperWithSelectedCourse:course];

    SectionsViewController *leftViewController = [self.controller.storyboard instantiateViewControllerWithIdentifier:@"seccionesVC"];
    leftViewController.sectionsHandler = sectionsHelper;
    sectionsHelper.sectionsHelperDelegateForSectionsView = leftViewController;

    LessonViewController *frontViewController = [self.controller.storyboard instantiateViewControllerWithIdentifier:@"lessonViewController"];
    frontViewController.sectionsHandler = sectionsHelper;
    sectionsHelper.sectionsHelperDelegateForLessonView = frontViewController;

    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:frontViewController];
    SWRevealViewController *revealController = [[SWRevealViewController alloc] initWithRearViewController:leftViewController frontViewController:navigationController];
    [self.controller presentViewController:revealController animated:YES completion:nil];
}

+ (CourseDescriptionViewCell *)getCourseDescriptionViewCell:(UITableView *)tableView
                                             withController:(UIViewController *)controller
                                                  andCourse:(NSDictionary *)course {
    CourseDescriptionViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCourseDescriptionCell];

    NSString *courseDescriptionHtml;

    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        courseDescriptionHtml = [NSString stringWithFormat:@"<html> <body style=\"font-size:30px;font-family:Helvetica Neue;background-color: transparent;color:#FFF;text-align:center;vertical-align:middle;\"> %@ </body></html>",
                                                           course[@"descripcion"]];
    } else {
        courseDescriptionHtml = [NSString stringWithFormat:@"<html> <body style=\"font-family:Helvetica Neue;background-color: transparent;color:#FFF;text-align:center;font-size:14px;vertical-align:middle;\"> %@ </body></html>",
                                                           course[@"descripcion"]];
    }
    cell.activityIndicator.hidden = NO;
    [cell.courseDescription loadHTMLString:courseDescriptionHtml baseURL:nil];
    cell.course = course;
    cell.controller = controller;

    cell.purchaseBtn.hidden = YES;
    cell.backgroundColor = [UIColor clearColor];
    cell.horizontalConstraintPlayButton.constant = cell.purchaseBtn.hidden ? 0 : cell.originalHorizontalConstraintPlayButtonConstant;

    return cell;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    self.activityIndicator.hidden = YES;
}

@end
