#import "CourseViewController.h"
#import "Course.h"
#import "UserSession.h"
#import "CourseHeaderViewCell.h"
#import "CourseDescriptionViewCell.h"
#import "AsyncImageView.h"

@interface CourseViewController ()
@property(strong, nonatomic) IBOutlet UILabel *currentCourseLabel;
@property(strong, nonatomic) NSArray *courses;
@property(strong, nonatomic) IBOutlet UIView *courseTableView;
@property(strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property(nonatomic, strong) SLExpandableTableView *coursesExpandableTableView;
@end


@implementation CourseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tabBarController.view.hidden = NO;

    self.courses = self.selectedCategory[@"cursos"];
    [self.coursesExpandableTableView reloadData];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self setupTableView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self updateCourses];
    [self setupCategoryIcon];
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
    [self setupTableView];
}

- (void)setupTableView {
    self.activityIndicator.hidden = NO;
    self.coursesExpandableTableView = [[SLExpandableTableView alloc] initWithFrame:self.courseTableView.bounds style:UITableViewStylePlain];
    self.coursesExpandableTableView.backgroundColor = [UIColor clearColor];
    [self.coursesExpandableTableView registerNib:[UINib nibWithNibName:@"CourseHeaderViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:kCourseHeaderCell];

    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
        [self.coursesExpandableTableView registerNib:[UINib nibWithNibName:@"CourseDescriptionViewCell_ipad" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:kCourseDescriptionCell];
    } else {
        [self.coursesExpandableTableView registerNib:[UINib nibWithNibName:@"CourseDescriptionViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:kCourseDescriptionCell];
    }
    self.coursesExpandableTableView.dataSource = self;
    self.coursesExpandableTableView.delegate = self;
    self.coursesExpandableTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.coursesExpandableTableView.backgroundColor = [UIColor clearColor];

    NSArray *viewsToRemove = [self.courseTableView subviews];
    for (UIView *view in viewsToRemove) {
        [view removeFromSuperview];
    }

    [self.courseTableView addSubview:self.coursesExpandableTableView];
    self.activityIndicator.hidden = YES;
}

- (void)setupCategoryIcon {
    self.iconImage.imageURL = [[NSURL alloc] initWithString:self.selectedCategory[@"icono_url"]];
    self.currentCourseLabel.text = self.selectedCategory[@"categoria"];
}

- (void)updateCourses {
    for (NSDictionary *categoryCourse in [UserSession instance].categories) {
        if ([self.selectedCategory[@"idCategory"] isEqualToString:categoryCourse[@"idCategory"]]) {
            self.courses = categoryCourse[@"cursos"];
            break;
        }
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.courses.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    return [CourseDescriptionViewCell getCourseDescriptionViewCell:tableView
                                                    withController:self
                                                         andCourse:self.courses[(NSUInteger) indexPath.section]];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
            return HEIGHT_SECTION_COURSE_IPAD;
        } else if ([[UIScreen mainScreen] bounds].size.height >= 568) {
            return HEIGHT_SECTION_COURSE;
        } else {
            return HEIGHT_SECTION_COURSE_IPHONE4;
        }
    } else {
        if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
            return HEIGHT_ROW_COURSE_IPAD;
        } else {
            return HEIGHT_ROW_COURSE;
        }
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

- (BOOL)tableView:(SLExpandableTableView *)tableView canExpandSection:(NSInteger)section {
    return YES;
}

- (BOOL)tableView:(SLExpandableTableView *)tableView needsToDownloadDataForExpandableSection:(NSInteger)section {
    return NO;
}

- (UITableViewCell <UIExpandingTableViewCell> *)tableView:(SLExpandableTableView *)tableView
                                  expandingCellForSection:(NSInteger)section {
    return [CourseHeaderViewCell getCourseHeaderViewCell:tableView
                                            withCategory:self.selectedCategory
                                               andCourse:self.courses[(NSUInteger) section]];
}

- (void)tableView:(SLExpandableTableView *)tableView downloadDataForExpandableSection:(NSInteger)section {

}

- (void)tableView:(SLExpandableTableView *)tableView didExpandSection:(NSUInteger)section animated:(BOOL)animated {
    [tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:section]
                     atScrollPosition:UITableViewScrollPositionMiddle
                             animated:YES];
}

@end
