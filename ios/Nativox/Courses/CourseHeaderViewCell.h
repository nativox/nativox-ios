
#import <UIKit/UIKit.h>
#import "SLExpandableTableView/SLExpandableTableView.h"
#import "CourseViewController.h"

@class Course;

static NSString *const kCourseHeaderCell = @"course_header_cell";

@interface CourseHeaderViewCell : UITableViewCell<UIExpandingTableViewCell>
@property (strong, nonatomic) IBOutlet UIView *backgroundCourseView;
@property (strong, nonatomic) IBOutlet UIView *progressCourseView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *progressCourseViewWidthConstraint;
@property (strong, nonatomic) IBOutlet UILabel *titleHeaderLabel;

@property (nonatomic, assign, getter = isLoading) BOOL loading;

@property (nonatomic, readonly) UIExpansionStyle expansionStyle;
- (void)setExpansionStyle:(UIExpansionStyle)expansionStyle animated:(BOOL)animated;

+ (CourseHeaderViewCell *)getCourseHeaderViewCell:(UITableView *)tableView
                                     withCategory:(NSDictionary *)categoryCourse
                                        andCourse:(NSDictionary *)course;

@end
