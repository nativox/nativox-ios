#import "CourseHeaderViewCell.h"
#import "Course.h"
#import "UIColor+Expanded.h"
#import "CourseStats.h"

@implementation CourseHeaderViewCell

- (void)awakeFromNib {
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
        self.backgroundCourseView.layer.cornerRadius = 10;
        self.progressCourseView.layer.cornerRadius = 10;
    } else {
        self.backgroundCourseView.layer.cornerRadius = 5;
        self.progressCourseView.layer.cornerRadius = 5;
    }
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)setExpansionStyle:(UIExpansionStyle)expansionStyle animated:(BOOL)animated {

}

+ (CourseHeaderViewCell *)getCourseHeaderViewCell:(UITableView *)tableView
                                     withCategory:(NSDictionary *)categoryCourse
                                        andCourse:(NSDictionary *)course {
    CourseHeaderViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCourseHeaderCell];

    cell.backgroundCourseView.backgroundColor = [UIColor colorWithHexString:categoryCourse[@"color_hex"]];
    CGRect backgroundCourseFrame = tableView.frame;
    cell.progressCourseViewWidthConstraint.constant = [course[@"stats_curso"][@"porcentaje_completado"] floatValue]/100 * (backgroundCourseFrame.size.width - 5);
    cell.progressCourseView.backgroundColor = [cell lighterColorForColor:[UIColor colorWithHexString:categoryCourse[@"color_hex"]]];
    cell.backgroundColor = [UIColor clearColor];
    cell.titleHeaderLabel.text = course[@"curso"];

    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
        [cell.titleHeaderLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:39]];
    }
    return cell;
}

- (UIColor *)lighterColorForColor:(UIColor *)c
{
    CGFloat r, g, b, a;
    if ([c getRed:&r green:&g blue:&b alpha:&a])
        return [UIColor colorWithRed:MIN(r + 0.1, 1.0)
                               green:MIN(g + 0.1, 1.0)
                                blue:MIN(b + 0.1, 1.0)
                               alpha:a];
    return nil;
}



@end
