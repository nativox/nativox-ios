#import "MyCoursesInteractor.h"
#import "UserSession.h"
#import "CategoryProvider.h"


@implementation MyCoursesInteractor

- (void)screenDidAppear {
    if ([UserSession instance].purchasedCourses.count == 0 && [UserSession instance].coursesLoaded) {
        [self.userInterface showCoursesButton];
    } else if (![UserSession instance].coursesLoaded) {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(initialDataLoadHasFinished:)
                                                     name:kInitialDataLoadHasFinished
                                                   object:nil];
    } else {
        [self.userInterface loadMyCoursesTable];
    }
}

- (void)goToCoursesButtonPressed {
    [self.userInterface showCoursesTab];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)initialDataLoadHasFinished:(NSNotification *)notification {
    if ([UserSession instance].purchasedCourses.count > 0) {
        [self.userInterface loadMyCoursesTable];
    } else {
        [self.userInterface showCoursesButton];
    }
}

@end