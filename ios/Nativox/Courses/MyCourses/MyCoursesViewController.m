//
//  MyCoursesViewController.m
//  Nativox
//
//  Created by redhair84 on 23/02/2014.
//  Copyright (c) 2014 Francisco Javier Morant. All rights reserved.
//

#import "SLExpandableTableView/SLExpandableTableView.h"
#import "MyCoursesViewController.h"
#import "UserSession.h"
#import "CategoryCourse.h"
#import "Course.h"
#import "NativoxIAHelper.h"
#import "CourseHeaderViewCell.h"
#import "CourseDescriptionViewCell.h"
#import "GAIDictionaryBuilder.h"
#import "GAI.h"
#import "GAIFields.h"
#import "MyCoursesInteractor.h"
#import "DeviceUtils.h"
#import "Answers.h"

@interface MyCoursesViewController ()
@property(strong, nonatomic) IBOutlet UIView *myCoursesView;
@property(strong, nonatomic) IBOutlet UIActivityIndicatorView *loadingCoursesIndicator;
@property(nonatomic, strong) SLExpandableTableView *myCoursesTableView;
@property(strong, nonatomic) IBOutlet UIButton *goToCoursesButton;
@property(strong, nonatomic) IBOutlet UILabel *goToCoursesLabel;
@property(retain) id eventHandler;
@end

@implementation MyCoursesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    MyCoursesInteractor *myCoursesInteractor = [[MyCoursesInteractor alloc] init];
    myCoursesInteractor.userInterface = self;
    self.eventHandler = myCoursesInteractor;

    self.goToCoursesButton.hidden = YES;
    self.goToCoursesButton.layer.cornerRadius = [DeviceUtils instance].isOnIpad ? 10 : 5;
    self.goToCoursesLabel.hidden = YES;
    self.goToCoursesLabel.text = NSLocalizedString(@"mycourses_gotocourses_label", @"");
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = NO;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:IAPHelperProductRestoredNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:IAPHelperProductRestoredFailedNotification object:nil];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];

    [self.eventHandler screenDidAppear];
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
    [self.eventHandler screenDidAppear];
}

#pragma mark UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [UserSession instance].purchasedCourses.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSMutableDictionary *course = [UserSession instance].purchasedCourses[(NSUInteger) indexPath.section];
    course[@"purchased"] = @YES;

    return [CourseDescriptionViewCell getCourseDescriptionViewCell:tableView withController:self andCourse:course];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
            return HEIGHT_SECTION_COURSE_IPAD;
        } else if ([[UIScreen mainScreen] bounds].size.height >= 568) {
            return HEIGHT_SECTION_COURSE;
        } else {
            return HEIGHT_SECTION_COURSE_IPHONE4;
        }
    } else {
        if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
            return HEIGHT_ROW_COURSE_IPAD;
        } else {
            return HEIGHT_ROW_COURSE;
        }
    }
}


- (BOOL)tableView:(SLExpandableTableView *)tableView canExpandSection:(NSInteger)section {
    return YES;
}

- (BOOL)tableView:(SLExpandableTableView *)tableView needsToDownloadDataForExpandableSection:(NSInteger)section {
    return NO;
}

- (UITableViewCell <UIExpandingTableViewCell> *)tableView:(SLExpandableTableView *)tableView
                                  expandingCellForSection:(NSInteger)section {
    NSDictionary *course = [UserSession instance].purchasedCourses[(NSUInteger) section];
    NSString *idCategory = course[@"id_categoria"];
    NSDictionary *category = [[UserSession instance].categories filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"id_categoria like %@", idCategory]][0];

    return [CourseHeaderViewCell getCourseHeaderViewCell:tableView
                                            withCategory:category
                                               andCourse:course];

}

- (void)tableView:(SLExpandableTableView *)tableView downloadDataForExpandableSection:(NSInteger)section {

}

- (void)tableView:(SLExpandableTableView *)tableView didExpandSection:(NSUInteger)section animated:(BOOL)animated {
    [tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:section]
                     atScrollPosition:UITableViewScrollPositionMiddle
                             animated:YES];
}

- (void)loadMyCoursesTable {
    self.loadingCoursesIndicator.hidden = NO;

    self.myCoursesTableView = [[SLExpandableTableView alloc] initWithFrame:self.myCoursesView.bounds style:UITableViewStylePlain];
    self.myCoursesTableView.backgroundColor = [UIColor clearColor];
    [self.myCoursesTableView registerNib:[UINib nibWithNibName:@"CourseHeaderViewCell" bundle:[NSBundle mainBundle]]
                  forCellReuseIdentifier:kCourseHeaderCell];

    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
        [self.myCoursesTableView registerNib:[UINib nibWithNibName:@"CourseDescriptionViewCell_ipad" bundle:[NSBundle mainBundle]]
                      forCellReuseIdentifier:kCourseDescriptionCell];
    } else {
        [self.myCoursesTableView registerNib:[UINib nibWithNibName:@"CourseDescriptionViewCell"
                                                            bundle:[NSBundle mainBundle]]
                      forCellReuseIdentifier:kCourseDescriptionCell];
    }
    self.myCoursesTableView.dataSource = self;
    self.myCoursesTableView.delegate = self;
    self.myCoursesTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.myCoursesTableView.backgroundColor = [UIColor clearColor];

    NSArray *viewsToRemove = [self.myCoursesView subviews];
    for (UIView *view in viewsToRemove) {
        [view removeFromSuperview];
    }

    [self.myCoursesView addSubview:self.myCoursesTableView];
    self.loadingCoursesIndicator.hidden = YES;
}

- (void)showCoursesButton {
    self.goToCoursesButton.hidden = NO;
    self.goToCoursesLabel.hidden = NO;
    self.loadingCoursesIndicator.hidden = YES;
}

- (void)showCoursesTab {
    [self.tabBarController setSelectedIndex:0];
}

- (IBAction)goToCoursesButtonPressed:(id)sender {
    [self.eventHandler goToCoursesButtonPressed];
}

@end
