//
// Created by redhair84 on 30/05/15.
// Copyright (c) 2015 Francisco Javier Morant. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EventHandler.h"

@protocol MyCoursesEventHandler <EventHandler>

- (void)goToCoursesButtonPressed;
@end