#import <Foundation/Foundation.h>
#import "MyCoursesEventHandler.h"
#import "MyCoursesUserInterface.h"


@interface MyCoursesInteractor : NSObject <MyCoursesEventHandler>

@property(nonatomic, strong) id userInterface;

@end