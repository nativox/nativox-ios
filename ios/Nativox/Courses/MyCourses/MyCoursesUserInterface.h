//
// Created by redhair84 on 30/05/15.
// Copyright (c) 2015 Francisco Javier Morant. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol MyCoursesUserInterface <NSObject>
- (void)loadMyCoursesTable;

- (void)showCoursesButton;

- (void)showCoursesTab;
@end