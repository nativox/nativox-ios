
#import <UIKit/UIKit.h>
#import "MyCoursesUserInterface.h"
#import "BaseViewController.h"

@class SLExpandableTableView;

@interface MyCoursesViewController : BaseViewController <UITableViewDataSource, UITableViewDelegate,
        SLExpandableTableViewDatasource, SLExpandableTableViewDelegate, MyCoursesUserInterface>


@end
