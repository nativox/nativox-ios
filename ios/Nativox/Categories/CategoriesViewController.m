#import "SalesProvider.h"
#import "CategoriesViewController.h"
#import "UserSession.h"
#import "ProvidersManager.h"
#import "MRProgressOverlayView.h"
#import "BusinessError.h"
#import "CustomIOSAlertView.h"
#import "PackCourse.h"
#import "NativoxIAHelper.h"
#import "AlertDialogView.h"
#import "ReactNativeView.h"

@interface CategoriesViewController ()

@property(strong, nonatomic) IBOutlet UITableView *tableCategories;
@property(strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property(nonatomic, strong) CustomIOSAlertView *purchaseAlertView;
@property(strong, nonatomic) IBOutlet ReactNativeView *reactNativeView;
@end

@implementation CategoriesViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(onCategoriesLoaded:)
                                                 name:@"OnCategoriesLoaded"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(onCategorySelected:)
                                                 name:@"OnCategorySelected"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(OnSessionExpired:)
                                                 name:@"OnSessionExpired"
                                               object:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    [self.reactNativeView initializeReactView:@"CategoriesScreen"
                               viewController:self
                                   andOptions:@{}];
}


- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)onCategoriesLoaded:(NSNotification *)categories {
    [[UserSession instance] generateAllCourses:categories.object[@"categories"]];
}

- (void)onCategorySelected:(NSNotification *)category {
    CourseViewController *courseViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"categoryView"];

    courseViewController.selectedCategory = category.object;
    [self.navigationController pushViewController:courseViewController animated:YES];
}

- (void)OnSessionExpired:(id)OnSessionExpired {
    [self logoutFromLessonScreen];
}

#pragma -- Purchase Courses

- (void)purchaseButtonPressed:(id)categoryData {
    [self.purchaseAlertView close];

    NSString *categoryDataString = categoryData;
    NSString *categoryId = [categoryDataString stringByReplacingOccurrencesOfString:@"goToBuy:" withString:@""];

    CategoryCourse *category = [[UserSession instance] findCategory:categoryId];
    if (category) {
        [self purchaseCategory:category];
    } else if ([categoryId isEqualToString:@"superpack"]) {
        [self purchaseSuperPack];
    } else {
        [AlertDialogView showDialog:NSLocalizedString(@"error_category_not_identified", @"")];
    }
}

- (void)purchaseCategory:(CategoryCourse *)category {
    PackCourse *packCourse = [[PackCourse alloc] init];
    [packCourse addCourses:category.cursos];
    packCourse.inAppId = category.inAppId;

    [ProvidersManager instance].salesProvider.delegate = self;
    [[ProvidersManager instance].salesProvider purchaseCourse:packCourse];
}

- (void)purchaseSuperPack {
    PackCourse *packCourse = [[PackCourse alloc] init];
    [packCourse addCourses:[UserSession instance].allCourses];
    packCourse.isSuperPack = YES;
    packCourse.inAppId = ID_COURSE_SUPER_PACK;

    [ProvidersManager instance].salesProvider.delegate = self;
    [[ProvidersManager instance].salesProvider purchaseCourse:packCourse];
}

- (void)coursesCheckoutStarted {
    self.tableCategories.hidden = YES;
    self.activityIndicator.hidden = NO;

    [AlertDialogView showDialog:NSLocalizedString(@"successful_courses_update", @"")];
}

- (void)coursesCheckoutFailed:(NSString *)message {
    [AlertDialogView showDialog:message];
}

- (void)coursesCheckoutCompleted:(BusinessError *)error andData:(NSDictionary *)data {
    if (error) {
        [AlertDialogView showDialog:error.message];
    }
}

- (void)coursesPurchaseStarted {
    [MRProgressOverlayView showOverlayAddedTo:self.tabBarController.navigationController.view
                                        title:NSLocalizedString(@"buying_course", @"")
                                         mode:MRProgressOverlayViewModeIndeterminate
                                     animated:YES];
}

- (void)coursesPurchaseFailed:(NSString *)message {
    [MRProgressOverlayView dismissOverlayForView:self.tabBarController.navigationController.view
                                        animated:YES];
    [AlertDialogView showDialog:message];
}

- (void)coursesPurchaseCancelled {
    [MRProgressOverlayView dismissOverlayForView:self.tabBarController.navigationController.view
                                        animated:YES];
}

- (void)coursesPurchaseCompleted {
    [MRProgressOverlayView dismissOverlayForView:self.tabBarController.navigationController.view
                                        animated:YES];
}

@end
