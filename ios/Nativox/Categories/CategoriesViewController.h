#import <UIKit/UIKit.h>
#import "CourseViewController.h"

@class PackCourse;
@class SKProduct;
@class CustomIOSAlertView;
@class CategoryCourse;
@class BaseViewController;

@protocol CategoriesViewControllerDelegate <NSObject>

- (void)purchaseButtonPressed:(id)categoryData;

@end

@interface CategoriesViewController : BaseViewController <UIAlertViewDelegate, CategoriesViewControllerDelegate>

- (void)purchaseCategory:(CategoryCourse *)category;

@end
