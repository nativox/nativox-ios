
#import <UIKit/UIKit.h>
#import "UserProvider.h"
#import "RegisterViewController.h"
#import "BaseViewController.h"
#import "MPCoachMarks.h"

@class UserProvider;
@class BusinessError;
@class DeviceUtils;

@interface LoginViewController : BaseViewController <UITextFieldDelegate, UIAlertViewDelegate> {

}

- (void)presentDashBoardController;

@end
