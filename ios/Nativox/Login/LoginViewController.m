#import <AVFoundation/AVFoundation.h>
#import "LoginViewController.h"
#import "MRProgress.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "ProvidersManager.h"
#import "NotificationChannelSubscriber.h"
#import "UserSession.h"
#import "User.h"
#import "BusinessError.h"
#import "Response.h"
#import "AlertDialogView.h"
#import "FeedbackSender.h"
#import "ReactNativeView.h"

@interface LoginViewController ()
@property(nonatomic, strong) NotificationChannelSubscriber *notificationChannelSubscriber;
@property(strong, nonatomic) IBOutlet ReactNativeView *reactNativeView;
@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.notificationChannelSubscriber = [[NotificationChannelSubscriber alloc] init];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(onSignUpButtonPressed:)
                                                 name:@"PressedSignUpButton"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(onLoginComplete:)
                                                 name:@"OnLoginComplete"
                                               object:nil];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [[AVAudioSession sharedInstance] requestRecordPermission:^(BOOL granted) {
        if (!granted) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [AlertDialogView showDialog:NSLocalizedString(@"request_microphone", @"")];
            });
        }
    }];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    [self.reactNativeView initializeReactView:@"LoginScreen"
                               viewController:self
                                   andOptions:@{}];

    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];

    if (![userDefaults boolForKey:@"hasPresentedRegister"]) {
        RegisterViewController *registerViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"registerView"];
        [self.navigationController pushViewController:registerViewController animated:YES];
    }
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)onLoginComplete:(NSNotification *)userData {
    [[UserSession instance] userLoggedIn:[self mapUserData:userData.object]];
    [self presentDashBoardController];
    [self.notificationChannelSubscriber subscribeToChannel:kUserHasLoggedInChannel];
}

- (User *)mapUserData:(NSDictionary *)dictionary {
    User *user = [[User alloc] init];
    user.token = dictionary[@"token"];
    int idUser = [dictionary[@"id_usuario"] intValue];
    user.idUser = [NSString stringWithFormat:@"%d", idUser];
    user.name = dictionary[@"nombre"];
    user.freeCoursesTime = [dictionary[@"tiempo_restante"] intValue];
    user.email = dictionary[@"email"];
    return user;
}

- (void)onSignUpButtonPressed:(id)onSignUpButtonPressed {
    RegisterViewController *registerViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"registerView"];
    [self.navigationController pushViewController:registerViewController animated:YES];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}

- (void)presentDashBoardController {
    UIViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"homeTabBarController"];
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 1) {
        [MRProgressOverlayView showOverlayAddedTo:self.view
                                            title:NSLocalizedString(@"loading_progress_dialog", @"")
                                             mode:MRProgressOverlayViewModeIndeterminate
                                         animated:YES];
        [[FeedbackSender instance] sendFeedbackEvent:@"Recover Password" andExtraInfo:nil];
        [[ProvidersManager instance].userProvider recoverPassword:[alertView textFieldAtIndex:0].text
                                                       andHandler:^(Response *response, BusinessError *error) {
                                                           [MRProgressOverlayView dismissAllOverlaysForView:self.view
                                                                                                   animated:YES];

                                                           if (!error && [response.status isEqualToString:@"true"]) {
                                                               [AlertDialogView showDialog:response.result];
                                                           } else {
                                                               [AlertDialogView showDialog:error.message];
                                                           }
                                                       }];
    }
}

@end
