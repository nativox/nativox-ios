#import <AVFoundation/AVFoundation.h>
#import "RegisterViewController.h"
#import "AlertDialogView.h"
#import "ReactNativeView.h"
#import "MRProgressOverlayView.h"
#import "BusinessError.h"
#import "UserSession.h"
#import "NotificationChannelSubscriber.h"

@interface RegisterViewController ()
@property(strong, nonatomic) IBOutlet ReactNativeView *reactNativeView;
@property(nonatomic, strong) NotificationChannelSubscriber *notificationChannelSubscriber;
@end

@implementation RegisterViewController {
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    self.navigationController.navigationBar.hidden = YES;
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];

    [self.reactNativeView initializeReactView:@"RegisterScreen"
                               viewController:self
                                   andOptions:@{
                                           @"hasPresentedRegister" : @([userDefaults boolForKey:@"hasPresentedRegister"])
                                   }];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    __block RegisterViewController *registerViewControllerBlock = self;
    ^(User *user, BusinessError *error) {
        [MRProgressOverlayView dismissAllOverlaysForView:registerViewControllerBlock.view.window animated:NO];
        if (!error) {
            [[UserSession instance] userLoggedIn:user];
            [registerViewControllerBlock presentDashBoardController];
            [registerViewControllerBlock.notificationChannelSubscriber subscribeToChannel:kUserHasLoggedInChannel];
        } else {
            [AlertDialogView showDialog:error.message];
        }
    };

    self.notificationChannelSubscriber = [[NotificationChannelSubscriber alloc] init];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];

    if (![userDefaults boolForKey:@"hasPresentedRegister"]) {
        [userDefaults setBool:YES forKey:@"hasPresentedRegister"];
    }

    [[AVAudioSession sharedInstance] requestRecordPermission:^(BOOL granted) {
        if (!granted) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [AlertDialogView showDialog:NSLocalizedString(@"request_microphone", @"")];
            });
        }
    }];
}

- (void)presentDashBoardController {
    UIViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"homeTabBarController"];
    [self.navigationController pushViewController:viewController animated:YES];
}

@end
