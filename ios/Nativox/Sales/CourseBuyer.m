#import <StoreKit/StoreKit.h>
#import "CourseBuyer.h"
#import "Course.h"
#import "NativoxIAHelper.h"

@implementation CourseBuyer {

}

+ (CourseBuyer *)instance {
    static CourseBuyer *_instance = nil;

    @synchronized (self) {
        if (_instance == nil) {
            _instance = [[self alloc] init];
        }
    }

    return _instance;
}

- (void)requestProduct:(Course *)course andDelegate:(BuyCourseCompletionHandler)completionHandler {
    [[NativoxIAHelper sharedInstance] requestProductsWithCompletionHandler:^(BOOL success, NSArray *products) {

        if (success && products.count > 0) {
            SKProduct *product = [self findProduct:products ofCourse:course];
            dispatch_async(dispatch_get_main_queue(), ^{
                completionHandler(YES, product);
            });
        } else {
            dispatch_async(dispatch_get_main_queue(), ^{
                completionHandler(NO, nil);
            });
        }
    }];
}

//TODO:Implement a better way to look for SKProduct
- (SKProduct *)findProduct:(NSArray *)array ofCourse:(Course *)course {
    SKProduct *skProduct;

    for (SKProduct *product in array) {
        if ([product.productIdentifier isEqualToString:course.inAppId]) {
            skProduct = product;
            break;
        }
    }
    return skProduct;
}

@end