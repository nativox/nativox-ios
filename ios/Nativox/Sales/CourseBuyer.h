//
// Created by redhair84 on 03/09/2014.
// Copyright (c) 2014 Francisco Javier Morant. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Course;
@class SKProduct;

typedef void (^BuyCourseCompletionHandler)(BOOL success, SKProduct *product);

@interface CourseBuyer : NSObject
+ (CourseBuyer *)instance;


- (void)requestProduct:(Course *)course andDelegate:(BuyCourseCompletionHandler)completionHandler;

- (SKProduct *)findProduct:(NSArray *)array ofCourse:(Course *)course;
@end