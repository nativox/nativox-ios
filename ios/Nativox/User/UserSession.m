#import <Crashlytics/Crashlytics/Crashlytics.h>
#import "UserSession.h"
#import "FreeCoursesTimer.h"
#import "User.h"
#import "UserSessionPreferences.h"

static NSString *const kUserToken = @"userToken";
static NSString *const kUserId = @"userId";
static NSString *const kUserEmail = @"userEmail";
static NSString *const kUserName = @"userName";
static NSString *const kStartSession = @"startSession";

@interface UserSession ()
@property(nonatomic, strong) UserSessionPreferences *userSessionPreferences;
@end

@implementation UserSession {
}

- (id)init {
    self = [super init];
    if (self) {
        _userSessionPreferences = [[UserSessionPreferences alloc] init];
        _purchasedCourses = [[NSMutableArray alloc] init];
        _freeCoursesTimer = [[FreeCoursesTimer alloc] init];
        _token = [_userSessionPreferences retrieve:kUserToken];
        _idUser = [_userSessionPreferences retrieve:kUserId];
        _email = [_userSessionPreferences retrieve:kUserEmail];
        _name = [_userSessionPreferences retrieve:kUserName];
        _startSession = [[_userSessionPreferences retrieve:kStartSession] doubleValue];
    }

    return self;
}

+ (UserSession *)instance {
    static UserSession *_instance = nil;

    @synchronized (self) {
        if (_instance == nil) {
            _instance = [[self alloc] init];
        }
    }

    return _instance;
}

- (void)resetSession {
    self.token = nil;
    self.idUser = nil;
    self.categories = nil;
    self.email = nil;
    self.name = nil;
    self.startSession = 0;
    [self.freeCoursesTimer setCountDownTime:0];
    [self.purchasedCourses removeAllObjects];

    [self.userSessionPreferences deleteValue:kUserToken];
    [self.userSessionPreferences deleteValue:kUserId];
    [self.userSessionPreferences deleteValue:kUserEmail];

    [self.userSessionPreferences commit];
}

- (void)generateAllCourses:(NSArray *)categories {
    self.categories = [categories mutableCopy];
    self.allCourses = [[NSMutableArray alloc] init];
    [self.purchasedCourses removeAllObjects];

    for (NSMutableDictionary *category in self.categories) {
        [self.allCourses addObjectsFromArray:category[@"cursos"]];
        for (NSMutableDictionary *course in category[@"cursos"]) {
            if (course[@"comprado"] && ![course[@"demo"] isEqualToString:@"1"]) {
                [self.purchasedCourses addObject:course];
            }
        }
    }

    [[NSNotificationCenter defaultCenter] postNotificationName:@"initialDataLoadHasFinished" object:nil];
    [UserSession instance].coursesLoaded = YES;
}

- (void)userLoggedIn:(User *)user {
    self.token = user.token;
    self.idUser = user.idUser;
    self.name = user.name;
    self.email = user.email;
    self.startSession = [NSDate date].timeIntervalSince1970;

    [CrashlyticsKit setUserIdentifier:user.idUser];
    [CrashlyticsKit setUserEmail:user.email];
    [CrashlyticsKit setUserName:user.name];

    [self.userSessionPreferences insert:user.token andKey:kUserToken];
    [self.userSessionPreferences insert:user.idUser andKey:kUserId];
    [self.userSessionPreferences insert:self.email andKey:kUserEmail];
    [self.userSessionPreferences insert:self.name andKey:kUserName];
    [self.userSessionPreferences insert:[NSString stringWithFormat:@"%lf", self.startSession] andKey:kStartSession];

    [self.userSessionPreferences commit];
}

- (void)reloadUserSession:(NSDictionary *)data {
    if (data) {
        self.name = data[@"info_usuario"][@"nombre"];
        self.email = data[@"info_usuario"][@"correo"];
        [self.freeCoursesTimer setCountDownTime:[data[@"info_usuario"][@"tiempo_restante"] intValue]];
    }
}

- (void)setLanguage:(NSString *)language {
    [self.userSessionPreferences insert:language andKey:[NSString stringWithFormat:@"%@_lang", self.email]];
    [self.userSessionPreferences commit];
}

- (NSString *)language {
    NSString *savedLanguage = [self.userSessionPreferences retrieve:[NSString stringWithFormat:@"%@_lang", self.email]];
    NSString *language = savedLanguage && ![savedLanguage isEqualToString:@""] ? savedLanguage : [[NSLocale preferredLanguages][0] componentsSeparatedByString:@"-"][0];
    return language;
}

- (NSString *)localeLanguage {
    NSString *savedLanguage = [self.userSessionPreferences retrieve:[NSString stringWithFormat:@"%@_lang", self.email]];
    NSString *language = savedLanguage && ![savedLanguage isEqualToString:@""] ? [savedLanguage componentsSeparatedByString:@"-"][0] : [[NSLocale preferredLanguages][0] componentsSeparatedByString:@"-"][0];

    return language;
}

- (BOOL)isUserLoggedIn {
    return self.token != nil;
}

- (CategoryCourse *)findCategory:(NSString *)idCategory {
    for (CategoryCourse *category in self.categories) {
        if ([category.idCategory isEqualToString:idCategory]) {
            return category;
        }
    }

    return nil;
}
@end
