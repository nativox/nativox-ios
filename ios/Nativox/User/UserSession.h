#import <Foundation/Foundation.h>
#import "CategoryCourse.h"

@class Course;
@class FreeCoursesTimer;
@class User;

@interface UserSession : NSObject

@property(nonatomic, strong) NSString *password;
@property(nonatomic, strong) NSString *token;
@property(nonatomic, strong) NSString *idUser;
@property(nonatomic, strong) NSString *email;
@property(nonatomic, strong) NSString *name;
@property(nonatomic, strong) NSString *language;
@property(nonatomic, strong) NSMutableArray *categories;
@property(nonatomic, strong) NSMutableArray *purchasedCourses;
@property(nonatomic, strong) FreeCoursesTimer *freeCoursesTimer;
@property(nonatomic, strong) NSMutableArray *allCourses;
@property(nonatomic) NSTimeInterval startSession;

@property(nonatomic) BOOL coursesLoaded;

+ (UserSession *)instance;

- (void)resetSession;

- (void)generateAllCourses:(NSArray *)categories;

- (void)userLoggedIn:(User *)user;

- (void)reloadUserSession:(NSDictionary *)data;

- (BOOL)isUserLoggedIn;

- (NSString *)localeLanguage;

- (CategoryCourse *)findCategory:(NSString *)idCategory;
@end