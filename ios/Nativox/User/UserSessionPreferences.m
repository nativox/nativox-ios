//
// Created by redhair84 on 03/04/15.
// Copyright (c) 2015 Francisco Javier Morant. All rights reserved.
//

#import "UserSessionPreferences.h"
#import "SSKeychainQuery.h"
#import "SSKeychain.h"

@interface UserSessionPreferences ()

@property(nonatomic, strong) NSMutableDictionary *userDefaults;
@end

@implementation UserSessionPreferences {

}

- (instancetype)init {
    self = [super init];
    if (self) {
    }

    return self;
}

- (void)insert:(NSString *)value andKey:(NSString *)key {
    if (value) {
        [SSKeychain setPassword:value forService:@"user" account:key];
    }
}

- (id)retrieve:(NSString *)key {
    return [SSKeychain passwordForService:@"user" account:key];
}

- (void)deleteValue:(NSString *)key {
    [SSKeychain deletePasswordForService:@"user" account:key];
}

- (void)commit {
}

@end