//
// Created by redhair84 on 03/04/15.
// Copyright (c) 2015 Francisco Javier Morant. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SSKeychainQuery;


@interface UserSessionPreferences : NSObject

- (void)insert:(NSString *)value andKey:(NSString *)key;

- (id)retrieve:(NSString *)key;

- (void)deleteValue:(NSString *)key;

- (void)commit;

@end