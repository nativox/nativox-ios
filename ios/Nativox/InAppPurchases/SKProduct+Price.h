//
// Created by redhair84 on 06/01/15.
// Copyright (c) 2015 Francisco Javier Morant. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <StoreKit/StoreKit.h>

@interface SKProduct (Price)

- (NSString *)priceProduct;

@end