#import "IAPHelper.h"
#import "SKProduct+Price.h"
#import "Answers.h"
#import "UserSession.h"

NSString *const IAPHelperProductPurchasedNotification = @"IAPHelperProductPurchasedNotification";
NSString *const IAPHelperProductPurchaseErrorNotification = @"IAPHelperProductPurchaseErrorNotification";
NSString *const IAPHelperProductPurchaseCancelNotification = @"IAPHelperProductPurchaseCancelNotification";
NSString *const IAPHelperProductRestoredNotification = @"IAPHelperProductRestoredNotification";
NSString *const IAPHelperProductRestoredFailedNotification = @"IAPHelperProductRestoredFailedNotification";


@interface IAPHelper () <SKProductsRequestDelegate, SKPaymentTransactionObserver>
@property(nonatomic, strong) SKProduct *selectedProduct;
@end

@implementation IAPHelper {
    SKProductsRequest *_productsRequest;
    RequestProductsCompletionHandler _completionHandler;
    NSSet *_productIdentifiers;
    NSMutableSet *_purchasedProductIdentifiers;
}

- (id)initWithProductIdentifiers:(NSSet *)productIdentifiers {

    if ((self = [super init])) {

        _productIdentifiers = productIdentifiers;
        _purchasedProductIdentifiers = [NSMutableSet set];
        for (NSString *productIdentifier in _productIdentifiers) {
            BOOL productPurchased = [[NSUserDefaults standardUserDefaults] boolForKey:productIdentifier];
            if (productPurchased) {
                [_purchasedProductIdentifiers addObject:productIdentifier];
                DLog(@"Previously purchased: %@", productIdentifier);
            }
        }
    }

    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    return self;
}

- (void)requestProductsWithCompletionHandler:(RequestProductsCompletionHandler)completionHandler {
    _completionHandler = [completionHandler copy];
    _productsRequest = [[SKProductsRequest alloc] initWithProductIdentifiers:_productIdentifiers];
    _productsRequest.delegate = self;
    [_productsRequest start];
}

#pragma mark - SKProductsRequestDelegate

- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response {
    _productsRequest = nil;
    if (_completionHandler) {
        _completionHandler(YES, response.products);
    }

    _completionHandler = nil;

}

- (void)request:(SKRequest *)request didFailWithError:(NSError *)error {

    DLog(@"Failed to load list of products. Error: %@", error.localizedDescription);
    _productsRequest = nil;
    if (_completionHandler) {
        _completionHandler(NO, nil);
    }

    _completionHandler = nil;
}

- (void)buyProduct:(SKProduct *)product withQuantity:(int)quantity {
    DLog(@"Buying %@...", product.productIdentifier);

    self.selectedProduct = product;
    SKMutablePayment *payment = [SKMutablePayment paymentWithProduct:product];
    payment.quantity = quantity;
    [[SKPaymentQueue defaultQueue] addPayment:payment];
}

- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions {
    for (SKPaymentTransaction *transaction in transactions) {
        switch (transaction.transactionState) {
            case SKPaymentTransactionStatePurchased:
                [self completeTransaction:transaction];
                break;
            case SKPaymentTransactionStateFailed:
                [self failedTransaction:transaction];
                break;
            case SKPaymentTransactionStateRestored:
                [self restoreTransaction:transaction];
            default:
                break;
        }
    };
}


- (void)completeTransaction:(SKPaymentTransaction *)transaction {
    DLog(@"completeTransaction...");

    [self reportTransaction:transaction];
    [self provideContentForProductIdentifier:transaction];
    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
}

- (void)restoreTransaction:(SKPaymentTransaction *)transaction {
    DLog(@"restoreTransaction...");
    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
}

- (void)failedTransaction:(SKPaymentTransaction *)transaction {
    DLog(@"failedTransaction...");

    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];

    if (transaction.error.code != SKErrorPaymentCancelled) {
        DLog(@"Transaction error: %@", transaction.error.localizedDescription);
        [[NSNotificationCenter defaultCenter] postNotificationName:IAPHelperProductPurchaseErrorNotification object:nil userInfo:nil];
    } else {
        [[NSNotificationCenter defaultCenter] postNotificationName:IAPHelperProductPurchaseCancelNotification object:nil userInfo:nil];
    }
}

- (void)provideContentForProductIdentifier:(SKPaymentTransaction *)transaction {
    [_purchasedProductIdentifiers addObject:transaction.payment.productIdentifier];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:transaction.payment.productIdentifier];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[NSNotificationCenter defaultCenter] postNotificationName:IAPHelperProductPurchasedNotification
                                                        object:transaction userInfo:nil];
}

- (void)paymentQueueRestoreCompletedTransactionsFinished:(SKPaymentQueue *)queue {
    [[NSNotificationCenter defaultCenter] postNotificationName:IAPHelperProductRestoredNotification object:nil userInfo:nil];
}

- (void)paymentQueue:(SKPaymentQueue *)queue restoreCompletedTransactionsFailedWithError:(NSError *)error {
    [[NSNotificationCenter defaultCenter] postNotificationName:IAPHelperProductRestoredFailedNotification object:nil userInfo:nil];
}

- (void)reportTransaction:(SKPaymentTransaction *)transaction {
    NSMutableDictionary *customAttributes = [[NSMutableDictionary alloc] initWithDictionary:@{
            @"idUser" : [UserSession instance].idUser ? [UserSession instance].idUser : @"",
            @"email" : [UserSession instance].email ? [UserSession instance].email : @"",
            @"language" : [UserSession instance].language ? [UserSession instance].language : @""
    }];
    [Answers logPurchaseWithPrice:self.selectedProduct.price
                         currency:[self.selectedProduct priceProduct]
                          success:@YES
                         itemName:self.selectedProduct.localizedTitle
                         itemType:self.selectedProduct.description
                           itemId:transaction.payment.productIdentifier
                 customAttributes:customAttributes];
}

@end
