#import <Foundation/Foundation.h>
#import <StoreKit/StoreKit.h>

@class SKProduct;
UIKIT_EXTERN NSString *const IAPHelperProductPurchasedNotification;
UIKIT_EXTERN NSString *const IAPHelperProductRestoredNotification;
UIKIT_EXTERN NSString *const IAPHelperProductPurchaseErrorNotification;
UIKIT_EXTERN NSString *const IAPHelperProductPurchaseCancelNotification;
UIKIT_EXTERN NSString *const IAPHelperProductRestoredFailedNotification;

typedef void (^RequestProductsCompletionHandler)(BOOL success, NSArray *products);

@interface IAPHelper : NSObject

- (id)initWithProductIdentifiers:(NSSet *)productIdentifiers;

- (void)requestProductsWithCompletionHandler:(RequestProductsCompletionHandler)completionHandler;

- (void)buyProduct:(SKProduct *)product withQuantity:(int)quantity;

@end
