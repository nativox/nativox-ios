#import "SKProduct+Price.h"

@implementation SKProduct (Price)

- (NSString *)priceProduct {
        NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
        [formatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
        [formatter setNumberStyle:NSNumberFormatterCurrencyStyle];
        [formatter setLocale:[self priceLocale]];

        NSString *priceString = [formatter stringFromNumber:[self price]];
        return priceString;
}


@end
