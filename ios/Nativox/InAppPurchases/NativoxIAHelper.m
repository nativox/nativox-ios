#import "NativoxIAHelper.h"

@implementation NativoxIAHelper

+ (NativoxIAHelper *)sharedInstance {
        static dispatch_once_t once;
        static NativoxIAHelper *sharedInstance;
        dispatch_once(&once, ^{
                NSSet *productIdentifiers = [NSSet setWithObjects:ID_COURSE_BUSINESS_PACK, ID_COURSE_COMMON_PACK,
                                             ID_COURSE_SUPER_PACK, nil];
                sharedInstance = [[self alloc] initWithProductIdentifiers:productIdentifiers];
        });
        return sharedInstance;
}

@end
