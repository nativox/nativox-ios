#import "IAPHelper.h"

static NSString *const ID_COURSE_BUSINESS_PACK = @"id_business_english_pack";
static NSString *const ID_COURSE_COMMON_PACK = @"id_common_pack";
static NSString *const ID_COURSE_SUPER_PACK = @"id_super_pack";

@interface NativoxIAHelper : IAPHelper

+ (NativoxIAHelper *)sharedInstance;

@end
