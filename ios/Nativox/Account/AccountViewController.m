
#import "AccountViewController.h"
#import "UserSession.h"
#import "AlertDialogView.h"
#import "FeedbackSender.h"
#import "ReactNativeView.h"

@interface AccountViewController ()
@property(strong, nonatomic) IBOutlet ReactNativeView *reactNativeView;

@end

@implementation AccountViewController

- (void)viewDidLoad {
    [super viewDidLoad];

}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(onLogoutButtonPressed:)
                                                 name:@"LogoutButtonPressed"
                                               object:nil];

    [self.reactNativeView initializeReactView:@"UpdateAccountScreen"
                               viewController:self
                                   andOptions:@{}];

}

- (void)onLogoutButtonPressed:(id)object {
    [self attemptLogoutUser];
    [[FeedbackSender instance] sendFeedbackEvent:@"Logout Button Pressed" andExtraInfo:nil];
}

- (void)attemptLogoutUser {
    if ([UserSession instance].idUser) {
        [self logoutUser];
    } else {
        [self moveBack];
    }
}

- (void)logoutUser {
    [[UserSession instance] resetSession];
    [self moveBack];
}

- (void)moveBack {
    [self.tabBarController.navigationController popToRootViewControllerAnimated:YES];
}

@end
