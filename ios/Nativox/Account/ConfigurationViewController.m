#import "ConfigurationViewController.h"
#import "NativoxAppDelegate.h"
#import "CustomIOSAlertView.h"
#import "AlarmReminderDialog.h"
#import "DeviceUtils.h"
#import "ProvidersManager.h"
#import "BusinessError.h"
#import "AlarmReminder.h"
#import "UserSession.h"
#import "LanguageResolver.h"
#import "FeedbackSender.h"

@interface ConfigurationViewController ()
@property(strong, nonatomic) IBOutlet UISwitch *showSentenceAfterPlayedVideoSwitch;
@property(strong, nonatomic) IBOutlet UILabel *alarmReminderLabel;
@property(strong, nonatomic) IBOutlet UILabel *languageLabel;
@property(strong, nonatomic) IBOutlet UIButton *changeAlarmsButton;
@property(strong, nonatomic) IBOutlet UIButton *changeLanguageButton;

@end

@implementation ConfigurationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.alarmReminderLabel.text = NSLocalizedString(@"settings_alarm_training_label", @"");
    self.languageLabel.text = NSLocalizedString(@"settings_language_label", @"");
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = NO;

    [self.changeAlarmsButton setTitle:[AlarmReminder instance].formatSelectedTime forState:UIControlStateNormal];
    NSString *selectLanguageTitle = [[LanguageResolver instance] languageTitle:[UserSession instance].language];
    [self.changeLanguageButton setTitle:selectLanguageTitle forState:UIControlStateNormal];
    [self.showSentenceAfterPlayedVideoSwitch setOn:[[[NSUserDefaults standardUserDefaults] objectForKey:kShowSentenceAfterPlayedVideo] boolValue] animated:YES];
}

- (IBAction)showSentenceHasChanged:(id)sender {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setBool:self.showSentenceAfterPlayedVideoSwitch.on forKey:kShowSentenceAfterPlayedVideo];
    [[ProvidersManager instance].userProvider saveUserOptions:^(BusinessError *error) {
        if (error) {
            DLog(@"%@", error.message);
        }
    }];

    [[FeedbackSender instance] sendFeedbackEvent:@"Changed Show Sentence After Video Played" andExtraInfo:@{
            @"showSentenceAfterPlayedVideo" : @(self.showSentenceAfterPlayedVideoSwitch.on)
    }];

}

- (IBAction)selectTimeHasBeenPressed:(id)sender {
    CustomIOSAlertView *alarmReminderDialogAlertView = [[CustomIOSAlertView alloc] init];
    NSString *alarmReminderDialogName = [DeviceUtils instance].isOnIpad ? @"AlarmReminderDialog_ipad" : @"AlarmReminderDialog";
    AlarmReminderDialog *alarmReminderDialog = [[[NSBundle mainBundle] loadNibNamed:alarmReminderDialogName owner:nil options:nil] lastObject];
    alarmReminderDialog.completionBlock = ^{
        [self.changeAlarmsButton setTitle:[AlarmReminder instance].formatSelectedTime forState:UIControlStateNormal];
    };

    alarmReminderDialog.alertViewContainer = alarmReminderDialogAlertView;
    [alarmReminderDialogAlertView setContainerView:alarmReminderDialog];
    [alarmReminderDialogAlertView setButtonTitles:NULL];
    [alarmReminderDialogAlertView setUseMotionEffects:YES];
    [alarmReminderDialogAlertView show];
}

- (IBAction)selectLanguageButtonPressed:(id)sender {
    CustomIOSAlertView *dialogAlertView = [[CustomIOSAlertView alloc] init];
    DataPickerDialog *dataPickerDialog = [[[NSBundle mainBundle] loadNibNamed:@"DataPickerDialog" owner:nil options:nil] lastObject];
    dataPickerDialog.dialogAlerView = dialogAlertView;
    dataPickerDialog.delegate = self;
    dataPickerDialog.selectedData = [UserSession instance].language;
    [dataPickerDialog setSelectedOption];

    [dialogAlertView setContainerView:dataPickerDialog];
    [dialogAlertView setButtonTitles:NULL];
    [dialogAlertView setUseMotionEffects:YES];
    dialogAlertView.alphaContainer = 1;
    [dialogAlertView show];
}

- (void)onDataSelected:(NSString *)valueSelected withLanguageCode:(NSString *)code {
    [UserSession instance].language = code;
    NSString *selectLanguageTitle = [UserSession instance].language ?
            [[LanguageResolver instance] languageTitle:[UserSession instance].language] :
            NSLocalizedString(@"settings_alarm_select_button", @"");
    [self.changeLanguageButton setTitle:selectLanguageTitle forState:UIControlStateNormal];
    [self retrieveCategories];
    [[FeedbackSender instance] sendFeedbackEvent:@"Changed language" andExtraInfo:@{
            @"language" : selectLanguageTitle
    }];
}

@end
