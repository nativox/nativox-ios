#import "AlarmReminderDialog.h"
#import "CustomIOSAlertView.h"
#import "TimePickerDialog.h"
#import "AlarmReminderInteractor.h"
#import "Alarm.h"
#import "AlarmViewModel.h"
#import "UIColor+NativoxColors.h"
#import "DeviceUtils.h"
#import "AlertDialogView.h"
#import "AlarmReminder.h"

@interface AlarmReminderDialog ()

@property(strong, nonatomic) IBOutlet UIButton *cancelAllAlarmButton;
@property(strong, nonatomic) IBOutlet UIImageView *backgroundApp;
@property(strong, nonatomic) IBOutlet UIButton *mondayButton;
@property(strong, nonatomic) IBOutlet UIButton *tuesdayButton;
@property(strong, nonatomic) IBOutlet UIButton *wednesdayButton;
@property(strong, nonatomic) IBOutlet UIButton *thursdayButton;
@property(strong, nonatomic) IBOutlet UIButton *fridayButton;
@property(strong, nonatomic) IBOutlet UIButton *saturdayButton;
@property(strong, nonatomic) IBOutlet UIButton *sundayButton;
@property(strong, nonatomic) IBOutlet UIButton *selectTimeButton;
@property(strong, nonatomic) IBOutlet UIButton *setAlarmsButton;
@property(strong, nonatomic) IBOutlet UILabel *alarmReminderExplanationLabel;
@property(strong, nonatomic) IBOutlet UILabel *alarmReminderWhatTimeLabel;

@property(retain) id eventHandler;

@end

@implementation AlarmReminderDialog

- (void)awakeFromNib {
    [super awakeFromNib];

    self.backgroundApp.layer.cornerRadius = 5;
    self.backgroundApp.clipsToBounds = YES;

    self.alarmReminderExplanationLabel.text = NSLocalizedString(@"alarm_reminder_explanation", @"");
    self.alarmReminderWhatTimeLabel.text = NSLocalizedString(@"alarm_reminder_hour_remind", @"");
    [self.setAlarmsButton setTitle:NSLocalizedString(@"alarm_reminder_set_alarms_button", @"") forState:UIControlStateNormal];
    [self.cancelAllAlarmButton setTitle:NSLocalizedString(@"alarm_reminder_cancel_alarms", @"") forState:UIControlStateNormal];

    [self.mondayButton setTitle:NSLocalizedString(@"alarm_reminder_monday_label", @"") forState:UIControlStateNormal];
    [self.tuesdayButton setTitle:NSLocalizedString(@"alarm_reminder_tuesday_label", @"") forState:UIControlStateNormal];
    [self.wednesdayButton setTitle:NSLocalizedString(@"alarm_reminder_wednesday_label", @"") forState:UIControlStateNormal];
    [self.thursdayButton setTitle:NSLocalizedString(@"alarm_reminder_thursday_label", @"") forState:UIControlStateNormal];
    [self.fridayButton setTitle:NSLocalizedString(@"alarm_reminder_friday_label", @"") forState:UIControlStateNormal];
    [self.saturdayButton setTitle:NSLocalizedString(@"alarm_reminder_saturday_label", @"") forState:UIControlStateNormal];
    [self.sundayButton setTitle:NSLocalizedString(@"alarm_reminder_sunday_label", @"") forState:UIControlStateNormal];

    AlarmReminderInteractor *alarmReminderInteractor = [[AlarmReminderInteractor alloc] init];
    alarmReminderInteractor.userInterface = self;
    self.eventHandler = alarmReminderInteractor;
    [self.eventHandler screenDidAppear];

    self.cancelAllAlarmButton.enabled = [AlarmReminder instance].activatedAlarms.count > 0;
}

- (IBAction)selectTimePressed:(id)sender {
    [self.eventHandler timePickerPressed];
}

- (IBAction)closeButtonPressed:(id)sender {
    [self.alertViewContainer close];
}

- (IBAction)mondayButtonPressed:(id)sender {
    [self.eventHandler dayOfTheWeekPressed:MONDAY];
}

- (IBAction)tuesdayButtonPressed:(id)sender {
    [self.eventHandler dayOfTheWeekPressed:TUESDAY];
}

- (IBAction)wednesdayButtonPressed:(id)sender {
    [self.eventHandler dayOfTheWeekPressed:WEDNESDAY];
}

- (IBAction)thursdayButtonPressed:(id)sender {
    [self.eventHandler dayOfTheWeekPressed:THURDSDAY];
}

- (IBAction)fridayButtonPressed:(id)sender {
    [self.eventHandler dayOfTheWeekPressed:FRIDAY];
}

- (IBAction)saturdayButtonPressed:(id)sender {
    [self.eventHandler dayOfTheWeekPressed:SATURDAY];
}

- (IBAction)sundayButtonPressed:(id)sender {
    [self.eventHandler dayOfTheWeekPressed:SUNDAY];
}

- (void)showTimePicker:(NSDate *)alarmTime {
    CustomIOSAlertView *alarmReminderDialogAlertView = [[CustomIOSAlertView alloc] init];
    NSString *timePickerDialogName = [DeviceUtils instance].isOnIpad ? @"TimePickerDialog_ipad" : @"TimePickerDialog";
    TimePickerDialog *timerPickerDialog = [[[NSBundle mainBundle] loadNibNamed:timePickerDialogName owner:nil options:nil] lastObject];
    [timerPickerDialog setTime:alarmTime];
    timerPickerDialog.alertViewContainer = alarmReminderDialogAlertView;
    timerPickerDialog.completionBlockSetTime = ^(NSDate *selectedDate) {
        [self.eventHandler timeAlarmSelected:selectedDate];
    };

    [alarmReminderDialogAlertView setContainerView:timerPickerDialog];
    [alarmReminderDialogAlertView setButtonTitles:NULL];
    [alarmReminderDialogAlertView setUseMotionEffects:YES];
    [alarmReminderDialogAlertView show];
}

- (void)dismissAlarmReminderDialog {
    [self.alertViewContainer close];
    if (self.completionBlock) {
        self.completionBlock();
    }
}

- (void)showSelectedDaysAndTime:(NSArray *)alarmViewModels {
    if (alarmViewModels.count > 0) {
        AlarmViewModel *alarmViewModel = alarmViewModels[0];
        [self.selectTimeButton setTitle:alarmViewModel.selectedTime forState:UIControlStateNormal];
    }

    for (AlarmViewModel *alarmViewModel in alarmViewModels) {
        switch (alarmViewModel.alarmDayType) {
            case MONDAY:
                [self.mondayButton setBackgroundColor:[self backgroundColorOfDay:alarmViewModel]];
                break;
            case TUESDAY:
                [self.tuesdayButton setBackgroundColor:[self backgroundColorOfDay:alarmViewModel]];
                break;
            case WEDNESDAY:
                [self.wednesdayButton setBackgroundColor:[self backgroundColorOfDay:alarmViewModel]];
                break;
            case THURDSDAY:
                [self.thursdayButton setBackgroundColor:[self backgroundColorOfDay:alarmViewModel]];
                break;
            case FRIDAY:
                [self.fridayButton setBackgroundColor:[self backgroundColorOfDay:alarmViewModel]];
                break;
            case SATURDAY:
                [self.saturdayButton setBackgroundColor:[self backgroundColorOfDay:alarmViewModel]];
                break;
            case SUNDAY:
                [self.sundayButton setBackgroundColor:[self backgroundColorOfDay:alarmViewModel]];
                break;
        }
    }
}

- (void)showAlertMessage:(NSString *)message {
    [AlertDialogView showDialog:message];
}

- (IBAction)saveButtonPressed:(id)sender {
    [self.eventHandler saveButtonPressed];
}

- (IBAction)cancelAlarmsButtonPressed:(id)sender {
    [self.eventHandler cancelAlarmsButtonsPressed];
}

- (UIColor *)backgroundColorOfDay:(AlarmViewModel *)alarmViewModel {
    return alarmViewModel.selectedDay ? [UIColor nativoxBlueColor] : [UIColor lightGrayColor];
}

@end
