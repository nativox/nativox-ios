//
// Created by redhair84 on 06/07/15.
// Copyright (c) 2015 Francisco Javier Morant. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Alarm.h"


@interface AlarmViewModel : NSObject

@property(nonatomic) AlarmDayType alarmDayType;
@property(nonatomic) BOOL selectedDay;
@property(nonatomic, strong) NSString *selectedTime;
@end