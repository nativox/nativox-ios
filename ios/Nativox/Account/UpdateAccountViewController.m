#import "UpdateAccountViewController.h"
#import "AlertDialogView.h"
#import "ReactNativeView.h"

@interface UpdateAccountViewController ()

@property(weak, nonatomic) IBOutlet ReactNativeView *reactNativeView;
@end

@implementation UpdateAccountViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
    [self.reactNativeView initializeReactView:@"UpdateAccountScreen" viewController:self andOptions:nil];
}

@end
