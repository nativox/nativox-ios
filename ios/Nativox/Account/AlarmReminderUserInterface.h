//
// Created by redhair84 on 06/07/15.
// Copyright (c) 2015 Francisco Javier Morant. All rights reserved.
//

#import <Foundation/Foundation.h>

@class AlarmViewModel;

@protocol AlarmReminderUserInterface <NSObject>

- (void)showTimePicker:(NSDate *)alarmTime;

- (void)dismissAlarmReminderDialog;

- (void)showSelectedDaysAndTime:(NSArray *)alarmViewModels;

- (void)showAlertMessage:(NSString *)message;

@end