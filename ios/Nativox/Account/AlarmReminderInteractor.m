#import "AlarmReminderInteractor.h"
#import "AlarmViewModel.h"
#import "AlarmReminder.h"
#import "AlarmReminderUserInterface.h"
#import "BusinessError.h"
#import "ProvidersManager.h"
#import "NativoxAppDelegate.h"
#import "FeedbackSender.h"


@interface AlarmReminderInteractor ()
@property(nonatomic) NSDate *alarmTime;
@property(nonatomic, strong) NSMutableArray *activatedAlarmsTemporary;
@end

@implementation AlarmReminderInteractor

- (void)screenDidAppear {
    self.activatedAlarmsTemporary = [[AlarmReminder instance].activatedAlarms mutableCopy];

    if (self.activatedAlarmsTemporary.count > 0) {
        Alarm *selectedAlarm = self.activatedAlarmsTemporary[0];
        [self saveAlarmTimeWithHour:selectedAlarm.hour andMinute:selectedAlarm.minute];
    }
    [self showDaysAndTime];
}

- (void)showDaysAndTime {
    NSMutableArray *alarmViewModels = [[NSMutableArray alloc] init];

    for (int i = 0; i < 7; i++) {
        AlarmViewModel *alarmViewModel = [[AlarmViewModel alloc] init];
        alarmViewModel.alarmDayType = (AlarmDayType) (i + 1);
        alarmViewModel.selectedTime = [self formatSelectedTime];
        [alarmViewModels addObject:alarmViewModel];
    }

    for (Alarm *alarm in self.activatedAlarmsTemporary) {
        AlarmViewModel *alarmViewModel = alarmViewModels[(NSUInteger) (alarm.day - 1)];
        alarmViewModel.selectedDay = YES;
    }

    [self.userInterface showSelectedDaysAndTime:alarmViewModels];
}

- (NSString *)formatSelectedTime {
    if (self.alarmTime) {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
        return [dateFormatter stringFromDate:self.alarmTime];
    } else {
        return NSLocalizedString(@"alarm_reminder_select_time_button", @"");
    }
}

- (void)dayOfTheWeekPressed:(int)day {
    Alarm *existingAlarm = [self findAlarmIfExists:day];

    if (existingAlarm) {
        [self removeDayAlarm:day];
    } else {
        Alarm *alarm = [[Alarm alloc] init];
        alarm.day = day;
        [self.activatedAlarmsTemporary addObject:alarm];
    }
    [self showDaysAndTime];
}

- (void)timePickerPressed {
    [self.userInterface showTimePicker:self.alarmTime];
}

- (void)timeAlarmSelected:(NSDate *)date {
    self.alarmTime = date;
    [self showDaysAndTime];
}

- (void)saveButtonPressed {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];

    if (![self hasSetAlarmReminder]) {
        [self.userInterface showAlertMessage:NSLocalizedString(@"alarmReminder_no_day_selected", @"")];
    } else if (!self.alarmTime) {
        [self.userInterface showAlertMessage:NSLocalizedString(@"alarmReminder_no_time_selected", @"")];
    } else {
        NSCalendar *calendar = [NSCalendar currentCalendar];
        NSDateComponents *components = [calendar components:(NSCalendarUnitHour | NSCalendarUnitMinute) fromDate:self.alarmTime];
        NSInteger hour = [components hour];
        NSInteger minute = [components minute];
        [[AlarmReminder instance] saveAlarms:self.activatedAlarmsTemporary withHour:hour andMinutes:minute];

        [self.userInterface dismissAlarmReminderDialog];
        [[ProvidersManager instance].userProvider saveUserOptions:^(BusinessError *error) {
            if (error) {
                DLog(@"%@", error.message);
            }
        }];
        [userDefaults setBool:YES forKey:kHasDisplayedAlarmReminderDialog];
        [userDefaults synchronize];

        [[FeedbackSender instance] sendFeedbackEvent:@"Alarm Reminder Set" andExtraInfo:@{
                @"alarms" : [[AlarmReminder instance] printAlarms]
        }];
    }
}

- (void)cancelAlarmsButtonsPressed {
    self.alarmTime = nil;
    [[AlarmReminder instance] removeAllAlarms];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setBool:NO forKey:kHasDisplayedAlarmReminderDialog];
    [userDefaults synchronize];
    [self.userInterface dismissAlarmReminderDialog];
}

- (BOOL)hasSetAlarmReminder {
    return self.activatedAlarmsTemporary.count != 0;
}

- (void)removeDayAlarm:(NSInteger)day {
    NSArray *alarms = [self.activatedAlarmsTemporary copy];
    for (Alarm *alarm in alarms) {
        if (alarm.day == day) {
            [self.activatedAlarmsTemporary removeObject:alarm];
            break;
        }
    }
}

- (Alarm *)findAlarmIfExists:(NSInteger)day {
    NSArray *filteredAlarms = [self.activatedAlarmsTemporary copy];
    filteredAlarms = [filteredAlarms filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:[NSString stringWithFormat:@"day == %ld", (long) day]]];
    return filteredAlarms.count > 0 ? filteredAlarms[0] : nil;
}

- (void)saveAlarmTimeWithHour:(NSInteger)hour andMinute:(NSInteger)minute {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:NSHourCalendarUnit | NSMinuteCalendarUnit
                                               fromDate:[NSDate date]];
    [components setHour:hour];
    [components setMinute:minute];

    self.alarmTime = [calendar dateFromComponents:components];
}

@end
