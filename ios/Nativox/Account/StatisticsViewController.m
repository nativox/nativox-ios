//
//  StatisticsViewController.m
//  Nativox
//
//  Created by redhair84 on 12/01/2015.
//  Copyright (c) 2015 Francisco Javier Morant. All rights reserved.
//

#import "StatisticsViewController.h"
#import "UserSession.h"

@interface StatisticsViewController ()
@property(strong, nonatomic) IBOutlet UIWebView *statisticsWebView;
@property(strong, nonatomic) IBOutlet UIView *anonymousView;
@property(strong, nonatomic) IBOutlet UIButton *loginBtn;

@end

@implementation StatisticsViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self setupStatisticsWebView];

    self.anonymousView.hidden = [UserSession instance].idUser != nil;
    self.loginBtn.layer.cornerRadius = 5;
}

- (void)setupStatisticsWebView {
    NSString *userId = [UserSession instance].idUser;
    NSString *token = [UserSession instance].token;
    NSString *size = [self statisticsSize];
    NSString *url = [NSString stringWithFormat:@"http://nativox.com/index"
                                                       ".php/api/stats_usuario?id_usuario=%@&token=%@&tamanyo=%@",
                                               userId, token, size];
    [self.statisticsWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:url]]];
}

- (NSString *)statisticsSize {
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
        return @"tablet";
    } else {
        return @"movil";
    }
}

@end
