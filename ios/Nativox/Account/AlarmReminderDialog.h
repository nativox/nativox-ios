//
//  AlarmReminderDialog.h
//  Nativox
//
//  Created by redhair84 on 06/07/2015.
//  Copyright (c) 2015 Francisco Javier Morant. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AlarmReminderUserInterface.h"

@class CustomIOSAlertView;

@interface AlarmReminderDialog : UIView <AlarmReminderUserInterface>

@property(nonatomic, strong) CustomIOSAlertView *alertViewContainer;
@property(nonatomic, copy) void (^completionBlock)();
@end
