#import <Foundation/Foundation.h>
#import "EventHandler.h"

@protocol AlarmReminderEventHandler <EventHandler>

- (void)dayOfTheWeekPressed:(int)day;

- (void)timePickerPressed;

- (void)timeAlarmSelected:(NSDate *)date;

- (void)saveButtonPressed;

- (void)cancelAlarmsButtonsPressed;
@end