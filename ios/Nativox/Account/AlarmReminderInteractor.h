#import <Foundation/Foundation.h>
#import "AlarmReminderEventHandler.h"

static NSString *const kHasDisplayedAlarmReminderDialog = @"hasDisplayedShowAlarmReminderDialog";

@protocol AlarmReminderUserInterface;

@interface AlarmReminderInteractor : NSObject <AlarmReminderEventHandler>

@property(retain) id userInterface;

@end