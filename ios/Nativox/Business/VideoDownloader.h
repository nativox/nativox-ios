//
// Created by redhair84 on 11/03/2014.
// Copyright (c) 2014 Francisco Javier Morant. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol FileDownloadManagerDelegate

- (void)downloadFileFinished:(BOOL)status withDestinationURL:(NSString *)url andError:(NSError *)error;

- (void)downloadVideoCompletedWithError:(NSError *)error;

- (void)progressDownload:(int64_t)bytesWritten
        withTotalWritten:(int64_t)totalBytesWritten
        andTotalExpected:(int64_t)totalBytesExpected;

- (void)resumeDownloadWithOffset:(int64_t)offset andTotalExpected:(int64_t)totalBytesExpected;
@end

@interface VideoDownloader : NSObject <NSURLSessionDelegate, NSURLSessionTaskDelegate, NSURLSessionDownloadDelegate>


@property(weak, nonatomic) id <FileDownloadManagerDelegate> fileManagerDelegate;

- (instancetype)initWithVideoManager;

- (void)startDownload:(NSString *)urlFile;

- (void)invalidateSession;

@end