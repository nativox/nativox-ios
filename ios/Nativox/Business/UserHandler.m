//
// Created by redhair84 on 23/01/2014.
// Copyright (c) 2014 Francisco Javier Morant. All rights reserved.
//

#import "UserHandler.h"
#import "BusinessError.h"
#import "Response.h"
#import "NativoxAppDelegate.h"
#import "User.h"
#import "HttpHandler.h"
#import "UserSession.h"

#define kResponseResult @"result"
#define kResponseToken @"token"
#define kResponseIdUser @"id_usuario"
#define kResponseName @"name"
#define kResponseStatus @"status"
#define kResponseMessage @"mensaje"

@implementation UserHandler {

}


+ (void)loginWithEmail:(NSString *)email
           andPassword:(NSString *)password
            andHandler:(void (^)(User *, BusinessError *))handler {


    [HttpHandler sendPostRequest:@{
             @"correo" : email,
             @"password" : password
     }
                         withUrl:[NSString stringWithFormat:@"%@%@", kUrlHostname, kUrlServiceLogin]
                      andHandler:^(NSDictionary *resDictionary, NSError *error) {
                          if (!error) {

                              if ([resDictionary[kResponseStatus] isEqualToString:@"true"]) {
                                  NSDictionary *resultDict = resDictionary[kResponseResult];
                                  User *user = [[User alloc] init];
                                  user.token = resultDict[kResponseToken];
                                  user.idUser = resultDict[kResponseIdUser];
                                  user.name = resultDict[kResponseName];
                                  handler(user, nil);
                              } else {
                                  handler(nil,
                                          [BusinessError errorWithCode:@"01" message:resDictionary[kResponseResult]]);
                              }

                          } else {
                              handler(nil,
                                      [BusinessError errorWithCode:@"01"
                                                           message:NSLocalizedString(@"error_login",
                                                                                     @"Hubo un error logueandote, por favor intentalo más tarde.")]);
                          }
                      }];
}

+ (void)loginWithFB:(NSString *)email
           userName:(NSString *)userName
         idFacebook:(NSString *)idFacebook
            fbToken:(NSString *)fbToken
            andName:(NSString *)name
         andHandler:(void (^)(User *, BusinessError *))handler {
    [HttpHandler sendPostRequest:@{
             @"correo" : email,
             @"nombre_usuario" : userName,
             @"id_facebook" : idFacebook,
             @"nombre" : name,
             @"access_token" : fbToken
     }
                         withUrl:[NSString stringWithFormat:@"%@%@", kUrlHostname, kUrlServiceLoginFb]
                      andHandler:^(NSDictionary *resDictionary, NSError *error) {
                          if (!error) {

                              if ([resDictionary[kResponseStatus] isEqualToString:@"true"]) {
                                  NSDictionary *resultDict = resDictionary[kResponseResult];
                                  User *user = [[User alloc] init];
                                  user.token = resultDict[kResponseToken];
                                  user.idUser = resultDict[kResponseIdUser];
                                  user.name = resultDict[kResponseName];
                                  handler(user, nil);
                              } else {
                                  handler(nil,
                                          [BusinessError errorWithCode:@"01" message:resDictionary[kResponseResult]]);
                              }

                          } else {
                              handler(nil,
                                      [BusinessError errorWithCode:@"01"
                                                           message:NSLocalizedString(@"error_login_facebook",
                                                                                     @"Error logueandote através de facebook. Por favor intentalo más tarde.")]);
                          }
                      }];
}

+ (void)registerUserWithEmail:(NSString *)email
                     withName:(NSString *)name
                 withPassword:(NSString *)password
                 andRegSource:(NSString *)regSource
                   andHandler:(void (^)(Response *, BusinessError *))handler {

    [HttpHandler sendPostRequest:@{
             @"correo" : email,
             @"nombre" : name,
             @"password" : password
     }
                         withUrl:[NSString stringWithFormat:@"%@%@", kUrlHostname, kUrlServiceRegister]
                      andHandler:^(NSDictionary *resDictionary, NSError *error) {
                          if (!error) {
                              if ([resDictionary[kResponseStatus] isEqualToString:@"true"]) {
                                  handler([Response responseWithResult:resDictionary[kResponseMessage]
                                                                status:resDictionary[kResponseStatus]], nil);
                              } else {
                                  handler(nil,
                                          [BusinessError errorWithCode:@"01" message:resDictionary[kResponseResult]]);
                              }
                          } else {
                              handler(nil,
                                      [BusinessError errorWithCode:@"01"
                                                           message:NSLocalizedString(@"error_registering",
                                                                                     @"Error registrandote, intentalo de nuevo más tarde")]);
                          }
                      }];
}

+ (void)recoverPassword:(NSString *)email andHandler:(void (^)(Response *, BusinessError *))handler {
    [HttpHandler sendPostRequest:@{
             @"correo" : email
     }
                         withUrl:[NSString stringWithFormat:@"%@%@", kUrlHostname, kUrlServiceLostPassword]
                      andHandler:^(NSDictionary *resDictionary, NSError *error) {
                          if (!error) {
                              if ([resDictionary[kResponseStatus] isEqualToString:@"true"]) {
                                  handler([Response responseWithResult:resDictionary[kResponseResult]
                                                                status:resDictionary[kResponseStatus]], nil);
                              } else {
                                  handler(nil,
                                          [BusinessError errorWithCode:@"01" message:resDictionary[kResponseMessage]]);
                              }
                          } else {
                              handler(nil, [BusinessError errorWithCode:@"01" message:@"error_recovering_password"]);
                          }
                      }];

}

+ (void)testToken:(NSString *)idUser
        withToken:(NSString *)token
       andHandler:(void (^)(NSDictionary *, BusinessError *))handler {
    [HttpHandler sendGetRequest:[NSString stringWithFormat:@"%@%@?id_usuario=%@&token=%@",
                                                           kUrlHostname,
                                                           kUrlServiceTestToken,
                                                           idUser,
                                                           token]
                     andHandler:^(NSDictionary *dictionary, NSError *error) {
                         if (!error) {
                             NSDictionary *resultDict = dictionary[kResponseStatus];
                             handler(resultDict, nil);
                         } else {
                             BusinessError *businessManagerError = [[BusinessError alloc]
                                                                                   init];
                             businessManagerError.message = @"Error in the service";
                             handler(nil, businessManagerError);
                         }
                     }];
}

+ (void)retrieveUserInfo:(NSString *)idUser
               withToken:(NSString *)token
              andHandler:(void (^)(NSDictionary *, BusinessError *))handler {
    [HttpHandler sendGetRequest:[NSString stringWithFormat:@"%@%@?id_usuario=%@&token=%@",
                                                           kUrlHostname,
                                                           kUrlServiceUserInfo,
                                                           idUser,
                                                           token]
                     andHandler:^(NSDictionary *dictionary, NSError *error) {
                         if (error == nil) {
                             NSDictionary *resultDict = dictionary[kResponseResult];
                             handler(resultDict, nil);
                         } else {
                             BusinessError *businessManagerError = [[BusinessError alloc]
                                                                                   init];
                             businessManagerError.message = NSLocalizedString(@"error_getting_user_info",
                                                                              @"Error obteniendo información de usuario.");
                             handler(nil, businessManagerError);
                         }
                     }];
}

+ (void)logoutIdUsuario:(NSString *)idUser
              withToken:(NSString *)token
             andHandler:(void (^)(Response *, BusinessError *))handler {
    NSDictionary *userParams = @{
            @"id_usuario" : idUser,
            @"token" : token
    };
    [HttpHandler sendPostRequest:userParams
                         withUrl:[NSString stringWithFormat:@"%@%@", kUrlHostname, kUrlServiceLogout]
                      andHandler:^(NSDictionary *resDictionary, NSError *error) {
                          if (!error) {
                              if ([resDictionary[kResponseStatus] isEqualToString:@"true"]) {
                                  handler([Response responseWithResult:resDictionary[kResponseMessage]
                                                                status:resDictionary[kResponseStatus]], nil);
                              } else {
                                  handler(nil,
                                          [BusinessError errorWithCode:@"01"
                                                               message:NSLocalizedString(@"error_logging_out",
                                                                                         @"Error saliendo de nativox")]);
                              }
                          } else {
                              handler(nil,
                                      [BusinessError errorWithCode:@"01" message:NSLocalizedString(@"error_logging_out",
                                                                                                   @"Error saliendo de nativox")]);
                          }
                      }];
}


+ (void)updateUserInfoData:(NSDictionary *)dictionary andHandler:(void (^)(Response *, BusinessError *))handler {
    [HttpHandler sendPostRequest:dictionary
                         withUrl:[NSString stringWithFormat:@"%@%@", kUrlHostname, kUrlServiceEditarUserInfo]
                      andHandler:^(NSDictionary *resDictionary, NSError *error) {
                          if (!error) {
                              if ([resDictionary[kResponseStatus] isEqualToString:@"true"]) {
                                  handler([Response responseWithResult:resDictionary[kResponseMessage]
                                                                status:resDictionary[kResponseStatus]], nil);
                              } else {
                                  handler(nil,
                                          [BusinessError errorWithCode:@"01"
                                                               message:NSLocalizedString(@"error_updating user_info",
                                                                                         @"Error updating user info")]);
                              }
                          } else {
                              handler(nil,
                                      [BusinessError errorWithCode:@"01"
                                                           message:NSLocalizedString(@"error_updating user_info",
                                                                                     @"")]);
                          }
                      }];
}

+ (void)sendContact:(NSString *)idUser
        withMessage:(NSString *)message
           andEmail:(NSString *)email
         andHandler:(void (^)(BusinessError *))handler {

    NSDictionary *contactDict = @{
            @"mensaje" : message,
            @"correo" : email,
            @"id_usuario" : idUser ?: @""
    };

    [HttpHandler sendPostRequest:contactDict
                         withUrl:[NSString stringWithFormat:@"%@%@", kUrlHostname, kUrlServiceContact]
                      andHandler:^(NSDictionary *resDictionary, NSError *error) {
                          if (!error) {
                              if ([resDictionary[kResponseStatus] isEqualToString:@"true"]) {
                                  handler(nil);
                              } else {
                                  handler([BusinessError errorWithCode:@"01"
                                                               message:NSLocalizedString(@"error_contact_service",
                                                                                         @"Error contact service")]);
                              }
                          } else {
                              handler([BusinessError errorWithCode:@"01"
                                                           message:NSLocalizedString(@"error_contact_service",
                                                                                     @"")]);
                          }
                      }];
}

+ (void)updateUserInfoCourse:(NSDictionary *)updateInfoDict andHandler:(void (^)(BusinessError *))handler {

    [HttpHandler sendPostRequest:updateInfoDict
                         withUrl:[NSString stringWithFormat:@"%@%@", kUrlHostname, kUrlServiceUpdateInfoCourseUser]
                      andHandler:^(NSDictionary *resDictionary, NSError *error) {
                          if (!error) {
                              if ([resDictionary[kResponseStatus] isEqualToString:@"true"]) {
                                  handler(nil);
                              } else {
                                  handler([BusinessError errorWithCode:@"01"
                                                               message:NSLocalizedString(@"error_update_user_info_course_service",
                                                                                         @"Error updating user information course")]);
                              }
                          } else {
                              handler([BusinessError errorWithCode:@"01"
                                                           message:NSLocalizedString(@"error_update_user_info_course_service",
                                                                                     @"Error updating user information course")]);
                          }
                      }];
}


@end