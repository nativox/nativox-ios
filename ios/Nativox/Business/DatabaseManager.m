//
//  DatabaseManager.m
//  Nativox
//
//  Created by redhair84 on 09/03/2014.
//  Copyright (c) 2014 Francisco Javier Morant. All rights reserved.
//

#import "DatabaseManager.h"
#import "VideoInfo.h"

static NSString *const kVideoEntityName = @"VideoInfo";
static DatabaseManager *_instance = nil;

@implementation DatabaseManager

- (instancetype)initWithManager:(NSManagedObjectContext *)manager {
    self = [super init];
    if (self) {
        self.manager = manager;
        _instance = self;
    }

    return self;
}

+ (instancetype)managerWithManager:(NSManagedObjectContext *)manager {

    _instance = [[self alloc] initWithManager:manager];
    return _instance;
}

+ (DatabaseManager *)instance {

    @synchronized (self) {
        if (_instance == nil) {
            _instance = [[self alloc] init];
        }
    }

    return _instance;
}

- (BOOL)insertVideoInfo:(NSDictionary *)videoInfo {
    VideoInfo *videoInfoNew = [NSEntityDescription insertNewObjectForEntityForName:kVideoEntityName
                                                            inManagedObjectContext:_manager];

    videoInfoNew.id = videoInfo[@"id"];
    videoInfoNew.acceso = videoInfo[@"acceso"];
    videoInfoNew.pathVideo = videoInfo[@"pathVideo"];
    videoInfoNew.url = videoInfo[@"url"];
    videoInfoNew.pathAudio = videoInfo[@"pathAudio"];

    NSError *error;
    return [_manager save:&error];
}

- (VideoInfo *)getVideoInfo:(NSString *)videoId {
    NSEntityDescription *entity = [NSEntityDescription entityForName:kVideoEntityName inManagedObjectContext:_manager];

    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entity];
    [request setPredicate:[NSPredicate predicateWithFormat:@"id == %@", videoId]];

    NSError *error;
    NSArray *videos = [_manager executeFetchRequest:request error:&error];

    if (error) {
        DLog(@"Error retrieving videoInfo : %@", error.localizedDescription);
        return nil;
    }
    else if (videos.count > 0) {
        return videos[videos.count - 1];
    } else {
        return nil;
    }
}

- (void)deleteAllObjects:(NSString *)entityDescription {
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityDescription inManagedObjectContext:_manager];
    [fetchRequest setEntity:entity];

    NSError *error;
    NSArray *items = [_manager executeFetchRequest:fetchRequest error:&error];

    for (NSManagedObject *managedObject in items) {
        [_manager deleteObject:managedObject];
        DLog(@"%@ object deleted", entityDescription);
    }
    if (![_manager save:&error]) {
        DLog(@"Error deleting %@ - error:%@", entityDescription, error);
    }
}

@end
