#import "HttpHandler.h"
#import "UserSession.h"

@implementation HttpHandler {

}

- (void)sendPostRequest:(NSDictionary *)parameters
                withUrl:(NSString *)url
             andHandler:(void (^)(NSDictionary

             *, NSError *))handler {
    NSString *urlComposed = [NSString stringWithFormat:@"%@%@", kUrlHostname, url];

    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration
                                                          delegate:(id <NSURLSessionDelegate>) self
                                                     delegateQueue:nil];

    NSString *urlAppended = [self addExtraParamsToUrl:urlComposed];
    DLog(@"URL: %@", urlAppended);

    NSURL *urlRequest = [NSURL URLWithString:urlAppended];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:urlRequest
                                                           cachePolicy:NSURLRequestReloadIgnoringLocalCacheData
                                                       timeoutInterval:TIMEOUT_REQUEST_HTTP];

    [request addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Accept"];
    [request setHTTPMethod:@"POST"];

    NSString *bodyString = @"";
    NSUInteger count = parameters.allKeys.count;
    int i = 0;

    for (NSString *key in parameters.allKeys) {
        NSString *stringValue;

        if (i >= (count - 1)) {
            stringValue = [NSString stringWithFormat:@"%@=%@", key, parameters[key]];
        } else {
            stringValue = [NSString stringWithFormat:@"%@=%@&", key, parameters[key]];
        }

        if ([parameters[key] isKindOfClass:[NSArray class]]) {
            stringValue = [stringValue stringByReplacingOccurrencesOfString:@"(" withString:@""];
            stringValue = [stringValue stringByReplacingOccurrencesOfString:@")" withString:@""];
            stringValue = [stringValue stringByReplacingOccurrencesOfString:@"\n" withString:@""];
            stringValue = [stringValue stringByReplacingOccurrencesOfString:@" " withString:@""];
        }

        bodyString = [bodyString stringByAppendingString:stringValue];
        i++;
    }

    if (bodyString) {
        DLog(@"Request: %@", bodyString);
        [request setHTTPBody:[bodyString dataUsingEncoding:NSUTF8StringEncoding]];
    }

    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request
                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *errorRequest) {
                                                        if (!errorRequest) {
                                                            NSError *jsonError = nil;
                                                            NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:data
                                                                                                                         options:(NSJSONReadingOptions) kNilOptions
                                                                                                                           error:&jsonError];

                                                            if (!jsonError) {
                                                                dispatch_async(dispatch_get_main_queue(), ^{
                                                                    handler(jsonResponse, nil);
                                                                });

                                                            } else {
                                                                dispatch_async(dispatch_get_main_queue(), ^{
                                                                    handler(nil, jsonError);
                                                                });
                                                            }
                                                        } else {
                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                handler(nil, errorRequest);
                                                            });
                                                        }
                                                    }];

    [postDataTask resume];

}

- (void)sendGetRequest:(NSDictionary *)dataParams withServiceName:(NSString *)service andHandler:(void (^)(NSDictionary

*,
        NSError *))handler {
    NSString *url = [NSString stringWithFormat:@"%@%@", kUrlHostname, service];
    NSString *urlAppended = [self addExtraParamsToUrl:[self addParams:dataParams toUrl:url]];
    DLog(@"URL: %@", urlAppended);

    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:urlAppended]
                                            completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                if (!error) {
                                                    NSError *jsonError = nil;
                                                    NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:data
                                                                                                                 options:(NSJSONReadingOptions) kNilOptions
                                                                                                                   error:&jsonError];
                                                    if (!jsonError) {
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            handler(jsonResponse, nil);
                                                        });

                                                    } else {
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            handler(nil, jsonError);
                                                        });
                                                    }
                                                } else {
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        handler(nil, error);
                                                    });
                                                }

                                            }];
    [dataTask resume];
}

- (NSString *)addParams:(NSDictionary *)params toUrl:(NSString *)url {
    NSMutableString *queryUrl = [[NSMutableString alloc] initWithString:url];
    if (params) {
        for (id key in params) {
            NSString *keyUrl = [key description];
            NSString *valueUrl = [params[key] description];
            if ([queryUrl rangeOfString:@"?"].location == NSNotFound) {
                [queryUrl appendFormat:@"?%@=%@", keyUrl, valueUrl];
            } else {
                [queryUrl appendFormat:@"&%@=%@", keyUrl, valueUrl];
            }
        }
    }

    return queryUrl;
}

- (NSString *)addExtraParamsToUrl:(NSString *)url {
    NSString *urlAppended = [self urlAppend:url withField:@"os" withValue:@"ios"];

    urlAppended = [self urlAppend:urlAppended withField:@"lang" withValue:[UserSession instance].localeLanguage];
    urlAppended = [self urlAppend:urlAppended withField:@"id_device" withValue:[UIDevice currentDevice].identifierForVendor.UUIDString];
    urlAppended = [self urlAppend:urlAppended withField:@"id_session" withValue:[NSString stringWithFormat:@"%d", (int) [UserSession instance].startSession]];
    urlAppended = [self urlAppend:urlAppended withField:@"device" withValue:[[UIDevice currentDevice] name]];

    CGRect screenRect = [[UIScreen mainScreen] bounds];
    urlAppended = [self urlAppend:urlAppended withField:@"height" withValue:[NSString stringWithFormat:@"%f", screenRect.size.height * 0.8]];
    urlAppended = [self urlAppend:urlAppended withField:@"width" withValue:[NSString stringWithFormat:@"%f", screenRect.size.width * 0.8]];

    NSString *version = [[NSBundle mainBundle] infoDictionary][@"CFBundleShortVersionString"];
    urlAppended = [self urlAppend:urlAppended withField:@"version" withValue:version];

    return urlAppended;
};

- (NSString *)urlAppend:(NSString *)url withField:(NSString *)fieldName withValue:(NSString *)value {
    NSString *urlAppended;

    NSCharacterSet *set = [NSCharacterSet URLQueryAllowedCharacterSet];


    if ([url rangeOfString:@"?"].location != NSNotFound) {
        urlAppended = [url stringByAppendingString:[NSString stringWithFormat:@"&%@=%@", fieldName, [value
                stringByAddingPercentEncodingWithAllowedCharacters:set]]];
    } else {
        urlAppended = [url stringByAppendingString:[NSString stringWithFormat:@"?%@=%@", fieldName, [value
                stringByAddingPercentEncodingWithAllowedCharacters:set]]];
    }
    return urlAppended;
}


@end