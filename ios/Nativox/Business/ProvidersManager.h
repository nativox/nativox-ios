
#import <Foundation/Foundation.h>

@class UserProvider;
@class SalesProvider;
@class CategoryProvider;
@class MetricsProvider;

static NSString *const kResult = @"result";
static NSString *const kStatus = @"status";
static NSString *const kResponseIdUser = @"id_usuario";

@interface ProvidersManager : NSObject

@property(nonatomic, strong) UserProvider *userProvider;
@property(nonatomic, strong) MetricsProvider *metricsProvider;
@property(nonatomic, strong) SalesProvider *salesProvider;
@property(nonatomic, strong) CategoryProvider *categoryProvider;

+ (ProvidersManager *)instance;

@end