#import "UserProvider.h"
#import "BusinessError.h"
#import "Response.h"
#import "User.h"
#import "HttpHandler.h"
#import "ProvidersManager.h"
#import "AlarmReminder.h"
#import "NativoxAppDelegate.h"
#import "UserSession.h"
#import "Answers.h"

#define kResponseName @"nombre"
#define kResponseMessage @"mensaje"

static NSString *const kFreeCoursesTime = @"tiempo_restante";

@interface UserProvider ()

@end

@implementation UserProvider {

}

- (instancetype)init {
    self = [super init];
    if (self) {
        _httpHandler = [[HttpHandler alloc] init];
    }

    return self;
}

- (User *)mapUserData:(NSDictionary *)dictionary {
    User *user = [[User alloc] init];
    id resultDictionary = dictionary[kResult];
    //user.token = resultDictionary[kResponseToken];
    int idUser = [resultDictionary[kResponseIdUser] intValue];
    user.idUser = [NSString stringWithFormat:@"%d", idUser];
    user.name = resultDictionary[kResponseName];
    user.freeCoursesTime = [resultDictionary[kFreeCoursesTime] intValue];
    return user;
}

- (void)loginWithFB:(NSString *)email
           userName:(NSString *)userName
         idFacebook:(NSString *)idFacebook
            fbToken:(NSString *)fbToken
            andName:(NSString *)name
         andHandler:(void (^)(User *, BusinessError *))handler {
    [UserSession instance].email = email;
    [self.httpHandler sendPostRequest:@{
                    @"correo" : email ? email : @"",
                    @"nombre_usuario" : userName ? userName : @"",
                    @"id_facebook" : idFacebook ? idFacebook : @"",
                    @"nombre" : name ? name : @"",
                    @"access_token" : fbToken ? fbToken : @""
            }
                              withUrl:kUrlServiceLoginFb
                           andHandler:^(NSDictionary *dictionary, NSError *error) {
                               if (!error) {
                                   if ([dictionary[kStatus] isEqualToString:@"true"]) {
                                       [Answers logLoginWithMethod:@"Facebook"
                                                           success:@YES
                                                  customAttributes:@{
                                                          @"Email" : email}];
                                       User *user = [self mapUserData:dictionary];
                                       handler(user, nil);
                                   } else {
                                       [UserSession instance].email = nil;
                                       handler(nil, [BusinessError withError:nil message:dictionary[kResult]]);
                                   }
                               } else {
                                   [UserSession instance].email = nil;
                                   handler(nil, [BusinessError withError:error message:NSLocalizedString(@"error_login_facebook", @"")]);
                               }
                           }];
}

- (void)registerUserWithEmail:(NSString *)email withName:(NSString *)name withPassword:(NSString *)password withLanguage:(NSString *)language andHandler:(void (^)(Response *, BusinessError *))handler {
    [self.httpHandler sendPostRequest:@{
                    @"correo" : email,
                    @"nombre" : name,
                    @"password" : password,
                    @"lang" : language
            }
                              withUrl:kUrlServiceRegister
                           andHandler:^(NSDictionary *resDictionary, NSError *error) {
                               if (!error) {
                                   if ([resDictionary[kStatus] isEqualToString:@"true"]) {
                                       [Answers logSignUpWithMethod:@"Email"
                                                            success:@YES
                                                   customAttributes:@{
                                                           @"email" : email,
                                                           @"name" : name,
                                                           @"language" : language
                                                   }];
                                       handler([Response responseWithResult:resDictionary[kResult][kResponseMessage]
                                                                     status:resDictionary[kStatus]], nil);
                                   } else {
                                       handler(nil,
                                               [BusinessError withError:nil message:resDictionary[kResult]]);
                                   }
                               } else {
                                   handler(nil,
                                           [BusinessError withError:error
                                                            message:NSLocalizedString(@"error_registering", @"")]);
                               }
                           }];
}

- (void)recoverPassword:(NSString *)email
             andHandler:(void (^)(Response *, BusinessError *))handler {
    [self.httpHandler sendPostRequest:@{
                    @"correo" : email
            }
                              withUrl:kUrlServiceLostPassword
                           andHandler:^(NSDictionary *response, NSError *error) {
                               if (!error) {
                                   if ([response[kStatus] isEqualToString:@"true"]) {
                                       handler([Response responseWithResult:response[kResult]
                                                                     status:response[kStatus]], nil);
                                   } else {
                                       handler(nil,
                                               [BusinessError withError:nil
                                                                message:response[kResult]]);
                                   }
                               } else {
                                   handler(nil,
                                           [BusinessError withError:error
                                                            message:NSLocalizedString(@"error_recovering_password", @"")]);
                               }
                           }];

}

- (void)testToken:(void (^)(BOOL, BusinessError *))handler {

    NSDictionary *data = @{
            @"id_usuario" : [UserSession instance].idUser,
            @"token" : [UserSession instance].token
    };

    [self.httpHandler sendGetRequest:data
                     withServiceName:kUrlServiceTestToken
                          andHandler:^(NSDictionary *dictionary, NSError *error) {
                              if (!error) {
                                  handler([dictionary[kStatus] boolValue], nil);
                              } else {
                                  BusinessError *businessManagerError = [[BusinessError alloc]
                                          init];
                                  businessManagerError.message = @"Error in the service";
                                  handler(nil, businessManagerError);
                              }
                          }];
}

- (void)retrieveUserInfo:(NSString *)idUser
               withToken:(NSString *)token
              andHandler:(void (^)(NSDictionary *, BusinessError *))handler {
    if (!idUser || !token) {
        return;
    }

    NSDictionary *data = @{
            @"id_usuario" : idUser,
            @"token" : token,
            @"incluir_cursos" : @"0"
    };

    [self.httpHandler sendGetRequest:data
                     withServiceName:kUrlServiceUserInfo
                          andHandler:^(NSDictionary *dictionary, NSError *error) {
                              if (error == nil) {
                                  if ([dictionary[kStatus] boolValue]) {
                                      NSDictionary *resultDict = dictionary[kResult];
                                      [[UserSession instance] reloadUserSession:resultDict];

                                      handler(resultDict, nil);
                                  } else {
                                      BusinessError *businessManagerError = [[BusinessError alloc] init];
                                      businessManagerError.message = dictionary[kResult];
                                      handler(nil, businessManagerError);
                                  }
                              } else {
                                  BusinessError *businessManagerError = [[BusinessError alloc] init];
                                  businessManagerError.message = NSLocalizedString(@"error_getting_user_info", @"");
                                  handler(nil, businessManagerError);
                              }
                          }];
}

- (void)updateUserInfoData:(NSDictionary *)dictionary
                andHandler:(void (^)(Response *, BusinessError *))handler {
    [self.httpHandler sendPostRequest:dictionary
                              withUrl:kUrlServiceEditarUserInfo
                           andHandler:^(NSDictionary *resDictionary, NSError *error) {
                               if (!error) {
                                   if ([resDictionary[kStatus] isEqualToString:@"true"]) {
                                       handler([Response responseWithResult:resDictionary[kResponseMessage]
                                                                     status:resDictionary[kStatus]], nil);
                                   } else {
                                       handler(nil,
                                               [BusinessError withError:nil
                                                                message:NSLocalizedString(@"error_updating user_info", @"")]);
                                   }
                               } else {
                                   handler(nil,
                                           [BusinessError withError:error
                                                            message:NSLocalizedString(@"error_updating user_info", @"")]);
                               }
                           }];
}

- (void)updateUserInfoCourse:(NSDictionary *)updateInfoDict
                  andHandler:(void (^)(BusinessError *, NSDictionary *))handler {
    [self.httpHandler sendPostRequest:updateInfoDict
                              withUrl:kUrlServiceUpdateInfoCourseUser
                           andHandler:^(NSDictionary *response, NSError *error) {
                               DLog(@"%@", response);
                               if (!error) {
                                   if ([response[kStatus] isEqualToString:@"true"]) {
                                       handler(nil, response);
                                   } else {
                                       handler([BusinessError withError:nil message:NSLocalizedString(@"error_update_user_info_course_service", @"")], nil);
                                   }
                               } else {
                                   handler([BusinessError withError:error message:NSLocalizedString(@"error_update_user_info_course_service", @"")], nil);
                               }
                           }];
}

- (void)saveUserOptions:(void (^)(BusinessError *))handler {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];

    NSDictionary *data = @{
            @"ver_frase" : [userDefaults boolForKey:kShowSentenceAfterPlayedVideo] ? @"true" : @"false",
            @"calendario" : [[AlarmReminder instance] printAlarms]
    };

    [self.httpHandler sendPostRequest:data
                              withUrl:kUrlServiceSaveUserOptions
                           andHandler:^(NSDictionary *resDictionary, NSError *error) {
                               if (!error) {
                                   if ([resDictionary[kStatus] isEqualToString:@"true"]) {
                                       handler(nil);
                                   } else {
                                       handler([BusinessError withError:nil
                                                                message:NSLocalizedString(@"error_sending_user_options", @"")]);
                                   }
                               } else {
                                   handler([BusinessError withError:error
                                                            message:NSLocalizedString(@"error_sending_user_options", @"")]);
                               }
                           }];
}

- (void)sendTranslation:(NSString *)idUser
              withVideo:(NSString *)idVideo
           withLanguage:(NSString *)language
        withTranslation:(NSString *)translation
             andHandler:(void (^)(BusinessError *))handler {

    [self.httpHandler sendPostRequest:@{
                    @"id_video" : idVideo,
                    @"id_usuario" : idUser,
                    @"traduccion" : translation,
                    @"idioma" : language
            }
                              withUrl:kUrlServiceAddTranslation
                           andHandler:^(NSDictionary *dictionary, NSError *error) {
                               if (!error) {
                                   if ([dictionary[kStatus] isEqualToString:@"true"]) {
                                       handler(nil);
                                   } else {
                                       handler(
                                               [BusinessError withError:nil
                                                                message:dictionary[kResult]]);
                                   }
                               } else {
                                   handler([BusinessError withError:error
                                                            message:NSLocalizedString(@"error_sending_translation", @"")]);
                               }
                           }];
}


@end
