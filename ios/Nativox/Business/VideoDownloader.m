//
// Created by redhair84 on 11/03/2014.
// Copyright (c) 2014 Francisco Javier Morant. All rights reserved.
//

#import "VideoDownloader.h"

@interface VideoDownloader ()
@property(nonatomic) NSURLSession *session;
@property(nonatomic) NSURLSessionDownloadTask *downloadTask;
@end

@implementation VideoDownloader

- (instancetype)initWithVideoManager {
    self = [super init];
    if (self) {
        _session = [self urlSession];
    }

    return self;
}

- (NSURLSession *)urlSession {
    static NSURLSession *session = nil;
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];

    [configuration setHTTPAdditionalHeaders:@{@"Accept" : @"application/json"}];
    configuration.timeoutIntervalForRequest = 60.0;
    configuration.timeoutIntervalForResource = 60.0;

    session = [NSURLSession sessionWithConfiguration:configuration
                                            delegate:self
                                       delegateQueue:[NSOperationQueue mainQueue]];
    return session;
}

- (void)URLSession:(NSURLSession *)session
             downloadTask:(NSURLSessionDownloadTask *)downloadTask
didFinishDownloadingToURL:(NSURL *)location {
    NSFileManager *fileManager = [NSFileManager defaultManager];

    NSURL *documentsDirectory = [[[NSFileManager defaultManager]
                                                 URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask]
                                                 lastObject];

    double timestamp = [[NSDate date] timeIntervalSince1970];
    int64_t currentTime = (int64_t) (timestamp * 1000);
    NSString *videoFile = [NSString stringWithFormat:@"%qi.%@", currentTime, @"mp4"];
    NSURL *destinationURL = [documentsDirectory URLByAppendingPathComponent:videoFile];

    NSError *errorCopy;
    [fileManager moveItemAtURL:location toURL:destinationURL error:&errorCopy];

    [self.fileManagerDelegate downloadFileFinished:errorCopy == nil
                                withDestinationURL:videoFile
                                          andError:errorCopy];

}

- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error {
    [self.fileManagerDelegate downloadVideoCompletedWithError:error];

    self.downloadTask = nil;
}

- (void)URLSession:(NSURLSession *)session
      downloadTask:(NSURLSessionDownloadTask *)downloadTask
      didWriteData:(int64_t)bytesWritten
        totalBytesWritten:(int64_t)totalBytesWritten
totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite {

    if (downloadTask == self.downloadTask) {
        [self.fileManagerDelegate progressDownload:bytesWritten
                                  withTotalWritten:totalBytesWritten
                                  andTotalExpected:totalBytesExpectedToWrite];
    }
}

- (void)URLSession:(NSURLSession *)session
      downloadTask:(NSURLSessionDownloadTask *)downloadTask
 didResumeAtOffset:(int64_t)fileOffset
expectedTotalBytes:(int64_t)expectedTotalBytes {
    if (downloadTask == self.downloadTask) {
        [self.fileManagerDelegate resumeDownloadWithOffset:fileOffset andTotalExpected:expectedTotalBytes];
    }
}

- (void)URLSession:(NSURLSession *)session didBecomeInvalidWithError:(NSError *)error {
    self.downloadTask = nil;
}

- (void)startDownload:(NSString *)urlFile {
    NSURL *downloadURL = [NSURL URLWithString:urlFile];
    self.downloadTask = [self.session downloadTaskWithURL:downloadURL];

    [self.downloadTask resume];
}

- (void)invalidateSession {
    [self.session invalidateAndCancel];
    self.session = nil;
    self.fileManagerDelegate = nil;
}

@end