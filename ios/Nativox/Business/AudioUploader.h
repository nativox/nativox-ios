
#import <Foundation/Foundation.h>

@protocol FileUploadManagerDelegate

- (void)uploadAudioFileFinished:(NSDictionary *)data andError:(NSError *)error;

@end

@interface AudioUploader : NSObject <NSURLSessionDelegate>

@property(weak, nonatomic) id <FileUploadManagerDelegate> fileUploadManagerDelegate;

- (instancetype) initFileUploadManager;

- (void)startUpload:(NSString *)urlFile withVideoId:(NSString *)videoId andWithUserId:(NSString *)userId;

@end