
#import <Foundation/Foundation.h>

@class BusinessError;
@class HttpHandler;

static NSString *const kServiceCategories = @"/categorias";
static NSString *const kInitialDataLoadHasFinished = @"initialDataLoadHasFinished";
static NSString *const kInitialDataLoadHasStarted = @"initialDataLoadHasStarted";

@interface CategoryProvider : NSObject

@property(nonatomic, strong) HttpHandler *httpHandler;

- (void)categoriesWith:(NSString *)idUser withToken:(NSString *)token andHandler:(void (^)(NSArray *, BusinessError *))handler;

@end