#import <Foundation/Foundation.h>

static const float TIMEOUT_REQUEST_HTTP = 45.0;
static NSString *const kUrlHostname = @"https://nativox.com/apis/2.1";

@interface HttpHandler : NSObject <NSURLSessionDelegate>

- (void)sendPostRequest:(NSDictionary *)parameters
                withUrl:(NSString *)url
             andHandler:(void (^)(NSDictionary *, NSError *))handler;

- (void)sendGetRequest:(NSDictionary *)dataParams
       withServiceName:(NSString *)service
            andHandler:(void (^)(NSDictionary *, NSError *))handler;

@end
