
#import <Foundation/Foundation.h>

@class BusinessError;

static NSString *const kUrlPingNotification = @"/ping_notificacion";

@interface MetricsProvider : NSObject

- (void)pingNotificationWithTitle:(NSString *)title withMessage:(NSString *)message
             withNotificationType:(NSString *)notificationType
                       andHandler:(void (^)(BusinessError *))handler;

@end