#import "DCKeyValueObjectMapping/DCParserConfiguration.h"
#import "CategoryProvider.h"
#import "BusinessError.h"
#import "CategoryCourse.h"
#import "HttpHandler.h"
#import "DCArrayMapping.h"
#import "Course.h"
#import "BooleanConverter.h"
#import "CourseStats.h"
#import "ProvidersManager.h"
#import "UserSession.h"
#import "AsyncImageView.h"

@interface CategoryProvider ()
@property(nonatomic, strong) DCParserConfiguration *configuration;
@property(nonatomic, strong) NSMutableArray *categories;
@end

@implementation CategoryProvider {

}

- (instancetype)init {
    self = [super init];
    if (self) {
        _httpHandler = [[HttpHandler alloc] init];
    }

    return self;
}

- (void)categoriesWith:(NSString *)idUser
             withToken:(NSString *)token
            andHandler:(void (^)(NSArray *, BusinessError *))handler {
    DLog(@"Id User: %@", idUser);
    DLog(@"Token: %@", token);

    [UserSession instance].categories = [@[] mutableCopy];
    [[NSNotificationCenter defaultCenter] postNotificationName:kInitialDataLoadHasStarted object:nil];
    [self.httpHandler sendGetRequest:[self categoriesParams:idUser token:token]
                     withServiceName:kServiceCategories
                          andHandler:^(NSDictionary *dictionary, NSError *error) {
                              if (!error) {
                                  [self parseCategories:dictionary andHandler:handler];
                              } else {
                                  BusinessError *businessManagerError = [[BusinessError alloc] init];
                                  businessManagerError.message = NSLocalizedString(@"error_getting_categories", @"");
                                  [[NSNotificationCenter defaultCenter] postNotificationName:kInitialDataLoadHasFinished object:nil];
                                  handler(nil, businessManagerError);
                              }
                          }];
}

- (void)parseCategories:(NSDictionary *)dictionary andHandler:(void (^)(NSArray *, BusinessError *))handler {
    NSArray *results = dictionary[kResult];
    NSString *status = dictionary[kStatus];

    if ([status isEqualToString:@"true"]) {
        self.categories = [[NSMutableArray alloc] init];
        self.configuration = [self prepareMapperConfig];

        for (NSDictionary *categoryDict in results) {
            CategoryCourse *category = [self parseCategoryFromDictionary:categoryDict];
            [[AsyncImageLoader sharedLoader] loadImageWithURL:[NSURL URLWithString:category.iconoUrl]];
            [self.categories addObject:category];
        }

        [[UserSession instance] generateAllCourses:self.categories];
        [[NSNotificationCenter defaultCenter] postNotificationName:kInitialDataLoadHasFinished object:nil];
        handler(self.categories, nil);
    } else {
        [[NSNotificationCenter defaultCenter] postNotificationName:kInitialDataLoadHasFinished object:nil];
        BusinessError *businessManagerError = [[BusinessError alloc] init];
        businessManagerError.message = dictionary[kResult];
        handler(nil, businessManagerError);
    }
}

- (CategoryCourse *)parseCategoryFromDictionary:(NSDictionary *)dictionary {
    return [CategoryCourse categoryFromDictionary:dictionary
                                 andConfiguration:self.configuration];
}

- (NSDictionary *)categoriesParams:(NSString *)idUser token:(NSString *)token {
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
    dictionary[@"incluir_cursos"] = @"1";

    if (idUser) {
        dictionary[@"id_usuario"] = idUser;
    }

    if (token) {
        dictionary[@"token"] = token;
    }

    return dictionary;
}

- (DCParserConfiguration *)prepareMapperConfig {
    DCParserConfiguration *parserConfiguration = [DCParserConfiguration configuration];

    [self prepareConfigurationForCategoryCourse:parserConfiguration];
    [self prepareConfigurationForCourse:parserConfiguration];
    [self prepareConfigurationForSection:parserConfiguration];
    [self prepareConfigurationForSubsection:parserConfiguration];
    [self prepareConfigurationForVideo:parserConfiguration];

    return parserConfiguration;
}

- (void)prepareConfigurationForVideo:(DCParserConfiguration *)parserConfiguration {
//    BooleanConverter *converter = [[BooleanConverter alloc] init];
//    converter.mapKeyPath = @"superado";
//    DCObjectMapping *passedMapping = [DCObjectMapping mapKeyPath:@"superado" toAttribute:@"passed" onClass:[Video class] converter:converter];
//    [parserConfiguration addObjectMapping:passedMapping];
//
//    converter.mapKeyPath = @"muestra";
//    DCObjectMapping *showMapping = [DCObjectMapping mapKeyPath:@"muestra" toAttribute:@"show" onClass:[Video class]
//                                                     converter:converter];
//    [parserConfiguration addObjectMapping:showMapping];
}

- (void)prepareConfigurationForSubsection:(DCParserConfiguration *)parserConfiguration {
//    DCObjectMapping *idSubSeccionMapping = [DCObjectMapping mapKeyPath:@"id_subseccion" toAttribute:@"idSubsection" onClass:[Subsection class]];
//    [parserConfiguration addObjectMapping:idSubSeccionMapping];
//
//    DCObjectMapping *subSeccionNameMapping = [DCObjectMapping mapKeyPath:@"subseccion" toAttribute:@"name" onClass:[Subsection class]];
//    [parserConfiguration addObjectMapping:subSeccionNameMapping];
//
//    DCObjectMapping *maxPasadoMapping = [DCObjectMapping mapKeyPath:@"maximo_pasado" toAttribute:@"maxPasado" onClass:[Subsection class]];
//    [parserConfiguration addObjectMapping:maxPasadoMapping];
//
//    DCArrayMapping *mapperVideos = [DCArrayMapping mapperForClassElements:[Video class] forAttribute:@"videos" onClass:[Subsection class]];
//    [parserConfiguration addArrayMapper:mapperVideos];
}

- (void)prepareConfigurationForSection:(DCParserConfiguration *)parserConfiguration {
//    DCObjectMapping *idSeccionMapping = [DCObjectMapping mapKeyPath:@"id_seccion" toAttribute:@"idSection" onClass:[Section class]];
//    [parserConfiguration addObjectMapping:idSeccionMapping];
//
//    DCObjectMapping *sectionNameMapping = [DCObjectMapping mapKeyPath:@"seccion" toAttribute:@"name" onClass:[Section class]];
//    [parserConfiguration addObjectMapping:sectionNameMapping];
//
//    DCArrayMapping *mapperSubSections = [DCArrayMapping mapperForClassElements:[Subsection class] forAttribute:@"subsecciones" onClass:[Section class]];
//    [parserConfiguration addArrayMapper:mapperSubSections];
}

- (void)prepareConfigurationForCourse:(DCParserConfiguration *)parserConfiguration {
    DCObjectMapping *idCourseMapping = [DCObjectMapping mapKeyPath:@"id_curso" toAttribute:@"idCourse" onClass:[Course
            class]];
    [parserConfiguration addObjectMapping:idCourseMapping];

    DCObjectMapping *courseNameMapping = [DCObjectMapping mapKeyPath:@"curso" toAttribute:@"name" onClass:[Course class]];
    [parserConfiguration addObjectMapping:courseNameMapping];

    DCObjectMapping *inAppIdMapping = [DCObjectMapping mapKeyPath:@"id_in_app_ios" toAttribute:@"inAppId" onClass:[Course class]];
    [parserConfiguration addObjectMapping:inAppIdMapping];

    DCObjectMapping *boughtMapping = [DCObjectMapping mapKeyPath:@"comprado" toAttribute:@"purchased" onClass:[Course
            class]];
    [parserConfiguration addObjectMapping:boughtMapping];

    DCObjectMapping *demoMapping = [DCObjectMapping mapKeyPath:@"demo" toAttribute:@"demo" onClass:[Course
            class]];
    [parserConfiguration addObjectMapping:demoMapping];

    DCObjectMapping *idCategoriaMapping = [DCObjectMapping mapKeyPath:@"id_categoria" toAttribute:@"idCategory" onClass:[Course class]];
    [parserConfiguration addObjectMapping:idCategoriaMapping];

    DCObjectMapping *statsCourseMapping = [DCObjectMapping mapKeyPath:@"stats_curso" toAttribute:@"courseStats" onClass:[Course class]];
    [parserConfiguration addObjectMapping:statsCourseMapping];

    DCObjectMapping *progressCourseMapping = [DCObjectMapping mapKeyPath:@"nota_media_ultima_per_video_y_total_videos"
                                                             toAttribute:@"progress"
                                                                 onClass:[CourseStats class]];
    [parserConfiguration addObjectMapping:progressCourseMapping];

    DCObjectMapping *completedCourseMapping = [DCObjectMapping mapKeyPath:@"porcentaje_completado"
                                                              toAttribute:@"completed"
                                                                  onClass:[CourseStats class]];
    [parserConfiguration addObjectMapping:completedCourseMapping];

    DCObjectMapping *averageGradeCourseMapping = [DCObjectMapping mapKeyPath:@"nota_media_mas_alta_per_video"
                                                                 toAttribute:@"averageGrade"
                                                                     onClass:[CourseStats class]];
    [parserConfiguration addObjectMapping:averageGradeCourseMapping];

//    DCArrayMapping *mapperSections = [DCArrayMapping mapperForClassElements:[Section class] forAttribute:@"secciones"
//                                                                    onClass:[Course class]];
//    [parserConfiguration addArrayMapper:mapperSections];
}

- (void)prepareConfigurationForCategoryCourse:(DCParserConfiguration *)parserConfiguration {
    DCObjectMapping *categoryNameMapping = [DCObjectMapping mapKeyPath:@"categoria" toAttribute:@"name" onClass:[CategoryCourse class]];
    [parserConfiguration addObjectMapping:categoryNameMapping];

    DCObjectMapping *categoryColorHexMapping = [DCObjectMapping mapKeyPath:@"color_hex" toAttribute:@"colorHex"
                                                                   onClass:[CategoryCourse class]];
    [parserConfiguration addObjectMapping:categoryColorHexMapping];

    DCObjectMapping *categoryInAppMapping = [DCObjectMapping mapKeyPath:@"id_in_app_ios" toAttribute:@"inAppId"
                                                                onClass:[CategoryCourse class]];
    [parserConfiguration addObjectMapping:categoryInAppMapping];

    DCObjectMapping *categoryPurchasedMapping = [DCObjectMapping mapKeyPath:@"comprado" toAttribute:@"purchased"
                                                                    onClass:[CategoryCourse class]];
    [parserConfiguration addObjectMapping:categoryPurchasedMapping];

    DCObjectMapping *categoryInfoPopupMapping = [DCObjectMapping mapKeyPath:@"info" toAttribute:@"infoPopup"
                                                                    onClass:[CategoryCourse class]];
    [parserConfiguration addObjectMapping:categoryInfoPopupMapping];

    DCObjectMapping *categoryContentMapping = [DCObjectMapping mapKeyPath:@"contenido" toAttribute:@"content"
                                                                  onClass:[CategoryCourse class]];
    [parserConfiguration addObjectMapping:categoryContentMapping];

    DCObjectMapping *categoryIconoUrlMapping = [DCObjectMapping mapKeyPath:@"icono_url" toAttribute:@"iconoUrl"
                                                                   onClass:[CategoryCourse class]];
    [parserConfiguration addObjectMapping:categoryIconoUrlMapping];

    DCObjectMapping *categoryIdMapping = [DCObjectMapping mapKeyPath:@"id_categoria" toAttribute:@"idCategory" onClass:[CategoryCourse class]];
    [parserConfiguration addObjectMapping:categoryIdMapping];

    DCArrayMapping *mapperCourses = [DCArrayMapping mapperForClassElements:[Course class] forAttribute:@"cursos"
                                                                   onClass:[CategoryCourse class]];
    [parserConfiguration addArrayMapper:mapperCourses];
}

@end
