#import <StoreKit/StoreKit.h>
#import "SalesProvider.h"
#import "BusinessError.h"
#import "HttpHandler.h"
#import "NativoxAppDelegate.h"
#import "CategoryProvider.h"
#import "UserSession.h"
#import "ProvidersManager.h"
#import "IAPHelper.h"
#import "Course.h"
#import "CourseBuyer.h"
#import "NativoxIAHelper.h"
#import "PackCourse.h"
#import "SKProduct+Price.h"
#import "NotificationChannelSubscriber.h"

static NSString *const kSandboxValidationTrue = @"true";
static NSString *const kSandboxValidationFalse = @"false";

@interface SalesProvider ()

@property(nonatomic, strong) HttpHandler *httpHandler;
@property(nonatomic, strong) NSDictionary *selectedCourse;
@property(nonatomic, strong) SKProduct *selectedProduct;
@property(nonatomic, copy) NSString *idUser;
@property(nonatomic, copy) NSString *token;
@property(nonatomic, strong) NotificationChannelSubscriber *notificationChannelSubscriber;
@end

@implementation SalesProvider

- (instancetype)init {
    self = [super init];
    if (self) {
        _httpHandler = [[HttpHandler alloc] init];
        _notificationChannelSubscriber = [[NotificationChannelSubscriber alloc] init];
    }

    return self;
}

#pragma mark Checkout order

- (void)purchaseCourse:(Course *)course {
    [self registerInAppNotifications];

    if ([self isPossibleToRunMethod:@selector(coursesPurchaseStarted) withObject:self.delegate]) {
        [self.delegate coursesPurchaseStarted];
    }

    self.selectedCourse = course;
    self.idUser = [UserSession instance].idUser;
    self.token = [UserSession instance].token;

    [[CourseBuyer instance] requestProduct:course andDelegate:^(BOOL success, SKProduct *product) {
        if (!success) {
            [self unRegisterInAppNotifications];

            if ([self isPossibleToRunMethod:@selector(coursesPurchaseFailed:) withObject:self.delegate]) {
                [self.delegate coursesPurchaseFailed:NSLocalizedString(@"error_getting_iap", @"")];
            }
        } else {
            self.selectedProduct = product;
            [[NativoxIAHelper sharedInstance] buyProduct:self.selectedProduct withQuantity:1];
        }
    }];
}

- (void)checkoutOrderWithCourses:(NSArray *)coursesIds withTotal:(float)total withCurrency:(NSString *)currency
                withTrasactionId:(NSString *)transactionId withInAppPurchaseId:(NSString *)inAppPurchaseId {
    NSString *receiptEncoded = [self retrieveEncodedReceipt];

    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:coursesIds forKey:kCoursesIdsPendingRegister];
    [userDefaults setFloat:total forKey:kTotalPricePendingToRegister];
    [userDefaults setObject:currency forKey:kCurrencyPendingToRegister];
    [userDefaults setObject:transactionId forKey:kTransactionIdPendingToRegister];
    [userDefaults setObject:inAppPurchaseId forKey:kInAppIdPendingToRegister];
    [userDefaults setObject:self.idUser forKey:kUserId];
    [userDefaults setObject:self.token forKey:kToken];
    [userDefaults synchronize];

    NSString *sandbox = kSandboxValidationFalse;

#if defined(DEBUG) || defined(TESTING)
    sandbox = kSandboxValidationTrue;
#endif

    NSDictionary *parameters = @{
            @"id_usuario" : self.idUser,
            @"token" : self.token,
            @"total" : @(total),
            @"lang" : @"es",
            @"moneda" : currency,
            @"transactionId" : transactionId,
            @"receipt" : [receiptEncoded stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"],
            @"in_app_purchase_id" : inAppPurchaseId,
            @"bundle_id" : [[NSBundle mainBundle] bundleIdentifier],
            kCoursesData : coursesIds,
            @"sandbox" : sandbox
    };

    if ([self isPossibleToRunMethod:@selector(coursesCheckoutStarted) withObject:self.delegate]) {
        [self.delegate coursesCheckoutStarted];
    }

    [self.httpHandler sendPostRequest:parameters
                              withUrl:kServiceCompletedOrder
                           andHandler:^(NSDictionary *resDictionary, NSError *error) {
                               if (!error) {
                                   [self removePendingPurchase:userDefaults];
                                   if ([resDictionary[kStatus] isEqualToString:@"true"]) {
                                       [self retrieveCategories:^(BusinessError *errorGettingCategories, NSDictionary *dictionary) {
                                           if (errorGettingCategories && [self isPossibleToRunMethod:@selector(coursesCheckoutFailed:) withObject:self.delegate]) {
                                               [self.delegate coursesCheckoutFailed:errorGettingCategories.message];
                                           } else if ([self isPossibleToRunMethod:@selector(coursesCheckoutCompleted:andData:)
                                                                       withObject:self.delegate]) {
                                               [self.delegate coursesCheckoutCompleted:nil andData:dictionary];
                                           }
                                       }];
                                   } else if ([self isPossibleToRunMethod:@selector(coursesCheckoutFailed:)
                                                               withObject:self.delegate]) {
                                       [self.delegate coursesCheckoutFailed:NSLocalizedString(@"error_checking_out_sales", @"")];
                                   }
                               } else if ([self isPossibleToRunMethod:@selector(coursesCheckoutFailed:)
                                                           withObject:self.delegate]) {
                                   [self.delegate coursesCheckoutFailed:NSLocalizedString(@"error_checking_out_sales", @"")];
                               }
                           }];
}


- (void)removePendingPurchase:(NSUserDefaults *)userDefaults {
    [userDefaults setObject:nil forKey:kCoursesIdsPendingRegister];
    [userDefaults setFloat:0 forKey:kTotalPricePendingToRegister];
    [userDefaults setObject:nil forKey:kCurrencyPendingToRegister];
    [userDefaults setObject:nil forKey:kTransactionIdPendingToRegister];
    [userDefaults setObject:nil forKey:kInAppIdPendingToRegister];
    [userDefaults setObject:nil forKey:kUserId];
    [userDefaults setObject:nil forKey:kToken];
    [userDefaults synchronize];
}

- (NSString *)retrieveEncodedReceipt {
    NSString *receiptEncoded = @"";
    NSURL *receiptURL = [[NSBundle mainBundle] appStoreReceiptURL];
    NSData *receiptData = [NSData dataWithContentsOfURL:receiptURL];

    if (receiptData) {
        receiptEncoded = [receiptData base64EncodedStringWithOptions:0];
    }

    return receiptEncoded;
}

- (void)retrieveCategories:(void (^)(BusinessError *, NSDictionary *))handler {
    [UserSession instance].coursesLoaded = NO;
    [[ProvidersManager instance].categoryProvider categoriesWith:[UserSession instance].idUser
                                                       withToken:[UserSession instance].token
                                                      andHandler:^(NSArray *categories, BusinessError *error) {
                                                          if (!error) {
                                                              [self retrieveUserInfo:handler];
                                                          } else {
                                                              handler(error, nil);
                                                          }
                                                      }];
}

- (void)retrieveUserInfo:(void (^)(BusinessError *, NSDictionary *))handler {
    [[ProvidersManager instance].userProvider retrieveUserInfo:[UserSession instance].idUser
                                                     withToken:[UserSession instance].token
                                                    andHandler:^(NSDictionary *dictionary, BusinessError *errorGettingUserInfo) {
                                                        if (!errorGettingUserInfo) {
                                                            handler(nil, dictionary);
                                                        } else {
                                                            handler(errorGettingUserInfo, nil);
                                                        }
                                                    }];
}

#pragma mark - InApp Purchases

- (void)coursesPurchaseFailed:(id)coursesPurchasedFailed {
    [self unRegisterInAppNotifications];

    if ([self isPossibleToRunMethod:@selector(coursesPurchaseFailed:) withObject:self.delegate]) {
        [self.delegate coursesPurchaseFailed:NSLocalizedString(@"error_purchasing_courses", @"")];
    }
}

- (void)coursesPurchased:(NSNotification *)notification {
    [self unRegisterInAppNotifications];
    if ([self isPossibleToRunMethod:@selector(coursesPurchaseCompleted) withObject:self.delegate]) {
        [self.delegate coursesPurchaseCompleted];
    }

    [self.notificationChannelSubscriber subscribeToChannel:kUserHasBoughtCourseChannel];

//    if (!self.selectedCourse.isGift && [self.selectedCourse isKindOfClass:[PackCourse class]]) {
//        [self purchasedPackCourse:notification.object];
//    } else if (!self.selectedCourse.isGift) {
//        [self purchasedSingleCourse:notification.object];
//    }
}

- (void)coursesPurchaseCancelled:(NSNotification *)notification {
    [self unRegisterInAppNotifications];

    if ([self isPossibleToRunMethod:@selector(coursesPurchaseCancelled) withObject:self.delegate]) {
        [self.delegate coursesPurchaseCancelled];
    }
}

- (void)purchasedSingleCourse:(SKPaymentTransaction *)transaction {
    Course *course = self.selectedCourse;
    [self checkoutCourses:@[course.idCourse]
              withInAppId:transaction.payment.productIdentifier
         andTransactionId:transaction.transactionIdentifier
    ];
}

- (void)purchasedPackCourse:(SKPaymentTransaction *)transaction {
    NSMutableArray *coursesIds = [[NSMutableArray alloc] init];
    PackCourse *packCourse = (PackCourse *) self.selectedCourse;

    for (Course *course in packCourse.coursesPack) {
        [coursesIds addObject:course.idCourse];
    }

    [self checkoutCourses:coursesIds
              withInAppId:transaction.payment.productIdentifier
         andTransactionId:transaction.transactionIdentifier
    ];
}

- (void)checkoutCourses:(NSArray *)coursesIds withInAppId:(NSString *)inAppId andTransactionId:(NSString *)transactionId {
    float total = self.selectedProduct ? [self.selectedProduct.price floatValue] : 0;
    NSString *currency = self.selectedProduct ? [self.selectedProduct priceProduct] : @"";
    [self checkoutOrderWithCourses:coursesIds
                         withTotal:total
                      withCurrency:currency
                  withTrasactionId:transactionId
               withInAppPurchaseId:inAppId];
}

- (void)registerInAppNotifications {
    [[NSNotificationCenter defaultCenter]
            addObserver:self
               selector:@selector(coursesPurchased:)
                   name:IAPHelperProductPurchasedNotification
                 object:nil];

    [[NSNotificationCenter defaultCenter]
            addObserver:self
               selector:@selector(coursesPurchaseFailed:)
                   name:IAPHelperProductPurchaseErrorNotification
                 object:nil];

    [[NSNotificationCenter defaultCenter]
            addObserver:self
               selector:@selector(coursesPurchaseCancelled:)
                   name:IAPHelperProductPurchaseCancelNotification
                 object:nil];
}

- (void)unRegisterInAppNotifications {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:IAPHelperProductPurchasedNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:IAPHelperProductPurchaseErrorNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:IAPHelperProductPurchaseCancelNotification object:nil];
}

- (void)updatePendingOrder:(NSString *)user withToken:(NSString *)token withCursos:(NSArray *)courses
                 withTotal:(float)total withCurrency:(NSString *)currency withTrasactionId:(NSString *)transactionId
       withInAppPurchaseId:(NSString *)inAppPurchaseId {
    self.idUser = user;
    self.token = token;

    [self checkoutOrderWithCourses:courses
                         withTotal:total
                      withCurrency:currency
                  withTrasactionId:transactionId
               withInAppPurchaseId:inAppPurchaseId];
}

- (BOOL)isPossibleToRunMethod:(SEL)method withObject:(id)object {
    return object && [self doesObject:object haveSelectedMethod:method];
}

- (BOOL)doesObject:(id)object haveSelectedMethod:(SEL)selectorMethod {
    return [[object class] instancesRespondToSelector:selectorMethod];
}

@end
