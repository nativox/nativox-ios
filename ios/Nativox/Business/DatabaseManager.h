//
//  DatabaseManager.h
//  Nativox
//
//  Created by redhair84 on 09/03/2014.
//  Copyright (c) 2014 Francisco Javier Morant. All rights reserved.
//

#import <Foundation/Foundation.h>

@class VideoInfo;



@interface DatabaseManager : NSObject

@property(nonatomic, retain) NSManagedObjectContext *manager;

- (instancetype)initWithManager:(NSManagedObjectContext *)manager;

+ (instancetype)managerWithManager:(NSManagedObjectContext *)manager;

+ (DatabaseManager *)instance;


- (BOOL)insertVideoInfo:(NSDictionary *)videoInfo;

- (VideoInfo *)getVideoInfo:(NSString *)videoId;

- (void)deleteAllObjects:(NSString *)entityDescription;

@end
