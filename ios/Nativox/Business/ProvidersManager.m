//
// Created by redhair84 on 01/11/14.
// Copyright (c) 2014 Francisco Javier Morant. All rights reserved.
//

#import "ProvidersManager.h"
#import "UserProvider.h"
#import "SalesProvider.h"
#import "CategoryProvider.h"
#import "MetricsProvider.h"

@implementation ProvidersManager {

}

- (instancetype)init {
    self = [super init];
    if (self) {
        _categoryProvider = [[CategoryProvider alloc] init];
        _salesProvider = [[SalesProvider alloc] init];
        _userProvider = [[UserProvider alloc] init];
        _metricsProvider = [[MetricsProvider alloc] init];
    }
    return self;
}

+ (ProvidersManager *)instance {
    static ProvidersManager *_instance = nil;

    @synchronized (self) {
        if (_instance == nil) {
            _instance = [[self alloc] init];
        }
    }
    return _instance;
}

@end