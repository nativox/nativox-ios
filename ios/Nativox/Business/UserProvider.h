

#import <Foundation/Foundation.h>

@class BusinessError;
@class Response;
@class User;
@class HttpHandler;

static NSString *const kUrlServiceLogout = @"/logout";
static NSString *const kUrlServiceLoginFb = @"/login_fb";
static NSString *const kUrlServiceRegister = @"/register";
static NSString *const kUrlServiceLostPassword = @"/correo_recuperar_contrasenya";
static NSString *const kUrlServiceUserInfo = @"/usuario";
static NSString *const kUrlServiceTestToken = @"/test_token";
static NSString *const kUrlServiceEditarUserInfo = @"/usuario_editar";
static NSString *const kUrlServiceUpdateInfoCourseUser = @"/actualiza_info_curso_usuario";
static NSString *const kUrlServiceSaveUserOptions = @"/usuario_guardar_opciones";
static NSString *const kUrlServiceAddTranslation = @"/video_anyadir_traduccion";

@interface UserProvider : NSObject

@property(nonatomic, strong) HttpHandler *httpHandler;

- (void)loginWithFB:(NSString *)email userName:(NSString *)userName idFacebook:(NSString *)idFacebook fbToken:(NSString *)fbToken andName:(NSString *)name andHandler:(void (^)(User *, BusinessError *))handler;

- (void)registerUserWithEmail:(NSString *)email withName:(NSString *)name withPassword:(NSString *)password withLanguage:(NSString *)language andHandler:(void (^)(Response *, BusinessError *))handler;

- (void)recoverPassword:(NSString *)email andHandler:(void (^)(Response *, BusinessError *))handler;

- (void)testToken:(void (^)(BOOL, BusinessError *))handler;

- (void)retrieveUserInfo:(NSString *)idUser withToken:(NSString *)token andHandler:(void (^)(NSDictionary *, BusinessError *))handler;

- (void)updateUserInfoData:(NSDictionary *)dictionary andHandler:(void (^)(Response *, BusinessError *))handler;

- (void)updateUserInfoCourse:(NSDictionary *)updateInfoDict andHandler:(void (^)(BusinessError *, NSDictionary *))handler;

- (void)saveUserOptions:(void (^)(BusinessError *))handler;

- (void)sendTranslation:(NSString *)idUser withVideo:(NSString *)idVideo withLanguage:(NSString *)language withTranslation:(NSString *)translation andHandler:(void (^)(BusinessError *))handler;

@end