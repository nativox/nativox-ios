#import <Foundation/Foundation.h>

@class BusinessError;
@class HttpHandler;
@class Course;
@class SKProduct;

static NSString *const kServiceCompletedOrder = @"/pedido_completado";
static NSString *const kCoursesData = @"cursos";

@protocol SalesProviderDelegate <NSObject>

@optional

- (void)coursesCheckoutStarted;

- (void)coursesCheckoutFailed:(NSString *)message;

- (void)coursesCheckoutCompleted:(BusinessError *)error andData:(NSDictionary *)data;

- (void)coursesPurchaseStarted;

- (void)coursesPurchaseFailed:(NSString *)message;

- (void)coursesPurchaseCancelled;

- (void)coursesPurchaseCompleted;

@end

@interface SalesProvider : NSObject

@property(retain) id delegate;

- (void)purchaseCourse:(Course *)course;

- (void)updatePendingOrder:(NSString *)user withToken:(NSString *)token withCursos:(NSArray *)courses withTotal:(float)total withCurrency:(NSString *)currency withTrasactionId:(NSString *)transactionId withInAppPurchaseId:(NSString *)inAppPurchaseId;
@end
