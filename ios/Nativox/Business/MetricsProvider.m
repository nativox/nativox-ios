//
// Created by redhair84 on 31/07/15.
// Copyright (c) 2015 Francisco Javier Morant. All rights reserved.
//

#import "MetricsProvider.h"
#import "BusinessError.h"
#import "HttpHandler.h"
#import "ProvidersManager.h"

static NSString *const kServicePingNotification = @"/ping_notificacion";

@interface MetricsProvider ()
@property(nonatomic, strong) HttpHandler *httpHandler;
@end

@implementation MetricsProvider {

}

- (instancetype)init {
    self = [super init];
    if (self) {
        _httpHandler = [[HttpHandler alloc] init];
    }

    return self;
}

- (void)pingNotificationWithTitle:(NSString *)title
                      withMessage:(NSString *)message
             withNotificationType:(NSString *)notificationType
                       andHandler:(void (^)(BusinessError *))handler {

    NSString *encodedMessage = [message stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];

    NSDictionary *data = @{
            @"titulo" : title,
            @"mensaje" : encodedMessage,
            @"tipo_notificacion" : notificationType
    };

    [self.httpHandler sendGetRequest:data
                     withServiceName:kServicePingNotification
                          andHandler:^(NSDictionary *dictionary, NSError *error) {
                              if (error == nil) {
                                  if ([dictionary[kStatus] boolValue]) {
                                      handler(nil);
                                  } else {
                                      BusinessError *businessManagerError = [[BusinessError alloc] init];
                                      businessManagerError.message = dictionary[kResult];
                                      handler(businessManagerError);
                                  }
                              }
                          }];
}

@end