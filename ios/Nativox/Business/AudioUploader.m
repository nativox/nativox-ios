
#import "AudioUploader.h"
#import "UserSession.h"
#import "HttpHandler.h"

@interface AudioUploader ()

@property(nonatomic) NSURLSession *session;
@property(nonatomic) NSURLSessionUploadTask *uploadTask;

@end

@implementation AudioUploader

- (instancetype)initFileUploadManager {
    self = [super init];
    if (self) {
        _session = [self urlSession];
    }

    return self;
}

- (NSURLSession *)urlSession {
    static NSURLSession *session = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];

        [configuration setHTTPAdditionalHeaders:@{@"Accept": @"application/json"}];
        configuration.timeoutIntervalForRequest = 60.0;
        configuration.timeoutIntervalForResource = 60.0;

        session = [NSURLSession sessionWithConfiguration:configuration
                                                delegate:self
                                           delegateQueue:[NSOperationQueue mainQueue]];
    });
    return session;
}

- (void)startUpload:(NSString *)urlFile
        withVideoId:(NSString *)videoId
      andWithUserId:(NSString *)userId {
    NSURL *uploadURL = [NSURL fileURLWithPath:urlFile];
    NSString *urlRequest = [self urlRequest:videoId userId:userId];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:urlRequest]];
    [request setHTTPMethod:@"POST"];

    self.uploadTask = [self.session uploadTaskWithRequest:request
                                                 fromFile:uploadURL
                                        completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                            if (!error) {
                                                NSError *jsonError = nil;
                                                NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:data
                                                                                                             options:(NSJSONReadingOptions) kNilOptions
                                                                                                               error:&jsonError];

                                                if (!jsonError) {
                                                    [self.fileUploadManagerDelegate uploadAudioFileFinished:jsonResponse
                                                                                                   andError:nil];
                                                } else {
                                                    [self.fileUploadManagerDelegate uploadAudioFileFinished:nil
                                                                                                   andError:jsonError];
                                                }
                                            } else {
                                                [self.fileUploadManagerDelegate uploadAudioFileFinished:nil
                                                                                               andError:error];
                                            }
                                        }];

    [self.uploadTask resume];
}

- (NSString *)urlRequest:(const NSString *)videoId userId:(const NSString *)userId {
    NSString *urlRequest;
    NSString *language = [UserSession instance].language;

    urlRequest = [NSString stringWithFormat:@"%@/evalua_audio?id_video=%@&id_usuario=%@&realizar_notificaciones=false", kUrlHostname, videoId, userId];

    NSString *version = [[NSBundle mainBundle] infoDictionary][@"CFBundleShortVersionString"];
    urlRequest = [NSString stringWithFormat:@"%@&lang=%@&os=ios&id_device=%@&version=%@&no_guardar_datos_usuario", urlRequest, language,
                                            [UIDevice currentDevice].identifierForVendor.UUIDString, version];
    return urlRequest;
}

@end