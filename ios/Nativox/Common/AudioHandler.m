#import <AVFoundation/AVFoundation.h>
#import "AudioHandler.h"

@interface AudioHandler ()

@property(nonatomic, strong) AVAudioRecorder *acousticAudioRecorder;
@property(nonatomic, strong) AVAudioPlayer *player;

@end

@implementation AudioHandler {

}
+ (instancetype)initAudioHandler {
    return [[AudioHandler alloc] init];
}

- (BOOL)setupRecorder {
    NSError *error;

    int64_t currentTime = (int64_t) ([[NSDate date] timeIntervalSince1970] * 1000);

    self.shortAudioFileName = [NSString stringWithFormat:@"%qi.wav", currentTime];
    NSArray *pathComponents = @[[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject], self.shortAudioFileName];
    self.outputAudioFileURL = [NSURL fileURLWithPathComponents:pathComponents];

    self.acousticAudioRecorder = [[AVAudioRecorder alloc] initWithURL:self.outputAudioFileURL settings:[self getRecordSettings:@44100.0f] error:&error];

    if (error) {
        return NO;
    }

    self.acousticAudioRecorder.delegate = nil;
    [self.acousticAudioRecorder prepareToRecord];

    return error == nil;
}

- (NSDictionary *)getRecordSettings:(NSNumber *)number {
    NSDictionary *recordSetting = @{AVFormatIDKey : @(kAudioFormatLinearPCM),
            AVEncoderAudioQualityKey : @(AVAudioQualityMedium),
            AVNumberOfChannelsKey : @1,
            AVLinearPCMBitDepthKey : @16,
            AVLinearPCMIsFloatKey : @NO,
            AVLinearPCMIsBigEndianKey : @NO,
            AVEncoderBitRateKey : @16,
            AVEncoderBitDepthHintKey : @16,
            AVSampleRateKey : number
    };
    return recordSetting;
}

- (BOOL)setupPlayer {
    NSError *error;
    self.player = [[AVAudioPlayer alloc] initWithContentsOfURL:self.outputAudioFileURL error:&error];

    if (!error) {
        [self.player setDelegate:nil];
    } else {
        return NO;
    }

    return YES;
}

- (void)startRecord {
    [self.acousticAudioRecorder record];
}

- (void)stopRecorder {
    [self.acousticAudioRecorder stop];
    self.acousticAudioRecorder = nil;
}

- (void)playPlayerRecordedAudio {
    [self.player play];
}

- (void)stopPlayerRecordedAudio {
    [self.player stop];
    self.player = nil;
}

+ (BOOL)setupAVAudioSession {
    AVAudioSession *session = [AVAudioSession sharedInstance];
    NSError *error;

    if (![session setCategory:AVAudioSessionCategoryPlayAndRecord error:&error]) {
        DLog(@"AVAudioSession error setting category:%@", error);
        return NO;
    }

    if (![self isHeadsetPluggedIn]) {
        if (![session overrideOutputAudioPort:AVAudioSessionPortOverrideSpeaker error:&error]) {
            DLog(@"AVAudioSession error overrideOutputAudioPort:%@", error);
            return NO;
        }
    }

    if (![session setActive:YES error:&error]) {
        DLog(@"AVAudioSession error activating: %@", error);
        return NO;
    }

    return YES;
}

- (void)cleanUp {
    AVAudioSession *session = [AVAudioSession sharedInstance];
    NSError *error;

    [session setCategory:AVAudioSessionCategoryPlayback error:&error];
    [session setActive:NO error:&error];
    self.player = nil;
    self.acousticAudioRecorder = nil;
}

+ (BOOL)isHeadsetPluggedIn {
    AVAudioSessionRouteDescription *route = [[AVAudioSession sharedInstance] currentRoute];
    for (AVAudioSessionPortDescription *desc in [route outputs]) {
        if ([[desc portType] isEqualToString:AVAudioSessionPortHeadphones])
            return YES;
    }
    return NO;
}


@end