#import <MRProgress/MRProgressOverlayView.h>
#import "ProgressDialogModule.h"
#import "NativoxAppDelegate.h"

@implementation ProgressDialogModule

RCT_EXPORT_MODULE();

RCT_EXPORT_METHOD(showMessage:(NSString *)message)
{
    dispatch_async(dispatch_get_main_queue(), ^{
        NativoxAppDelegate *appDelegate = (NativoxAppDelegate *) [[UIApplication sharedApplication] delegate];
        [MRProgressOverlayView showOverlayAddedTo:appDelegate.window
                                            title:message
                                             mode:MRProgressOverlayViewModeIndeterminate
                                         animated:YES];
    });

}

RCT_EXPORT_METHOD(dismissMessage) {
    dispatch_async(dispatch_get_main_queue(), ^{
        NativoxAppDelegate *appDelegate = (NativoxAppDelegate *) [[UIApplication sharedApplication] delegate];
        [MRProgressOverlayView dismissAllOverlaysForView:appDelegate.window
                                                animated:YES];
    });
}

@end