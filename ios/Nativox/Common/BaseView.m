#import <MRProgress/MRProgressOverlayView.h>
#import "BaseView.h"
#import "AlertDialogView.h"

@implementation BaseView {

}
- (void)presentMessage:(NSString *)message {
    [AlertDialogView showDialog:message];
}

- (void)showProgressViewWithTitle:(NSString *)title {
    [MRProgressOverlayView showOverlayAddedTo:self
                                        title:title
                                         mode:MRProgressOverlayViewModeIndeterminate
                                     animated:YES];
}

- (void)dismissProgressView {
    [MRProgressOverlayView dismissOverlayForView:self
                                        animated:YES];
}

@end