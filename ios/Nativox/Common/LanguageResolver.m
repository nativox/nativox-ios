//
// Created by redhair84 on 09/09/15.
// Copyright (c) 2015 Francisco Javier Morant. All rights reserved.
//

#import "LanguageResolver.h"


@interface LanguageResolver ()
@property(nonatomic, strong) NSArray *languages;
@end

@implementation LanguageResolver {

}

- (instancetype)init {
    self = [super init];
    if (self) {
        NSString *languages = [[NSBundle mainBundle] pathForResource:@"languages" ofType:@"plist"];
        self.languages = [[NSArray alloc] initWithContentsOfFile:languages];
    }

    return self;
}

+ (LanguageResolver *)instance {
    static LanguageResolver *_instance = nil;

    @synchronized (self) {
        if (_instance == nil) {
            _instance = [[self alloc] init];
        }
    }

    return _instance;
}

- (NSArray *)languagesTitles {
    NSMutableArray *languageTitles = [[NSMutableArray alloc] init];
    for (NSDictionary *languageData in self.languages) {
        [languageTitles addObject:languageData.allValues[0]];
    }
    return [languageTitles copy];
}

- (NSString *)languageTitle:(NSString *)languageCode {
    if (languageCode && ![languageCode isEqualToString:@""]) {
        for (NSDictionary *languageData in self.languages) {
            if (languageData[languageCode] && ![languageData[languageCode] isEqualToString:@""]) {
                return languageData[languageCode];
            }
        }
    }

    return NSLocalizedString(@"settings_alarm_select_button", @"");
}

- (NSString *)languageCode:(NSString *)languageTitle {
    if (languageTitle && ![languageTitle isEqualToString:@""]) {
        for (NSDictionary *languageData in self.languages) {
            NSArray *languageCodes = [languageData allKeysForObject:languageTitle];
            if (languageCodes && languageCodes.count > 0) {
                return languageCodes[0];
            }
        }
    }

    return nil;
}

@end