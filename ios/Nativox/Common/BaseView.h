//
// Created by redhair84 on 11/08/15.
// Copyright (c) 2015 Francisco Javier Morant. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserInterface.h"


@interface BaseView : UIView <UserInterface>
@end