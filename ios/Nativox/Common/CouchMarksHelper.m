//
// Created by redhair84 on 09/11/2015.
// Copyright (c) 2015 Francisco Javier Morant. All rights reserved.
//

#import "CouchMarksHelper.h"
#import "MPCoachMarks.h"


@implementation CouchMarksHelper {

}
+ (MPCoachMarks *)showMarksCoachMarks:(NSArray *)coachMarks
                     intermediateView:(UIView *)intermediateView
                            outerView:(UIView *)outerView
                          andDelegate:(id <MPCoachMarksViewDelegate>)delegate {

    NSMutableArray *coachMarksMutable = [[NSMutableArray alloc] init];
    for (NSDictionary *coachMark in coachMarks) {
        UIView *innerView = coachMark[@"view"];

        CGRect rect = [intermediateView convertRect:innerView.frame toView:outerView];
        NSMutableDictionary *coachMarkMutable = [coachMark mutableCopy];
        coachMarkMutable[@"rect"] = [NSValue valueWithCGRect:rect];
        [coachMarksMutable addObject:coachMarkMutable];
    }

    MPCoachMarks *coachMarksView = [[MPCoachMarks alloc] initWithFrame:outerView.bounds
                                                            coachMarks:coachMarksMutable];
    coachMarksView.animationDuration = 0.4f;
    coachMarksView.enableContinueLabel = NO;
    coachMarksView.enableSkipButton = NO;
    coachMarksView.maskColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.85];
    coachMarksView.delegate = delegate;
    [outerView addSubview:coachMarksView];
    [coachMarksView start];
    return coachMarksView;
}

@end