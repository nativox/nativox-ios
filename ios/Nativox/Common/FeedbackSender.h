
#import <Foundation/Foundation.h>

@interface FeedbackSender : NSObject

- (void)sendFeedbackEvent:(NSString *)event andExtraInfo:(NSDictionary *)extraInfo;

+ (FeedbackSender *)instance;

@end