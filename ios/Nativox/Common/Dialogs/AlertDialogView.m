#import "AlertDialogView.h"
#import "RCTRootView.h"
#import "NativoxAppDelegate.h"
#import "CustomIOSAlertView.h"

static AlertDialogView *instance = nil;

@interface AlertDialogView ()
@property(nonatomic, strong) NSString *message;
@property(nonatomic, strong) RCTRootView *rootView;
@end

@implementation AlertDialogView

RCT_EXPORT_MODULE();

- (void)loadAlertDialog {
    NativoxAppDelegate *delegate = (NativoxAppDelegate *) [[UIApplication sharedApplication] delegate];

    self.rootView = [[RCTRootView alloc] initWithBridge:delegate.bridge moduleName:@"AlertDialog"
                                              initialProperties:@{
                                                      @"message" : self.message
                                              }];
    [self.rootView setBackgroundColor:[UIColor clearColor]];
    [self addSubview:self.rootView];
    self.rootView.frame = self.bounds;
}

- (dispatch_queue_t)methodQueue {
    return dispatch_get_main_queue();
}

RCT_EXPORT_METHOD(closeDialog) {
    self.message = @"";
    [[AlertDialogView instance].customAlertView close];
    [AlertDialogView clear];
}

+ (AlertDialogView *)instance {
    @synchronized (self) {
        if (instance == nil) {
            instance = [[[NSBundle mainBundle] loadNibNamed:@"AlertDialog" owner:nil options:nil] lastObject];
        }
    }

    return instance;
}

+ (void)clear {
    instance = nil;
}

+ (void)showDialog:(NSString *)message {
    CustomIOSAlertView *dialogAlertView = [[CustomIOSAlertView alloc] init];
    AlertDialogView *alertDialogView = [AlertDialogView instance];
    alertDialogView.message = message;
    [dialogAlertView setContainerView:alertDialogView];
    [dialogAlertView setButtonTitles:NULL];
    [dialogAlertView setUseMotionEffects:YES];
    //dialogAlertView.alphaContainer = 1;
    [dialogAlertView show];
    //dialogAlertView.backgroundColor = [UIColor clearColor];
    dialogAlertView.dialogView.backgroundColor = [UIColor clearColor];
    dialogAlertView.containerView.backgroundColor = [UIColor clearColor];
    alertDialogView.customAlertView = dialogAlertView;
    [alertDialogView loadAlertDialog];
}

@end
