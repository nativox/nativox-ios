//
//  TimePickerDialog.h
//  Nativox
//
//  Created by redhair84 on 05/07/2015.
//  Copyright (c) 2015 Francisco Javier Morant. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CustomIOSAlertView;

@interface TimePickerDialog : UIView

@property(nonatomic, strong) CustomIOSAlertView *alertViewContainer;
@property(nonatomic, copy) void (^completionBlockSetTime)(NSDate *date);

- (void)setTime:(NSDate *)date;
@end
