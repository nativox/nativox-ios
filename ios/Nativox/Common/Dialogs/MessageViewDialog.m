
#import "MessageViewDialog.h"
#import "CustomIOSAlertView.h"

@interface MessageViewDialog ()
@property(strong, nonatomic) IBOutlet UILabel *messageLabel;
@property(strong, nonatomic) IBOutlet UIImageView *backgroundAppImage;
@property(strong, nonatomic) IBOutlet UIButton *okButton;
@property(nonatomic, strong) CustomIOSAlertView *dialogAlertView;
@end

@implementation MessageViewDialog

- (void)awakeFromNib {
    [super awakeFromNib];
    self.backgroundAppImage.layer.cornerRadius = 5;
    self.okButton.layer.cornerRadius = 5;
}

- (IBAction)okButtonPressed:(id)sender {
    [self.dialogAlertView close];
}

+ (void)showDialog:(NSString *)message {
    CustomIOSAlertView *dialogAlertView = [[CustomIOSAlertView alloc] init];
    MessageViewDialog *messageDialog = [[[NSBundle mainBundle] loadNibNamed:@"MessageDialog" owner:nil options:nil] lastObject];
    messageDialog.dialogAlertView = dialogAlertView;
    messageDialog.messageLabel.text = message;
    [dialogAlertView setContainerView:messageDialog];
    [dialogAlertView setButtonTitles:NULL];
    [dialogAlertView setUseMotionEffects:YES];
    dialogAlertView.alphaContainer = 1;
    [dialogAlertView show];
}

@end
