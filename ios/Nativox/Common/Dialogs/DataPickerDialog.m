
#import "DataPickerDialog.h"
#import "CustomIOSAlertView.h"
#import "LanguageResolver.h"

@interface DataPickerDialog ()
@property(strong, nonatomic) IBOutlet UIImageView *dataPickerDialogBackground;
@property(nonatomic, strong) NSString *selectedLanguageTitle;
@property(nonatomic, strong) id selectedLanguageCode;
@property(strong, nonatomic) IBOutlet UIPickerView *pickerView;
@end

@implementation DataPickerDialog

- (void)awakeFromNib {
    [super awakeFromNib];

    self.dataPickerDialogBackground.layer.cornerRadius = 5;

    self.selectedLanguageTitle = [[LanguageResolver instance] languagesTitles][(NSUInteger) 0];
    self.selectedLanguageCode = [[LanguageResolver instance] languageCode:self.selectedLanguageTitle];
}

- (IBAction)closeButtonPressed:(id)sender {
    [self.dialogAlerView close];
}

- (IBAction)setButtonPressed:(id)sender {
    [self.delegate onDataSelected:self.selectedLanguageTitle withLanguageCode:self.selectedLanguageCode];
    [self.dialogAlerView close];
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view {
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, pickerView.frame.size.width, 44)];
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor whiteColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:18];
    label.text = [[LanguageResolver instance] languagesTitles][(NSUInteger) row];
    return label;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    self.selectedLanguageTitle = [[LanguageResolver instance] languagesTitles][(NSUInteger) row];
    self.selectedLanguageCode = [[LanguageResolver instance] languageCode:self.selectedLanguageTitle];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return [[LanguageResolver instance] languagesTitles].count;
}

- (void)setSelectedOption {
    if (self.selectedData) {
        NSString *selectedLanguageTitle = [[LanguageResolver instance] languageTitle:self.selectedData];
        if (selectedLanguageTitle && ![selectedLanguageTitle isEqualToString:NSLocalizedString
        (@"settings_alarm_select_button", @"")]) {
            self.selectedLanguageTitle = selectedLanguageTitle;
            self.selectedLanguageCode = self.selectedData;
            [self.pickerView selectRow:[[[LanguageResolver instance] languagesTitles] indexOfObject:self.selectedLanguageTitle] inComponent:0 animated:YES];
        }
    }
}
@end
