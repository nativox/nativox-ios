#import <UIKit/UIKit.h>

@class CustomIOSAlertView;

@interface MessageViewDialog : UIView

+ (void)showDialog:(NSString *)message;

@end
