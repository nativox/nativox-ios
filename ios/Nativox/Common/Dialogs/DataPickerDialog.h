//
//  DataPickerDialog.h
//  Nativox
//
//  Created by redhair84 on 03/09/2015.
//  Copyright (c) 2015 Francisco Javier Morant. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CustomIOSAlertView;

@protocol DataPickerDialogDelegate

- (void)onDataSelected:(NSString *)valueSelected withLanguageCode:(NSString *)code;

@end

@interface DataPickerDialog : UIView <UIPickerViewDelegate, UIPickerViewDataSource>

@property(weak, nonatomic) id <DataPickerDialogDelegate> delegate;
@property(nonatomic, strong) CustomIOSAlertView *dialogAlerView;
@property(nonatomic, copy) NSString *selectedData;

- (void)setSelectedOption;
@end
