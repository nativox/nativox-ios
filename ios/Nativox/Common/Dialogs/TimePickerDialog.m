#import "TimePickerDialog.h"
#import "CustomIOSAlertView.h"

@interface TimePickerDialog ()
@property(strong, nonatomic) IBOutlet UIDatePicker *timePicker;
@property(strong, nonatomic) IBOutlet UIImageView *backgroundAppImageView;
@property(strong, nonatomic) IBOutlet UIImageView *backgroundDialogImageView;

@end

@implementation TimePickerDialog

- (void)awakeFromNib {
    [super awakeFromNib];

    UIDatePicker *datePicker = [[UIDatePicker alloc] initWithFrame:self.timePicker.frame];
    datePicker.date = self.timePicker.date;
    datePicker.datePickerMode = self.timePicker.datePickerMode;
    [self.timePicker removeFromSuperview];
    self.timePicker = datePicker;
    [self addSubview:self.timePicker];

    [self.timePicker setValue:[UIColor whiteColor] forKeyPath:@"textColor"];

    self.backgroundAppImageView.layer.cornerRadius = 5;
    self.backgroundAppImageView.clipsToBounds = YES;
    self.backgroundDialogImageView.layer.cornerRadius = 5;
    self.backgroundDialogImageView.clipsToBounds = YES;
}

- (IBAction)closeButtonPressed:(id)sender {
    [self.alertViewContainer close];
}

- (IBAction)setTimeButtonPressed:(id)sender {
    if (self.completionBlockSetTime) {
        self.completionBlockSetTime(self.timePicker.date);
    }
    [self.alertViewContainer close];
}

- (void)setTime:(NSDate *)date {
    if (date) {
        [self.timePicker setDate:date];
    }
}
@end
