#import <UIKit/UIKit.h>
#import "RCTBridgeModule.h"

@class CustomIOSAlertView;
@class RCTRootView;

@interface AlertDialogView : UIView <RCTBridgeModule>

@property(nonatomic, strong) CustomIOSAlertView *customAlertView;

- (void)loadAlertDialog;

+ (void)showDialog:(NSString *)message;

@end
