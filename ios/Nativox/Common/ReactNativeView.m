#import <React/RCTRootView.h>
#import "ReactNativeView.h"
#import "NativoxAppDelegate.h"
#import "UpdateAccountViewController.h"
#import "UserSession.h"
#import "UserSessionPreferences.h"

@interface ReactNativeView ()
@property(nonatomic, strong) BaseViewController *viewController;
@end

@implementation ReactNativeView {

}

RCT_EXPORT_MODULE();

- (void)initializeReactView:(NSString *)screenName
             viewController:(BaseViewController *)controller
                 andOptions:(NSDictionary *)screenOptions {

    [ReactNativeView instance].viewController = controller;

    NSDictionary *options = @{
            @"userEmail": [UserSession instance].email ? [UserSession instance].email : @"",
            @"userName": [UserSession instance].name ? [UserSession instance].name : @"",
            @"token": [UserSession instance].token ? [UserSession instance].token : @"",
            @"idUser": [UserSession instance].idUser ? [UserSession instance].idUser : @"",
            @"language": [UserSession instance].language ? [UserSession instance].language : @"",
            @"screenName": screenName,
            @"idSession": [NSString stringWithFormat:@"%d", (int) [UserSession instance].startSession]
    };

    NSMutableDictionary *reactOptions = [NSMutableDictionary dictionaryWithDictionary:options];
    [reactOptions addEntriesFromDictionary:screenOptions];

    if ([ReactNativeView instance].rootView == nil) {
        NativoxAppDelegate *delegate = (NativoxAppDelegate *) [[UIApplication sharedApplication] delegate];

        [ReactNativeView instance].rootView = [[RCTRootView alloc] initWithBridge:delegate.bridge moduleName:@"NativoxApp" initialProperties:reactOptions];
    } else {
        [[ReactNativeView instance].rootView setAppProperties:reactOptions];
    }

    [[ReactNativeView instance].rootView setBackgroundColor:[UIColor clearColor]];
    [ReactNativeView instance].rootView.translatesAutoresizingMaskIntoConstraints = NO;
    [self addSubview:[ReactNativeView instance].rootView];

    NSDictionary *views = @{@"rootView": [ReactNativeView instance].rootView};
    NSArray *constraints = @[];
    constraints = [constraints arrayByAddingObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[rootView]-0-|" options:0 metrics:nil views:views]];
    constraints = [constraints arrayByAddingObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[rootView]-0-|" options:0 metrics:nil views:views]];

    [self addConstraints:constraints];
    [self layoutIfNeeded];
}

+ (ReactNativeView *)instance {
    static ReactNativeView *_instance = nil;

    @synchronized (self) {
        if (_instance == nil) {
            _instance = [[self alloc] init];
        }
    }

    return _instance;
}

RCT_EXPORT_METHOD(saveUserSession:
    (NSDictionary *) userSession) {
    [UserSession instance].name = userSession[@"name"];
    [UserSession instance].email = userSession[@"email"];
}

RCT_EXPORT_METHOD(disableBackButton) {
    [ReactNativeView instance].viewController.navigationController.navigationBar.userInteractionEnabled = NO;
}

RCT_EXPORT_METHOD(enableBackButton) {
    [ReactNativeView instance].viewController.navigationController.navigationBar.userInteractionEnabled = YES;
}

RCT_EXPORT_METHOD(popScreen) {
    dispatch_async(dispatch_get_main_queue(), ^{
        [[ReactNativeView instance].viewController.navigationController popViewControllerAnimated:YES];
    });
}

RCT_EXPORT_METHOD(postNotification:
    (NSString *) eventName
            data:
            (NSDictionary *) data) {
    dispatch_async(dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter] postNotificationName:eventName object:data];
    });
}

RCT_EXPORT_METHOD(savePreferredLanguage:
    (NSString *) languageCode
            email:
            (NSString *) email) {
    UserSessionPreferences *userSessionPreferences = [[UserSessionPreferences alloc] init];
    [userSessionPreferences insert:languageCode andKey:[NSString stringWithFormat:@"%@_lang", email]];
    [userSessionPreferences commit];
}

@end