#import "MRProgress/MRProgressOverlayView.h"
#import "BaseViewController.h"
#import "LoginViewController.h"
#import "StartupTutorialViewController.h"
#import "MainNavigationViewController.h"
#import "NativoxAppDelegate.h"
#import "ProvidersManager.h"
#import "UserSession.h"
#import "BusinessError.h"
#import "CategoryProvider.h"
#import "AlertDialogView.h"
#import "GAITracker.h"
#import "GAI.h"
#import "GAIFields.h"
#import "GAIDictionaryBuilder.h"
#import "Answers.h"

@implementation BaseViewController {

}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:NSStringFromClass([self class])];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

- (void)logoutFromLessonScreen {
    UIViewController *rootViewController = [[[UIApplication sharedApplication] windows][0] rootViewController];
    LoginViewController *loginViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"loginView"];

    if ([rootViewController isKindOfClass:[StartupTutorialViewController class]]) {
        MainNavigationViewController *mainNavigationViewController = [self.storyboard instantiateViewControllerWithIdentifier:kInitialNavigationControllerIdentifier];
        [[[UIApplication sharedApplication] windows][0] setRootViewController:mainNavigationViewController];

        [mainNavigationViewController pushViewController:loginViewController animated:NO];
        mainNavigationViewController.navigationBar.hidden = NO;
    } else {
        UINavigationController *navigationController = (UINavigationController *) rootViewController;

        [navigationController popToRootViewControllerAnimated:NO];
        navigationController.navigationBar.hidden = NO;
    }

    [AlertDialogView showDialog:NSLocalizedString(@"user_session_expired", @"")];
    [[UserSession instance] resetSession];
}

- (void)presentMessage:(NSString *)message {
    [AlertDialogView showDialog:message];
}

- (void)showProgressViewWithTitle:(NSString *)title {
    [MRProgressOverlayView showOverlayAddedTo:self.view
                                        title:title
                                         mode:MRProgressOverlayViewModeIndeterminate
                                     animated:YES];
}

- (void)dismissProgressView {
    [MRProgressOverlayView dismissOverlayForView:self.view animated:YES];
}

- (void)retrieveCategories {
//    [[ProvidersManager instance].categoryProvider categoriesWith:[UserSession instance].idUser
//                                                       withToken:[UserSession instance].token
//                                                      andHandler:^(NSArray *categories, BusinessError *error) {
//                                                          if (!error) {
//                                                              NativoxAppDelegate *appDelegate = (NativoxAppDelegate *) [[UIApplication sharedApplication] delegate];
//                                                              [appDelegate updatePendingSales];
//                                                          } else {
//                                                              [self logoutSession:error];
//                                                          }
//                                                      }];
}

- (void)logoutSession:(BusinessError *)error {
    [AlertDialogView showDialog:error.message];
    [self.tabBarController.navigationController popViewControllerAnimated:YES];
    [[UserSession instance] resetSession];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

@end
