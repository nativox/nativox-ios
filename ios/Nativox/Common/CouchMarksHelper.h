//
// Created by redhair84 on 09/11/2015.
// Copyright (c) 2015 Francisco Javier Morant. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol MPCoachMarksViewDelegate;
@class MPCoachMarks;

@interface CouchMarksHelper : NSObject
+ (MPCoachMarks *)showMarksCoachMarks:(NSArray *)coachMarks intermediateView:(UIView *)intermediateView outerView:(UIView *)outerView andDelegate:(id <MPCoachMarksViewDelegate>)delegate;
@end