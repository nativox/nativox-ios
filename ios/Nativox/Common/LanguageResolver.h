//
// Created by redhair84 on 09/09/15.
// Copyright (c) 2015 Francisco Javier Morant. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface LanguageResolver : NSObject

+ (LanguageResolver *)instance;

- (NSArray *)languagesTitles;

- (NSString *)languageTitle:(NSString *)languageCode;

- (NSString *)languageCode:(NSString *)languageTitle;

@end