
#import <Foundation/Foundation.h>

@interface VideoPlayerHandler : NSObject

@property(strong, nonatomic) AVPlayer *avPlayer;
@property(strong, nonatomic) AVPlayerLayer *layer;

+ (instancetype)initVideo:(NSString *)filepath inView:(UIView *)view;

- (void)resetVideo:(NSString *)filepath;

- (void)playVideo;

- (void)stopVideo;

- (void)hideVideo:(BOOL)isHidden;

- (void)changeVolume:(float)volume;

- (float)volume;

- (void)seekVideoToZero;

- (void) cleanUpVideoPlayer;

@end