//
// Created by redhair84 on 20/08/2014.
// Copyright (c) 2014 Francisco Javier Morant. All rights reserved.
//

#import "AnimatorHandler.h"
#import "UIColor+NativoxColors.h"
#import "DeviceUtils.h"

@interface AnimatorHandler ()

@property(nonatomic, strong) CABasicAnimation *animation;
@property(nonatomic, strong) UIButton *targetView;
@property(nonatomic, strong) UIView *blueCover;

@end

@implementation AnimatorHandler {

}

- (id)init {
    self = [super init];
    if (self) {
    }

    return self;
}

- (void)addAnimationToView:(UIView *)view {
    if(self.targetView == view) {
        [self stopAnimation];
    }

    self.targetView = (UIButton *) view;

    CABasicAnimation *fadeAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    fadeAnimation.fromValue = @1.0F;
    fadeAnimation.toValue = @0.5F;
    fadeAnimation.duration = 0.9;
    fadeAnimation.repeatCount = INFINITY;
    fadeAnimation.autoreverses = YES;

    [self.targetView.layer addAnimation:fadeAnimation forKey:@"fadeInOut"];

    self.blueCover = [[UIView alloc] initWithFrame:self.targetView.bounds];
    self.blueCover.layer.cornerRadius = [DeviceUtils instance].isOnIpad ? 12 : 6;
    self.blueCover.alpha = 0.4F;
    self.blueCover.userInteractionEnabled = NO;
    [self.blueCover setBackgroundColor:[UIColor nativoxBlueColor]];
    [self.targetView addSubview:self.blueCover];
}

- (void)stopAnimation {
    if (self.targetView) {
        self.targetView.alpha = 1.0;
        [self.targetView.layer removeAllAnimations];
        [self.blueCover removeFromSuperview];
    }
}


@end