#import "FreeCoursesTimer.h"

static const int kCountDownInterval = 1;
static const int kCountDownTime10Minutes = 60 * 10;

@interface FreeCoursesTimer ()
@property(nonatomic, strong) NSTimer *timer;

@end

@implementation FreeCoursesTimer {
        NSTimeInterval countDownTime;
}

- (instancetype)init {
        self = [super init];
        if (self) {
                countDownTime = 0;
        }

        return self;
}

- (void)setCountDownTime:(NSTimeInterval)time {
        countDownTime = (time < 0) ? 0 : time;
}

- (NSTimeInterval)countDownTime {
        return countDownTime;
}

- (NSString *)countDownTimeFormatted {
        return [self stringFromTimeInterval];
}

- (NSString *)stringFromTimeInterval {
        NSInteger time = (NSInteger) self.countDownTime;
        NSInteger seconds = time % 60;
        NSInteger minutes = (time / 60) % 60;
        return [NSString stringWithFormat:@"%02ld:%02ld", (long) minutes, (long) seconds];
}

- (void)start {
        if (countDownTime > 0) {
                if (self.timer) {
                        [self.timer invalidate];
                        self.timer = nil;
                }

                self.timer = [NSTimer scheduledTimerWithTimeInterval:kCountDownInterval
                              target:self
                              selector:@selector(updateCountDownTime)
                              userInfo:nil
                              repeats:YES];
        }
}

- (void)updateCountDownTime {
        if (countDownTime > 0) {
                countDownTime = countDownTime - self.timer.timeInterval;

                if (countDownTime == kCountDownTime10Minutes) {
                        [self.delegate countDownTimerReachLessThan10Minutes];
                }
        } else {
                [self stop];
                countDownTime = 0;
                if (self.delegate) {
                        [self.delegate freeCoursesTimerReachZero];
                }
        }
}

- (void)stop {
        if ([self isRunning]) {
                [self.timer invalidate];
                self.timer = nil;
        }
}


- (BOOL)isRunning {
        return self.timer.isValid;
}
@end
