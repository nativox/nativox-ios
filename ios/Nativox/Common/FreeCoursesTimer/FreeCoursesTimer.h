#import <Foundation/Foundation.h>

@protocol FreeCoursesTimerDelegate <NSObject>

- (void)freeCoursesTimerReachZero;

- (void)countDownTimerReachLessThan10Minutes;

@optional

@end

@interface FreeCoursesTimer : NSObject

@property(retain) id delegate;

- (void)setCountDownTime:(NSTimeInterval)time;

- (NSTimeInterval)countDownTime;

- (NSString *)countDownTimeFormatted;

- (void)start;

- (void)stop;

- (BOOL)isRunning;
@end
