#import <Foundation/Foundation.h>
#import "React/RCTBridgeModule.h"
#import <React/RCTRootView.h>

@class BaseViewController;


@interface ReactNativeView : UIView <RCTBridgeModule>
@property(nonatomic, strong) RCTRootView *rootView;

+ (ReactNativeView *)instance;


- (void)initializeReactView:(NSString *)screenName viewController:(BaseViewController *)controller andOptions:(NSDictionary *)screenOptions;

@end
