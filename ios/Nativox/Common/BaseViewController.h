#import <Foundation/Foundation.h>
#import "UserInterface.h"

@interface BaseViewController : UIViewController <UserInterface>

- (void)logoutFromLessonScreen;

- (void)presentMessage:(NSString *)message;

- (void)showProgressViewWithTitle:(NSString *)title;

- (void)dismissProgressView;

- (void)retrieveCategories;

@end