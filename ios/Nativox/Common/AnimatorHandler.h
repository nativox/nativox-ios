//
// Created by redhair84 on 20/08/2014.
// Copyright (c) 2014 Francisco Javier Morant. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface AnimatorHandler : NSObject

- (void)addAnimationToView:(UIView *)view;

- (void)stopAnimation;
@end