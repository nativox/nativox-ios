#import <Foundation/Foundation.h>

@interface AudioHandler : NSObject

@property(nonatomic, strong) NSURL *outputAudioFileURL;

@property(nonatomic, strong) NSString *shortAudioFileName;

+ (instancetype)initAudioHandler;

+ (BOOL)setupAVAudioSession;

- (BOOL)setupRecorder;

- (BOOL)setupPlayer;

- (void)startRecord;

- (void)stopRecorder;

- (void)playPlayerRecordedAudio;

- (void)stopPlayerRecordedAudio;

- (void) cleanUp;

+ (BOOL)isHeadsetPluggedIn;

@end