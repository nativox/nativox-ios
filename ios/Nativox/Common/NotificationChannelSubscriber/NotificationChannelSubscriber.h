
#import <Foundation/Foundation.h>

static NSString *const kUserHasBoughtCourseChannel = @"app_usuario_ha_comprado";
static NSString *const kUserHasLoggedInChannel = @"app_usuario_ha_logueado";

@interface NotificationChannelSubscriber : NSObject

- (void)subscribeToChannel:(NSString *)channel;

@end