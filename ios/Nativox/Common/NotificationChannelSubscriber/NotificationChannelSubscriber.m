#import <Parse/Parse.h>
#import "NotificationChannelSubscriber.h"
#import "UserSession.h"

static NSString *const kChannels = @"channels";

@implementation NotificationChannelSubscriber {

}
- (void)subscribeToChannel:(NSString *)channel {
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    [currentInstallation addUniqueObject:channel forKey:kChannels];

    if ([UserSession instance].idUser) {
        [currentInstallation setObject:[UserSession instance].idUser forKey:@"id_usuario"];
    }

    [currentInstallation saveInBackground];
}

@end