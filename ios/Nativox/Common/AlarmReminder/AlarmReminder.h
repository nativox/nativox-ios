#import <Foundation/Foundation.h>

@class Alarm;

@interface AlarmReminder : NSObject

+ (AlarmReminder *)instance;

- (NSArray *)activatedAlarms;

- (void)addAlarm:(Alarm *)alarm;

- (void)saveAlarmsWithHour:(NSInteger)hour andMinutes:(NSInteger)minutes;

- (void)removeAllAlarms;

- (NSString *)printAlarms;

- (NSString *)formatSelectedTime;

- (void)saveAlarms:(NSMutableArray *)temporaryAlarms withHour:(NSInteger)hour andMinutes:(NSInteger)minutes;
@end