//
// Created by redhair84 on 03/08/15.
// Copyright (c) 2015 Francisco Javier Morant. All rights reserved.
//

#import <Foundation/Foundation.h>


static NSString *const kAlarmTypeTrainingReminder = @"alarma_calendario";
static NSString *const kAlarmTypeFreeCoursesReminder = @"alarma_cursos_gratis";

@interface AlarmConstants : NSObject
@end