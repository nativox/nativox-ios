#import "AlarmReminder.h"
#import "AlarmConstants.h"
#import "Alarm.h"

static NSString *const kSavedAlarms = @"savedAlarms";

@interface AlarmReminder ()
@property(nonatomic, strong) NSMutableArray *alarms;
@end

@implementation AlarmReminder {

}

- (instancetype)init {
    self = [super init];
    if (self) {
        NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:kSavedAlarms];
        if (data) {
            self.alarms = [[NSMutableArray alloc] initWithArray:[NSKeyedUnarchiver unarchiveObjectWithData:data]];
        } else {
            self.alarms = [[NSMutableArray alloc] init];
        }
    }

    return self;
}

+ (AlarmReminder *)instance {
    static AlarmReminder *_instance = nil;

    @synchronized (self) {
        if (_instance == nil) {
            _instance = [[self alloc] init];
        }
    }

    return _instance;
}

- (NSArray *)activatedAlarms {
    return self.alarms;
}

- (void)addAlarm:(Alarm *)alarm {
    [self.alarms addObject:alarm];
}

- (void)scheduleAlarm:(Alarm *)alarm {
    NSDate *alarmDate = [self alarmDate:alarm];

    UILocalNotification *localNotification = [[UILocalNotification alloc] init];
    localNotification.fireDate = alarmDate;
    localNotification.timeZone = [NSTimeZone defaultTimeZone];
    localNotification.alertBody = NSLocalizedString(@"alarm_reminder_notification_message", @"");
    localNotification.alertAction = kAlarmTypeTrainingReminder;
    localNotification.soundName = UILocalNotificationDefaultSoundName;
    localNotification.repeatInterval = NSWeekCalendarUnit;

    alarm.notification = localNotification;
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
}

- (NSDate *)alarmDate:(Alarm *)alarm {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    [calendar setFirstWeekday:2];

    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSYearCalendarUnit |
                    NSWeekOfYearCalendarUnit | NSWeekdayCalendarUnit
                                                                   fromDate:[NSDate date]];
    NSInteger weekday = [self convertWeekDayToSundayFirst:alarm];
    [components setWeekOfYear:[self shouldGetNextWeek:components weekday:weekday] ? [components weekOfYear] + 1 : [components weekOfYear]];
    [components setWeekday:weekday];
    [components setYear:[components year]];
    [components setHour:alarm.hour];
    [components setMinute:alarm.minute];

    NSDate *alarmDate = [calendar dateFromComponents:components];
    return alarmDate;
}

- (BOOL)shouldGetNextWeek:(NSDateComponents *)components weekday:(NSInteger)alarmWeekDay {
    NSInteger todayWeekDay = [components weekday];

    return todayWeekDay > alarmWeekDay && alarmWeekDay != 1;
}

- (NSInteger)convertWeekDayToSundayFirst:(Alarm *)alarm {
    return alarm.day < 7 ? alarm.day + 1 : 1;
}

- (void)persistAlarms:(NSMutableArray *)temporaryAlarms {
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:[temporaryAlarms copy]];
    [[NSUserDefaults standardUserDefaults] setObject:data forKey:kSavedAlarms];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)removeAllAlarms {
    [self.alarms removeAllObjects];
    [self persistAlarms:nil];
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
}

- (NSString *)printAlarms {
    NSString *result = @"";
    for (Alarm *alarm in self.alarms) {
        if ([alarm isEqual:[self.alarms lastObject]]) {
            result = [result stringByAppendingString:[self alarmDayString:alarm]];
        } else {
            result = [result stringByAppendingString:[NSString stringWithFormat:@"%@%@", [self
                    alarmDayString:alarm], @","]];
        }
    }

    if (self.alarms.count > 0) {
        Alarm *firstAlarm = self.alarms[0];

        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
        result = [NSString stringWithFormat:@"%@ %02ld:%02ld", result, (long) firstAlarm.hour, (long) firstAlarm.minute];
    }

    return result;
}

- (NSString *)formatSelectedTime {
    if (self.activatedAlarms.count > 0) {
        NSDate *alarmDate = [self alarmDate:self.activatedAlarms[0]];
        if (alarmDate) {
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
            return [dateFormatter stringFromDate:alarmDate];
        }
    }

    return NSLocalizedString(@"alarm_reminder_select_time", @"Select");
}

- (NSString *)alarmDayString:(Alarm *)alarm {
    switch (alarm.day) {
        case MONDAY:
            return @"L";
        case TUESDAY:
            return @"M";
        case WEDNESDAY:
            return @"X";
        case THURDSDAY:
            return @"J";
        case FRIDAY:
            return @"V";
        case SATURDAY:
            return @"S";
        case SUNDAY:
            return @"D";
        default:
            return @"";
    }
}


- (void)saveAlarms:(NSMutableArray *)temporaryAlarms withHour:(NSInteger)hour andMinutes:(NSInteger)minutes {
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    for (Alarm *alarm in temporaryAlarms) {
        alarm.hour = hour;
        alarm.minute = minutes;
        [self scheduleAlarm:alarm];
    }

    self.alarms =  temporaryAlarms;
    [self persistAlarms:temporaryAlarms];
}
@end