//
// Created by redhair84 on 11/10/2014.
// Copyright (c) 2014 Francisco Javier Morant. All rights reserved.
//

#import "UIColor+NativoxColors.h"


@implementation UIColor (NativoxColors)

+ (UIColor *)nativoxBlueColor {
    return [UIColor colorWithRed:(float) 62 / 255
                           green:(float) 185 / 255
                            blue:(float) 230 / 255
                           alpha:1.0];
}


@end