//
// Created by redhair84 on 28/06/2015.
// Copyright (c) 2015 Francisco Javier Morant. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol UserInterface <NSObject>

- (void)presentMessage:(NSString *)message;

- (void)showProgressViewWithTitle:(NSString *)title;

- (void)dismissProgressView;

@end