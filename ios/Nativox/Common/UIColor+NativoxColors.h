//
// Created by redhair84 on 11/10/2014.
// Copyright (c) 2014 Francisco Javier Morant. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIColor (NativoxColors)

+ (UIColor *)nativoxBlueColor;

@end