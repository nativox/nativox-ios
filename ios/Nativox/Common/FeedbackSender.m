#import "FeedbackSender.h"
#import "Answers.h"
#import "UserSession.h"

@implementation FeedbackSender {

}

- (void)sendFeedbackEvent:(NSString *)event andExtraInfo:(NSDictionary *)extraInfo {
    NSMutableDictionary *customAttributes = [[NSMutableDictionary alloc] initWithDictionary:@{
            @"idUser" : [UserSession instance].idUser ? [UserSession instance].idUser : @"",
            @"email" : [UserSession instance].email ? [UserSession instance].email : @"",
            @"language" : [UserSession instance].language ? [UserSession instance].language : @""
    }];

    if (extraInfo) {
        [customAttributes addEntriesFromDictionary:extraInfo];
    }

    [Answers logCustomEventWithName:event customAttributes:[customAttributes copy]];
}

+ (FeedbackSender *)instance {
    static FeedbackSender *_instance = nil;

    @synchronized (self) {
        if (_instance == nil) {
            _instance = [[self alloc] init];
        }
    }

    return _instance;
}

@end