#import <AVFoundation/AVFoundation.h>
#import "VideoPlayerHandler.h"

@interface VideoPlayerHandler ()

@property(nonatomic, strong) AVPlayerItem *playerItem;
@end

@implementation VideoPlayerHandler {

}

+ (instancetype)initVideo:(NSString *)filepath inView:(UIView *)view {
    VideoPlayerHandler *videoPlayerHandler = [[VideoPlayerHandler alloc] init];

    NSURL *urlVideoInfo = [NSURL fileURLWithPath:filepath];

    AVURLAsset *asset = [AVURLAsset URLAssetWithURL:urlVideoInfo options:nil];
    videoPlayerHandler.playerItem = [AVPlayerItem playerItemWithAsset:asset];

    videoPlayerHandler.avPlayer = [AVPlayer playerWithPlayerItem:videoPlayerHandler.playerItem];
    videoPlayerHandler.layer = [AVPlayerLayer playerLayerWithPlayer:videoPlayerHandler.avPlayer];
    videoPlayerHandler.avPlayer.actionAtItemEnd = AVPlayerActionAtItemEndNone;

    videoPlayerHandler.layer.frame = CGRectMake(0, 0, view.frame.size.width, view.frame.size.height);
    [view.layer addSublayer:videoPlayerHandler.layer];

    return videoPlayerHandler;
}

- (void)resetVideo:(NSString *)filepath {
    NSURL *urlVideoInfo = [NSURL fileURLWithPath:filepath];

    AVURLAsset *asset = [AVURLAsset URLAssetWithURL:urlVideoInfo options:nil];
    self.playerItem = [AVPlayerItem playerItemWithAsset:asset];

    [self.avPlayer replaceCurrentItemWithPlayerItem:self.playerItem];
    [self.avPlayer seekToTime:kCMTimeZero];
    [self.avPlayer setActionAtItemEnd:AVPlayerActionAtItemEndNone];
}

- (void)seekVideoToZero {
    [self.avPlayer seekToTime:kCMTimeZero];
}

- (void)cleanUpVideoPlayer {
    [self.avPlayer pause];
    [self.avPlayer seekToTime:kCMTimeZero];
    [self.layer removeFromSuperlayer];
    self.avPlayer = nil;
}

- (void)playVideo {
    [self.avPlayer play];
}

- (void)stopVideo {
    self.playerItem = nil;
    [self.avPlayer pause];
}

- (void)hideVideo:(BOOL)isHidden {
    self.layer.hidden = isHidden;
}

- (void)changeVolume:(float)volume {
    [self.avPlayer setVolume:volume];
}

- (float)volume {
    return self.avPlayer.volume;
}

@end