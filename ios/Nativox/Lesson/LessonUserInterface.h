#import <Foundation/Foundation.h>
#import "UserInterface.h"

@class StatisticsCourseViewModel;

@protocol LessonUserInterface <UserInterface>

- (void)showStatisticsButton:(NSString *)userProgress;

- (void)showStatistics:(StatisticsCourseViewModel *)model;

- (void)showMilestone:(NSString *)milestoneContent;

- (void)showPointsBar:(NSInteger)points;

- (void)showAlarmReminderDialog;

- (void)showTranslationDialog;

- (void)goBackToPreviousScreen;
@end