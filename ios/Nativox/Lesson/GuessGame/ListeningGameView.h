#import <UIKit/UIKit.h>

@class CustomIOSAlertView;
@class LessonViewController;

@interface ListeningGameView : UIView <UITextFieldDelegate>

@property(nonatomic, copy) NSString *sentence;
@property(nonatomic, strong) CustomIOSAlertView *dialogAlertView;
@property(nonatomic, weak) LessonViewController *lessonViewController;

- (void)loadView;

@end
