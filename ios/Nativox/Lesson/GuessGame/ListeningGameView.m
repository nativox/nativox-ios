#import "ListeningGameView.h"
#import "FAImageView.h"
#import "UILabel+Fix.h"
#import "CustomIOSAlertView.h"
#import "LessonViewController.h"
#import "DeviceUtils.h"

static NSString *const TYPEABLE_CHARACTERS = @"ABCDEFGHIJKLMNOPQRSTUVWYXZ";

@interface ListeningGameView ()

@property(strong, nonatomic) IBOutlet UILabel *listeningIcon;
@property(strong, nonatomic) IBOutlet UIView *sentenceViewContainer;
@property(strong, nonatomic) IBOutlet UILabel *wrongCharacterLabel;

@property(nonatomic, strong) NSArray *guessWords;
@property(nonatomic) NSUInteger currentWordPos;
@property(nonatomic) NSString *currentWord;
@property(nonatomic) NSUInteger currentCharacterPos;
@property(nonatomic, strong) NSMutableArray *guessWordLabels;
@property(nonatomic, strong) UIView *cursor;
@end

@implementation ListeningGameView

- (void)awakeFromNib {
    [super awakeFromNib];
    self.listeningIcon.font = [UIFont fontWithName:kFontAwesomeFamilyName size:20];
    self.listeningIcon.text = [NSString fontAwesomeIconStringForEnum:FAIconVolumeUp];
    self.listeningIcon.userInteractionEnabled = YES;
    self.wrongCharacterLabel.text = NSLocalizedString(@"listening_game_wrong_character", @"");
    UITapGestureRecognizer *tapGesture =
            [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onListeningIconPressed)];
    [self.listeningIcon addGestureRecognizer:tapGesture];

}

- (IBAction)pressKeyboardKey:(id)sender {
    [self checkIfCorrectCharacter:sender];
}

- (void)checkIfCorrectCharacter:(id)sender {
    NSString *character = [sender currentTitle];
    NSString *guessCharacter = [self currentCharacterToGuess];

    BOOL isGuessCharacterCorrect = [character isEqualToString:guessCharacter];
    self.wrongCharacterLabel.hidden = isGuessCharacterCorrect;
    if (isGuessCharacterCorrect) {
        UILabel *guessWordLabel = self.guessWordLabels[self.currentWordPos];
        unichar currentCharacterValue = [self.currentWord characterAtIndex:self.currentCharacterPos];
        NSString *currentCharacterString = [NSString stringWithFormat:@"%c", currentCharacterValue];

        NSRange currentCharacterRange = NSMakeRange(self.currentCharacterPos, 1);
        guessWordLabel.text = [guessWordLabel.text stringByReplacingCharactersInRange:currentCharacterRange
                                                                           withString:currentCharacterString];

        [self moveToNextCharacterNotAvoidableCharacter];
        if ([self hasFinishedGame]) {
            self.wrongCharacterLabel.hidden = NO;
            self.wrongCharacterLabel.text = NSLocalizedString(@"listening_game_congratulations", @"");
            self.wrongCharacterLabel.textColor = [UIColor greenColor];
            [self performSelector:@selector(closeDialog) withObject:nil afterDelay:2.0];
        }
    }
}

- (void)moveToNextCharacterNotAvoidableCharacter {
    [self moveToNextCharacter];
    while (![self isCurrentCharacterTypeable] && ![self hasFinishedGame]) {
        [self moveToNextCharacter];
    }
}

- (BOOL)isCurrentCharacterTypeable {
    return [TYPEABLE_CHARACTERS rangeOfString:[self currentCharacterToGuess]].location != NSNotFound;
}

- (BOOL)hasFinishedGame {
    return (self.currentWordPos) >= self.guessWords.count;
}

- (NSString *)currentCharacterToGuess {
    if (self.currentWordPos < self.guessWords.count) {
        return [[NSString stringWithFormat:@"%c", [self.currentWord characterAtIndex:self.currentCharacterPos]] uppercaseString];
    }

    return @"";
}

- (void)moveToNextCharacter {
    if ([self isNextCharacterInCurrentWord]) {
        self.currentCharacterPos++;
    } else {
        [self moveToNextWord];
    }

    if (self.currentWordPos < self.guessWords.count) {
        self.currentWord = self.guessWords[self.currentWordPos];
        [self showCursorInCurrentCharacter];
    }
}

- (BOOL)isNextCharacterInCurrentWord {
    return (self.currentCharacterPos + 1) < self.currentWord.length;
}

- (void)showCursorInCurrentCharacter {
    [self.cursor removeFromSuperview];

    UILabel *currentInput = self.guessWordLabels[self.currentWordPos];
    CGRect returnRect = [currentInput rectForLetterAtIndex:self.currentCharacterPos];

    self.cursor = [self showCursorInFrame:returnRect];
}

- (void)moveToNextWord {
    self.currentWordPos++;
    self.currentCharacterPos = 0;
}

- (void)loadView {
    CGRect containerFrame = self.sentenceViewContainer.frame;
    [self extractGuessWords];

    int posX = (int) [self initialPosX:self.guessWords];
    int posY = 20;
    self.currentWordPos = 0;
    self.currentCharacterPos = 0;
    self.currentWord = self.guessWords[self.currentWordPos];
    self.guessWordLabels = [[NSMutableArray alloc] init];

    for (NSString *word in self.guessWords) {
        if (((containerFrame).size.width - 30) < (posX)) {
            (posX) = 0;
            (posY) += 30;
        }

        NSMutableString *guessWord = [self extractGuessWord:word];
        UILabel *textFieldWord = [self createLabelField:posX posY:posY word:word guessWord:guessWord];

        [self.guessWordLabels addObject:textFieldWord];
        [self.sentenceViewContainer addSubview:textFieldWord];
        posX += (textFieldWord.frame.size.width + 3);
    }

    [self showCursorInCurrentCharacter];
}

- (NSMutableString *)extractGuessWord:(NSString *)word {
    NSMutableString *guessWord = [NSMutableString stringWithFormat:@""];
    NSInteger length = [word length];
    for (NSUInteger i = 0; i < length; i++) {
        char currentCharacter = (char) [word characterAtIndex:i];

        if (currentCharacter != ' ') {
            NSString *stringCharacter = [[NSString stringWithFormat:@"%c", currentCharacter] uppercaseString];
            if ([TYPEABLE_CHARACTERS rangeOfString:stringCharacter].location != NSNotFound) {
                [guessWord appendString:@"_"];
            } else {
                [guessWord appendString:stringCharacter];
            }
        }
    }
    return guessWord;
}

- (UILabel *)createLabelField:(int)posX posY:(int)posY word:(NSString *)word guessWord:(NSMutableString *)guessWord {
    UILabel *textFieldWord = [[UILabel alloc] initWithFrame:CGRectMake(posX, posY, 140, 40)];
    textFieldWord.text = word;
    [textFieldWord sizeToFit];
    textFieldWord.frame = CGRectMake(textFieldWord.frame.origin.x, textFieldWord.frame.origin.y, textFieldWord
            .frame.size.width + 3, textFieldWord.frame.size.height);

    textFieldWord.text = guessWord;
    textFieldWord.backgroundColor = [UIColor clearColor];
    textFieldWord.textColor = [UIColor whiteColor];
    textFieldWord.font = [UIFont systemFontOfSize:17];
    textFieldWord.tintColor = [UIColor whiteColor];
    textFieldWord.lineBreakMode = NSLineBreakByClipping;
    return textFieldWord;
}

- (void)extractGuessWords {
    NSString *sentenceWithoutStrangeWhiteSpaces = [self.sentence stringByReplacingOccurrencesOfString:@" "
                                                                                           withString:@" "];
    sentenceWithoutStrangeWhiteSpaces = [sentenceWithoutStrangeWhiteSpaces stringByTrimmingCharactersInSet:
            [NSCharacterSet whitespaceCharacterSet]];
    NSArray *words = [sentenceWithoutStrangeWhiteSpaces componentsSeparatedByString:@" "];

    NSMutableArray *guessWordsMutable = [[NSMutableArray alloc] initWithArray:words];

    for (NSString *word in words) {
        if ([word isEqualToString:@""]) {
            [guessWordsMutable removeObject:word];
        }
    }

    self.guessWords = [guessWordsMutable copy];
}

- (float)initialPosX:(NSArray *)guessWords {
    float widthContainer = self.sentenceViewContainer.frame.size.width;
    float predictedWidthWords = guessWords.count * 50;

    return predictedWidthWords > widthContainer ? 0 : widthContainer / 4;
}

- (UIView *)showCursorInFrame:(CGRect)frame {
    UIView *cursor = [[UIView alloc] initWithFrame:CGRectMake(frame.origin.x, frame.origin.y, 6, 17)];
    cursor.backgroundColor = [UIColor whiteColor];

    UILabel *currentInput = self.guessWordLabels[self.currentWordPos];
    [currentInput addSubview:cursor];

    [UIView animateWithDuration:1
                          delay:0
                        options:UIViewAnimationOptionRepeat
                     animations:^{
                         [cursor setAlpha:0];
                     }
                     completion:^(BOOL animated) {
                         [cursor setAlpha:1];
                     }];
    return cursor;
}

- (void)closeDialog {
    self.lessonViewController = nil;
    [self.dialogAlertView close];
}

- (void)onListeningIconPressed {
    [self.lessonViewController playVideoButtonPressed:nil];
}

@end
