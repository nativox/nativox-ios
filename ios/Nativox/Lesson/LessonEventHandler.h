//
// Created by redhair84 on 22/04/15.
// Copyright (c) 2015 Francisco Javier Morant. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EventHandler.h"

@protocol LessonEventHandler <EventHandler>

- (void)backButtonPressed;

- (void)playButtonPressed;

- (void)sendTranslationButtonPressed;

- (void)userAudioDataEvaluated:(NSDictionary *)data;

- (void)statisticsButtonPressed;

- (void)videoPlayerLoaded;

@end