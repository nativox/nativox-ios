//
//  StatisticsView.m
//  Nativox
//
//  Created by redhair84 on 11/06/2015.
//  Copyright (c) 2015 Francisco Javier Morant. All rights reserved.
//

#import "StatisticsView.h"
#import "PNColor.h"
#import "PNCircleChart.h"
#import "StatisticsCourseViewModel.h"
#import "CustomIOSAlertView.h"

@interface StatisticsView ()
@property(strong, nonatomic) IBOutlet UIView *pieChartView;
@property(strong, nonatomic) IBOutlet UIView *pieChartView2;
@property(strong, nonatomic) IBOutlet UIView *pieChartView3;
@property(strong, nonatomic) IBOutlet UIImageView *backgroundDialog;
@property(strong, nonatomic) IBOutlet UILabel *courseTitle;
@property(strong, nonatomic) IBOutlet UILabel *completedVideoLabel;
@property(strong, nonatomic) IBOutlet UILabel *progressLabel;
@property(strong, nonatomic) IBOutlet UILabel *averageGradeLabel;
@end

@implementation StatisticsView

- (id)initWithCoder:(NSCoder *)coder {
    self = [super initWithCoder:coder];
    if (self) {
    }

    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    self.backgroundDialog.layer.cornerRadius = 5;
    self.backgroundDialog.clipsToBounds = YES;
    
    self.completedVideoLabel.text = NSLocalizedString(@"statistics_completed_videos", @"");
    self.progressLabel.text = NSLocalizedString(@"statistics_progress", @"");
    self.averageGradeLabel.text = NSLocalizedString(@"statistics_average_grade", @"");
}

- (IBAction)closePopup:(id)sender {
    [self.statisticsAlertView close];
}

- (void)loadStatistics {
    [self.pieChartView addSubview:[self createCircleChart:32.0f current:@(self.viewModel.completedVideos)
                                             pieChartView:self.pieChartView]];
    [self.pieChartView2 addSubview:[self createCircleChart:40.0f current:@(self.viewModel.progress) pieChartView:self
            .pieChartView2]];
    [self.pieChartView3 addSubview:[self createCircleChart:32.0f current:@(self.viewModel.averageGrade)
                                              pieChartView:self
                                                      .pieChartView3]];

    self.courseTitle.text = self.viewModel.courseTitle;
}

- (UIView *)createCircleChart:(float)fontSize current:(NSNumber *)current pieChartView:(UIView *)pieChartView {
    UIColor *shadowColor = [UIColor colorWithRed:36.0 / 255.0f
                                           green:82.0 / 255.0f
                                            blue:120.0 / 255.0f
                                           alpha:0.5];
    PNCircleChart *circleChart = [[PNCircleChart alloc] initWithFrame:pieChartView.bounds
                                                                total:@100
                                                              current:current
                                                            clockwise:YES
                                                               shadow:YES
                                                          shadowColor:shadowColor
    ];
    circleChart.backgroundColor = [UIColor clearColor];
    [circleChart setStrokeColor:PNWhite];
    [circleChart setLineWidth:@12];
    [circleChart.countingLabel setCenter:CGPointMake(circleChart.frame.size.width / 2.0f, (CGFloat) (circleChart.frame.size.height
            * 0.4))];
    [circleChart.countingLabel setFont:[UIFont boldSystemFontOfSize:fontSize]];
    [circleChart.countingLabel setTextColor:[UIColor whiteColor]];
    [circleChart strokeChart];
    return circleChart;
}

@end
