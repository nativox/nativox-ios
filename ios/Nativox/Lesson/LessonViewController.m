#import <AVFoundation/AVFoundation.h>
#import <MRProgress/MRProgressOverlayView.h>
#import <SFCountdownView/SFCountdownView.h>
#import "VideoInfo.h"
#import "DatabaseManager.h"
#import "UIAlertView+ShowMessage.h"
#import "NetworkUtil.h"
#import "WebViewJavascriptBridge.h"
#import "VideoPlayerHandler.h"
#import "UserSession.h"
#import "AudioHandler.h"
#import "GradeMessageResolver.h"
#import "AnimatorHandler.h"
#import "LessonViewController.h"
#import "UIColor+NativoxColors.h"
#import "LessonInteractor.h"
#import "MilestonePopupView.h"
#import "StatisticsView.h"
#import "DeviceUtils.h"
#import "AlarmReminderDialog.h"
#import "TranslationDialog.h"
#import "SZTextView.h"
#import "MessageViewDialog.h"
#import "ListeningGameView.h"
#import "VideoGraphicLineRequester.h"
#import "CouchMarksHelper.h"
#import "UserFeedbackView.h"
#import "ProvidersManager.h"
#import "NativoxAppDelegate.h"
#import "SpeechRecognitor.h"
#import "BusinessError.h"
#import "FeedbackSender.h"

static NSString *const kHasDisplayedRecordToolTip = @"hasDisplayedRecordToolTip";
static NSString *const kHasDisplayedTestToolTip = @"hasDisplayedTestToolTip";
static NSString *const kHasDisplayedPlayToolTip = @"hasDisplayedPlayToolTip";
static NSString *const hasDisplayedNextButtonToolTip = @"hasDisplayedNextButtonToolTip";

static NSString *const kFreeSectionTitle = @"Free";
static const int kLimitToPassLesson = 69;
static const int kApplauseLimit = 80;
static NSString *const kAdUnitID = @"ca-app-pub-2824162014731088/4029596853";
static const int kInterestitialTimeout = 8 * 60;

@interface LessonViewController ()

@property(strong, nonatomic) VideoPlayerHandler *videoPlayerHandler;
@property(strong, nonatomic) AudioHandler *audioHandler;
@property(strong, nonatomic) SFCountdownView *countDownView;

@property(strong, nonatomic) NSString *pathVideo;
@property(strong, nonatomic) AnimatorHandler *animatorHandler;

@property(nonatomic) BOOL hasUploadedAudio;
@property(nonatomic, strong) NSUserDefaults *userDefaults;
@property(nonatomic, strong) MPCoachMarks *coachMarkTreeView;
@property(nonatomic) LessonState currentLessonState;
@property(nonatomic, strong) UITapGestureRecognizer *singleFingerTap;
@property(nonatomic) NSUInteger counterVideosPlayed;
@property(nonatomic) NSUInteger counterVideosRecorded;
@property(strong, nonatomic) IBOutlet UITextView *subtitleTextView;
@property(strong, nonatomic) IBOutlet NSLayoutConstraint *subtitleTextViewHeightConstraint;
@property(strong, nonatomic) IBOutlet NSLayoutConstraint *subtitleTextViewWidthConstraint;
@property(strong, nonatomic) IBOutlet UIButton *viewSubtitles;
@property(nonatomic) CGRect subtitleTextViewFrame;
@property(nonatomic, strong) NSMutableArray *notifications;
@property(nonatomic, strong) UIAlertView *registerMessage;
@property(nonatomic) BOOL showSentenceNextTime;
@property(strong, nonatomic) IBOutlet UIButton *statisticsButton;
@property(strong, nonatomic) IBOutlet UIButton *statisticsButtonPercentage;
@property(strong) id eventHandler;
@property(strong, nonatomic) IBOutlet UIActivityIndicatorView *audioUploadingIndicator;
@property(strong, nonatomic) IBOutlet UIView *infoContainer;
@property(strong, nonatomic) IBOutlet UIWebView *videoGraphicLineContainer;
@property(strong, nonatomic) IBOutlet UIButton *sendTranslationButton;
@property(strong, nonatomic) IBOutlet UIButton *sendTranslationPlusButton;
@property(strong, nonatomic) IBOutlet UIButton *guessGameButton;
@property(strong, nonatomic) IBOutlet NSLayoutConstraint *horizontalGuessGameButtonConstraint;
@property(strong, nonatomic) IBOutlet NSLayoutConstraint *horizontalSubtitlesButtonConstraint;
@property(strong, nonatomic) IBOutlet UIButton *previousLessonButton;
@property(strong, nonatomic) IBOutlet UIButton *nexLessonButtonBack;
@property(strong, nonatomic) IBOutlet UIButton *nextLessonButton;
@property(strong, nonatomic) IBOutlet UIButton *treeViewBtn;
@property(strong, nonatomic) IBOutlet UIButton *downloadVideoButton;
@property(strong, nonatomic) IBOutlet UINavigationItem *navigationItemButtons;
@property(strong, nonatomic) IBOutlet UIButton *testButton;
@property(strong, nonatomic) IBOutlet UIButton *playButton;
@property(strong, nonatomic) IBOutlet UIButton *recordButton;
@property(strong, nonatomic) IBOutlet UIButton *translationButton;
@property(strong, nonatomic) IBOutlet UIView *downloadContainer;
@property(strong, nonatomic) IBOutlet UILabel *notificationsDisplay;
@property(strong, nonatomic) IBOutlet UIView *videoContainer;
@property(strong, nonatomic) IBOutlet UIImageView *imageImpossible;

@property(nonatomic, strong) id interstitialAd;
@property(nonatomic, strong) NSTimer *interestitialTimer;
@property(nonatomic, strong) VideoGraphicLineRequester *graphicLineRequester;
@property(nonatomic, strong) MPCoachMarks *coachMarkStatistics;
@property(nonatomic) NSUInteger userCourseTries;
@property(nonatomic) NSMutableArray *videoPlayerObservers;
@property(nonatomic, strong) SpeechRecognitor *speechRecognitor;
@property(nonatomic, strong) MPCoachMarks *coachMarkTest;
@end

@implementation LessonViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.videoPlayerObservers = [[NSMutableArray alloc] init];

    [[ProvidersManager instance].userProvider testToken:^(BOOL status, BusinessError *error) {
        if (error || !status) {
            [self.revealViewController dismissViewControllerAnimated:YES completion:^{
                [self logoutFromLessonScreen];
            }];
        }
    }];

    LessonInteractor *lessonInteractor = [[LessonInteractor alloc] init];
    lessonInteractor.userInterface = self;
    lessonInteractor.sectionsHandler = self.sectionsHandler;
    self.eventHandler = lessonInteractor;

    self.navigationController.navigationBar.hidden = YES;
    [self prefersStatusBarHidden];

    self.animatorHandler = [[AnimatorHandler alloc] init];

    self.fileDownloadHandler = [[VideoDownloader alloc] initWithVideoManager];
    self.fileDownloadHandler.fileManagerDelegate = self;
    self.fileUploadHandler = [[AudioUploader alloc] initFileUploadManager];
    self.fileUploadHandler.fileUploadManagerDelegate = self;

    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];

    self.singleFingerTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTapVideoContainer:)];
    [self.videoContainer addGestureRecognizer:self.singleFingerTap];

    [self setupWebViewBridgeJavascript];

    self.audioHandler = [AudioHandler initAudioHandler];
    self.userDefaults = [NSUserDefaults standardUserDefaults];

    self.counterVideosPlayed = 0;
    self.counterVideosRecorded = 0;

    self.subtitleTextView.hidden = YES;
    self.subtitleTextViewFrame = self.subtitleTextView.frame;
    self.audioUploadingIndicator.hidden = YES;

    self.infoContainer.layer.cornerRadius = [DeviceUtils instance].isOnIpad ? 10 : 5;
    self.videoGraphicLineContainer.layer.cornerRadius = [DeviceUtils instance].isOnIpad ? 10 : 5;
    [self.videoGraphicLineContainer setClipsToBounds:YES];
    for (UIView *subview in [self.self.videoGraphicLineContainer subviews]) {
        [subview setOpaque:NO];
        [subview setBackgroundColor:[UIColor clearColor]];
    }

    [self.videoGraphicLineContainer setBackgroundColor:[UIColor clearColor]];
    [self.videoGraphicLineContainer setOpaque:NO];

    self.interstitialAd = [self createAndLoadInterstitial];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(audioHardwareRouteChanged:) name:AVAudioSessionRouteChangeNotification object:nil];

    [self.eventHandler screenDidAppear];
    self.userCourseTries = (NSUInteger) [self.userDefaults integerForKey:[NSString stringWithFormat:@"counter_testing_%@", self.sectionsHandler.selectedCourse[@"idCourse"]]];

    self.speechRecognitor = [[SpeechRecognitor alloc] init];

    NSString *applausePath = [NSString stringWithFormat:@"%@/applause_wow.mp3", [[NSBundle mainBundle] resourcePath]];
    NSURL *applauseUrl = [NSURL fileURLWithPath:applausePath];

    self.applausePlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:applauseUrl error:nil];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];

    self.graphicLineRequester = [[VideoGraphicLineRequester alloc] init];
    self.graphicLineRequester.videoGraphicLineContainer = self.videoGraphicLineContainer;
    [self.graphicLineRequester loadCurrentGraphicLineForVideo:[self.sectionsHandler getSelectedVideo] andCourse:self.selectedCourse];
    [self setupVideoLesson];

    if ([UserSession instance].purchasedCourses.count == 0 && !self.interestitialTimer) {
        self.interestitialTimer = [NSTimer scheduledTimerWithTimeInterval:kInterestitialTimeout
                                                                   target:self
                                                                 selector:@selector(presentInterestitial)
                                                                 userInfo:nil
                                                                  repeats:NO];
    } else if ([UserSession instance].purchasedCourses.count == 0 && self.interestitialTimer) {
        self.interestitialTimer = nil;
        self.interestitialTimer = [NSTimer scheduledTimerWithTimeInterval:kInterestitialTimeout / 2
                                                                   target:self
                                                                 selector:@selector(presentInterestitial)
                                                                 userInfo:nil
                                                                  repeats:NO];
    }
}

- (void)dealloc {
    [self.userDefaults setInteger:self.userCourseTries forKey:[NSString stringWithFormat:@"counter_testing_%@",
                                                                                         self.sectionsHandler.selectedCourse[@"idCourse"]]];
    [self.userDefaults synchronize];

    [self.videoPlayerHandler cleanUpVideoPlayer];
    [self.audioHandler cleanUp];
    [self.fileDownloadHandler invalidateSession];
    [self.interestitialTimer invalidate];

    if ([self.videoPlayerObservers containsObject:self.videoPlayerHandler.avPlayer.currentItem]) {
        [self.videoPlayerHandler.avPlayer.currentItem removeObserver:self forKeyPath:@"status" context:nil];
        [self.videoPlayerObservers removeObject:self.videoPlayerHandler.avPlayer.currentItem];
    }

    [[NSNotificationCenter defaultCenter] removeObserver:self name:AVAudioSessionRouteChangeNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:AVPlayerItemDidPlayToEndTimeNotification object:nil];
}

- (IBAction)onBackButtonPressed:(id)sender {
    [self.eventHandler backButtonPressed];
}

- (void)audioHardwareRouteChanged:(id)audioHardwareRouteChanged {
    AVAudioSession *session = [AVAudioSession sharedInstance];

    if (![AudioHandler isHeadsetPluggedIn]) {
        [session overrideOutputAudioPort:AVAudioSessionPortOverrideSpeaker error:nil];
    } else {
        [session overrideOutputAudioPort:AVAudioSessionPortOverrideNone error:nil];
    }
}

- (void)setupVideoLesson {
    [self.animatorHandler stopAnimation];
    self.selectedVideo = [self.sectionsHandler getSelectedVideo];
    self.selectedCourse = self.sectionsHandler.selectedCourse;
    [self.speechRecognitor setupSentence:self.selectedVideo[@"frase"]];
    self.pathVideo = nil;
    self.subtitleTextView.hidden = YES;
    self.viewSubtitles.hidden = YES;
    self.guessGameButton.hidden = YES;
    self.showSentenceNextTime = [self.selectedVideo[@"superado"] boolValue];
    self.audioUploadingIndicator.hidden = YES;
    self.sendTranslationButton.hidden = YES;
    self.sendTranslationButton.enabled = NO;
    self.sendTranslationPlusButton.hidden = YES;
    self.sendTranslationPlusButton.enabled = NO;

    [self presentVideoLesson];
}

- (void)presentVideoLesson {
    VideoInfo *videoInfo = [[DatabaseManager instance] getVideoInfo:self.selectedVideo[@"id_video"]];

    self.downloadContainer.hidden = YES;
    if (!videoInfo) {
        [self startDownloadingVideo];
    } else {
        [self setupAudioAndVideo:videoInfo];
    }
}

- (void)setupAudioAndVideo:(VideoInfo *)videoInfo {
    if (videoInfo.pathVideo && [self existsVideoFile:videoInfo]) {
        if (!self.videoPlayerHandler) {
            self.videoPlayerHandler = [VideoPlayerHandler initVideo:[self getURL:videoInfo].path inView:self.videoContainer];
        } else {
            [[NSNotificationCenter defaultCenter] removeObserver:self name:AVPlayerItemDidPlayToEndTimeNotification object:nil];
            [self.videoPlayerHandler resetVideo:[self getURL:videoInfo].path];
        }

        if ([self.videoPlayerObservers containsObject:self.videoPlayerHandler.avPlayer.currentItem]) {
            [self.videoPlayerHandler.avPlayer.currentItem removeObserver:self forKeyPath:@"status" context:nil];
            [self.videoPlayerObservers removeObject:self.videoPlayerHandler.avPlayer.currentItem];
        }

        [self.videoPlayerHandler.avPlayer.currentItem addObserver:self forKeyPath:@"status" options:0 context:nil];
        [self.videoPlayerObservers addObject:self.videoPlayerHandler.avPlayer.currentItem];

        [AudioHandler setupAVAudioSession];

        self.playButton.enabled = YES;
        self.recordButton.enabled = YES;
        self.testButton.enabled = YES;
        self.translationButton.enabled = YES;

        [self.playButton setSelected:YES];
        [self.recordButton setSelected:NO];
        [self.testButton setSelected:NO];
        [self.translationButton setSelected:NO];

        self.nextLessonButton.enabled = YES;
        self.nexLessonButtonBack.enabled = YES;

        if (![self.userDefaults boolForKey:kHasDisplayedPlayToolTip]) {
            [self showTreeViewCoachMark];
        }
    } else {
        [self showVideoHasFailedError];
    }
}

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context {
    if (object == self.videoPlayerHandler.avPlayer.currentItem && [keyPath isEqualToString:@"status"] &&
            self.videoPlayerHandler.avPlayer.currentItem.status == AVPlayerItemStatusReadyToPlay) {
        [self.eventHandler videoPlayerLoaded];

        if ([self.videoPlayerObservers containsObject:self.videoPlayerHandler.avPlayer.currentItem]) {
            [self.videoPlayerHandler.avPlayer.currentItem removeObserver:self forKeyPath:@"status" context:nil];
            [self.videoPlayerObservers removeObject:self.videoPlayerHandler.avPlayer.currentItem];
        }
    }
}

- (void)showVideoHasFailedError {
    self.playButton.enabled = NO;
    self.recordButton.enabled = NO;
    self.testButton.enabled = NO;
    self.translationButton.enabled = NO;
    [self.videoPlayerHandler hideVideo:YES];
    self.downloadContainer.hidden = NO;
    [self.downloadVideoButton setTitle:NSLocalizedString(@"retry_download_video", @"") forState:UIControlStateNormal];

    [self presentMessage:NSLocalizedString(@"error_downloading_video", @"")];
}

- (IBAction)pressedMenuButton:(id)sender {
    [self.revealViewController revealToggle:sender];

    [[FeedbackSender instance] sendFeedbackEvent:@"Pressed Menu Button" andExtraInfo:@{
            @"videoName": self.selectedVideo[@"frase"],
            @"courseName": self.selectedCourse[@"curso"]
    }];
}

- (void)setupWebViewBridgeJavascript {
    [WebViewJavascriptBridge enableLogging];
    self.bridge = [WebViewJavascriptBridge bridgeForWebView:self.videoGraphicLineContainer handler:nil];
}

- (void)setupCountDownView {
    self.countDownView = [[SFCountdownView alloc] initWithFrame:self.view.frame];
    self.countDownView.delegate = self;
    self.countDownView.backgroundAlpha = 0.2;
    self.countDownView.countdownColor = [UIColor whiteColor];
    self.countDownView.countdownFrom = 3;
    self.countDownView.finishText = NSLocalizedString(@"countdown_finish_text", @"");
    [self.countDownView updateAppearance];

    [self.view addSubview:self.countDownView];
    [self.countDownView start];
}

- (BOOL)saveVideoInfo:(NSString *)videoPath {
    [MRProgressOverlayView dismissOverlayForView:self.view animated:YES];
    self.downloadVideoButton.enabled = YES;

    BOOL resultInsert = [[DatabaseManager instance] insertVideoInfo:@{
            @"id": self.selectedVideo[@"id_video"],
            @"acceso": self.selectedVideo[@"acceso"],
            @"pathVideo": videoPath,
            @"url": self.selectedVideo[@"video_web_mp4"],
            @"pathAudio": @""
    }];

    return resultInsert;
}

#pragma mark - Record Audio

- (void)startRecordAudio {
    [self.audioHandler startRecord];
}

- (void)stopRecordAudio {
    [self.audioHandler stopRecorder];
}

- (void)playRecordedAudio {
    if ([self.audioHandler setupPlayer]) {
        [self.audioHandler playPlayerRecordedAudio];
    } else {
        [self presentMessage:NSLocalizedString(@"error_playing_recorded_audio", @"")];
    }
}

- (void)startRecordingAudioVoice {
    self.currentLessonState = RECORDING_STATE;

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(videoPlaybackEnded)
                                                 name:AVPlayerItemDidPlayToEndTimeNotification
                                               object:nil];

    if (self.videoPlayerHandler.volume > 0) {
        [self.videoPlayerHandler changeVolume:0];
    }

    self.videoGraphicLineContainer.hidden = NO;
    [self.videoPlayerHandler seekVideoToZero];
    if (![self.audioHandler setupRecorder]) {
        [self presentMessage:NSLocalizedString(@"error_start_audio_record", @"")];
    } else {
        [self.bridge callHandler:@"play_canvas" data:@"" responseCallback:^(id responseData) {
            [self startRecordAudio];
            [self.videoPlayerHandler playVideo];
        }];
    }

}

#pragma mark - Video Manager Delegate

- (void)startDownloadingVideo {
    if (![[NetworkUtil instance] isReachable]) {
        [self presentMessage:NSLocalizedString(@"need_internet_connection", @"")];
    } else {
        self.downloadVideoButton.enabled = NO;
        self.progressOverlayView = [MRProgressOverlayView showOverlayAddedTo:self.view
                                                                       title:NSLocalizedString(@"downloading_video_lesson", @"")
                                                                        mode:MRProgressOverlayViewModeIndeterminate
                                                                    animated:YES];
        self.progressOverlayView.mode = MRProgressOverlayViewModeDeterminateCircular;
        self.sectionsHandler.isDownloadingVideo = YES;
        [self.fileDownloadHandler startDownload:self.selectedVideo[@"video_web_mp4"]];
    }
}

- (void)downloadFileFinished:(BOOL)status withDestinationURL:(NSString *)path andError:(NSError *)error {
    self.sectionsHandler.isDownloadingVideo = NO;
    if (status) {
        [self handleDownloadFileFinished:path];
    } else {
        [self errorDownloadingVideo];
    }
}

- (void)handleDownloadFileFinished:(NSString *)path {
    if (!self.pathVideo) {
        self.pathVideo = path;
        if ([self saveVideoInfo:self.pathVideo]) {
            VideoInfo *videoInfo = [[DatabaseManager instance] getVideoInfo:self.selectedVideo[@"id_video"]];
            self.downloadContainer.hidden = YES;
            [self setupAudioAndVideo:videoInfo];
        } else {
            [self presentMessage:NSLocalizedString(@"error_downloading_video", @"")];
        }
    } else {
        [self presentMessage:NSLocalizedString(@"error_downloading_video", @"")];
    }
}

- (void)downloadVideoCompletedWithError:(NSError *)error {
    self.sectionsHandler.isDownloadingVideo = NO;
    [MRProgressOverlayView dismissOverlayForView:self.view animated:YES];
    if (error) {
        [self errorDownloadingVideo];
    }
}

- (void)progressDownload:(int64_t)bytesWritten
        withTotalWritten:(int64_t)totalBytesWritten
        andTotalExpected:(int64_t)totalBytesExpected {

    float progress = (float) totalBytesWritten / totalBytesExpected;
    if (progress >= 0 && progress <= 1) {
        [self.progressOverlayView setProgress:progress];
    }
}

- (void)resumeDownloadWithOffset:(int64_t)offset andTotalExpected:(int64_t)totalBytesExpected {
    float progress = (float) offset / totalBytesExpected;
    if (progress >= 0 && progress <= 1) {
        [self.progressOverlayView setProgress:progress];
    }
}

- (void)errorDownloadingVideo {
    self.downloadVideoButton.enabled = YES;
    [self presentMessage:NSLocalizedString(@"error_downloading_video", @"")];
}

#pragma - mark Video Lesson Management

- (void)videoPlaybackEnded {
    if (self.currentLessonState == TESTING_STATE) {
        [self endTestingPhase];
    } else if (self.currentLessonState == PLAYING_STATE) {
        [self endPlayingPhase];
    } else if (self.currentLessonState == RECORDING_STATE) {
        [self endRecordingPhase];
    }

    [[NSNotificationCenter defaultCenter] removeObserver:self name:AVPlayerItemDidPlayToEndTimeNotification object:nil];
    [self.videoPlayerHandler stopVideo];
}

- (void)newLessonSelected:(BOOL)toggle {
    if (toggle) {
        [self.revealViewController revealToggle:self];
    }

    [self.videoPlayerHandler stopVideo];
    [self.audioHandler stopRecorder];
    [self setupVideoLesson];
    [self resetLessonView];

    [self.graphicLineRequester loadCurrentGraphicLineForVideo:[self.sectionsHandler getSelectedVideo] andCourse:self.selectedCourse];
    self.videoGraphicLineContainer.hidden = YES;
}

- (IBAction)previousVideoButtonPressed:(id)sender {
    [[self sectionsHandler] moveToPreviousVideo];

    [[FeedbackSender instance] sendFeedbackEvent:@"Pressed Previous Video" andExtraInfo:@{
            @"videoName": self.selectedVideo[@"frase"],
            @"courseName": self.selectedCourse[@"curso"]
    }];
}

- (IBAction)nextVideoButtonPressed:(id)sender {
    [[self sectionsHandler] moveToNextVideo];

    [[FeedbackSender instance] sendFeedbackEvent:@"Pressed Next Video" andExtraInfo:@{
            @"videoName": self.selectedVideo[@"frase"],
            @"courseName": self.selectedCourse[@"curso"]
    }];
}

- (IBAction)downloadVideoButtonPressed:(id)sender {
    NSDictionary *section = self.sectionsHandler.selectedCourse[@"secciones"][(NSUInteger) self.sectionsHandler.selectedSeccion];

    if (!self.selectedCourse[@"comprado"] && ![section[@"seccion"] isEqualToString:kFreeSectionTitle] && ![UserSession instance].idUser) {
        self.registerMessage = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"app_name", @"")
                                                          message:NSLocalizedString(@"registrarse_comprar_leccion", @"")
                                                         delegate:self
                                                cancelButtonTitle:NSLocalizedString(@"cancel_button", @"")
                                                otherButtonTitles:NSLocalizedString(@"ok_button", @""), nil];
        [self.registerMessage show];
    } else {
        [self startDownloadingVideo];
    }
}

- (IBAction)playVideoButtonPressed:(id)sender {
    [self.eventHandler playButtonPressed];
    [self resetLessonView];
    [self.videoPlayerHandler hideVideo:NO];
    self.videoGraphicLineContainer.hidden = NO;
    self.subtitleTextView.hidden = YES;

    self.playButton.enabled = NO;
    self.nextLessonButton.enabled = NO;
    self.nexLessonButtonBack.enabled = NO;
    self.previousLessonButton.enabled = NO;
    self.singleFingerTap.enabled = NO;
    self.sendTranslationButton.hidden = YES;
    self.sendTranslationPlusButton.hidden = YES;

    [self.videoPlayerHandler changeVolume:1];
    [self.videoPlayerHandler seekVideoToZero];

    [self.bridge callHandler:@"play_canvas" data:@"" responseCallback:^(id responseData) {
        [self.videoPlayerHandler playVideo];
    }];

    self.counterVideosPlayed++;
    self.currentLessonState = PLAYING_STATE;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(videoPlaybackEnded)
                                                 name:AVPlayerItemDidPlayToEndTimeNotification
                                               object:nil];

    [[FeedbackSender instance] sendFeedbackEvent:@"Pressed Play Button" andExtraInfo:@{
            @"videoName": self.selectedVideo[@"frase"],
            @"courseName": self.selectedCourse[@"curso"]
    }];
}

- (void)endPlayingPhase {
    self.videoGraphicLineContainer.hidden = NO;

    self.recordButton.selected = YES;
    self.recordButton.enabled = YES;
    self.playButton.enabled = YES;
    self.nextLessonButton.enabled = YES;
    self.nexLessonButtonBack.enabled = YES;
    self.previousLessonButton.enabled = YES;
    self.singleFingerTap.enabled = YES;
    self.currentLessonState = RECORDING_STATE;

    if (!self.showSentenceNextTime) {
        self.viewSubtitles.hidden = ![[self.userDefaults objectForKey:kShowSentenceAfterPlayedVideo] boolValue];
    } else if (self.showSentenceNextTime) {
        [self showSentence];
    }

    self.guessGameButton.hidden = self.showSentenceNextTime;
    if (![self.userDefaults boolForKey:kHasDisplayedRecordToolTip]) {
        [self showRecordCoachMark];
    }
}

- (void)handleSingleTapVideoContainer:(id)handleSingleTapVideoContainer {
    [self playVideoButtonPressed:nil];
}

- (IBAction)recordVideo:(id)sender {
    self.counterVideosRecorded++;

    self.testButton.enabled = NO;
    self.testButton.selected = NO;
    self.translationButton.enabled = NO;
    self.translationButton.selected = NO;
    self.playButton.enabled = NO;
    self.recordButton.enabled = NO;
    self.nextLessonButton.enabled = NO;
    self.nexLessonButtonBack.enabled = NO;
    self.previousLessonButton.enabled = NO;
    self.singleFingerTap.enabled = NO;
    self.sendTranslationButton.hidden = YES;
    self.sendTranslationButton.enabled = NO;
    self.sendTranslationPlusButton.hidden = YES;
    self.sendTranslationPlusButton.enabled = NO;
    self.notificationsDisplay.text = @"";

    self.subtitleTextView.hidden = YES;
    [self setupCountDownView];

    [[FeedbackSender instance] sendFeedbackEvent:@"Pressed Record Button" andExtraInfo:@{
            @"videoName": self.selectedVideo[@"frase"],
            @"courseName": self.selectedCourse[@"curso"]
    }];
}

- (void)endRecordingPhase {
    [self stopRecordAudio];
    [self.audioHandler stopPlayerRecordedAudio];

    DLog(@"endRecordingPhase")

    self.testButton.selected = YES;
    self.testButton.enabled = YES;
    self.hasUploadedAudio = NO;
    self.audioUploadingIndicator.hidden = NO;

    VideoInfo *videoInfo = [[DatabaseManager instance] getVideoInfo:self.selectedVideo[@"id_video"]];

    [self.fileUploadHandler startUpload:self.audioHandler.outputAudioFileURL.path withVideoId:videoInfo.id andWithUserId:[UserSession instance].idUser];

    if (![self.userDefaults boolForKey:kHasDisplayedTestToolTip]) {
        [self showTestCoachMark];
    }
}

- (IBAction)testVideo:(id)sender {
    self.videoGraphicLineContainer.hidden = NO;
    [self.videoPlayerHandler hideVideo:NO];

    self.playButton.enabled = NO;
    self.recordButton.enabled = NO;
    self.testButton.enabled = NO;
    self.translationButton.enabled = NO;
    self.audioUploadingIndicator.hidden = YES;
    self.sendTranslationButton.hidden = YES;
    self.sendTranslationButton.enabled = NO;
    self.sendTranslationPlusButton.hidden = YES;
    self.sendTranslationPlusButton.enabled = NO;
    self.nextLessonButton.enabled = NO;
    self.nexLessonButtonBack.enabled = NO;
    self.previousLessonButton.enabled = NO;

    [self.videoPlayerHandler changeVolume:1];
    [self.videoPlayerHandler seekVideoToZero];

    [self.bridge callHandler:@"play_canvas" data:@"" responseCallback:^(id responseData) {
        [self.videoPlayerHandler playVideo];
        [self playRecordedAudio];
    }];

    self.currentLessonState = TESTING_STATE;

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(videoPlaybackEnded)
                                                 name:AVPlayerItemDidPlayToEndTimeNotification
                                               object:nil];

    [[FeedbackSender instance] sendFeedbackEvent:@"Pressed Test Button" andExtraInfo:@{
            @"videoName": self.selectedVideo[@"frase"],
            @"courseName": self.selectedCourse[@"curso"]
    }];
}

- (void)endTestingPhase {
    [self showTestState:(NSUInteger) [[self.sectionsHandler getSelectedVideo][@"puntuacion"] integerValue]];
    if (!self.hasUploadedAudio) {
        self.audioUploadingIndicator.hidden = NO;
    }
}

- (void)showTestState:(NSUInteger)points {
    self.notificationsDisplay.text = [GradeMessageResolver resolveMessageWithGrade:points];

    BOOL shouldShowTranslationButton = self.hasUploadedAudio && points > kLimitToPassLesson;
    self.translationButton.selected = shouldShowTranslationButton;
    self.translationButton.enabled = shouldShowTranslationButton;
    self.videoGraphicLineContainer.hidden = self.hasUploadedAudio;

    self.playButton.enabled = self.hasUploadedAudio;
    self.playButton.selected = self.hasUploadedAudio;
    self.recordButton.enabled = self.hasUploadedAudio;
    self.recordButton.selected = self.hasUploadedAudio;
    self.nextLessonButton.enabled = self.hasUploadedAudio;
    self.nexLessonButtonBack.enabled = self.hasUploadedAudio;
    self.previousLessonButton.enabled = self.hasUploadedAudio;
    self.testButton.enabled = YES;
    self.testButton.selected = YES;

    self.currentLessonState = TESTING_STATE;
}

- (IBAction)translationVideo:(id)sender {
    self.currentLessonState = TRASLATING_STATE;
    [self showTranslation];

    [[FeedbackSender instance] sendFeedbackEvent:@"Pressed Translation Button" andExtraInfo:@{
            @"videoName": self.selectedVideo[@"frase"],
            @"courseName": self.selectedCourse[@"curso"]
    }];
}

- (void)showTranslation {
    self.videoGraphicLineContainer.hidden = YES;
    NSString *translation = [[self sectionsHandler] getSelectedVideo][@"traduccion"];
    if (translation && ![translation isEqualToString:@""]) {
        self.notificationsDisplay.text = [NSString stringWithFormat:@"%@ \n\n %@",
                                                                    [[self sectionsHandler] getSelectedVideo][@"frase"],
                                                                    translation];
    } else {
        self.notificationsDisplay.text = [NSString stringWithFormat:@"%@\n\n",
                                                                    [[self sectionsHandler] getSelectedVideo][@"frase"]];
        self.sendTranslationButton.hidden = NO;
        self.sendTranslationButton.enabled = YES;
        self.sendTranslationPlusButton.hidden = NO;
        self.sendTranslationPlusButton.enabled = YES;
    }
}

- (void)showSentence {
    self.subtitleTextView.text = [NSString stringWithFormat:@"%@", [[self sectionsHandler] getSelectedVideo][@"frase"]];
    self.subtitleTextView.frame = self.subtitleTextViewFrame;

    CGFloat fixedWidth = self.subtitleTextView.frame.size.width;
    CGSize newSize = [self.subtitleTextView sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
    CGRect newFrame = self.subtitleTextView.frame;
    newFrame.size = CGSizeMake(newSize.width, newSize.height);
    self.subtitleTextView.frame = newFrame;
    self.subtitleTextView.scrollEnabled = NO;
    self.subtitleTextViewWidthConstraint.constant = newSize.width;
    self.subtitleTextViewHeightConstraint.constant = newSize.height;

    [self.subtitleTextView setNeedsUpdateConstraints];
    [self.subtitleTextView updateConstraints];

    [self.subtitleTextView setNeedsLayout];
    [self.subtitleTextView layoutIfNeeded];

    self.subtitleTextView.hidden = NO;
    self.viewSubtitles.hidden = YES;
    self.guessGameButton.hidden = YES;
    self.showSentenceNextTime = YES;
}

- (IBAction)showSubtitleButtonPressed:(id)sender {
    [self showSentence];

    [[FeedbackSender instance] sendFeedbackEvent:@"Pressed Subtitle Button" andExtraInfo:@{
            @"videoName": self.selectedVideo[@"frase"],
            @"courseName": self.selectedCourse[@"curso"]
    }];
}

- (void)resetLessonView {
    self.notificationsDisplay.text = @"";

    self.playButton.enabled = YES;
    self.playButton.selected = YES;

    self.recordButton.enabled = NO;
    self.recordButton.selected = NO;

    self.testButton.enabled = NO;
    self.testButton.selected = NO;

    self.translationButton.enabled = NO;
    self.translationButton.selected = NO;

    self.singleFingerTap.enabled = YES;
}

- (void)countdownFinished:(SFCountdownView *)view {
    view.hidden = YES;
    [self startRecordingAudioVoice];
}

#pragma mark - AlertView Delegate

- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (self.registerMessage == alertView && buttonIndex == 1) {
        [self.sectionsHandler moveOutSession:self];
    } else if (self.notifications.count > 0) {
        [UIAlertView showMessage:self.notifications[0]
                       withTitle:NSLocalizedString(@"app_name", @"")
                    withButtonOK:NSLocalizedString(@"ok_button", @"")
                     andDelegate:self];
        [self.notifications removeObject:self.notifications[0]];
    }
}

#pragma mark - Upload user data info

- (void)uploadAudioFileFinished:(NSDictionary *)data andError:(NSError *)error {
    DLog(@"uploadAudioFileFinished with Error: %@", error)

    if (error || ![data[@"status"] boolValue] || data[@"result"][@"error"]) {
        if (self.currentLessonState != TESTING_STATE) {
            self.playButton.enabled = YES;
            self.recordButton.enabled = YES;
            self.nextLessonButton.enabled = YES;
            self.nexLessonButtonBack.enabled = YES;
            self.previousLessonButton.enabled = YES;
            self.singleFingerTap.enabled = YES;
        }

        self.hasUploadedAudio = YES;
        self.audioUploadingIndicator.hidden = YES;
    }

    if (!error) {
        if ([data[@"status"] boolValue]) {
            NSDictionary *result = data[@"result"];
            NSString *errorMessage = result[@"error"];

            if (!errorMessage) {
                NSInteger points = [result[@"puntuacion"] integerValue];

                [self.speechRecognitor startListeningSpeech:self.audioHandler.outputAudioFileURL.path onComplete:^(int speechScore) {
                    [self presentUserFeedbackPopupIfRequired];

                    NSInteger totalScore = (points + speechScore) / 2;
                    [self updateUserInfoCourse:totalScore andWav:result[@"file"]];
                }];
            } else {
                [self presentMessage:errorMessage];
            }
        } else {
            [self presentMessage:data[@"result"]];
        }
    } else {
        [self presentMessage:NSLocalizedString(@"error_processing_uploaded_audio", @"")];
    }
}

- (void)presentUserFeedbackPopupIfRequired {
    self.userCourseTries++;
    if (self.userCourseTries == 6) {
        [UserFeedbackView show:self.sectionsHandler.selectedCourse[@"idCourse"]];
    }
}

- (void)updateUserInfoCourse:(NSInteger)points andWav:(NSString *)file {
    NSDictionary *params = @{
            @"id_video": [self.sectionsHandler getSelectedVideo][@"id_video"],
            @"id_curso": [self.sectionsHandler getSelectedVideo][@"id_curso"],
            @"id_usuario": [UserSession instance].idUser,
            @"token": [UserSession instance].token,
            @"puntuacion": @(points),
            @"archivo_wav": file,
            @"superado": points > 69 ? @YES : @NO
    };

    [[ProvidersManager instance].userProvider updateUserInfoCourse:params andHandler:^(BusinessError *error,
            NSDictionary *result) {

        if (self.currentLessonState != TESTING_STATE) {
            self.playButton.enabled = YES;
            self.recordButton.enabled = YES;
            self.nextLessonButton.enabled = YES;
            self.nexLessonButtonBack.enabled = YES;
            self.previousLessonButton.enabled = YES;
            self.singleFingerTap.enabled = YES;
        }

        self.hasUploadedAudio = YES;
        self.audioUploadingIndicator.hidden = YES;

        if (error) {
            DLog(@"Error: %@", error.message);
        } else {
            [self.sectionsHandler getSelectedVideo][@"puntuacion"] = @(points);

            if (points > kLimitToPassLesson) {
                [self passCurrentLevel];
            }

            if (points > kApplauseLimit) {
                [self.applausePlayer play];
            }

            [self.sectionsHandler refreshSectionsTree];
            [self showTestState:(NSUInteger) points];
            [self.eventHandler userAudioDataEvaluated:result[@"result"]];
        }
    }];
}

- (void)passCurrentLevel {
    self.nextLessonButton.enabled = YES;
    self.nexLessonButtonBack.enabled = YES;
    [self.sectionsHandler getSelectedVideo][@"superado"] = @YES;
    self.selectedVideo[@"superado"] = @YES;
    [self.animatorHandler addAnimationToView:self.nextLessonButton];
    if (!self.coachMarkTest && ![self.userDefaults boolForKey:hasDisplayedNextButtonToolTip]) {
        [self showNextPlayButtonCoachMark];
    }
}

- (BOOL)existsVideoFile:(VideoInfo *)videoInfo {
    NSURL *urlVideoInfo = [self getURL:videoInfo];
    return [[NSFileManager defaultManager] fileExistsAtPath:urlVideoInfo.path];
}

- (NSURL *)getURL:(VideoInfo *)videoInfo {
    NSURL *documentsDirectory = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
    return [documentsDirectory URLByAppendingPathComponent:videoInfo.pathVideo];
}

#pragma mark Show couch marks

- (void)showPlayCoachMark {
    NSArray *coachMarks = @[
            @{
                    @"view": self.playButton,
                    @"caption": NSLocalizedString(@"tooltip_play_button", @""),
                    @"position": @(LABEL_POSITION_LEFT),
                    @"showArrow": @YES

            }
    ];

    [CouchMarksHelper showMarksCoachMarks:coachMarks
                         intermediateView:self.playButton.superview
                                outerView:self.view.superview
                              andDelegate:self];

    [self.userDefaults setBool:YES forKey:kHasDisplayedPlayToolTip];
    [self.userDefaults synchronize];
}

- (void)showTreeViewCoachMark {
    NSArray *coachMarks = @[
            @{
                    @"view": self.treeViewBtn,
                    @"caption": NSLocalizedString(@"tooltip_treeview", @""),
                    @"position": @(LABEL_POSITION_RIGHT_BOTTOM),
                    @"alignment": @(LABEL_ALIGNMENT_CENTER),
                    @"showArrow": @YES
            }
    ];

    self.coachMarkTreeView = [CouchMarksHelper showMarksCoachMarks:coachMarks
                                                  intermediateView:self.view
                                                         outerView:self.view.superview
                                                       andDelegate:self];

}

- (void)showStatisticsCoachMark {
    NSArray *coachMarks = @[
            @{
                    @"view": self.statisticsButton,
                    @"caption": @"You can see your progress in this course here.",
                    @"position": @(LABEL_POSITION_RIGHT_BOTTOM),
                    @"alignment": @(LABEL_ALIGNMENT_CENTER),
                    @"showArrow": @YES
            }
    ];

    self.coachMarkStatistics = [CouchMarksHelper showMarksCoachMarks:coachMarks
                                                    intermediateView:self.view
                                                           outerView:self.view.superview
                                                         andDelegate:self];
}

- (void)showRecordCoachMark {
    NSArray *coachMarks = @[
            @{
                    @"view": self.recordButton,
                    @"caption": NSLocalizedString(@"tooltip_record_button", @""),
                    @"position": @(LABEL_POSITION_LEFT),
                    @"showArrow": @YES

            }
    ];

    [CouchMarksHelper showMarksCoachMarks:coachMarks
                         intermediateView:self.recordButton.superview
                                outerView:self.view.superview
                              andDelegate:self];

    [self.userDefaults setBool:YES forKey:kHasDisplayedRecordToolTip];
    [self.userDefaults synchronize];
}

- (void)showTestCoachMark {
    NSArray *coachMarks = @[
            @{
                    @"view": self.testButton,
                    @"caption": NSLocalizedString(@"notifications_display_press_test", @""),
                    @"position": @(LABEL_POSITION_LEFT),
                    @"showArrow": @YES

            }
    ];

    self.coachMarkTest = [CouchMarksHelper showMarksCoachMarks:coachMarks
                                              intermediateView:self.testButton.superview
                                                     outerView:self.view.superview
                                                   andDelegate:self];

    [self.userDefaults setBool:YES forKey:kHasDisplayedTestToolTip];
    [self.userDefaults synchronize];
}

- (void)showNextPlayButtonCoachMark {
    NSArray *coachMarks = @[
            @{
                    @"view": self.nextLessonButton,
                    @"caption": NSLocalizedString(@"notifications_display_press_next_play", @""),
                    @"position": @(LABEL_POSITION_LEFT),
                    @"showArrow": @YES

            }
    ];

    [CouchMarksHelper showMarksCoachMarks:coachMarks
                         intermediateView:self.nextLessonButton.superview
                                outerView:self.view.superview
                              andDelegate:self];

    [self.userDefaults setBool:YES forKey:hasDisplayedNextButtonToolTip];
    [self.userDefaults synchronize];
}

- (void)coachMarksViewDidCleanup:(MPCoachMarks *)coachMarksView {
    if (self.coachMarkStatistics == coachMarksView) {
        self.coachMarkStatistics = nil;
        [self showPlayCoachMark];
    }

    if (self.coachMarkTreeView == coachMarksView) {
        self.coachMarkTreeView = nil;
        [self showStatisticsCoachMark];
    }

    if (self.coachMarkTest == coachMarksView && ![self.userDefaults boolForKey:hasDisplayedNextButtonToolTip]) {
        [self showNextPlayButtonCoachMark];
    }
}

- (void)customIOS7dialogButtonTouchUpInside:(id)alertView clickedButtonAtIndex:(NSInteger)
        buttonIndex {
    CustomIOSAlertView *registrationSuggestionAlertView = alertView;
    if (buttonIndex == 0) {
        [registrationSuggestionAlertView close];
    } else if (buttonIndex == 1) {
        [registrationSuggestionAlertView close];
        [self.sectionsHandler moveOutSession:self];
    }
}

- (void)showStatisticsButton:(NSString *)userProgress {
    self.statisticsButton.hidden = NO;
    self.statisticsButtonPercentage.hidden = NO;
    [self.statisticsButtonPercentage setTitle:userProgress forState:UIControlStateNormal];
}

- (void)showStatistics:(StatisticsCourseViewModel *)model {
    NSString *statisticsViewDialogName = [DeviceUtils instance].isOnIpad ? @"StatisticsView_ipad" : @"StatisticsView";

    CustomIOSAlertView *statisticsAlertView = [[CustomIOSAlertView alloc] init];
    StatisticsView *statisticsPopupView = [[[NSBundle mainBundle] loadNibNamed:statisticsViewDialogName owner:nil options:nil] lastObject];

    statisticsPopupView.viewModel = model;
    statisticsPopupView.statisticsAlertView = statisticsAlertView;
    [statisticsAlertView setContainerView:statisticsPopupView];
    [statisticsAlertView setButtonTitles:NULL];
    [statisticsAlertView show];
    [statisticsPopupView loadStatistics];
}

- (IBAction)statisticsButtonPressed:(id)sender {
    [self.eventHandler statisticsButtonPressed];

    [[FeedbackSender instance] sendFeedbackEvent:@"Pressed Statistics Button" andExtraInfo:@{
            @"videoName": self.selectedVideo[@"frase"],
            @"courseName": self.selectedCourse[@"curso"]
    }];
}

- (void)showMilestone:(NSString *)milestoneContent {
    [MilestonePopupView showPopupView:milestoneContent];
}

- (void)showPointsBar:(NSInteger)points {
    if (self.pointsBar) {
        [self.pointsBar removeFromSuperview];
    }

    float percentage = points / 100.0f;
    self.pointsBar = [[UIView alloc] initWithFrame:CGRectMake(self.videoPlayerHandler.layer.videoRect.origin.x,
            self.videoPlayerHandler.layer.videoRect.origin.y + self.videoPlayerHandler.layer.videoRect.size.height,
            self.videoPlayerHandler.layer.videoRect.size.width * percentage, 2)];
    self.pointsBar.backgroundColor = [UIColor nativoxBlueColor];
    [self.videoContainer addSubview:self.pointsBar];
}

- (void)showAlarmReminderDialog {
    CustomIOSAlertView *alarmReminderDialogAlertView = [[CustomIOSAlertView alloc] init];
    NSString *alarmReminderDialogName = [DeviceUtils instance].isOnIpad ? @"AlarmReminderDialog_ipad" : @"AlarmReminderDialog";
    AlarmReminderDialog *alarmReminderDialog = [[[NSBundle mainBundle] loadNibNamed:alarmReminderDialogName owner:nil options:nil] lastObject];

    alarmReminderDialog.alertViewContainer = alarmReminderDialogAlertView;
    alarmReminderDialog.completionBlock = ^{
        [self goBackToPreviousScreen];
    };

    alarmReminderDialogAlertView.onCloseDialog = ^{
        [self goBackToPreviousScreen];
    };

    [alarmReminderDialogAlertView setContainerView:alarmReminderDialog];
    [alarmReminderDialogAlertView setButtonTitles:NULL];
    [alarmReminderDialogAlertView setUseMotionEffects:YES];
    [alarmReminderDialogAlertView show];
}

- (IBAction)sendTranslationButtonPressed:(id)sender {
    [self.eventHandler sendTranslationButtonPressed];
    [[FeedbackSender instance] sendFeedbackEvent:@"Pressed Send Translation Button" andExtraInfo:@{
            @"videoName": self.selectedVideo[@"frase"],
            @"courseName": self.selectedCourse[@"curso"]
    }];
}

- (void)showTranslationDialog {
    CustomIOSAlertView *dialogAlertView = [[CustomIOSAlertView alloc] init];
    TranslationDialog *translationDialogView = [[[NSBundle mainBundle] loadNibNamed:@"TranslationDialog" owner:nil options:nil] lastObject];
    translationDialogView.dialogAlertView = dialogAlertView;
    translationDialogView.videoId = [self.sectionsHandler getSelectedVideo][@"id_video"];
    translationDialogView.translationTextView.placeholder = [NSString stringWithFormat:@"Sentence to translate: %@", [self.sectionsHandler getSelectedVideo][@"frase"]];
    [dialogAlertView setContainerView:translationDialogView];
    [dialogAlertView setButtonTitles:NULL];
    [dialogAlertView setUseMotionEffects:YES];
    [dialogAlertView show];
}

- (void)goBackToPreviousScreen {
    self.eventHandler = nil;
    [self.revealViewController dismissViewControllerAnimated:YES completion:nil];
}

- (GADInterstitial *)createAndLoadInterstitial {
    GADInterstitial *interstitial = [[GADInterstitial alloc] initWithAdUnitID:kAdUnitID];
    interstitial.delegate = self;
    GADRequest *request = [GADRequest request];
    [interstitial loadRequest:request];
    return interstitial;
}

- (void)presentInterestitial {
    if ([self.interstitialAd isReady] && [UserSession instance].purchasedCourses.count == 0) {
        [self.interstitialAd presentFromRootViewController:self];

        [[FeedbackSender instance] sendFeedbackEvent:@"Presented interestitial ads" andExtraInfo:@{
                @"videoName": self.selectedVideo[@"frase"],
                @"courseName": self.selectedCourse[@"curso"]
        }];
    }
}

- (void)interstitialDidDismissScreen:(GADInterstitial *)interstitial {
    self.interstitialAd = [self createAndLoadInterstitial];
    [MessageViewDialog showDialog:NSLocalizedString(@"remove_ads_courses_message", @"")];
}

- (IBAction)guessGameButtonPressed:(id)sender {
    [[FeedbackSender instance] sendFeedbackEvent:@"Listening Game Opened" andExtraInfo:@{
            @"videoName": self.selectedVideo[@"frase"],
            @"courseName": self.selectedCourse[@"curso"]
    }];

    CustomIOSAlertView *dialogAlertView = [[CustomIOSAlertView alloc] init];
    ListeningGameView *listeningGameView = [[[NSBundle mainBundle] loadNibNamed:@"ListeningGameView" owner:nil options:nil] lastObject];
    listeningGameView.sentence = [[self sectionsHandler] getSelectedVideo][@"frase"];
    listeningGameView.dialogAlertView = dialogAlertView;
    listeningGameView.lessonViewController = self;
    [dialogAlertView setContainerView:listeningGameView];
    [dialogAlertView setButtonTitles:NULL];
    [dialogAlertView setUseMotionEffects:YES];
    [dialogAlertView show];
    [listeningGameView loadView];
}

@end
