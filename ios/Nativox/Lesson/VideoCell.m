//
//  SectionCell.m
//  Nativox
//
//  Created by redhair84 on 08/03/2014.
//  Copyright (c) 2014 Francisco Javier Morant. All rights reserved.
//

#import "VideoCell.h"

@interface VideoCell ()

@end

@implementation VideoCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
