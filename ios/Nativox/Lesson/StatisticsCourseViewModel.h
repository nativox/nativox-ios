//
// Created by redhair84 on 14/06/2015.
// Copyright (c) 2015 Francisco Javier Morant. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface StatisticsCourseViewModel : NSObject
@property(nonatomic) float progress;
@property(nonatomic) float completedVideos;
@property(nonatomic) float averageGrade;
@property(nonatomic, strong) NSString *courseTitle;

@end