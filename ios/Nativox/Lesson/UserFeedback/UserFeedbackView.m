#import "UserFeedbackView.h"
#import "RCTRootView.h"
#import "NativoxAppDelegate.h"
#import "CustomIOSAlertView.h"
#import "FeedbackSender.h"
#import "UserSession.h"

@interface UserFeedbackView ()
@property(nonatomic, strong) CustomIOSAlertView *customAlertView;
@property(nonatomic, strong) NSString *courseId;
@end

@implementation UserFeedbackView

RCT_EXPORT_MODULE();

- (void)loadUserFeedbackDialog {
    NativoxAppDelegate *delegate = (NativoxAppDelegate *) [[UIApplication sharedApplication] delegate];

    NSDictionary *options = @{
            @"userEmail" : [UserSession instance].email ? [UserSession instance].email : @"",
            @"userName" : [UserSession instance].name ? [UserSession instance].name : @"",
            @"token" : [UserSession instance].token ? [UserSession instance].token : @"",
            @"idUser" : [UserSession instance].idUser ? [UserSession instance].idUser : @"",
            @"language" : [UserSession instance].language ? [UserSession instance].language : @"",
            @"idSession": [NSString stringWithFormat:@"%d", (int) [UserSession instance].startSession]
    };

    RCTRootView *rootView = [[RCTRootView alloc] initWithBridge:delegate.bridge moduleName:@"UserFeedback"
                                              initialProperties:options];
    [rootView setBackgroundColor:[UIColor clearColor]];
    [self addSubview:rootView];
    rootView.frame = self.bounds;
}

- (dispatch_queue_t)methodQueue {
    return dispatch_get_main_queue();
}

RCT_EXPORT_METHOD(sentUserFeedback) {
    [[UserFeedbackView instance].customAlertView close];
    [UserFeedbackView instance].customAlertView = nil;
}

+ (UserFeedbackView *)instance {
    static UserFeedbackView *_instance = nil;

    @synchronized (self) {
        if (_instance == nil) {
            _instance = [[[NSBundle mainBundle] loadNibNamed:@"UserFeedback" owner:nil options:nil] lastObject];
        }
    }

    return _instance;
}

+ (void)show:(NSString *)courseId {
    CustomIOSAlertView *dialogAlertView = [[CustomIOSAlertView alloc] init];
    UserFeedbackView *userFeedbackView = [UserFeedbackView instance];
    [dialogAlertView setContainerView:userFeedbackView];
    [dialogAlertView setButtonTitles:NULL];
    [dialogAlertView setUseMotionEffects:YES];
    dialogAlertView.alphaContainer = 1;
    [dialogAlertView show];
    dialogAlertView.dialogView.backgroundColor = [UIColor clearColor];
    dialogAlertView.containerView.backgroundColor = [UIColor clearColor];
    userFeedbackView.customAlertView = dialogAlertView;
    userFeedbackView.courseId = courseId;
    [userFeedbackView loadUserFeedbackDialog];
}

@end
