#import <UIKit/UIKit.h>
#import "RCTBridgeModule.h"

@class CustomIOSAlertView;

@interface UserFeedbackView : UIView <RCTBridgeModule>

+ (void)show:(NSString *) courseId;

@end
