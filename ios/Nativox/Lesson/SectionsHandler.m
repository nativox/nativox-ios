#import "SectionsHandler.h"
#import "BaseViewController.h"

@implementation SectionsHandler {

}

- (instancetype)initWithSelectedCourse:(NSDictionary *)selectedCourse {
    self = [super init];
    if (self) {
        self.selectedCourse = selectedCourse;
        self.selectedSeccion = 0;
        self.selectedSubSeccion = 0;
        self.selectedVideo = 0;
        self.isDownloadingVideo = NO;
    }

    [self skipNextNotPassedVideo:0 withSelectedSubseccion:0 andSelectedVideo:0];
    return self;
}

+ (instancetype)helperWithSelectedCourse:(NSDictionary *)selectedCourse {
    return [[self alloc] initWithSelectedCourse:selectedCourse];
}

- (NSMutableDictionary *)getSelectedVideo {
    return self.selectedCourse[@"secciones"][self.selectedSeccion][@"subsecciones"][self.selectedSubSeccion][@"videos"][self.selectedVideo];
}

- (void)changeCurrentSelection:(NSUInteger)selectedSection
        withSelectedSubseccion:(NSUInteger)selectedSubSeccion
              andSelectedVideo:(NSUInteger)selectedVideo {
    self.selectedSeccion = selectedSection;
    self.selectedSubSeccion = selectedSubSeccion;
    self.selectedVideo = selectedVideo;

    [self.sectionsHelperDelegateForLessonView newLessonSelected:YES];
    [self.sectionsHelperDelegateForSectionsView newLessonSelected:YES];
}

- (void)moveToPreviousVideo {
    if (self.selectedSeccion >= 0 && self.selectedSubSeccion > 0 && self.selectedVideo == 0) {
        NSUInteger numVideosPrev = ((NSArray *) self.selectedCourse[@"secciones"][self.selectedSeccion][@"subsecciones"][self.selectedSubSeccion - 1][@"videos"]).count;

        [self changeCurrentSelection:self.selectedSeccion withSelectedSubseccion:self.selectedSubSeccion - 1 andSelectedVideo:numVideosPrev - 1];
    } else if (self.selectedSeccion > 0 && self.selectedSubSeccion == 0) {
        NSUInteger numSubSeccionesPrev = ((NSArray *) self.selectedCourse
        [@"secciones"][self.selectedSeccion - 1][@"subsecciones"]).count;
        NSUInteger numVideosPrev = ((NSArray *) self.selectedCourse[@"secciones"][self.selectedSeccion - 1][@"subsecciones"][numSubSeccionesPrev - 1][@"videos"]).count;
        [self changeCurrentSelection:self.selectedSeccion - 1 withSelectedSubseccion:numSubSeccionesPrev - 1 andSelectedVideo:numVideosPrev - 1];
    } else if (self.selectedSeccion >= 0 && self.selectedSubSeccion >= 0 && self.selectedVideo > 0) {
        [self changeCurrentSelection:self.selectedSeccion withSelectedSubseccion:self.selectedSubSeccion andSelectedVideo:self.selectedVideo - 1];
    }
}

- (void)moveToNextVideo {
    NSUInteger countSections = ((NSArray *) self.selectedCourse[@"secciones"]).count;
    NSUInteger countSubSections = ((NSArray *) self.selectedCourse[@"secciones"][(NSUInteger) self.selectedSeccion][@"subsecciones"]).count;
    NSUInteger countVideos = ((NSArray *) self.selectedCourse[@"secciones"][self.selectedSeccion][@"subsecciones"][self.selectedSubSeccion][@"videos"]).count;

    if (_selectedSeccion + 1 < countSections && (_selectedSubSeccion + 1) == countSubSections && (_selectedVideo + 1) == countVideos) {
        [self skipNextNotPassedVideo:self.selectedSeccion + 1 withSelectedSubseccion:0 andSelectedVideo:0];
    } else if (_selectedSeccion < countSections && _selectedSubSeccion + 1 < countSubSections && (_selectedVideo + 1) == countVideos) {
        [self skipNextNotPassedVideo:self.selectedSeccion withSelectedSubseccion:_selectedSubSeccion + 1 andSelectedVideo:0];
    } else if (_selectedSeccion < countSections && _selectedSubSeccion < countSubSections && _selectedVideo + 1 < countVideos) {
        [self skipNextNotPassedVideo:self.selectedSeccion withSelectedSubseccion:_selectedSubSeccion andSelectedVideo:_selectedVideo + 1];
    } else if (_selectedSeccion == (countSections + 1) && _selectedSubSeccion == (countSubSections + 1) && _selectedVideo + 1 < countVideos) {
        [self skipNextNotPassedVideo:self.selectedSeccion withSelectedSubseccion:_selectedSubSeccion andSelectedVideo:_selectedVideo + 1];
    }

    [self changeCurrentSelection:self.selectedSeccion withSelectedSubseccion:self.selectedSubSeccion andSelectedVideo:self.selectedVideo];
}

- (void)skipNextNotPassedVideo:(NSUInteger)selectedSection
        withSelectedSubseccion:(NSUInteger)selectedSubSeccion
              andSelectedVideo:(NSUInteger)selectedVideo {
    for (NSUInteger i = selectedSection; i < ((NSArray *) self.selectedCourse[@"secciones"]).count; i++) {
        NSDictionary *section = self.selectedCourse[@"secciones"][(NSUInteger) i];
        for (NSUInteger j = selectedSubSeccion; j < ((NSArray *) section[@"subsecciones"]).count; j++) {
            NSDictionary *subsection = section[@"subsecciones"][(NSUInteger) j];
            for (NSUInteger z = selectedVideo; z < ((NSArray *) subsection[@"videos"]).count; z++) {
                NSDictionary *video = subsection[@"videos"][(NSUInteger) z];
                if ([video[@"puntuacion"] integerValue] < 70) {
                    self.selectedSeccion = (NSUInteger) i;
                    self.selectedSubSeccion = (NSUInteger) j;
                    self.selectedVideo = (NSUInteger) z;
                    return;
                }
            }
        }
    }
}

- (void)refreshSectionsTree {
    [self.sectionsHelperDelegateForSectionsView refreshSectionsTree];
    [self.sectionsHelperDelegateForSectionsView newLessonSelected:NO];
}

- (void)moveOutSession:(BaseViewController *)controller {
    [controller dismissViewControllerAnimated:NO completion:^{
        [controller logoutFromLessonScreen];
    }];
}

@end
