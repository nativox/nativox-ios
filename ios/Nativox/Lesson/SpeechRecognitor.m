#import "SpeechRecognitor.h"
#import <OpenEars/OELanguageModelGenerator.h>
#import <OpenEars/OEAcousticModel.h>
#import <OpenEars/OEPocketsphinxController.h>

extern OSStatus DoConvertFile(CFURLRef sourceURL, CFURLRef destinationURL, OSType outputFormat, Float64 outputSampleRate);

@interface SpeechRecognitor ()
@property(nonatomic, copy) NSString *lmPath;
@property(nonatomic, copy) NSString *dicPath;
@property(nonatomic, copy) NSString *sentence;
@property(nonatomic) CFURLRef sourceURL;
@property(nonatomic) CFURLRef destinationURL;

@end

@implementation SpeechRecognitor {
}
- (void)setupSentence:(NSString *)sentence {
    self.sentence = sentence;
    OELanguageModelGenerator *lmGenerator = [[OELanguageModelGenerator alloc] init];

    NSArray *words = @[sentence, @"blahh", @"bla", @"baaaahhh", @"meeeehhhh", @"eeeeeeehhr", @"ooooaaaaahhh"];
    NSString *name = @"NameIWantForMyLanguageModelFiles";
    NSError *error = [lmGenerator generateLanguageModelFromArray:words
                                                  withFilesNamed:name
                                          forAcousticModelAtPath:[OEAcousticModel
                                                  pathToModel:@"AcousticModelEnglish"]];

    if (error == nil) {
        self.lmPath = [lmGenerator pathToSuccessfullyGeneratedLanguageModelWithRequestedName:@"NameIWantForMyLanguageModelFiles"];
        self.dicPath = [lmGenerator pathToSuccessfullyGeneratedDictionaryWithRequestedName:@"NameIWantForMyLanguageModelFiles"];

    } else {
        DLog(@"Error: %@", [error localizedDescription]);
    }


    [[OEPocketsphinxController sharedInstance] setActive:TRUE error:nil];


    self.openEarsEventsObserver = [[OEEventsObserver alloc] init];
    [self.openEarsEventsObserver setDelegate:self];

}

- (void)startListeningSpeech:(NSString *)path onComplete:(void (^)(int))complete {
    self.onComplete = complete;
    self.totalScore = 0;
    self.sourceURL = CFURLCreateWithFileSystemPath(kCFAllocatorDefault, (__bridge CFStringRef) path, kCFURLPOSIXPathStyle, false);

    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = paths[0];
    NSString *destinationFilePath = [[NSString alloc] initWithFormat:@"%@/speech.wav", documentsDirectory];
    self.destinationURL = CFURLCreateWithFileSystemPath(kCFAllocatorDefault, (__bridge CFStringRef) destinationFilePath, kCFURLPOSIXPathStyle, false);

    DoConvertFile(self.sourceURL, self.destinationURL, kAudioFormatLinearPCM, 16000.0f);

    [[OEPocketsphinxController sharedInstance] runRecognitionOnWavFileAtPath:destinationFilePath
                                                    usingLanguageModelAtPath:self.lmPath
                                                            dictionaryAtPath:self.dicPath
                                                         acousticModelAtPath:[OEAcousticModel pathToModel:@"AcousticModelEnglish"]
                                                         languageModelIsJSGF:NO];

    [self performSelector:@selector(speechRecognisitionTimeout) withObject:self afterDelay:8.0];
}

- (void)pocketsphinxDidReceiveHypothesis:(NSString *)hypothesis
                        recognitionScore:(NSString *)recognitionScore
                             utteranceID:(NSString *)utteranceID {
    NSArray *words = [self.sentence.lowercaseString componentsSeparatedByString:@" "];

    NSMutableArray *wordsSentence = [[NSMutableArray alloc] init];
    for (NSString *word in words) {
        if (![[word stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:@""]) {
            [wordsSentence addObject:word];
        }
    }

    NSArray *wordsHypothesis = [hypothesis.lowercaseString componentsSeparatedByString:@" "];

    float totalScore = 0;
    int i = 0;

    for (NSString *word in wordsSentence) {
        if (i < wordsHypothesis.count) {
            NSString *hypoWord = wordsHypothesis[(NSUInteger) i];
            NSString *nextHypoWord = i + 1 < wordsHypothesis.count ? wordsHypothesis[(NSUInteger) i + 1] : @"";
            NSString *prevHypoWord = i - 1 > 0 ? wordsHypothesis[(NSUInteger) i - 1] : @"";
            NSString *nextTwoHypoWord = i + 2 < wordsHypothesis.count ? wordsHypothesis[(NSUInteger) i + 2] : @"";
            NSString *prevTwoHypoWord = i - 2 > 0 ? wordsHypothesis[(NSUInteger) i - 1] : @"";

            if ([hypoWord.lowercaseString isEqualToString:word.lowercaseString]) {
                totalScore++;
            } else if ([nextHypoWord.lowercaseString isEqualToString:word.lowercaseString]) {
                totalScore += 0.75f;
            } else if ([prevHypoWord.lowercaseString isEqualToString:word.lowercaseString]) {
                totalScore += 0.75f;
            } else if ([nextTwoHypoWord.lowercaseString isEqualToString:word.lowercaseString]) {
                totalScore += 0.5f;
            } else if ([prevTwoHypoWord.lowercaseString isEqualToString:word.lowercaseString]) {
                totalScore += 0.5f;
            }
        }

        i++;
    }

    self.totalScore = (int) ((totalScore / wordsSentence.count) * 100);

    if (self.onComplete) {
        self.onComplete(self.totalScore);
        self.onComplete = nil;
    }
}

- (void) pocketSphinxContinuousTeardownDidFailWithReason:(NSString *)reasonForFailure {
    if (self.onComplete) {
        self.onComplete(0);
        self.onComplete = nil;
    }
}

- (void) pocketSphinxContinuousSetupDidFailWithReason:(NSString *)reasonForFailure {
    if (self.onComplete) {
        self.onComplete(0);
        self.onComplete = nil;
    }
}

- (void) pocketsphinxDidSuspendRecognition {
    if (self.onComplete) {
        self.onComplete(0);
        self.onComplete = nil;
    }
}

- (void)speechRecognisitionTimeout {
    if (self.onComplete) {
        self.onComplete(0);
        self.onComplete = nil;
    }
}

@end
