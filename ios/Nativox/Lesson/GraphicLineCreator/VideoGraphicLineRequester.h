//
// Created by redhair84 on 29/10/2015.
// Copyright (c) 2015 Francisco Javier Morant. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Video;


@interface VideoGraphicLineRequester : NSObject

@property(nonatomic, strong) UIWebView *videoGraphicLineContainer;

- (void)loadCurrentGraphicLineForVideo:(NSMutableDictionary *)video andCourse:(NSDictionary *)course;

@end