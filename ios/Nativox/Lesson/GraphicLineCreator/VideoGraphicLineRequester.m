
#import "VideoGraphicLineRequester.h"
#import "HttpHandler.h"
#import "UserSession.h"


@implementation VideoGraphicLineRequester {

}

- (void)loadCurrentGraphicLineForVideo:(NSMutableDictionary *)video andCourse:(NSDictionary *)course {
    NSURL *url = [NSURL URLWithString:[self urlGraphicForVideo:video andCourse:course]];
    NSURLRequest *request = [NSURLRequest requestWithURL:url
                                             cachePolicy:NSURLRequestUseProtocolCachePolicy
                                         timeoutInterval:TIMEOUT_REQUEST_HTTP];

    [self.videoGraphicLineContainer loadRequest:request];
}

- (NSString *)urlGraphicForVideo:(NSMutableDictionary *)video andCourse:(NSDictionary *)course {
    NSString *version = [[NSBundle mainBundle] infoDictionary][@"CFBundleShortVersionString"];

    if ([UserSession instance].idUser && course[@"comprado"]) {
        return [NSString stringWithFormat:@"%@/video_linea?id_video=%@&id_usuario=%@&token=%@&width=%d&height=%d"
                                                  "&lang=%@&os=ios&version=%@",
                                          kUrlHostname,
                                          video[@"id_video"],
                                          [UserSession instance].idUser,
                                          [UserSession instance].token,
                                          (int) self.videoGraphicLineContainer.frame.size.width,
                                          (int) self.videoGraphicLineContainer.frame.size.height,
                                          [UserSession instance].language,
                                          version
        ];
    } else {
        return [NSString stringWithFormat:@"%@/video_linea?id_video=%@&width=%d&height=%d&lang=%@&os=ios",
                                          kUrlHostname,
                                          video[@"id_video"],
                                          (int) self.videoGraphicLineContainer.frame.size.width,
                                          (int) self.videoGraphicLineContainer.frame.size.height,
                                          [UserSession instance].language
        ];
    }
}

@end