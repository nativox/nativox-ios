#import "LessonInteractor.h"
#import "LessonUserInterface.h"
#import "UserSession.h"
#import "StatisticsCourseViewModel.h"
#import "SectionsHandler.h"
#import "AlarmReminderInteractor.h"

@interface LessonInteractor ()

@property(nonatomic) int timesPlayed;

@end

@implementation LessonInteractor

- (void)backButtonPressed {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    BOOL hasDisplayedAlarmReminderDialog = [userDefaults boolForKey:kHasDisplayedAlarmReminderDialog];
    if (!hasDisplayedAlarmReminderDialog) {
        [userDefaults setBool:YES forKey:kHasDisplayedAlarmReminderDialog];
        [userDefaults synchronize];
        [self.userInterface showAlarmReminderDialog];
    } else {
        [self.userInterface goBackToPreviousScreen];
    }
}

- (void)playButtonPressed {
    self.timesPlayed++;
}

- (void)sendTranslationButtonPressed {
    [self.userInterface showTranslationDialog];
}

- (void)userAudioDataEvaluated:(NSDictionary *)data {
    if (data[@"stats_curso"]) {
        [self updateCourseStats:data];
        float completed = [data[@"stats_curso"][@"porcentaje_completado"] floatValue];
        [self.userInterface showStatisticsButton:[NSString stringWithFormat:@"%d%%", (int) completed]];
    }

    if (data[@"mensaje"] && ![data[@"mensaje"] isEqualToString:@""]) {
        [self.userInterface showMilestone:data[@"mensaje"]];
    }

    [self.userInterface showPointsBar:[[self.sectionsHandler getSelectedVideo][@"puntuacion"] integerValue]];
}

- (void)updateCourseStats:(NSDictionary *)data {
    float progressInCourse = [data[@"stats_curso"][@"nota_media_ultima_per_video_y_total_videos"] floatValue];
    self.sectionsHandler.selectedCourse[@"stats_curso"][@"nota_media_ultima_per_video_y_total_videos"] = @(progressInCourse);
    float completed = [data[@"stats_curso"][@"porcentaje_completado"] floatValue];
    self.sectionsHandler.selectedCourse[@"stats_curso"][@"porcentaje_completado"] = @(completed);
    float averageGrade = [data[@"stats_curso"][@"nota_media_mas_alta_per_video"] floatValue];
    self.sectionsHandler.selectedCourse[@"stats_curso"][@"nota_media_grabaciones"] = @(averageGrade);
}

- (void)statisticsButtonPressed {
    StatisticsCourseViewModel *statisticsCourseViewModel = [self statisticsCourseViewModel];
    [self.userInterface showStatistics:statisticsCourseViewModel];
}

- (void)videoPlayerLoaded {
    [self.userInterface showPointsBar:[[self.sectionsHandler getSelectedVideo][@"puntuacion"] integerValue]];
}

- (void)screenDidAppear {
    self.timesPlayed = 0;
    if ([UserSession instance].idUser) {
        if (self.sectionsHandler.selectedCourse[@"stats_curso"][@"porcentaje_completado"] != nil) {
            NSString *userProgress = [NSString stringWithFormat:@"%d%%", [self.sectionsHandler.selectedCourse[@"stats_curso"][@"porcentaje_completado"] integerValue]];
            [self.userInterface showStatisticsButton:userProgress];
        } else {
            [self.userInterface showStatisticsButton:@"0%"];
        }
    }
}

- (StatisticsCourseViewModel *)statisticsCourseViewModel {
    StatisticsCourseViewModel *statisticsCourseViewModel = [[StatisticsCourseViewModel alloc] init];
    statisticsCourseViewModel.courseTitle = self.sectionsHandler.selectedCourse[@"curso"];

    if (self.sectionsHandler.selectedCourse[@"stats_curso"] != nil) {
        statisticsCourseViewModel.progress = [self.sectionsHandler.selectedCourse[@"stats_curso"][@"nota_media_ultima_per_video"] floatValue];
        statisticsCourseViewModel.completedVideos = [self.sectionsHandler.selectedCourse[@"stats_curso"][@"porcentaje_completado"] floatValue];
        statisticsCourseViewModel.averageGrade = [self.sectionsHandler.selectedCourse[@"stats_curso"][@"nota_media_ultima_per_video_y_total_videos"] floatValue];
    } else {
        statisticsCourseViewModel.progress = 0;
        statisticsCourseViewModel.completedVideos = 0;
        statisticsCourseViewModel.averageGrade = 0;
    }
    return statisticsCourseViewModel;
}

@end
