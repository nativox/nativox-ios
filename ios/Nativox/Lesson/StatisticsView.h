//
//  StatisticsView.h
//  Nativox
//
//  Created by redhair84 on 11/06/2015.
//  Copyright (c) 2015 Francisco Javier Morant. All rights reserved.
//

#import <UIKit/UIKit.h>

@class StatisticsCourseViewModel;
@class CustomIOSAlertView;

@interface StatisticsView : UIView

@property(nonatomic, strong) StatisticsCourseViewModel *viewModel;

@property(nonatomic, strong) CustomIOSAlertView *statisticsAlertView;

- (void)loadStatistics;
@end
