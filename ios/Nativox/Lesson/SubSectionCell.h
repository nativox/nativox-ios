//
//  SectionCell.h
//  Nativox
//
//  Created by redhair84 on 08/03/2014.
//  Copyright (c) 2014 Francisco Javier Morant. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SubSectionCell : UITableViewCell

@property(nonatomic, strong) UILabel *title;

@end
