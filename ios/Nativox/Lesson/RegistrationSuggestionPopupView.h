//
//  RegistrationSuggestionPopupView.h
//  Nativox
//
//  Created by redhair84 on 24/01/2015.
//  Copyright (c) 2015 Francisco Javier Morant. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CustomIOSAlertView;

@interface RegistrationSuggestionPopupView : UIView

@property(nonatomic, strong) CustomIOSAlertView *alertView;

@end
