#import "SectionsViewController.h"
#import "VideoCell.h"
#import "RATreeView+Private.h"
#import "UserSession.h"

@interface SectionsViewController ()
@property(strong, nonatomic) IBOutlet UIScrollView *vistaArbolScroll;

@end

@implementation SectionsViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    CGRect viewFrame = self.vistaArbolScroll.bounds;
    RATreeView *treeView = [[RATreeView alloc] initWithFrame:viewFrame];
    treeView.delegate = self;
    treeView.dataSource = self;
    treeView.separatorStyle = RATreeViewCellSeparatorStyleNone;

    self.treeView = treeView;
    [self.vistaArbolScroll addSubview:treeView];
    [self.treeView registerNib:[UINib nibWithNibName:@"SectionCellView"
                                              bundle:[NSBundle mainBundle]]
        forCellReuseIdentifier:@"sectionCell"];
    [self.treeView registerNib:[UINib nibWithNibName:@"SubSectionCellView"
                                              bundle:[NSBundle mainBundle]]
        forCellReuseIdentifier:@"subsectionCell"];
    [self.treeView registerNib:[UINib nibWithNibName:@"VideoCellView"
                                              bundle:[NSBundle mainBundle]]
        forCellReuseIdentifier:@"videoCell"];

    [self.treeView setBackgroundColor:[UIColor blackColor]];
    [self.treeView reloadData];
    [self showSelectedVideo];
}

- (NSInteger)treeView:(RATreeView *)treeView numberOfChildrenOfItem:(id)item {
    if (item == nil) {
        return ((NSArray *) self.sectionsHandler.selectedCourse[@"secciones"]).count;
    }

    if (item[@"seccion"]) {
        return ((NSArray *) item[@"subsecciones"]).count;
    }

    if (item[@"subseccion"]) {
        return ((NSArray *) item[@"videos"]).count;
    }

    return 0;
}

- (UITableViewCell *)treeView:(RATreeView *)treeView cellForItem:(id)item {
    if (item[@"seccion"]) {
        UITableViewCell *cell = [treeView dequeueReusableCellWithIdentifier:@"sectionCell"];
        UILabel *title = [cell viewWithTag:1];
        title.text = item[@"seccion"];
        return cell;
    }

    if (item[@"subseccion"]) {
        UITableViewCell *cell = [treeView dequeueReusableCellWithIdentifier:@"subsectionCell"];
        UILabel *title = [cell viewWithTag:1];
        title.text = item[@"subseccion"];
        return cell;
    }

    if (item[@"id_video"]) {
        VideoCell *cell = [treeView dequeueReusableCellWithIdentifier:@"videoCell"];
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.titleVideo.text = item[@"resumen"];

        if (item != self.sectionsHandler.getSelectedVideo) {
            cell.passedVideo.image = item[@"superado"] ? [UIImage imageNamed:@"check_passed"] : [UIImage imageNamed:@"check_yes_still"];
        } else {
            cell.passedVideo.image = [UIImage imageNamed:@"check_yes_current"];
        }

        cell.passedVideo.hidden = NO;
        cell.buyCourseImage.hidden = YES;

        cell.percentagePassed.text = [NSString stringWithFormat:@"%ld%%", (long) [item[@"puntuacion"] integerValue]];
        cell.percentagePassed.hidden = NO;
        cell.selectedVideoView.hidden = item != self.sectionsHandler.getSelectedVideo;

        return cell;
    }

    return nil;
}

- (id)treeView:(RATreeView *)treeView child:(NSInteger)index ofItem:(id)item {
    if (item == nil) {
        return self.sectionsHandler.selectedCourse[@"secciones"][(NSUInteger) index];
    }

    if (item[@"seccion"]) {
        return item[@"subsecciones"][(NSUInteger) index];
    }

    if (item[@"subseccion"]) {
        return item[@"videos"][(NSUInteger) index];
    }

    return nil;
}

- (void)treeView:(RATreeView *)treeView didSelectRowForItem:(id)item {
    if (item[@"id_video"] && !self.sectionsHandler.isDownloadingVideo) {
        NSDictionary *section = [self.sectionsHandler.selectedCourse[@"secciones"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF contains[id_seccion] %@", item[@"id_seccion"]]][0];
        NSDictionary *subsection = [section[@"subsecciones"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF contains[id_subseccion] %@", item[@"id_subseccion"]]][0];

        [self.sectionsHandler changeCurrentSelection:[self.sectionsHandler.selectedCourse[@"secciones"] indexOfObject:section]
                              withSelectedSubseccion:[section[@"subsecciones"] indexOfObject:subsection]
                                    andSelectedVideo:[subsection[@"videos"] indexOfObject:item]
        ];
        [self.treeView reloadData];
        [self showSelectedVideo];
    }
}

- (void)treeView:(RATreeView *)treeView didExpandRowForItem:(id)item {
    if (item[@"seccion"]) {
        NSUInteger numSubsections = ((NSArray *) item[@"subsecciones"]).count;
        if (numSubsections == 1) {
            [self performSelector:@selector(expandSectionWithOneSubsection:)
                       withObject:item[@"subsecciones"][0]
                       afterDelay:0.1];
        }
    }
}


- (void)expandSectionWithOneSubsection:(id)subsection {
    [self.treeView expandRowForItem:subsection withRowAnimation:RATreeViewRowAnimationAutomatic];
}

- (CGFloat)treeView:(RATreeView *)treeView heightForRowForItem:(id)item {
    return HEIGHT_ROW_SECTION;
}

- (CGFloat)treeView:(RATreeView *)treeView estimatedHeightForRowForItem:(id)item {
    return HEIGHT_ROW_SECTION;
}

- (BOOL)treeView:(RATreeView *)treeView canEditRowForItem:(id)item {
    return NO;
}

- (void)newLessonSelected:(BOOL)toogle {
    [self.treeView reloadData];
    [self showSelectedVideo];
}

- (void)showSelectedVideo {
    NSDictionary *section = (self.sectionsHandler.selectedCourse[@"secciones"])[(NSUInteger) self.sectionsHandler.selectedSeccion];
    NSDictionary *subsection = (section[@"subsecciones"])[(NSUInteger) self.sectionsHandler.selectedSubSeccion];
    NSDictionary *videoSelected = (subsection[@"videos"])[(NSUInteger) self.sectionsHandler.selectedVideo];

    [self.treeView expandRowForItem:section withRowAnimation:RATreeViewRowAnimationAutomatic];
    [self.treeView expandRowForItem:subsection withRowAnimation:RATreeViewRowAnimationAutomatic];
    [self.treeView expandRowForItem:videoSelected withRowAnimation:RATreeViewRowAnimationAutomatic];
    [self.treeView selectRowForItem:videoSelected animated:YES scrollPosition:RATreeViewScrollPositionMiddle];
}

- (void)refreshSectionsTree {
    [self.treeView reloadData];
}

- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 1) {
        [self.sectionsHandler moveOutSession:self];
    }
}

@end
