//
//  SectionCell.h
//  Nativox
//
//  Created by redhair84 on 08/03/2014.
//  Copyright (c) 2014 Francisco Javier Morant. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VideoCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *titleVideo;
@property (strong, nonatomic) IBOutlet UIImageView *passedVideo;
@property (strong, nonatomic) IBOutlet UILabel *percentagePassed;
@property (strong, nonatomic) IBOutlet UIView *selectedVideoView;
@property (strong, nonatomic) IBOutlet UIImageView *buyCourseImage;

@end
