//
// Created by redhair84 on 21/04/15.
// Copyright (c) 2015 Francisco Javier Morant. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LessonEventHandler.h"
#import "SalesProvider.h"

@protocol LessonUserInterface;
@class SectionsHandler;


@interface LessonInteractor : NSObject <LessonEventHandler>

@property(strong) id userInterface;

@property(nonatomic, strong) SectionsHandler *sectionsHandler;
@end