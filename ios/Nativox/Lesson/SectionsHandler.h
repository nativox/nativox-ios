//
// Created by redhair84 on 18/03/2014.
// Copyright (c) 2014 Francisco Javier Morant. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SectionsViewController;
@class BaseViewController;

@protocol SectionsHelperDelegate
@optional
- (void)newLessonSelected:(BOOL)toogle;

- (void)refreshSectionsTree;
@end


@interface SectionsHandler : NSObject

@property(weak) id <SectionsHelperDelegate> sectionsHelperDelegateForLessonView;
@property(weak) id <SectionsHelperDelegate> sectionsHelperDelegateForSectionsView;

@property(nonatomic, strong) NSDictionary *selectedCourse;
@property(nonatomic) NSUInteger selectedSeccion;
@property(nonatomic) NSUInteger selectedSubSeccion;
@property(nonatomic) NSUInteger selectedVideo;
@property(nonatomic) BOOL isDownloadingVideo;

- (instancetype)initWithSelectedCourse:(NSDictionary *)selectedCourse;

+ (instancetype)helperWithSelectedCourse:(NSDictionary *)selectedCourse;

- (NSMutableDictionary *)getSelectedVideo;

- (void)changeCurrentSelection:(NSUInteger)selectedSection withSelectedSubseccion:(NSUInteger)selectedSubSeccion andSelectedVideo:(NSUInteger)selectedVideo;

- (void)moveToPreviousVideo;

- (void)moveToNextVideo;

- (void)refreshSectionsTree;

- (void)moveOutSession:(BaseViewController *)controller;

@end
