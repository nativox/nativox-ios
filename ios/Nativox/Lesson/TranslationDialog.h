
#import <UIKit/UIKit.h>
#import "BaseView.h"

@class CustomIOSAlertView;
@class SZTextView;

@interface TranslationDialog : BaseView <UITextFieldDelegate, UITextViewDelegate>

@property(nonatomic, strong) CustomIOSAlertView *dialogAlertView;
@property(nonatomic, copy) NSString *videoId;
@property(strong, nonatomic) IBOutlet SZTextView *translationTextView;
@end
