//
// Created by redhair84 on 22/07/2014.
// Copyright (c) 2014 Francisco Javier Morant. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface GradeMessageResolver : NSObject

+ (NSString *)resolveMessageWithGrade:(NSUInteger)grade;
@end