
#import <AVFoundation/AVFoundation.h>
#import "SWRevealViewController.h"
#import "VideoDownloader.h"
#import "SectionsHandler.h"
#import "SFCountdownView.h"
#import "AudioUploader.h"
#import "BaseViewController.h"
#import "SalesProvider.h"
#import "FreeCoursesTimer.h"
#import "LessonUserInterface.h"
#import "CustomIOSAlertView.h"
#import <GoogleMobileAds/GoogleMobileAds.h>
#import "MPCoachMarks.h"

@class AVAudioRecorder;
@class AVAudioPlayer;
@class Course;
@class Video;
@class SectionsHandler;
@class MRProgressOverlayView;
@class WebViewJavascriptBridge;
@protocol LessonEventHandler;
@class VideoGraphicLineRequester;

typedef enum LessonState {
    TESTING_STATE,
    PLAYING_STATE,
    RECORDING_STATE,
    TRASLATING_STATE

} LessonState;

@interface LessonViewController : BaseViewController <AVAudioRecorderDelegate, AVAudioPlayerDelegate,
        UIAlertViewDelegate, FileDownloadManagerDelegate, SectionsHelperDelegate, SFCountdownViewDelegate,
        FileUploadManagerDelegate, CustomIOSAlertViewDelegate, SalesProviderDelegate,
        LessonUserInterface, GADInterstitialDelegate, MPCoachMarksViewDelegate>

@property(nonatomic, strong) SectionsHandler *sectionsHandler;
@property(nonatomic, strong) NSMutableDictionary *selectedVideo;
@property(nonatomic, strong) NSDictionary *selectedCourse;

@property(nonatomic, strong) MRProgressOverlayView *progressOverlayView;
@property(nonatomic, strong) WebViewJavascriptBridge *bridge;

@property(nonatomic, strong) VideoDownloader *fileDownloadHandler;
@property(nonatomic, strong) AudioUploader *fileUploadHandler;

@property(nonatomic, strong) UIView *pointsBar;

@property(nonatomic, strong) AVAudioPlayer *applausePlayer;

- (IBAction)testVideo:(id)sender;

- (IBAction)playVideoButtonPressed:(id)sender;

- (IBAction)recordVideo:(id)sender;

- (IBAction)translationVideo:(id)sender;

@end
