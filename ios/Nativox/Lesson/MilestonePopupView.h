#import <UIKit/UIKit.h>

@class CustomIOSAlertView;

@interface MilestonePopupView : UIView

@property(nonatomic, strong) CustomIOSAlertView *alertViewContainer;

- (void)loadMilestoneContent:(NSString *)milestoneContent;

+ (void)showPopupView:(NSString *)milestoneContent;

@end
