//
//  RegistrationSuggestionPopupView.m
//  Nativox
//
//  Created by redhair84 on 24/01/2015.
//  Copyright (c) 2015 Francisco Javier Morant. All rights reserved.
//

#import "RegistrationSuggestionPopupView.h"
#import "CustomIOSAlertView.h"

@interface RegistrationSuggestionPopupView ()
@property(strong, nonatomic) IBOutlet UIImageView *backgroundAppImage;
@property(strong, nonatomic) IBOutlet UIImageView *backgroundRegistrationImage;
@property(strong, nonatomic) IBOutlet UILabel *registrationLabel;
@property(strong, nonatomic) IBOutlet UIButton *registerButton;
@end

@implementation RegistrationSuggestionPopupView

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setupPopupView];

    [self.registerButton setTitle:NSLocalizedString(@"register_suggestion_button", @"") forState:UIControlStateNormal];
    self.registrationLabel.text = NSLocalizedString(@"registration_suggestion_message", @"");
}

- (void)setupPopupView {
    self.backgroundAppImage.layer.cornerRadius = 5;
    self.backgroundAppImage.clipsToBounds = YES;
    self.backgroundRegistrationImage.layer.cornerRadius = 5;
    self.backgroundRegistrationImage.clipsToBounds = YES;
}

- (IBAction)registerButtonPressed:(id)sender {
    if (self.alertView.delegate) {
        [self.alertView.delegate customIOS7dialogButtonTouchUpInside:self.alertView clickedButtonAtIndex:1];
    }
}

- (IBAction)closeButtonPressed:(id)sender {
    [self.alertView close];
}

@end
