
#import <Foundation/Foundation.h>
#import <OpenEars/OEEventsObserver.h>
#import <AudioToolbox/AudioToolbox.h>

@class OEEventsObserver;


@interface SpeechRecognitor : NSObject<OEEventsObserverDelegate>

@property (strong, nonatomic) OEEventsObserver *openEarsEventsObserver;


@property(nonatomic, strong) void (^onComplete)(int);

@property(nonatomic) int totalScore;

- (void)setupSentence:(NSString *)sentence;

- (void)startListeningSpeech:(NSString *)path onComplete:(void (^)(int))complete;

@end