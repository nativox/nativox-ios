#import "MilestonePopupView.h"
#import "CustomIOSAlertView.h"

@interface MilestonePopupView ()
@property(strong, nonatomic) IBOutlet UIWebView *milestoneWebView;
@property(strong, nonatomic) IBOutlet UIImageView *backgroundAppImageView;

@end

@implementation MilestonePopupView

- (void)awakeFromNib {
    [super awakeFromNib];
    self.backgroundAppImageView.layer.cornerRadius = 5;
    self.backgroundAppImageView.clipsToBounds = YES;
}

- (void)loadMilestoneContent:(NSString *)milestoneContent {
    DLog(@"%@", milestoneContent);
    NSString *htmlString = [NSString stringWithFormat:@"<html> <body style=\"background-color: transparent;\"> %@ </body></html>",
                                                      milestoneContent];

    [self.milestoneWebView loadHTMLString:milestoneContent baseURL:nil];
}

+ (void)showPopupView:(NSString *)milestoneContent {
    CustomIOSAlertView *milestoneAlertView = [[CustomIOSAlertView alloc] init];
    MilestonePopupView *milestonePopupView = [[[NSBundle mainBundle] loadNibNamed:@"MilestonePopupView" owner:nil options:nil] lastObject];

    milestonePopupView.alertViewContainer = milestoneAlertView;
    [milestonePopupView loadMilestoneContent:milestoneContent];
    [milestoneAlertView setContainerView:milestonePopupView];
    [milestoneAlertView setButtonTitles:NULL];
    [milestoneAlertView setUseMotionEffects:YES];
    [milestoneAlertView show];
}

- (IBAction)closeButtonPressed:(id)sender {
    [self.alertViewContainer close];
}
@end
