
#import <UIKit/UIKit.h>
#import "RATreeView.h"
#import "SectionsHandler.h"
#import "BaseViewController.h"

@class SectionsHandler;

static const int HEIGHT_ROW_SECTION = 60;

@interface SectionsViewController : BaseViewController <RATreeViewDelegate, RATreeViewDataSource, SectionsHelperDelegate, UIAlertViewDelegate>

@property(nonatomic, strong) SectionsHandler *sectionsHandler;
@property(nonatomic, strong) RATreeView *treeView;

@end
