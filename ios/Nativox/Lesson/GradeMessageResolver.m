//
// Created by redhair84 on 22/07/2014.
// Copyright (c) 2014 Francisco Javier Morant. All rights reserved.
//

#import "GradeMessageResolver.h"

static NSString *const kUnexpectedGrade = @"Unexpected grade";

@implementation GradeMessageResolver {

}
+ (NSString *)resolveMessageWithGrade:(NSUInteger)grade {

    unsigned long calculatedGrade = grade / 10;

    switch (calculatedGrade) {
        case 0:
            return [self resolveMessageWithGrade:grade andKey:@"result_grade_between_0_and_9"];
        case 1:
            return [self resolveMessageWithGrade:grade andKey:@"result_grade_between_10_and_19"];
        case 2:
            return [self resolveMessageWithGrade:grade andKey:@"result_grade_between_20_and_29"];
        case 3:
            return [self resolveMessageWithGrade:grade andKey:@"result_grade_between_30_and_39"];
        case 4:
            return [self resolveMessageWithGrade:grade andKey:@"result_grade_between_40_and_49"];
        case 5:
            return [self resolveMessageWithGrade:grade andKey:@"result_grade_between_50_and_59"];
        case 6:
            return [self resolveMessageWithGrade:grade andKey:@"result_grade_between_60_and_69"];
        case 7:
            return [self resolveMessageWithGrade:grade andKey:@"result_grade_between_70_and_79"];
        case 8:
            return [self resolveMessageWithGrade:grade andKey:@"result_grade_between_80_and_89"];
        case 9:
            return [self resolveMessageWithGrade:grade andKey:@"result_grade_between_90_and_99"];
        case 10:
            return [self resolveMessageWithGrade:grade andKey:@"result_grade_100"];
        default:
            return kUnexpectedGrade;
    }
}

+ (NSString *)resolveMessageWithGrade:(NSUInteger)grade andKey:(NSString *)gradeKey {
    return [NSString stringWithFormat:@"%lu%% %@", (unsigned long) grade, NSLocalizedString(gradeKey, @"")];
}

@end