
#import "TranslationDialog.h"
#import "CustomIOSAlertView.h"
#import "ProvidersManager.h"
#import "UserProvider.h"
#import "UserSession.h"
#import "BusinessError.h"
#import "SZTextView.h"
#import "AlertDialogView.h"

@interface TranslationDialog ()

@property(strong, nonatomic) IBOutlet UIButton *sendButton;
@property(strong, nonatomic) IBOutlet UIImageView *backgroundImage;
@property(strong, nonatomic) IBOutlet UILabel *translationInfoLabel;

@end

@implementation TranslationDialog

- (void)awakeFromNib {
    [super awakeFromNib];
    self.sendButton.layer.cornerRadius = 5;

    self.backgroundImage.layer.cornerRadius = 5;
    self.backgroundImage.clipsToBounds = YES;

    self.translationTextView.placeholderTextColor = [UIColor darkGrayColor];

    self.translationInfoLabel.text = NSLocalizedString(@"translation_dialog_help_others", @"");
}

- (IBAction)closeButtonPressed:(id)sender {
    [self.dialogAlertView close];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    NSInteger nextTag = textField.tag + 1;

    UIResponder *nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder) {
        [nextResponder becomeFirstResponder];
    } else {
        [textField resignFirstResponder];
    }
    return NO;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if (range.length == 0 && [text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    } else {
        return YES;
    }
}

- (IBAction)sendButtonPressed:(id)sender {
    if (!self.translationTextView.text || [self.translationTextView.text isEqualToString:@""]) {
        [AlertDialogView showDialog:@"Add a translation to be able to send it."];
    } else {
        [self sendTranslation];
    }

}

- (void)sendTranslation {
    [self showProgressViewWithTitle:@"Sending translation"];
    [[ProvidersManager instance].userProvider sendTranslation:[UserSession instance].idUser
                                                    withVideo:self.videoId
                                                 withLanguage:[UserSession instance].localeLanguage
                                              withTranslation:self.translationTextView.text
                                                   andHandler:^(BusinessError *error) {
                                                       [self dismissProgressView];
                                                       if (error) {
                                                           [AlertDialogView showDialog:error.message];
                                                       } else {
                                                           [self.dialogAlertView close];
                                                           [AlertDialogView showDialog:@"Translation sent successfully."];
                                                       }
                                                   }];
}
@end
